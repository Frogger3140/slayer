// +-------------------------------------------+
// |  ___   _     _____   __  __  ____   ____  |
// | | __| | |   | /_\ |  \ \/ / | ___| | /\ | |
// | |__ | | |_  |  _  |   \  /  | __|  | \/ / |
// | |___| |___| |_| |_|   |__|  |____| |_| \\ |
// |  Greek2me              Blockland ID 11902 |
// +-----------------------------+-------------+
// | DO NOT EDIT BELOW THIS LINE |
// +-----------------------------+

if(!$Slayer::Server::Dependencies::Gamemodes)
	exec("Add-ons/Gamemode_Slayer/Dependencies/Gamemodes.cs");
Slayer.Gamemodes.addMode("Giants","Giants",1,1);

if(!$Slayer::Server::Dependencies::Preferences)
	exec("Add-ons/Gamemode_Slayer/Dependencies/Preferences.cs");
Slayer.Prefs.addPref("Giants","Giant Size","%mini.Giants_playerScale","slide 0 5 4 1",5,0,1,-1,"Rules Giants Player","%mini.updatePlayerScale(%1);");

function Slayer_Giants_onSpawn(%mini,%client)
{
	if(!isObject(%client.player))
		return;

	%size = %mini.Giants_playerScale;
	%client.player.setScale(%size SPC %size SPC %size);
}

function Slayer_MinigameSO::updatePlayerScale(%this,%flag)
{
	if(%this.mode !$= "Giants")
		return;

	%flag = Slayer_Support::mRestrict(%flag,0,5);

	for(%i=0; %i < %this.numMembers; %i ++)
	{
		%cl = %this.member[%i];
		if(isObject(%cl.player))
			%cl.player.setScale(%flag SPC %flag SPC %flag);
	}
}