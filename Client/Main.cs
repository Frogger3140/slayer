// +-------------------------------------------+
// |  ___   _     _____   __  __  ____   ____  |
// | | __| | |   | /_\ |  \ \/ / | ___| | /\ | |
// | |__ | | |_  |  _  |   \  /  | __|  | \/ / |
// | |___| |___| |_| |_|   |__|  |____| |_| \\ |
// |  Greek2me              Blockland ID 11902 |
// +-----------------------------+-------------+
// | DO NOT EDIT BELOW THIS LINE |
// +-----------------------------+

// +--------------+
// | SlayerClient |
// +--------------+
function SlayerClient::onAdd(%this)
{
	//DISPLAY INFO
	warn("Slayer Version" SPC $Slayer::Client::Version);
	warn(" + Compatible Version:" SPC $Slayer::Client::CompatibleVersion);
	warn(" + Core Directory:" SPC $Slayer::Client::Directory);
	warn(" + Config Directory:" SPC $Slayer::Client::ConfigDir);
	warn(" + Debug Mode:" SPC $Slayer::Client::Debug);

	//LOAD SUPPORT FILES
	exec($Slayer::Client::Directory @ "/Support/Support.cs");
	SlayerClient_Support::loadFiles($Slayer::Client::Directory @ "/Support/Support_*.cs","Support.cs");

	//DELETE PRE-V3 CONFIG FILES
	%this.DeleteOldConfigFiles();

	//INITIALIZE MAIN COMPONENTS
	SlayerClient_Support::LoadFiles($Slayer::Client::Directory @ "/Dependencies/*.cs"); //load dependencies

	if(!isObject(Slayer_WindowProfile))
		exec("./Profiles.cs");

	SlayerClient_Support::LoadFiles($Slayer::Client::Directory @ "/GUI/*init.cs"); //load GUI files

	SlayerClient_Support::LoadFiles($Slayer::Client::Directory @ "/Modules/*.cs"); //load modules

	//MINIGAME STATE
	%this.miniState = "NONE";
}

function SlayerClient::onRemove(%this)
{
	//export options config
	%this.Options.exportOptions($Slayer::Client::ConfigDir @ "/config_last.cs");

	//clear data
	%this.clearAllData();

	SlayerClient_Support::Debug(0,"Deactivating Slayer");

	//close the debug logger
	if(isObject(%this.debugFO))
		%this.debugFO.close();
}

function SlayerClient::onDisconnect(%this)
{
	SlayerClient_Support::Debug(0,"Disconnecting from server");

	%this.serverHasSlayer = 0;

	//JOIN MINIGAME GUI
	JMG_Slayer.setVisible(0);
	JMG_Slayer_Default.setVisible(0);
	JMG_List.columns = "0 102 165 493";

	//HIDE GUIS
	clientCmdSlayer_ForceGUI("ALL",0);

	//CLEAR ALL DATA
	%this.clearAllData();
}

function SlayerClient::sendData(%this,%reset)
{
	//GET NUMBER OF CHANGED PREFS
	%count = 0;
	for(%i=0; %i < %this.serverPrefs.getCount(); %i++) 
	{
		%p = %this.serverPrefs.getObject(%i);
		if(%p.getValue() !$= %p.origValue)
			%count ++;
	}
	for(%i=0; %i < %this.Teams.getCount(); %i++)
	{
		%team = %this.Teams.getObject(%i);

		if(%team.ID < 0)
		{
			%count ++;
			break;
		}

		//get the number of changed prefs
		for(%e=0; %e < %this.TeamPrefs.getCount(); %e++)
		{
			%p = %this.TeamPrefs.getObject(%e);
			%orig = %p.getValue(%team,"_ORIG");
			%value = %p.getValue(%team);

			if(%orig !$= %value)
			{
				%changed = 1;
				break;
			}
		}

		if(%changed)
		{
			%count ++;
			break;
		}
	}
	%count += getFieldCount(%this.Teams.removeTeamList);

	SlayerClient_Support::Debug(1,"Sending Preferences",%count);

	//BEGIN PREF TRANSFER
	commandToServer('Slayer_getPrefs_Start',%reset,%count,$Slayer::Client::NotifyPlayersOnUpdate);

	//PREF TRANSFER TICK
	for(%i=0; %i < %this.serverPrefs.getCount(); %i++)
	{
		%p = %this.serverPrefs.getObject(%i);
		%val = %p.getValue();
		if(%val !$= %p.origValue)
		{
			SlayerClient_Support::Debug(2,"Sending pref",%p.category TAB %p.title TAB %val);
			commandToServer('Slayer_getPrefs_Tick',%p.category,%p.title,%val);
		}
	}

	//SEND TEAMS
	%this.Teams.SendTeams();

	//END PREF TRANSFER
	commandToServer('Slayer_getPrefs_End');
}

function SlayerClient::clearAllData(%this)
{
	SlayerClient_Support::Debug(1,"Clearing data");

	%this.serverPrefs.clearPrefs();
	%this.serverPrefs.clearRules("ALL");
	%this.Teams.clearTeams();
	%this.Gamemodes.deleteAll();

	Slayer_Main_Announcements.clearMessages("SERVER");

	for(%i=0; %i < 5; %i++)
	{
		%ctrl = Slayer_General_Player_StartEquip @ %i;
		%ctrl.clear();

		%ctrl = Slayer_Teams_Edit_StartEquip @ %i;
		%ctrl.clear();
	}
	Slayer_General_Player_datablock.clear();
	Slayer_Teams_Edit_Datablock.clear();
	Slayer_General_Mode_Selector.clear();
}

function SlayerClient::DeleteOldConfigFiles(%this)
{
	%path = $Slayer::Client::ConfigDir;

	if(isFile(%path @ "/Config.cs"))
	{
		fileDelete(%path @ "/Config.cs");
		%warn = 1;
	}

	if(isFile(%path @ "/client.log"))
	{
		fileDelete(%path @ "/client.log");
		%warn = 1;
	}

	%favs = %path @ "/Favorites";
	for(%i=0; %i < 10; %i++)
	{
		%file = %favs @ "/Fav" @ %i @ ".cs";

		if(isFile(%file))
		{
			fileDelete(%file);
			%warn = 1;
		}
	}

	if(%warn)
		SlayerClient_Support::Error("Deleting old config files");
}

function SlayerClient::editMinigame(%this,%mini)
{
	if(!%this.DataRecieved)
		commandToServer('Slayer',Edit,%mini);

	canvas.pushDialog(Slayer_Main);
}

// +------------+
// | clientCmds |
// +------------+
function clientCmdSlayer_Handshake(%version,%modVersions)
{
	SlayerClient_Support::Debug(0,"Handshake Recieved","Server has Version" SPC %version);

	if(%version !$= $Slayer::Client::Version)
	{
		%newer = SlayerClient_Support::getNewerVersion(%version,$Slayer::Client::CompatibleVersion);
		if($Slayer::Client::CompatibleVersion !$= "" && (%newer $= %version || %newer $= "-1"))
		{
			%clVersion = %version;
		}

		if(SlayerClient_Support::isNewerVersion($Slayer::Client::Version,%version))
		{
			newChatSO.addLine("\c0WARNING: \c6You have a \c3newer \c6version of \c3Slayer \c6than the server. Ask the host to update it.");
			if(%clVersion !$= "")
				newChatSO.addLine("\c0 + \c6You are still able to use the Slayer GUI, but some features may not work.");
		}
	}

	if(%clVersion $= "")
		%clVersion = $Slayer::Client::Version;

	commandToServer('Slayer_Handshake',%clVersion,$Slayer::Client::modVersions);
}

function clientCmdSlayer_getInitialData_Start()
{
	SlayerClient_Support::Debug(0,"Handshake Accepted","Recieving initial data...");

	//make sure that everything is cleared before we recieve new data
	SlayerClient.clearAllData();

	SlayerClient.serverHasSlayer = 1;

	JMG_Slayer.setVisible(1);
	JMG_Slayer_Default.setVisible(1);
	JMG_List.columns = "0 102 165 490 450";
}

function clientCmdSlayer_getInitialData_End()
{
	SlayerClient_Support::Debug(1,"Initial Data Recieved");

	//create item and player lists
	if(serverConnection.isLocal())
		%group = datablockGroup;
	else
		%group = serverConnection;
	if(isObject(%group))
	{
		%count = %group.getCount();
		for(%i=0; %i < %count; %i++)
		{
			%obj = %group.getObject(%i);
			%cn = %obj.getClassName();
			if(%cn $= "MissionArea")
				break;
			if(%obj.uiName $= "")
				continue;
			if(%cn $= "ItemData")
			{
				for(%e=0; %e < 5; %e++)
				{
					%ctrl = Slayer_General_Player_StartEquip @ %e;
					%ctrl.add(%obj.uiName,%obj);

					%ctrl = Slayer_Teams_Edit_StartEquip @ %e;
					%ctrl.add(%obj.uiName,%obj);
				}
				SlayerClient_Support::Debug(2,"ItemData Added",%obj TAB %obj.uiName);
			}
			else if(%cn $= "PlayerData")
			{
				Slayer_General_Player_Datablock.add(%obj.uiName,%obj);
				Slayer_Teams_Edit_Datablock.add(%obj.uiName,%obj);
				SlayerClient_Support::Debug(2,"PlayerData Added",%obj TAB %obj.uiName);
			}
		}
	}
	else
	{
		SlayerClient_Support::Error("Unable to generate datablock lists","Group not found!");
	}

	//add the NONE values and sort lists
	for(%i=0; %i < 5; %i++)
	{
		%ctrl = Slayer_General_Player_StartEquip @ %i;
		if(isObject(%ctrl))
		{
			%ctrl.sort();
			%ctrl.addFront("NONE",0);
		}

		%ctrl = Slayer_Teams_Edit_StartEquip @ %i;
		if(isObject(%ctrl))
		{
			%ctrl.sort();
			%ctrl.addFront("NONE",0);
		}
	}
	Slayer_General_Player_datablock.sort();
	Slayer_Teams_Edit_datablock.sort();
}

function clientCmdSlayer_getData_Start(%mini,%adminLevel,%miniState)
{
	Slayer_Main_Content_Loading_Status.setText("Recieving Data");

	SlayerClient.editingMinigame = %mini;

	SlayerClient.adminLevel = %adminLevel;

	SlayerClient.miniState = %miniState;
	if(%miniState $= "NONE")
	{
		Slayer_Main_Buttons_Update.setActive(0);
		Slayer_Main_Buttons_Reset.setActive(0);
		Slayer_Main_Buttons_End.setActive(0);
	}
	else
	{
		if(%miniState $= "EDIT")
		{
			Slayer_Main_Buttons_Update.setText("Update");
			Slayer_Main_Buttons_Reset.setActive(1);
			Slayer_Main_Buttons_End.setActive(1);
		}
		else if(%miniState $= "CREATE")
		{
			Slayer_Main_Buttons_Update.setText("Create");
			Slayer_Main_Buttons_Reset.setActive(0);
			Slayer_Main_Buttons_End.setActive(0);
		}
		Slayer_Main_Buttons_Update.setActive(1);
	}
}

function clientCmdSlayer_getData_End(%gui)
{
	SlayerClient.DataRecieved = 1;

	Slayer_Main_Content_Loading_Status.setText("");

	if(%gui $= "Slayer_Main" && Slayer_Main.isAwake())
	{
		if(Slayer_Main_Content_Loading.isVisible())
			Slayer_Main_TabList.setSelectedById(0);	
		Slayer_Main.refreshAllValues(); //refresh GUI values
	}
	else if(isObject(%gui))
		canvas.pushDialog(%gui);
}

function clientCmdSlayer_getMiniState(%miniState,%canEditCurrent)
{
	SlayerClient.miniState = %miniState;
	SlayerClient.canEditCurrent = %canEditCurrent;

	if(JMG_Slayer.isVisible())
		JMG_Slayer.onWake();

	if(NewPlayerListGui.isAwake())
		NewPlayerListGui.clickList();
}

// +--------------------+
// | Packaged Functions |
// +--------------------+
package Slayer_Client_Main
{
	function disconnect(%a)
	{
		parent::disconnect(%a);

		if(isObject(SlayerClient))
		{
			SlayerClient.onDisconnect();
		}
	}

	function onExit()
	{
		parent::onExit();

		if(isObject(SlayerClient))
			SlayerClient.delete();
	}
};
activatePackage(Slayer_Client_Main);

// +-------------------+
// | Initialize Slayer |
// +-------------------+
if(!isObject(SlayerClient))
{
	$Slayer::Client = new scriptGroup(SlayerClient);
}