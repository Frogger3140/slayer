// +-------------------------------------------+
// |  ___   _     _____   __  __  ____   ____  |
// | | __| | |   | /_\ |  \ \/ / | ___| | /\ | |
// | |__ | | |_  |  _  |   \  /  | __|  | \/ / |
// | |___| |___| |_| |_|   |__|  |____| |_| \\ |
// |  Greek2me              Blockland ID 11902 |
// +-----------------------------+-------------+
// | DO NOT EDIT BELOW THIS LINE |
// +-----------------------------+

$Slayer::Client::Dependencies::GUI = 1;

$Slayer::Client::GUI::RestrictInputTimeout = 2000;

// +-----------------------+
// | GUI Support Functions |
// +-----------------------+
function setPopup(%gui,%fade,%type,%icon,%title,%text,%cmd)
{
	if(!isObject(%gui))
		return;

	if(%gui.getClassName() $= "GuiControl" && %gui.getObject(0).getClassName() $= "GuiWindowCtrl")
		%gui = %gui.getObject(0);

	%swatch = new GuiSwatchCtrl()
	{
		profile = "GuiDefaultProfile";
		horizSizing = "right";
		vertSizing = "bottom";
		position = "0 0";
		extent = %gui.getExtent();
		minExtent = "8 2";
		visible = "1";
		color = (%fade == 1 ? "200 200 200 200" : "0 0 0 0");
	};
	%gui.add(%swatch);
	%swatch.setCentered();

	%window = new GuiWindowCtrl()
	{
		profile = "Slayer_WindowProfile";
		horizSizing = "center";
		vertSizing = "center";
		position = "125 125";
		extent = "300 100";
		minExtent = "200 100";
		visible = "1";
		text = "     " @ %title;
		maxLength = "255";
		resizeWidth = "0";
		resizeHeight = "0";
		canMove = "1";
		canClose = "0";
		canMinimize = "0";
		canMaximize = "0";
		minSize = "50 50";
	};
	%swatch.add(%window);

	%bitmap = new GuiBitmapCtrl()
	{
		profile = "GuiDefaultProfile";
		horizSizing = "right";
		vertSizing = "bottom";
		position = "5 4";
		extent = "16 16";
		minExtent = "8 2";
		visible = "1";
		bitmap = $Slayer::Client::Directory @ "/Images/" @ %icon;
		wrap = "0";
		lockAspectRatio = "0";
		alignLeft = "0";
		overflowImage = "0";
		keepCached = "0";
	};
	%window.add(%bitmap);

	%textCtrl = new GuiMLTextCtrl()
	{
		profile = "Slayer_TextProfile";
		horizSizing = "right";
		vertSizing = "bottom";
		position = "10 29";
		extent = "280 14";
		minExtent = "8 2";
		visible = "1";
		lineSpacing = "2";
		allowColorChars = "1";
		maxChars = "-1";
		text = %text;
		maxBitmapHeight = "-1";
		selectable = "1";
	};
	%window.add(%textCtrl);

	%confirm = new GuiBitmapButtonCtrl()
	{
		profile = "Slayer_ButtonProfile";
		horizSizing = "left";
		vertSizing = "top";
		position = "255 69";
		extent = "38 25";
		minExtent = "8 2";
		visible = "1";
		command = %swatch @ ".delete();" SPC %cmd;
		accelerator = "enter";
		text = "OK";
		groupNum = "-1";
		buttonType = "PushButton";
		bitmap = "base/client/ui/button2";
		lockAspectRatio = "0";
		alignLeft = "0";
		overflowImage = "0";
		mKeepCached = "0";
		mColor = "255 255 255 255";
	};
	%window.add(%confirm);

	if(%type $= "YESNO")
	{
		%confirm.text = "YES";

		%deny = new GuiBitmapButtonCtrl()
		{
			profile = "Slayer_ButtonProfile";
			horizSizing = "left";
			vertSizing = "top";
			position = "215 69";
			extent = "38 25";
			minExtent = "8 2";
			visible = "1";
			command = %swatch @ ".delete();";
			text = "NO";
			groupNum = "-1";
			buttonType = "PushButton";
			bitmap = "base/client/ui/button2";
			lockAspectRatio = "0";
			alignLeft = "0";
			overflowImage = "0";
			mKeepCached = "0";
			mColor = "255 255 255 255";
		};
		%window.add(%deny);
	}

	//window resizing...
	%textCtrl.forceReflow();
	%posY = getWord(%textCtrl.getPosition(),1);
	%extY = getWord(%textCtrl.getExtent(),1);
	%confPosY = getWord(%confirm.getPosition(),1);
	%winExtX = getWord(%window.getExtent(),0);
	%winExtY = getWord(%window.getExtent(),1);
	%extentY = %extY + %posY + %winExtY - %confPosY + 5;
	%window.resize(0,0,%winExtX,%extentY);
	%window.setCentered(%swatch);
}

function GuiControl::getLowestChildPoint(%this)
{
	%lowest = 0;

	for(%i=0; %i < %this.getCount(); %i++)
	{
		%obj = %this.getObject(%obj);
		%low = getWord(%obj.position,1) + getWord(%obj.extent,1);
		if(%low > %lowest)
			%lowest = %low;
	}

	return %lowest;
}

function GuiControl::setCenteredY(%this)
{
	%parent = %this.getGroup();
	if(!isObject(%parent))
		return;

	%parExtY = getWord(%parent.getExtent(),1);
	%extY = getWord(%this.getExtent(),1);
	%posY = (%parExtY / 2) - (%extY / 2);

	%this.position = getWord(%this.getPosition(),0) SPC %posY;
}

function GuiControl::setCenteredX(%this)
{
	%parent = %this.getGroup();
	if(!isObject(%parent))
		return;

	%parExtX = getWord(%parent.getExtent(),0);
	%extX = getWord(%this.getExtent(),0);
	%posX = (%parExtX / 2) - (%extX / 2);

	%this.position = %posX SPC getWord(%this.getPosition(),1);
}

function GuiControl::setCentered(%this)
{
	%this.setCenteredY();
	%this.setCenteredX();
}

function GuiControl::refreshAllValues(%this)
{
	%control = %this;
	%index = 0;

	while(isObject(%control))
	{
		if(%control.popupVar $= "")
			%var = %control.variable;
		else
			%var = %control.popupVar;

		if(%var !$= "")
		{
			%value = SlayerClient_Support::getDynamicVariable(%var);
			%class = %control.getClassName();

			switch$(%class)
			{
				case "GuiTextEditCtrl":
					%control.setValue(%value);

				case "GuiMLTextEditCtrl":
					%control.setValue(%value);

				case "GuiCheckBoxCtrl":
					%control.setValue(%value);

				case "GuiPopUpMenuCtrl":
					%control.setSelected(%value);

				case "GuiSliderCtrl":
					%control.setValue(%value);
					%var = %control.variable;
					if(%var !$= "")
						SlayerClient_Support::setDynamicVariable(%var,SlayerClient_Support::stripTrailingZeros(%value));

				default:
					SlayerClient_Support::Error("GuiControl::RefreshAllValues unable to find class",%class);
			}
		}

		if(%control.getCount() <= %index)
		{
			if(isObject(%control.getGroup()))
			{
				if(%control.getGroup().getCount() > %control.getGroupID() + 1)
				{
					//we don't want to go above %this
					if(%control.getGroup() == %this.getGroup())
						break;

					%control = %control.getGroup().getObject(%control.getGroupID() + 1);
					%index = 0;
				}
				else
				{
					%index = %control.getGroupID() + 1;
					%control = %control.getGroup();
				}
			}
			else
			{
				break;
			}
		}
		else
		{
			%index = 0;
			%control = %control.getObject(%index);
		}

		//we don't want to go above %this
		if(%control == %this.getGroup())
			break;
	}
}

function GuiTextEditCtrl::restrictNumberInput(%this)
{
	cancel(%this.restrictInputTimer);

	%val = %this.getValue();
	if(SlayerClient_Support::isNumber(%val))
		%value = SlayerClient_Support::mRestrict(%val,%this.minValue,%this.maxValue);
	else
		%value = %this.minValue;

	%this.restrictInputTimer = %this.schedule($Slayer::Client::GUI::RestrictInputTimeout,"setText",%value);
}

//used for keybind
function SlayerClient_pushMain(%state) 
{
	if(%state)
		SlayerClient.editMinigame();
}

//used for keybind
function SlayerClient_pushOptions(%state) 
{
	if(%state)
		canvas.pushDialog(Slayer_Options);
}

// +------------+
// | clientCmds |
// +------------+
function clientCmdSlayer_ForceGUI(%gui,%state)
{
	if(%gui $= "ALL")
	{
		clientCmdSlayer_ForceGUI("Slayer_Main",%state);
		clientCmdSlayer_ForceGUI("Slayer_Options",%state);
		clientCmdSlayer_ForceGUI("Slayer_Scores",%state);
	}
	else if(isObject(%gui))
	{
		if(%gui.isHUD)
		{
			if(%state && !$Slayer::Client::GUI::Disable_[%gui])
				playGui.add(%gui);
			else if(playGui.isMember(%gui))
				playGui.remove(%gui);
		}
		else
		{
			if(%state && !$Slayer::Client::GUI::Disable_[%gui])
				canvas.pushDialog(%gui);
			else
				canvas.popDialog(%gui);
		}
	}
}

function clientCmdSlayer_setGUIVisible(%gui,%flag)
{
	if(isObject(%gui))
		%gui.setVisible(%flag);
}

package Slayer_Client_Dependencies_GUI
{
	function GuiPopUpMenuCtrl::onSelect(%this,%id)
	{
		if(%this.popupVar !$= "")
			SlayerClient_Support::setDynamicVariable(%this.popupVar,%id);

		if(isFunction(parent,onSelect))
		{
			return parent::onSelect(%this,%id);
		}
	}

	function GuiMLTextCtrl::onAdd(%this)
	{
		//parent::onAdd(%this);

		if(isObject(%this.profile) && %this.profile.getName() $= "Slayer_TextProfile")
			%this.setText(parseCustomTML(%this.text,%this,%this.TML_parserFunction));
	}

	//allows anchors
	function GuiMLTextCtrl::onURL(%this,%url)
	{
		%firstChar = getSubStr(%url,0,1);
		if(%firstChar $= "#")
		{
			%parentObj = %this.getGroup();
			if(isObject(%parentObj) && isFunction(%parentObj.getClassName(),"scrollToBottom"))
				%parentObj.scrollToBottom();

			%restChars = getSubStr(%url,1,strLen(%url));
			%this.scrollToTag(%restChars);
		}
		else
		{
			return parent::onURL(%this,%url);
		}
	}

	//NEWPLAYERLISTGUI - For kick/invite to minigame features
	function NewPlayerListGui::onWake(%this)
	{
		parent::onWake(%this);

		commandToServer('Slayer_SendMiniState');
	}

	function NewPlayerListGui::clickList(%this)
	{
		parent::clickList(%this);

		if(SlayerClient.canEditCurrent)
		{
			NPL_MiniGameInviteBlocker.setVisible(0);
			NPL_MiniGameRemoveBlocker.setVisible(0);
		}
	}
};
activatePackage(Slayer_Client_Dependencies_GUI);

SlayerClient_Support::Debug(2,"Dependency Loaded","GUI");