// +-------------------------------------------+
// |  ___   _     _____   __  __  ____   ____  |
// | | __| | |   | /_\ |  \ \/ / | ___| | /\ | |
// | |__ | | |_  |  _  |   \  /  | __|  | \/ / |
// | |___| |___| |_| |_|   |__|  |____| |_| \\ |
// |  Greek2me              Blockland ID 11902 |
// +-----------------------------+-------------+
// | DO NOT EDIT BELOW THIS LINE |
// +-----------------------------+

$Slayer::Client::Dependencies::Options = 1;

// +------------------------------+
// | SlayerClient_OptionHandlerSO |
// +------------------------------+
function SlayerClient_OptionHandlerSO::onAdd(%this)
{
	%this.addOption("Welcome Screen","Display on Start","$Slayer::Client::Options::showStartScreen","bool",0,"Options");
	%this.addOption("Minigames","Notify Players on Update","$Slayer::Client::NotifyPlayersOnUpdate","bool",1,"Options");
	%this.addOption("EoRR","Disable End of Round Report","$Slayer::Client::GUI::Disable_Slayer_Scores","bool",0,"Options");
	%this.addOption("Auto Update","Enable","$Slayer::Client::AU::Enable","bool",1,"Options");
	%this.addOption("Auto Update","Silent Updates","$Slayer::Client::AU::SilentUpdates","bool",1,"Options");

	//little hack
	SlayerClient.Options = %this;

	if(isFile($Slayer::Client::ConfigDir @ "/config_last.cs"))
		exec($Slayer::Client::ConfigDir @ "/config_last.cs");
}

function SlayerClient_OptionHandlerSO::addOption(%this,%category,%title,%variable,%type,%defaultValue,%list)
{
	%option = new scriptObject()
	{
		class = SlayerClient_OptionSO;

		title = %title;
		category = %category;
		type = %type;
		variable = %variable;
		defaultValue = %defaultValue;
		list = %list;
	};
	%this.add(%option);
	if(isObject(missionCleanup) && missionCleanup.isMember(%option))
		missionCleanup.remove(%option);

	%option.setValue(%defaultValue);

	return %option;
}

function SlayerClient_OptionHandlerSO::setOption(%this,%category,%title,%value)
{
	%option = %this.getOptionSO(%category,%title);
	if(!isObject(%option))
		return;

	return %option.setValue(%value);
}

function SlayerClient_OptionHandlerSO::getOption(%this,%category,%title)
{
	%option = %this.getOptionSO(%category,%title);
	if(!isObject(%option))
		return;

	return %option.getValue();
}

function SlayerClient_OptionHandlerSO::getOptionSO(%this,%category,%title)
{
	for(%i=0; %i < %this.getCount(); %i++)
	{
		%p = %this.getObject(%i);
		if(%p.category $= %category && %p.title $= %title)
		{
			return %p;
		}
	}

	return 0;
}

function SlayerClient_OptionHandlerSO::clearOptions(%this)
{
	for(%i=0; %i < %this.getCount(); %i++)
	{
		%p = %this.getObject(%i);
		%p.setValue("");
	}
	%this.deleteAll();

	Slayer_Options_Selector.clear();
}

function SlayerClient_OptionHandlerSO::exportOptions(%this,%path)
{
	%file = new fileObject();
	%file.openForWrite(%path);

	%file.writeLine("//Slayer -----" SPC getDateTime());
	%file.writeLine("//By Greek2me, Blockland ID 11902");
	%file.writeLine("");
	%file.writeLine("//Version:" SPC $Slayer::Client::Version);
	%file.writeLine("//Debug Mode:" SPC $Slayer::Client::Debug);
	%file.writeLine("//Core Directory:" SPC $Slayer::Client::Directory);
	%file.writeLine("//Config Directory:" SPC $Slayer::Client::ConfigDir);
	%file.writeLine("");
	%file.writeLine("$Slayer::Client::Debug =" SPC $Slayer::Client::Debug @ "; //For advanced users only. int 0-3");
	%file.writeLine("$Slayer::Client::LastVersion = \"" @ $Slayer::Client::Version @ "\"; //DO NOT EDIT!");
	%file.writeLine("");

	%file.writeLine("//OPTIONS //USAGE: (category,title,value)");
	for(%i=0; %i < %this.getCount(); %i++)
	{
		%p = %this.getObject(%i);
		%v = %p.getValue();

		%file.writeLine("SlayerClient.Options.setOption(\"" @ %p.category @ "\",\"" @ %p.title @ "\",\"" @ %v @ "\");");
	}

	%file.writeLine("");
	%file.writeLine("SlayerClient_Support::Debug(2,\"Options Loaded\",\"" @ %path @ "\");");

	%file.close();
	%file.delete();

	SlayerClient_Support::Debug(1,"Exporting Options",%path);
}

// +-----------------------+
// | SlayerClient_OptionSO |
// +-----------------------+
function SlayerClient_OptionSO::setValue(%this,%value)
{
	%proof = %this.idiotProof(%value);
	if(getField(%proof,0))
		%value = getField(%proof,1);
	else
		return false;

	SlayerClient_Support::setDynamicVariable(%this.variable,%value);

	SlayerClient_Support::Debug(2,"Option Set",%this.category TAB %this.title TAB %value);

	%this.updateGUI(%value);

	return true;
}

function SlayerClient_OptionSO::getValue(%this)
{
	return SlayerClient_Support::getDynamicVariable(%this.variable);
}

function SlayerClient_OptionSO::idiotProof(%this,%value)
{
	%type = %this.type;
	switch$(getWord(%type,0))
	{
		case bool:
			if(%value !$= false && %value !$= true)
				return false;

		case string:
			%value = getSubStr(%value,0,getWord(%type,1));

		case int:
			if(!Slayer_Support::isNumber(%value))
				return false;
			%value = Slayer_Support::StripTrailingZeros(%value);
			%value = Slayer_Support::mRestrict(%value,getWord(%type,1),getWord(%type,2));

		case slide:
			if(!Slayer_Support::isNumber(%value))
				return false;
			%value = Slayer_Support::StripTrailingZeros(%value);
			%value = Slayer_Support::mRestrict(%value,getWord(%type,1),getWord(%type,2));

		case list:
			%count = getFieldCount(%type);
			if(%count > 1)
			{
				for(%i = 1; %i < %count; %i ++)
				{
					%f = getField(%type,%i);
					%w = firstWord(%f);
					if(%w == %value)
					{
						%ok = true;
						break;
					}
				}
				if(!%ok)
					return false;
			}
	}

	return true TAB %value;
}

function SlayerClient_OptionSO::updateGUI(%this,%value)
{
	%category = %this.category;
	%title = %this.title;
	%type = %this.type;
	%ctrl = Slayer_Options_Selector;

	if(isObject(%ctrl))
	{
		for(%i=0; %i < %ctrl.rowCount(); %i++)
		{
			%text = %ctrl.getRowText(%i);
			if(getField(%text,0) $= %category && getField(%text,1) $= %title)
			{
				switch$(getWord(%type,0))
				{
					case bool:
						if(%value)
							%val = "True";
						else
							%val = "False";
	
					case list:
						for(%i=0; %i < getFieldCount(%type); %i++)
						{
							%f = getField(%type,%i);
							if(firstWord(%f) $= %value)
							{
								%val = restWords(%f);
								break;
							}
						}
	
					default:
						%val = %value;
				}

				%id = %ctrl.getRowID(%i);
				%ctrl.setRowByID(%id,%category TAB %title TAB %val TAB %value TAB %this.defaultValue);

				break;
			}
		}
	}
}

// +----------------------+
// | Initialize Component |
// +----------------------+
if(!isObject(SlayerClient.Options))
{
	if(!isObject(SlayerClient))
		exec("Add-ons/Gamemode_Slayer/Client/Main.cs");

	SlayerClient.Options = new scriptGroup(SlayerClient_OptionHandlerSO);
	SlayerClient.add(SlayerClient.Options);
}

SlayerClient_Support::Debug(2,"Dependency Loaded","Options");