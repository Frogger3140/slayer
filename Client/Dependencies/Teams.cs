// +-------------------------------------------+
// |  ___   _     _____   __  __  ____   ____  |
// | | __| | |   | /_\ |  \ \/ / | ___| | /\ | |
// | |__ | | |_  |  _  |   \  /  | __|  | \/ / |
// | |___| |___| |_| |_|   |__|  |____| |_| \\ |
// |  Greek2me              Blockland ID 11902 |
// +-----------------------------+-------------+
// | DO NOT EDIT BELOW THIS LINE |
// +-----------------------------+

$Slayer::Client::Dependencies::Teams = 1;

if(!isObject(SlayerClient.Teams))
{
	if(!isObject(SlayerClient))
		exec("Add-ons/Gamemode_Slayer/Client/Main.cs");

	SlayerClient.Teams = new scriptGroup(SlayerClient_TeamHandlerSO);
	SlayerClient.add(SlayerClient.Teams);
}

if(!isObject(SlayerClient.TeamPrefs))
{
	if(!isObject(SlayerClient))
		exec("Add-ons/Gamemode_Slayer/Client/Main.cs");

	SlayerClient.TeamPrefs = new scriptGroup(SlayerClient_TeamPrefHandlerSO);
	SlayerClient.add(SlayerClient.TeamPrefs);
}

// +----------------------------+
// | SlayerClient_TeamHandlerSO |
// +----------------------------+
function SlayerClient_TeamHandlerSO::addTeam(%this,%id,%name)
{
	SlayerClient_Support::Debug(2,"Team data recieved",%id TAB %name);

	%team = new scriptObject()
	{
		ID = %id;
		Team_Name = %name;
	};
	%this.add(%team);
	if(isObject(missionCleanup) && missionCleanup.isMember(%team))
		missionCleanup.remove(%team);

	for(%i=0; %i < SlayerClient.TeamPrefs.getCount(); %i++)
	{
		%p = SlayerClient.TeamPrefs.getObject(%i);

		%p.setValue(%team,%p.defaultValue);
		%p.setValue(%team,%p.defaultValue,"_ORIG");
	}

	Slayer_Teams_Selector.addRow(%team,%name);

	return %team;
}

function SlayerClient_TeamHandlerSO::removeTeam(%this,%team)
{
	if(%team.ID > 0)
		%this.removeTeamList = setField(%this.removeTeamList,getFieldCount(%this.removeTeamList),%team.ID);

	if(isObject(%team))
		%team.delete();

	if(Slayer_Teams_Selector.getSelectedID() == %team)
		Slayer_Teams_Edit.setVisible(0);
	Slayer_Teams_Selector.removeRowByID(%team);
}

function SlayerClient_TeamHandlerSO::clearTeams(%this,%removeFromServer)
{
	if(%removeFromServer)
	{
		for(%i=%this.getCount()-1; %i >= 0; %i--)
		{
			%t = %this.getObject(%i);
			%this.removeTeam(%t);
		}
	}
	else
	{
		%this.removeTeamList = "";
		%this.deleteAll();
	}

	%this.numNewTeams = 0;
	Slayer_Teams_Selector.clear();
}

function SlayerClient_TeamHandlerSO::getTeamFromID(%this,%id)
{
	for(%i=0; %i < %this.getCount(); %i++)
	{
		%t = %this.getObject(%i);
		if(%t.ID $= %id)
		{
			return %t;
		}
	}

	return -1;
}

function SlayerClient_TeamHandlerSO::getTeamFromName(%this,%name)
{
	for(%i=0; %i < %this.getCount(); %i++)
	{
		%t = %this.getObject(%i);
		if(%t.name $= %name)
		{
			return %t;
		}
	}

	return -1;
}

function SlayerClient_TeamHandlerSO::sendTeams(%this)
{
	//REMOVE TEAMS
	%rtl = %this.removeTeamList;
	if(getFieldCount(%rtl) > 0)
		commandToServer('Slayer_removeTeams',%rtl);

	for(%i=0; %i < %this.getCount(); %i++)
	{
		%team = %this.getObject(%i);

		//get the number of changed prefs
		%cnt = 0;
		for(%e=0; %e < SlayerClient.TeamPrefs.getCount(); %e++)
		{
			%p = SlayerClient.TeamPrefs.getObject(%e);
			%orig = %p.getValue(%team,"_ORIG");
			%value = %p.getValue(%team);

			if(%orig !$= %value)
				%cnt ++;
		}

		//START THE TEAM PREF TRANSFER
		commandToServer('Slayer_getTeamPrefs_Start',%team.ID,%cnt);

		for(%e=0; %e < SlayerClient.TeamPrefs.getCount(); %e++)
		{
			%p = SlayerClient.TeamPrefs.getObject(%e);
			%orig = %p.getValue(%team,"_ORIG");
			%value = %p.getValue(%team);

			if(%value !$= %orig)
				commandToServer('Slayer_getTeamPrefs_Tick',%team.ID,%p.category,%p.title,%value);
		}

		//END THE TEAM PREF TRANSFER
		commandToServer('Slayer_getTeamPrefs_End',%team.ID);
	}
}

// +--------------------------------+
// | SlayerClient_TeamPrefHandlerSO |
// +--------------------------------+
function SlayerClient_TeamPrefHandlerSO::addPref(%this,%category,%title,%type,%objectClassName,%defaultValue,%canEdit,%list)
{
	%stripChars = " `~!@#$%^&*()-=+[{]}\\|;:\'\",<.>/?";
	%variable = stripChars(%category,%stripChars) @ "_" @ stripChars(%title,%stripChars);

	%pref = new scriptObject()
	{
		class = SlayerClient_TeamPrefSO;

		title = %title;
		category = %category;
		type = %type;
		objectClassName = %objectClassName;
		variable = %variable;
		defaultValue = %defaultValue;
		canEdit = %canEdit;
		list = %list;
	};
	%this.add(%pref);
	if(isObject(missionCleanup) && missionCleanup.isMember(%pref))
		missionCleanup.remove(%pref);

	switch$(%list)
	{
		case Advanced:
			Slayer_Teams_Advanced_Selector.addRow(Slayer_Teams_Advanced_Selector.rowCount(),%category TAB %title TAB "" TAB "" TAB %type TAB %defaultValue TAB %canEdit);
	}

	return %pref;
}

function SlayerClient_TeamPrefHandlerSO::setPref(%this,%team,%category,%title,%value)
{
	if(!isObject(%team))
		return false;

	%pref = %this.getPrefSO(%category,%title);
	if(!isObject(%pref))
		return false;

	return %pref.setValue(%team,%value);
}

function SlayerClient_TeamPrefHandlerSO::getPref(%this,%team,%category,%title) //get the value of a pref
{
	if(!isObject(%team))
		return false;

	%pref = %this.getPrefSO(%category,%title);
	if(!isObject(%pref))
		return false;

	return %pref.getValue(%team);
}

function SlayerClient_TeamPrefHandlerSO::getPrefSO(%this,%category,%title) //get the SO of a pref
{
	for(%i=0; %i < %this.getCount(); %i++)
	{
		%p = %this.getObject(%i);
		if(%p.category $= %category && %p.title $= %title)
		{
			return %p;
		}
	}
	
	return 0;
}

function SlayerClient_TeamPrefHandlerSO::clearPrefs(%this)
{
	%this.deleteAll();

	Slayer_Teams_Advanced_Selector.clear();
}

// +-------------------------+
// | SlayerClient_TeamPrefSO |
// +-------------------------+
function SlayerClient_TeamPrefSO::setValue(%this,%team,%value,%ext)
{
	if(!isObject(%team))
		return false;

	SlayerClient_Support::setDynamicVariable(%team @ "." @ %this.variable @ %ext,%value);

	return true;
}

function SlayerClient_TeamPrefSO::getValue(%this,%team,%ext)
{
	if(!isObject(%team))
		return false;

	return SlayerClient_Support::getDynamicVariable(%team @ "." @ %this.variable @ %ext);
}

// +------------+
// | clientCmds |
// +------------+
function clientCmdSlayer_getTeams_Start(%expectedCount)
{
	SlayerClient_Support::Debug(2,"Recieving Teams...");
	SlayerClient.Teams.clearTeams();

	SlayerClient.Teams.expectedCount = %expectedCount;

	Slayer_Main_Content_Loading_Status.setText("Recieving Teams");
	Slayer_Main_Content_Loading_Progress.setValue(0);
}

function clientCmdSlayer_getTeams_Tick(%id,%name)
{
	SlayerClient.Teams.addTeam(%id,%name);

	Slayer_Main_Content_Loading_Progress.setValue(SlayerClient.Teams.getCount() / SlayerClient.Teams.expectedCount);
}

function clientCmdSlayer_getTeams_PrefTick(%id,%category,%title,%value)
{
	%team = SlayerClient.Teams.getTeamFromID(%id);
	if(!isObject(%team))
		return;

	%pref = SlayerClient.TeamPrefs.getPrefSO(%category,%title);
	if(!isObject(%pref))
		return;

	%pref.setValue(%team,%value);
	%pref.setValue(%team,%value,"_ORIG");
}

function clientCmdSlayer_getTeams_End()
{
	SlayerClient_Support::Debug(2,"Teams Recieved");

	SlayerClient.Teams.expectedCount = "";

	Slayer_Main_Content_Loading_Status.setText("");
	Slayer_Main_Content_Loading_Progress.setValue(0);
}

function clientCmdSlayer_getTeamPrefs_Start(%expectedCount)
{
	SlayerClient.TeamPrefs.clearPrefs();

	SlayerClient.TeamPrefs.expectedCount = %expectedCount;

	Slayer_Main_Content_Loading_Status.setText("Recieving Team Preferences");
	Slayer_Main_Content_Loading_Progress.setValue(0);
}

function clientCmdSlayer_getTeamPrefs_Tick(%category,%title,%type,%objectClassName,%defaultValue,%canEdit,%list)
{
	SlayerClient.TeamPrefs.addPref(%category,%title,%type,%objectClassName,%defaultValue,%canEdit,%list);

	Slayer_Main_Content_Loading_Progress.setValue(SlayerClient.TeamPrefs.getCount() / SlayerClient.TeamPrefs.expectedCount);
}

function clientCmdSlayer_getTeamPrefs_End()
{
	SlayerClient.TeamPrefs.expectedCount = "";

	Slayer_Main_Content_Loading_Status.setText("");
	Slayer_Main_Content_Loading_Progress.setValue(0);
}

SlayerClient_Support::Debug(2,"Dependency Loaded","Teams");