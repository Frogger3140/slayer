// +-------------------------------------------+
// |  ___   _     _____   __  __  ____   ____  |
// | | __| | |   | /_\ |  \ \/ / | ___| | /\ | |
// | |__ | | |_  |  _  |   \  /  | __|  | \/ / |
// | |___| |___| |_| |_|   |__|  |____| |_| \\ |
// |  Greek2me              Blockland ID 11902 |
// +-----------------------------+-------------+
// | DO NOT EDIT BELOW THIS LINE |
// +-----------------------------+

$Slayer::Client::Dependencies::ServerPrefs = 1;

if(!isObject(SlayerClient.ServerPrefs))
{
	if(!isObject(SlayerClient))
		exec("Add-ons/Gamemode_Slayer/Client/Main.cs");

	SlayerClient.ServerPrefs = new scriptGroup(SlayerClient_ServerPrefHandlerSO);
	SlayerClient.add(SlayerClient.ServerPrefs);
}

// +----------------------------------+
// | SlayerClient_ServerPrefHandlerSO |
// +----------------------------------+
function SlayerClient_ServerPrefHandlerSO::addPref(%this,%category,%title,%type,%objectClassName,%value,%defaultValue,%canEdit,%list)
{
	%numPrefs = %this.getCount();
	%stripChars = " `~!@#$%^&*()-=+[{]}\\|;:\'\",<.>/?";
	%variable = "$Slayer::Client::ServerPrefs::" @ stripChars(%category,%stripChars) @ "::" @ stripChars(%title,%stripChars);

	%pref = new scriptObject()
	{
		class = SlayerClient_ServerPrefSO;

		title = %title;
		category = %category;
		type = %type;
		isObject = (%className !$= "0" && %className !$= "");
		objectClassName = %objectClassName;
		variable = %variable;
		origValue = %value;
		defaultValue = %defaultValue;
		canEdit = %canEdit;
		list = %list;
	};
	%this.add(%pref);
	if(isObject(missionCleanup) && missionCleanup.isMember(%pref))
		missionCleanup.remove(%pref);

	%pref.setValue(%value);

	//for the announcement system
	if(%category $= "Server" && %title $= "Announcements")
	{
		%val = strReplace(%value,";","\t");
		for(%i=0; %i < getFieldCount(%val); %i++)
		{
			%f = trim(getField(%val,%i));
			Slayer_Main_Announcements.addMessage(%f,"SERVER");
		}
	}

	if(%list $= "Advanced" && Slayer_Advanced_CategoryFilter.findText(%category) < 0)
		Slayer_Advanced_CategoryFilter.add(%category,%pref);

	return %pref;
}

function SlayerClient_ServerPrefHandlerSO::setPref(%this,%category,%title,%value)
{
	%pref = %this.getPrefSO(%category,%title);
	if(!isObject(%pref))
		return;

	return %pref.setValue(%value);
}

function SlayerClient_ServerPrefHandlerSO::getPref(%this,%category,%title)
{
	%pref = %this.getPrefSO(%category,%title);
	if(!isObject(%pref))
		return;

	return %pref.getValue();
}

function SlayerClient_ServerPrefHandlerSO::getPrefSO(%this,%category,%title)
{
	for(%i=0; %i < %this.getCount(); %i++)
	{
		%p = %this.getObject(%i);
		if(%p.category $= %category && %p.title $= %title)
		{
			return %p;
		}
	}
	
	return 0;
}

function SlayerClient_ServerPrefHandlerSO::clearPrefs(%this)
{
	%this.deleteAll();

	Slayer_Advanced_Selector.clear();
	Slayer_Advanced_CategoryFilter.clear();

	Slayer_Main_Announcements.clearMessages("SERVER");
}

function SlayerClient_ServerPrefHandlerSO::exportPrefs(%this,%path)
{
	%file = new fileObject();
	%file.openForWrite(%path);

	%file.writeLine("//Slayer -----" SPC getDateTime());
	%file.writeLine("//By Greek2me, Blockland ID 11902");
	%file.writeLine("");
	%file.writeLine("//Version:" SPC $Slayer::Client::Version);
	%file.writeLine("//Debug Mode:" SPC $Slayer::Client::Debug);
	%file.writeLine("//Core Directory:" SPC $Slayer::Client::Directory);
	%file.writeLine("//Config Directory:" SPC $Slayer::Client::ConfigDir);
	%file.writeLine("");

	%file.writeLine("//PREFERENCES //USAGE: (category,title,value)");
	for(%i=0; %i < %this.getCount(); %i++)
	{
		%p = %this.getObject(%i);
		%v = %p.getValue();

		if(%p.objectClassName $= "")
			%val = "\"" @ %v @ "\"";
		else
		{
			%val = strReplace(%v.uiName,"\'","\\'");
			%val = "SlayerClient_Support::getIDFromUiName(\"" @ %val @ "\",\"" @ %p.objectClassName @ "\")";
		}

		%file.writeLine("SlayerClient.ServerPrefs.setPref(\"" @ %p.category @ "\",\"" @ %p.title @ "\"," @ %val @ ");");
	}

	//TEAMS
	%numNewTeams = 0;

	%file.writeLine("");
	%file.writeLine("//TEAMS");
	%file.writeLine("SlayerClient.Teams.clearTeams(1);");
	for(%i=0; %i < SlayerClient.Teams.getCount(); %i++)
	{
		%t = SlayerClient.Teams.getObject(%i);

		%file.writeLine("%team = SlayerClient.Teams.addTeam(\"" @ -%numNewTeams - 2 @ "\",\"" @ %t.Team_Name @ "\");");
		for(%e=0; %e < SlayerClient.TeamPrefs.getCount(); %e++)
		{
			%p = SlayerClient.TeamPrefs.getObject(%e);
			%v = %p.getValue(%t);

			if(%p.objectClassName $= "")
				%val = "\"" @ %v @ "\"";
			else
			{
				%val = strReplace(%v.uiName,"\'","\\'");
				%val = "SlayerClient_Support::getIDFromUiName(\"" @ %val @ "\",\"" @ %p.objectClassName @ "\")";
			}

			%file.writeLine("\tSlayerClient.TeamPrefs.setPref(%team,\"" @ %p.category @ "\",\"" @ %p.title @ "\"," @ %val @ ");");
		}

		%numNewTeams ++;
	}

	%file.writeLine("");
	%file.writeLine("SlayerClient_Support::Debug(1,\"Preferences Loaded\");");

	%file.close();
	%file.delete();

	SlayerClient_Support::Debug(1,"Exporting Preferences",%path);
}

//RULES
function SlayerClient_ServerPrefHandlerSO::addRule(%this,%category,%title,%type,%variable)
{
	%gui = "Slayer_Rules_" @ %category;
	if(!isObject(%gui))
		return;

	%value = SlayerClient_Support::getDynamicVariable(%variable);

	if(%gui.getCount() <= 0)
		%pos = "3 3";
	else
		%pos = 3 SPC getWord(%gui.getObject(%gui.getCount()-1).position,1) + getWord(%gui.getObject(%gui.getCount()-1).extent,1) + 3;
	%ext = getWord(%gui.extent,0)-6 SPC 30;

	%swatch = new GuiSwatchCtrl()
	{
		profile = "GuiDefaultProfile";
		horizSizing = "right";
		vertSizing = "bottom";
		position = %pos;
		extent = %ext;
		minExtent = "8 2";
		visible = "1";
		color = "50 50 50 50";
	};

	%txt = new GuiMLTextCtrl()
	{
		profile = "Slayer_TextProfile";
		horizSizing = "right";
		vertSizing = "center";
		position = "3 1"; //this is properly set below
		extent = (getWord(%ext,0) / 3) - 10 SPC "14";
		minExtent = "8 2";
		visible = "1";
		lineSpacing = "2";
		allowColorChars = "0";
		maxChars = "-1";
		text = %title;
		maxBitmapHeight = "-1";
		selectable = "1";
	};
	%swatch.add(%txt);

	%extX = getWord(%swatch.extent,0) - getWord(%txt.extent,0);
	%pos = getWord(%swatch.extent,0) - %extX + 10 SPC 0;
	%extX -= 15;

	switch$(getWord(%type,0))
	{
		case string:
			%ctrl = new GuiTextEditCtrl()
			{
				profile = "GuiTextEditProfile";
				horizSizing = "right";
				vertSizing = "center";
				position = %pos;
				extent = %extX SPC 18;
				minExtent = "8 2";
				visible = "1";
				variable = %variable;
				maxLength = getWord(%type,1);
				historySize = "0";
				password = "0";
				tabComplete = "0";
				sinkAllKeyEvents = "0";
			};
			
		case int:
			%ctrl = new GuiTextEditCtrl()
			{
				profile = "GuiTextEditProfile";
				horizSizing = "right";
				vertSizing = "center";
				position = %pos;
				extent = %extX SPC 18;
				minExtent = "8 2";
				visible = "1";
				variable = %variable;
				maxLength = "50";
				command = "$ThisControl.restrictNumberInput();";
				minValue = getWord(%type,1);
				maxValue = getWord(%type,2);
				historySize = "0";
				password = "0";
				tabComplete = "0";
				sinkAllKeyEvents = "0";
			};

		case bool:
			%ctrl = new GuiCheckBoxCtrl()
			{
				profile = "Slayer_CheckBoxProfile";
				horizSizing = "right";
				vertSizing = "center";
				position = %pos;
				extent = %extX SPC 30;
				minExtent = "8 2";
				visible = "1";
				variable = %variable;
				text = "";
				groupNum = "-1";
				buttonType = "ToggleButton";
			};

		case slide:
			%ctrl = new GuiSliderCtrl()
			{
				profile = "GuiSliderProfile";
				horizSizing = "right";
				vertSizing = "center";
				position = %pos;
				extent = %extX SPC 31;
				minExtent = "8 2";
				visible = "1";
				variable = %variable;
				range = getWords(%type,1,2);
				ticks = getWord(%type,3);
				value = "0.5";
				snap = getWord(%type,4);
			};
			%ctrl.setValue(%value);
			//slide controls add .000000 to the end of every number...
			%ctrl.command = %variable SPC "=" SPC "SlayerClient_Support::stripTrailingZeros(" @ %ctrl @ ".getValue());";
			schedule(50,0,eval,%ctrl.command);

		case list:
			%ctrl = new GuiPopUpMenuCtrl()
			{
				profile = "Slayer_PopUpMenuProfile";
				horizSizing = "right";
				vertSizing = "center";
				position = %pos;
				extent = %extX SPC 22;
				minExtent = "8 2";
				visible = "1";
				maxLength = "255";
				maxPopupHeight = "200";
			};
			%ctrl.command = %variable SPC "=" SPC %ctrl @ ".getSelected();";

			for(%i=1; %i < getFieldCount(%type); %i++)
			{
				%f = getField(%type,%i);
				%ctrl.add(restWords(%f),firstWord(%f));
			}
			%ctrl.setSelected(%value);

		default:
			%swatch.delete();
			return;
	}
	%swatch.add(%ctrl);
	%ctrl.setCenteredY();

	%guiExtY = getWord(%gui.extent,1);
	%swatchExtY = getWord(%swatch.extent,1);
	%swatchPosY = getWord(%swatch.position,1);

	%extX = getWord(%gui.extent,0);
	%extY = %guiExtY + %swatchExtY + 3;
	%gui.extent = %extX SPC %extY;

	%gui.add(%swatch);

	%txt.forceReflow();
	%txt.setCenteredY();

	%gui.setVisible(1); //refresh so that the scroll box works

	return %swatch;
}

function SlayerClient_ServerPrefHandlerSO::updateRules(%this)
{
	SlayerClient_Support::Debug(2,"Updating Rules");

	for(%i=0; %i < %this.getCount(); %i++)
	{
		%pref = %this.getObject(%i);
		%list = %pref.list;

		if(getWord(%list,0) $= "Rules")
		{
			%add = 0;
			%condition = getWord(%list,1);

			if(%condition $= SlayerClient.Gamemodes.Mode.fName)
				%add = 1;
			if(%condition $= "ALL")
				%add = 1;
			if(%condition $= "Teams" && SlayerClient.Gamemodes.teams)
				%add = 1;
			if(%condition $= "!Teams" && !SlayerClient.Gamemodes.teams)
				%add = 1;
			if(%condition $= "Rounds" && SlayerClient.Gamemodes.rounds)
				%add = 1;
			if(%condition $= "!Rounds" && !SlayerClient.Gamemodes.rounds)
				%add = 1;

			if(%add)
				%this.addRule(getWord(%pref.list,2),%pref.title,%pref.type,%pref.variable);
		}
	}
}

function SlayerClient_ServerPrefHandlerSO::clearRules(%this,%category)
{
	if(%category $= "ALL")
	{
		%this.clearRules("Mode");
		%this.clearRules("Player");
		%this.clearRules("Respawn");
		%this.clearRules("Points");
	}
	else
	{
		%gui = "Slayer_Rules_" @ %category;
		if(!isObject(%gui))
			return;

		%gui.deleteAll();
		%gui.extent = getWord(%gui.extent,0) SPC 3;
		%gui.setVisible(1); //refresh so that the scroll box works
	}
}

// +---------------------------+
// | SlayerClient_ServerPrefSO |
// +---------------------------+
function SlayerClient_ServerPrefSO::setValue(%this,%value)
{
	SlayerClient_Support::setDynamicVariable(%this.variable,%value);

	return true;
}

function SlayerClient_ServerPrefSO::getValue(%this)
{
	return SlayerClient_Support::getDynamicVariable(%this.variable);
}

// +------------+
// | clientCmds |
// +------------+
function clientCmdSlayer_getPrefs_Start(%expectedCount)
{
	SlayerClient_Support::Debug(2,"Recieving Preferences...");
	SlayerClient.ServerPrefs.clearPrefs();

	SlayerClient.ServerPrefs.expectedCount = %expectedCount;

	Slayer_Main_Content_Loading_Status.setText("Recieving Preferences");
	Slayer_Main_Content_Loading_Progress.setValue(0);
}

function clientCmdSlayer_getPrefs_Tick(%category,%title,%type,%objectClassName,%defaultValue,%value,%canEdit,%list)
{
	SlayerClient_Support::Debug(2,"Preference Recieved",%category TAB %title TAB %type TAB %objectClassName TAB %value TAB %defaultValue TAB %canEdit TAB %list);

	SlayerClient.ServerPrefs.addPref(%category,%title,%type,%objectClassName,%value,%defaultValue,%canEdit,%list);

	Slayer_Main_Content_Loading_Progress.setValue(SlayerClient.ServerPrefs.getCount() / SlayerClient.ServerPrefs.expectedCount);
}

function clientCmdSlayer_getPrefs_End(%prefCount)
{
	SlayerClient_Support::Debug(1,"Preferences Recieved",%prefCount SPC "Prefs");

	SlayerClient.ServerPrefs.expectedCount = "";
	Slayer_Main_Content_Loading_Status.setText("");
	Slayer_Main_Content_Loading_Progress.setValue(0);

	Slayer_Advanced_CategoryFilter.sort();
	Slayer_Advanced_CategoryFilter.addFront("All",-1);
}

SlayerClient_Support::Debug(2,"Dependency Loaded","ServerPrefs");