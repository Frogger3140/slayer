// +-------------------------------------------+
// |  ___   _     _____   __  __  ____   ____  |
// | | __| | |   | /_\ |  \ \/ / | ___| | /\ | |
// | |__ | | |_  |  _  |   \  /  | __|  | \/ / |
// | |___| |___| |_| |_|   |__|  |____| |_| \\ |
// |  Greek2me              Blockland ID 11902 |
// +-----------------------------+-------------+
// | DO NOT EDIT BELOW THIS LINE |
// +-----------------------------+

$Slayer::Client::Dependencies::Gamemodes = 1;

if(!isObject(SlayerClient.Gamemodes))
{
	if(!isObject(SlayerClient))
		exec("Add-ons/Gamemode_Slayer/Client/Main.cs");

	SlayerClient.Gamemodes = new scriptGroup(SlayerClient_GamemodeHandlerSO);
	SlayerClient.add(SlayerClient.Gamemodes);
}

// +--------------------------------+
// | SlayerClient_GamemodeHandlerSO |
// +--------------------------------+
function SlayerClient_GamemodeHandlerSO::addMode(%this,%name,%fname,%teams,%rounds)
{
	%mode = new scriptObject()
	{
		uiName = %name;
		fName = %fName;
		Teams = %teams;
		Rounds = %rounds;
	};
	%this.add(%mode);
	if(isObject(missionCleanup) && missionCleanup.isMember(%mode))
		missionCleanup.remove(%mode);

	Slayer_General_Mode_Selector.add(%name,%mode); //add to GUI lists
}

function SlayerClient_GamemodeHandlerSO::setMode(%this,%flag)
{
	%mode = %this.getModeFromFName(%flag);
	if(!isObject(%mode))
	{
		SlayerClient_Support::Debug(1,"Set Gamemode","Incorrect arguements! You must have a valid gamemode fName.");
		return;
	}

	$Slayer::Client::ServerPrefs::Minigame::Gamemode = %flag;

	%this.mode = %mode;
	%this.teams = %mode.teams;
	%this.rounds = %mode.rounds;
}

function SlayerClient_GamemodeHandlerSO::getModeFromFName(%this,%flag)
{
	for(%i=0; %i < %this.getCount(); %i++)
	{
		if(%this.getObject(%i).fName $= %flag)
		{
			return %this.getObject(%i);
		}
	}

	return 0;
}

// +------------+
// | clientCmds |
// +------------+
function clientCmdSlayer_getGamemodes_Start()
{

}

function clientCmdSlayer_getGamemodes_Tick(%name,%fname,%teams,%rounds)
{
	SlayerClient_Support::Debug(2,"Gamemode Recieved",%name TAB %fname TAB %teams TAB %rounds);

	SlayerClient.Gamemodes.addMode(%name,%fName,%teams,%rounds);
}

function clientCmdSlayer_getGamemodes_End()
{
	Slayer_General_Mode_Selector.sort();
}

SlayerClient_Support::Debug(2,"Dependency Loaded","Gamemodes");