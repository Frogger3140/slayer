// +-------------------------------------------+
// |  ___   _     _____   __  __  ____   ____  |
// | | __| | |   | /_\ |  \ \/ / | ___| | /\ | |
// | |__ | | |_  |  _  |   \  /  | __|  | \/ / |
// | |___| |___| |_| |_|   |__|  |____| |_| \\ |
// |  Greek2me              Blockland ID 11902 |
// +-----------------------------+-------------+
// | DO NOT EDIT BELOW THIS LINE |
// +-----------------------------+

$Slayer::Client::Modules::Help = 1;

$Slayer::Client::Modules::Help::ServerURL = "greekmods.webs.com:80";
$Slayer::Client::Modules::Help::ServerDir = "/mods/Gamemode_Slayer/Help/help_Release.txt";

if(!isObject(SlayerClient.HelpHTTP))
{
	if(!isObject(SlayerClient))
		exec("Add-ons/Gamemode_Slayer/Client/Main.cs");

	SlayerClient.HelpHTTP = new HTTPObject(SlayerClient_HelpHTTP);
	SlayerClient.add(SlayerClient.HelpHTTP);
}

// +-----------------------+
// | SlayerClient_HelpHTTP |
// +-----------------------+
function SlayerClient_HelpHTTP::DownloadHelpFile(%this)
{
	%this.get($Slayer::Client::Modules::Help::ServerURL,$Slayer::Client::Modules::Help::ServerDir);
}

function SlayerClient_HelpHTTP::onConnected(%this)
{
	$Slayer::Client::Modules::Help::HelpText = "";
}

//error handling
function SlayerClient_HelpHTTP::onConnectFailed(%this)
{
	Slayer_Help_Text.setText("<just:center>\n\nError Retrieving Help File:\nUnspecified Error");
}

function SlayerClient_HelpHTTP::onDNSFailed(%this)
{
	Slayer_Help_Text.setText("<just:center>\n\nError Retrieving Help File:\nDNS Failure");
}

function SlayerClient_HelpHTTP::onLine(%this,%line)
{
	SlayerClient_Support::Debug(2,"Help File Line Recieved",%line);

	%value = trim(%line);

	if(striPos(%line,"<title>Not Found - Webs.com</title>") >= 0)
	{
		%this.disconnect();
		Slayer_Help_Text.schedule(0,setText,"<just:center>\n\nError Retrieving Help File:\n404 File Not Found");
		return;
	}

	%value = parseCustomTML(%value,Slayer_Help_Text,"default");

	%help = $Slayer::Client::Modules::Help::HelpText;
	if(%help $= "" || %help $= Slayer_Help_Text.text)
		$Slayer::Client::Modules::Help::HelpText = %value;
	else
		$Slayer::Client::Modules::Help::HelpText = %help @ %value;

	Slayer_Help_Text.setText($Slayer::Client::Modules::Help::HelpText);
}