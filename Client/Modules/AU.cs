// +-------------------------------------------+
// |  ___   _     _____   __  __  ____   ____  |
// | | __| | |   | /_\ |  \ \/ / | ___| | /\ | |
// | |__ | | |_  |  _  |   \  /  | __|  | \/ / |
// | |___| |___| |_| |_|   |__|  |____| |_| \\ |
// |  Greek2me              Blockland ID 11902 |
// +-----------------------------+-------------+
// | DO NOT EDIT BELOW THIS LINE |
// +-----------------------------+

$Slayer::Client::Modules::AU = 1;

$Slayer::Client::AU::ServerURL = "greekmods.webs.com:80";
$Slayer::Client::AU::ServerDir = "/mods/Gamemode_Slayer/Updater/version_Release.txt";

// +-------------------+
// | Check for updates |
// +-------------------+
function SlayerClient_autoUpdateHTTP::checkForUpdate(%this)
{
	%this.noNewVersion = "";

	deleteVariables("$Slayer::Client::AU::newVersion*");
	$Slayer::Client::AU::newVersion = "";

	//PREVIOUS UPDATE WAS SUCCESSFUL?
	if($Slayer::Client::Update)
	{
		if(isObject(RTB_Overlay))
			canvas.popDialog(RTB_Overlay);

		if(isObject(RTBCC_NotificationManager))
		{
			//CREATE A NOTIFICATION
			%title = "Slayer - v" @ $Slayer::Client::Version;
			%message = "Slayer has been updated.";
			%icon = "information";
			%key = "slayer_updated";
			%holdTime = 10000;
			RTBCC_NotificationManager.push(%title,%message,%icon,%key,%holdTime);
			%notification = RTBCC_NotificationManager.getObject(RTBCC_NotificationManager.getCount() - 1);
			%notification.window.getObject(3).command = "Slayer_Update.pushChangeLog();" @ %notification @ ".state = \"done\";" @ %notification @ ".step();";
		}

		if($Slayer::Client::Update::RequiresRestart)
			setPopup(canvas,0,"YESNO","exclamation","Slayer | Update","<just:center>\nSlayer will not work until you restart Blockland.\n\nClose Blockland now?","quit();");
		else if(!$Slayer::Client::Update::Silent)
			setPopup(canvas,0,"OK","information","Slayer | Update","<just:center>\nUpdate Complete!\n\nYou were successfully updated from Slayer v" @ $Slayer::Client::Update::OldVersion SPC "to Slayer v" @ $Slayer::Client::Version @ ".");
	}
	else //CHECK FOR NEW VERSION
	{
		%this.get($Slayer::Client::AU::ServerURL,$Slayer::Client::AU::ServerDir);
	}
}

function SlayerClient_autoUpdateHTTP::onLine(%this,%line)
{
	SlayerClient_Support::Debug(2,"Auto Update Line Recieved",%line);

	//comments
	if(strlen(%line) >= 2 && getSubStr(%line,0,2) $= "//")
	{
		return;
	}

	parseCustomTML(%line,%this,"slayerAutoUpdate");
}

function customTMLParser_slayerAutoUpdate(%obj,%value0,%value1,%value2,%value3,%value4,%value5,%value6,%value7,%value8,%value9,%value10,%value11,%value12,%value13,%value14,%value15)
{
	if(%value0 $= "version")
	{
		if(!$Slayer::Client::AU::Enable)
			return;

		if(%value1 $= "latest")
		{
			if(%value2 !$= $Slayer::Client::Version) //maybe check for a newer version, not just a different version...
			{
				SlayerClient_Support::Debug(0,"New version available",%value2);
				$Slayer::Client::AU::newVersion = %value2;
			}
			else
			{
				%obj.noNewVersion = 1;
				return;
			}
		}
		else if(%value1 $= "download")
		{
			if(%obj.noNewVersion)
				return;

			//GET THE SERVER AND DIRECTORY INFO
			%pos = strPos(%value2,"/");
			$Slayer::Client::AU::newVersion::URL = getSubStr(%value2,0,%pos);
			$Slayer::Client::AU::newVersion::Dir = getSubStr(%value2,%pos,strLen(%value2));

			//IS IT ON RTB?
			$Slayer::Client::AU::newVersion::rtbFileID = %value3;

			SlayerClient_Support::Debug(0,"Download Version" SPC $Slayer::Client::AU::newVersion SPC "at",%value2);

			//SILENT UPDATES
			if($Slayer::Client::AU::SilentUpdates && !$Slayer::Client::AU::newVersion::forcePrompt && isObject(RTBMM_TransferQueue) && $Slayer::Client::AU::newVersion::rtbFileID !$= "")
			{
				SlayerClient_auDownloadUpdate(1);
			}
			//USER-PROMPT UPDATES
			else if(!$Slayer::Client::AU::UpdateDisplayed && !$Slayer::Client::AU::newVersion::silentOnly)
			{
				canvas.pushDialog(Slayer_Update);
			}
			//MANUAL UPDATES
			else
			{
				//UPDATE THE OPTIONS GUI
				Slayer_Options_Updates_Text.setText("<just:center>New version available:" SPC $Slayer::Client::AU::newVersion);
				Slayer_Options_Updates_Button.setText("Download");
				Slayer_Options_Updates_Button.command = "canvas.pushDialog(Slayer_Update);";
			}
		}
		else if(%value1 $= "forcePrompt")
		{
			$Slayer::Client::AU::newVersion::forcePrompt = 1;
		}
		else if(%value1 $= "silentOnly")
		{
			$Slayer::Client::AU::newVersion::silentOnly = 1;
		}
		else if(%value1 $= "clientRequiresRestart")
		{
			$Slayer::Client::AU::newVersion::RequiresRestart = 1;
		}
		else if(%value1 $= "latestInstaller")
		{
			$Slayer::Client::LastInstallerVersion = %value2;
			Slayer_Welcome.checkFirstRun();
		}
	}
	else if(%value0 $= "changeLog")
	{
		if(!$Slayer::Client::AU::Enable || %obj.noNewVersion)
			return;

		if(%value1 $= "URL")
		{
			%pos = strPos(%value2,"/");
			$Slayer::Client::AU::ChangeLog::URL = getSubStr(%value2,0,%pos);
			$Slayer::Client::AU::ChangeLog::Dir = getSubStr(%value2,%pos,strLen(%value2));

			SlayerClient.CLHTTP.downloadChangeLog();
		}
	}
	else if(%value0 $= "announcement")
	{
		%val = "<bullet>" @ %value1 @ %value2 @ %value3 @ %value4 @ %value5 @ %value6 @ %value7 @ %value8 @ %value9 @ %value10 @ %value11 @ %value12 @ %value13 @ %value14 @ %value15;
		%ctrl = Slayer_Main_Content_StartScreen_Announcements;

		%val = parseCustomTML(%val,%ctrl,"default");

		%text = %ctrl.getText();
		if(%text $= "")
			%ctrl.setText(%val);
		else
			%ctrl.setText(%text @ "\n\n" @ %val);

		%val = stripMLControlChars(%value);
		Slayer_Main_Announcements.addMessage(%val,"WEB");
	}
}

// +---------------------+
// | Download the Update |
// +---------------------+
function SlayerClient_auDownloadUpdate(%silent)
{
	if(isObject(RTBMM_TransferQueue) && $Slayer::Client::AU::newVersion::rtbFileID !$= "")
	{
		//CREATE A NOTIFICATION
		if(isObject(RTBCC_NotificationManager) && %silent)
		{
			%title = "Slayer - Update";
			%message = "Slayer is downloading updates.";
			%icon = "new";
			%key = "slayer_updateDownload";
			%holdTime = 5000;
			RTBCC_NotificationManager.push(%title,%message,%icon,%key,%holdTime);
			%notification = RTBCC_NotificationManager.getObject(RTBCC_NotificationManager.getCount() - 1);
			%notification.window.getObject(3).command = "Slayer_Update.pushChangeLog();" @ %notification @ ".state = \"done\";" @ %notification @ ".step();";
		}

		%rtbFileID = $Slayer::Client::AU::newVersion::rtbFileID;

		$Slayer::Client::Update = 1;
		$Slayer::Client::Update::OldVersion = $Slayer::Client::Version;
		$Slayer::Client::Update::RequiresRestart = $Slayer::Client::AU::newVersion::RequiresRestart;
		$Slayer::Client::Update::Silent = %silent;

		deleteVariables("$Slayer::Client::AU::NewVersion*"); //make sure stuff is ready for when it reloads
		$Slayer::Client::AU::NewVersion = "";

		if(!%silent)
		{
			canvas.pushDialog(RTB_Overlay);
		}
		RTB_ModManager.setVisible(1);
		RTBMM_TransferView_Init();

		RTBMM_TransferQueue.addItem(%rtbFileID);
	}
	else
	{
		goToWebPage($Slayer::Client::AU::newVersion::URL @ $Slayer::Client::AU::newVersion::Dir);
		setPopup(canvas,0,"YESNO","information","Slayer | Update","<just:center>\nYou will need to restart Blockland after installing the update.\n\nClose Blockland now?","quit();");
	}
}

// +-------------------------+
// | Download the Change Log |
// +-------------------------+
function SlayerClient_changeLogHTTP::downloadChangeLog(%this)
{
	%this.get($Slayer::Client::AU::ChangeLog::URL @ ":80",$Slayer::Client::AU::ChangeLog::Dir);
}

function SlayerClient_changeLogHTTP::onConnected(%this)
{
	$Slayer::Client::AU::ChangeLog = "";
}

function SlayerClient_changeLogHTTP::onConnectFailed(%this)
{
	Slayer_Update_CL_Text.setText("<just:center>\n\nError Retrieving Change Log:\nUnspecified Error");
}

function SlayerClient_changeLogHTTP::onLine(%this,%line)
{
	SlayerClient_Support::Debug(2,"Change Log Line Recieved",%line);

	%value = trim(%line);

	//404 NOT FOUND
	if(striPos(%line,"<title>Not Found - Webs.com</title>") >= 0)
	{
		%this.disconnect();
		Slayer_Update_CL_Text.schedule(0,setText,"<just:center>\n\nError Retrieving Change Log:\n404 File Not Found");
		return;
	}

	//PARSE IT FOR TML FORMATTING
	%value = parseCustomTML(%value,Slayer_Update_CL_Text,"default\tslayerChangeLog");

	//UPDATE THE VARIABLE AND DISPLAY
	%cl = $Slayer::Client::AU::ChangeLog;
	if(%cl $= "" || %cl $= Slayer_Update_CL_Text.text)
		$Slayer::Client::AU::ChangeLog = %value;
	else
		$Slayer::Client::AU::ChangeLog = %cl @ %value;

	Slayer_Update_CL_Text.setText($Slayer::Client::AU::ChangeLog);
	Slayer_Update_CL_Window_Text.setText($Slayer::Client::AU::ChangeLog);
}

function customTMLParser_slayerChangeLog(%obj,%value0,%value1)
{
	switch$(%value[0])
	{
		case "version":
			%replace = "<b>Version" SPC %value[1] @ "</b><br>";

		case "/version":
			%replace = "";

		case "img":
			%replace = "<bitmap:" @ $Slayer::Client::Directory @ "/Images/" @ %value[1] @ ">";
	}

	return %replace;
}

// +------------+
// | Initialize |
// +------------+
if(!isObject(SlayerClient.AUHTTP))
{
	if(!isObject(SlayerClient))
		exec("Add-ons/Gamemode_Slayer/Client/Main.cs");

	SlayerClient.AUHTTP = new HTTPObject(SlayerClient_autoUpdateHTTP);
	SlayerClient.add(SlayerClient.AUHTTP);
}

if(!isObject(SlayerClient.CLHTTP))
{
	if(!isObject(SlayerClient))
		exec("Add-ons/Gamemode_Slayer/Client/Main.cs");

	SlayerClient.CLHTTP = new HTTPObject(SlayerClient_changeLogHTTP);
	SlayerClient.add(SlayerClient.CLHTTP);
}

//UPDATE CHECK
SlayerClient.AUHTTP.checkForUpdate();

SlayerClient_Support::Debug(2,"Module Loaded","AU");