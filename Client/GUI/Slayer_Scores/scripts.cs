// +-------------------------------------------+
// |  ___   _     _____   __  __  ____   ____  |
// | | __| | |   | /_\ |  \ \/ / | ___| | /\ | |
// | |__ | | |_  |  _  |   \  /  | __|  | \/ / |
// | |___| |___| |_| |_|   |__|  |____| |_| \\ |
// |  Greek2me              Blockland ID 11902 |
// +-----------------------------+-------------+
// | DO NOT EDIT BELOW THIS LINE |
// +-----------------------------+

// +---------------+
// | Slayer_Scores |
// +---------------+
function Slayer_Scores::onWake(%this)
{
	%this.setCentered();
}

function Slayer_Scores::initList(%this,%tabs,%header)
{
	Slayer_Scores_Text.setText("");

	%text = "<color:ffffff>" @ parseCustomTML(%header,Slayer_Scores_Text,"default");
	if(%tabs !$= "")
		%text = "<tab:"@ %tabs @ ">" @ %text;

	Slayer_Scores_Text.setText(%text);
}

function Slayer_Scores::addLine(%this,%line)
{
	%line = parseCustomTML(%line,Slayer_Scores_Text,"default");

	Slayer_Scores_Text.setText(Slayer_Scores_Text.getText() NL %line);
}

// +------------+
// | clientCmds |
// +------------+
function clientCmdSlayer_scoreListInit(%tabs,%header)
{
	if(getTag(%header) !$= %header) //see if it's a tagged string
		%header = restWords(%header);
	Slayer_Scores.initList(%tabs,%header);
}

function clientCmdSlayer_scoreListAdd(%line)
{
	if(getTag(%line) !$= %line) //see if it's a tagged string
		%line = restWords(%line);
	Slayer_Scores.addLine(%line);
}