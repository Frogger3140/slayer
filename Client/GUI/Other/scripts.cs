// +-------------------------------------------+
// |  ___   _     _____   __  __  ____   ____  |
// | | __| | |   | /_\ |  \ \/ / | ___| | /\ | |
// | |__ | | |_  |  _  |   \  /  | __|  | \/ / |
// | |___| |___| |_| |_|   |__|  |____| |_| \\ |
// |  Greek2me              Blockland ID 11902 |
// +-----------------------------+-------------+
// | DO NOT EDIT BELOW THIS LINE |
// +-----------------------------+

// +-----------------+
// | joinMinigameGUI |
// +-----------------+
function JMG_List::onSelect(%this,%id)
{
	%rowText = %this.getRowTextById(%id);

	if(getField(%rowText,5))
		JMG_Slayer_Edit.setActive(1);
	else
		JMG_Slayer_Edit.setActive(0);
}

function JMG_Slayer::onWake(%this)
{
	if(%this.isVisible())
	{
		%miniState = SlayerClient.miniState;

		if(%miniState $= "CREATE")
		{
			%create = 1;
		}
		else
		{
			%create = 0;
		}

		JMG_Slayer_Create.setActive(%create);
	}
}

function JMG_Slayer::createMinigame(%this)
{
	if(SlayerClient.miniState !$= "CREATE")
	{
		setPopup(JMG_Window,1,"OK","exclamation","Oops!","You don't have permission to create a Slayer minigame.\n\nPlease contact the host for assistance.");
		return;
	}

	SlayerClient.editMinigame(-1);

	canvas.popDialog(joinMinigameGUI);
}

function JMG_Slayer::editMinigame(%this)
{
	%id = JMG_List.getSelectedId();
	%rowText = JMG_List.getRowTextById(%id);

	%isDefault = getField(%rowText,4);
	%canEdit = getField(%rowText,5);

	if(%id < 0)
	{
		setPopup(JMG_Window,1,"OK","exclamation","Oops!","Please select a minigame to edit.");
		return;
	}

	if(%isDefault $= "" && %canEdit $= "")
	{
		setPopup(JMG_Window,1,"OK","exclamation","Oops!","The minigame you have selected is not a Slayer minigame.\n\nPlease select a Slayer minigame to edit.");
		return;
	}

	if(!%canEdit)
	{
		setPopup(JMG_Window,1,"OK","exclamation","Oops!","You don't have permission to edit that minigame.\n\nPlease contact the owner for assistance.");
		return;
	}

	SlayerClient.editMinigame(%id);

	canvas.popDialog(joinMinigameGUI);
}

// +--------------------+
// | Packaged Functions |
// +--------------------+
package Slayer_Client_GUI_JMG
{
	function joinMiniGameGui::onWake(%this)
	{
		commandToServer('Slayer_sendMiniState');

		parent::onWake(%this);
	}
};
activatePackage(Slayer_Client_GUI_JMG);

SlayerClient_Support::Debug(2,"GUI Scripts Loaded","JoinMinigameGUI");