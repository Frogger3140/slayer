// +-------------------------------------------+
// |  ___   _     _____   __  __  ____   ____  |
// | | __| | |   | /_\ |  \ \/ / | ___| | /\ | |
// | |__ | | |_  |  _  |   \  /  | __|  | \/ / |
// | |___| |___| |_| |_|   |__|  |____| |_| \\ |
// |  Greek2me              Blockland ID 11902 |
// +-----------------------------+-------------+
// | DO NOT EDIT BELOW THIS LINE |
// +-----------------------------+

// +----------+
// | Advanced |
// +----------+
function Slayer_Advanced::onWake(%this,%tabSet)
{
	if(%tabSet)
	{
		Slayer_Advanced_bool.setVisible(0);
		Slayer_Advanced_list.setVisible(0);
		Slayer_Advanced_string.setVisible(0);
		Slayer_Advanced_int.setVisible(0);
		Slayer_Advanced_slide.setVisible(0);
		Slayer_Advanced_Apply.setVisible(0);

		%selected = Slayer_Advanced_CategoryFilter.getSelected();
		if(%selected != 0)
		{
			%category = Slayer_Advanced_CategoryFilter.getTextByID(%selected);
			%this.refreshPrefs(%category);
		}
		else
			Slayer_Advanced_CategoryFilter.setSelected(-1);
	}
}

function Slayer_Advanced::refreshPrefs(%this,%category)
{
	Slayer_Advanced_Selector.clear();

	for(%i=0; %i < SlayerClient.ServerPrefs.getCount(); %i++)
	{
		%pref = SlayerClient.ServerPrefs.getObject(%i);
		if(%pref.list !$= "Advanced")
			continue;
		if(%category !$= "" && %category !$= "All" && %category !$= %pref.category)
			continue;

		%type = %pref.type;
		%value = %pref.getValue();

		switch$(getWord(%type,0))
		{
			case bool:
				if(%value)
					%val = "True";
				else
					%val = "False";

			case list:
				for(%e=0; %e < getFieldCount(%type); %e++)
				{
					%f = getField(%type,%e);
					if(firstWord(%f) $= %value)
					{
						%val = restWords(%f);
						break;
					}
				}

			default:
				%val = %value;
		}

		Slayer_Advanced_Selector.addRow(Slayer_Advanced_Selector.rowCount(),%pref.category TAB %pref.title TAB %val TAB %value TAB getField(%type,0) TAB %pref.defaultValue TAB %pref.canEdit);
	}

	Slayer_Advanced_Selector.sort(0);
}

function Slayer_Advanced_CategoryFilter::onSelect(%this,%id)
{
	%category = %this.getTextByID(%id);
	Slayer_Advanced.refreshPrefs(%category);
}

function Slayer_Advanced_Selector::onSelect(%this,%id)
{
	%entry = Slayer_Advanced_Selector.getRowTextByID(%id);

	Slayer_Advanced_bool.setVisible(0);
	Slayer_Advanced_list.setVisible(0);
	Slayer_Advanced_string.setVisible(0);
	Slayer_Advanced_int.setVisible(0);
	Slayer_Advanced_slide.setVisible(0);

	%canEdit = getField(%entry,6);
	if(!%canEdit && %canEdit != -1 && %canEdit !$= "")
	{
		setPopup(Slayer_Main,0,"OK","exclamation","Access Denied","You don't have permission to edit that setting. Please contact the host for assistance.");
		Slayer_Advanced_Apply.setVisible(0);
		Slayer_Advanced_Selector.clearSelection();
		return;
	}

	Slayer_Advanced_Apply.setVisible(1);

	%control = "Slayer_Advanced_" @ getWord(getField(%entry,4),0);
	%control.setVisible(1);

	switch$(getWord(getField(%entry,4),0))
	{
		case bool:
			%control.setText(getField(%entry,1));
			%control.setValue(getField(%entry,3));

		case string:
			%control.maxLength = getWord(getField(%entry,4),1);
			%control.setValue(getField(%entry,3));

		case int:
			%control.minValue = getWord(getField(%entry,4),1);
			%control.maxValue = getWord(getField(%entry,4),2);
			%control.setValue(getField(%entry,3));

		case slide:
			%control.range = getWords(getField(%entry,4),1,2);
			%control.ticks = getWord(getField(%entry,4),3);
			%control.snap = getWord(getField(%entry,4),4);
			%control.setValue(getField(%entry,3));

		case list:
			%control.clear();
			%pref = SlayerClient.ServerPrefs.getPrefSO(getField(%entry,0),getField(%entry,1));

			for(%i=1; %i < getFieldCount(%pref.type); %i++)
			{
				%f = getField(%pref.type,%i);
				%control.add(restWords(%f),firstWord(%f));
			}
			%control.setSelected(getField(%entry,3));
	}

}

function Slayer_Advanced::Apply(%this,%id)
{
	if(%id < 0)
		return;

	%entry = Slayer_Advanced_Selector.getRowTextByID(%id);

	Slayer_Advanced_Apply.setVisible(0);
	%type = getWord(getField(%entry,4),0);
	%control = Slayer_Advanced_ @ %type;
	%control.setVisible(0);

	switch$(%type)
	{
		case list:
			%entry = setField(%entry,3,%control.getSelected());
			%entry = setField(%entry,2,%control.getText());

		case bool:
			%value = %control.getValue();
			%entry = setField(%entry,3,%value);
			if(%value)
				%entry = setField(%entry,2,"True");
			else
				%entry = setField(%entry,2,"False");

		default:
			%entry = setField(%entry,2,%control.getValue());
			%entry = setField(%entry,3,%control.getValue());
	}
	SlayerClient.ServerPrefs.setPref(getField(%entry,0),getField(%entry,1),getField(%entry,3));
	Slayer_Advanced_Selector.setRowByID(%id,%entry);
	Slayer_Advanced_Selector.clearSelection();
}

SlayerClient_Support::Debug(2,"GUI Scripts Loaded","Slayer_Advanced");