// +-------------------------------------------+
// |  ___   _     _____   __  __  ____   ____  |
// | | __| | |   | /_\ |  \ \/ / | ___| | /\ | |
// | |__ | | |_  |  _  |   \  /  | __|  | \/ / |
// | |___| |___| |_| |_|   |__|  |____| |_| \\ |
// |  Greek2me              Blockland ID 11902 |
// +-----------------------------+-------------+
// | DO NOT EDIT BELOW THIS LINE |
// +-----------------------------+

// +------+
// | Help |
// +------+
function Slayer_Help::onWake(%this,%tabSet)
{
	if(%tabSet)
	{
		if($Slayer::Client::Modules::Help)
		{
			%help = $Slayer::Client::Modules::Help::HelpText;
			if(%help $= "")
				SlayerClient.HelpHTTP.downloadHelpFile();
			else
				Slayer_Help_Text.setText(%help);
		}
		else
			Slayer_Help_Text.setText("<just:center>\n\nError Retrieving Help File:\nHelp Module Missing");
	}
}

SlayerClient_Support::Debug(2,"GUI Scripts Loaded","Slayer_Help");