// +-------------------------------------------+
// |  ___   _     _____   __  __  ____   ____  |
// | | __| | |   | /_\ |  \ \/ / | ___| | /\ | |
// | |__ | | |_  |  _  |   \  /  | __|  | \/ / |
// | |___| |___| |_| |_|   |__|  |____| |_| \\ |
// |  Greek2me              Blockland ID 11902 |
// +-----------------------------+-------------+
// | DO NOT EDIT BELOW THIS LINE |
// +-----------------------------+

// +-------+
// | Teams |
// +-------+
function Slayer_Teams::onWake(%this,%tabSet)
{
	if(%tabSet)
	{
		Slayer_Teams_MovePlayerName.clear();
		for(%i=0; %i < NPL_List.rowCount(); %i++)
		{
			%text = NPL_List.getRowText(%i);
			if(getField(%text,5))
			{
				%name = getField(%text,1);
				%id = NPL_List.getRowID(%i);
				Slayer_Teams_MovePlayerName.add(%name,%id);
			}
		}
		Slayer_Teams_MovePlayerName.setText("Select a Player");
	}
	else
		%this.cancel();
}

function Slayer_Teams::newTeam(%this)
{
	%id = -SlayerClient.Teams.numNewTeams - 2;
	%team = SlayerClient.Teams.addTeam(%id,"New Team");
	Slayer_Teams_Selector.setSelectedByID(%team);
	SlayerClient.Teams.numNewTeams ++;

	Slayer_Teams_Name.setText("<color:ffffff>Team: New Team");

	%this.resetDatablockLists();

	Slayer_Teams_Edit.setVisible(1);
}

function Slayer_Teams::deleteTeam(%this,%team)
{
	if(isObject(%team))
		SlayerClient.Teams.removeTeam(%team);

	%this.cancel();
}

function Slayer_Teams_Selector::onSelect(%this,%team)
{
	if(!isObject(%team))
	{
		Slayer_Teams.cancel();
		return;
	}

	for(%i=0; %i < SlayerClient.TeamPrefs.getCount(); %i++)
	{
		%p = SlayerClient.TeamPrefs.getObject(%i);

		%val = %p.getValue(%team);
		Slayer_Teams.TempValue_[%p.variable] = %val;
	}

	for(%i=0; %i < Slayer_Teams_Edit.getCount(); %i++)
	{
		%obj = Slayer_Teams_Edit.getObject(%i);
		%category = getField(%obj.slyrTeamPref,0);
		%title = getField(%obj.slyrTeamPref,1);
		%p = SlayerClient.TeamPrefs.getPrefSO(%category,%title);

		if(!isObject(%p))
			continue;

		%val = %p.getValue(%team);

		switch$(getWord(%p.type,0))
		{
			case list:
				%obj.setSelected(%val);

			case colorID:
				Slayer_Teams.pickColor(%val);

			default:
				%obj.setValue(%val);
		}
	}

	Slayer_Teams_Name.setText("<color:ffffff>Team:" SPC %team.Team_Name);
	Slayer_Teams_Edit.setVisible(1);
}

function Slayer_Teams_Selector::onDeleteKey(%this,%name)
{
	%team = %this.getSelectedTeam();

	Slayer_Teams.deleteTeam(%team);
}

function Slayer_Teams_Selector::getSelectedTeam(%this)
{
	%team = %this.getSelectedID();

	return %team;
}

function Slayer_Teams_Selector::getSelectedTeamID(%this)
{
	%team = %this.getSelectedID();

	return %team.id;
}

function Slayer_Teams::Apply(%this,%team)
{
	if(!Slayer_Teams_Edit.isVisible())
		return;

	Slayer_Teams_Edit.setVisible(0);
	Slayer_Teams_Selector.clearSelection();
}

function Slayer_Teams::Cancel(%this)
{
	%team = Slayer_Teams_Selector.getSelectedTeam();

	if(isObject(%team))
	{
		for(%i=0; %i < SlayerClient.TeamPrefs.getCount(); %i++)
		{
			%p = SlayerClient.TeamPrefs.getObject(%i);

			%orig = %this.TempValue_[%p.variable];
			%p.setValue(%team,%orig);
			%this.TempValue_[%p.variable] = "";
		}

		Slayer_Teams_Selector.setRowById(%team,%team.Team_Name);
	}

	Slayer_Teams_Edit.setVisible(0);
	Slayer_Teams_Selector.clearSelection();
}

function Slayer_Teams::resetDatablockLists(%this)
{
	for(%i=0; %i < 5; %i++)
	{
		%ctrlA = "Slayer_Teams_Edit_StartEquip" @ %i;
		%ctrlB = "Slayer_General_Player_StartEquip" @ %i;

		%ctrlA.setSelected(%ctrlB.getSelected());
	}

	%ctrlA = Slayer_Teams_Edit_Datablock;
	%ctrlB = Slayer_General_Player_Datablock;

	%ctrlA.setSelected(%ctrlB.getSelected());
}

function Slayer_Teams::createColorMenu(%this,%obj)
{
	if(isObject(Slayer_Teams_ColorMenu))
	{
		Slayer_Teams_ColorMenu.delete();
		return;
	}

	if(isObject(Avatar_ColorMenu))
		Avatar_ColorMenu.delete();

	WrenchEventsDlg::createColorMenu(%this.getObject(0),%obj);

	Avatar_ColorMenu.setName("Slayer_Teams_ColorMenu");

	Slayer_Teams_Edit.add(Slayer_Teams_ColorMenu);

	%ext = Slayer_Teams_ColorMenu.getExtent();
	%w = getWord(%ext,0);
	%h = getWord(%ext,1);

	%objPos = Slayer_Teams_Color.getPosition();
	%objX = getWord(%objPos,0);
	%objY = getWord(%objPos,1);

	%objExt = Slayer_Teams_Color.getExtent();
	%objW = getWord(%objExt,0);
	%objH = getWord(%objExt,1);

	Slayer_Teams_ColorMenu.resize(%objX+%objW,%objY,%w,%h);

	for(%i=0;%i<64;%i++)
	{
		%num = (%i*2)+1;

		if(%num >= Slayer_Teams_ColorMenu.getObject(0).getCount())
			break;

		%o = Slayer_Teams_ColorMenu.getObject(0).getObject(%num);
		%o.command = "Slayer_Teams.pickColor(" @ %i @ ");";
	}
}

function Slayer_Teams::pickColor(%this,%c)
{
	%team = Slayer_Teams_Selector.getSelectedID();
	if(isObject(Slayer_Teams_ColorMenu))
		Slayer_Teams_ColorMenu.delete();

	Slayer_Teams_Color.value = %c;
	Slayer_Teams_Color.setColor(getColorIDTable(%c));

	SlayerClient.TeamPrefs.setPref(Slayer_Teams_Selector.getSelectedTeam(),"Team","Color",Slayer_Teams_Color.value);
}

//ADVANCED TEAM SETTINGS
function Slayer_Teams_Advanced::onWake(%this,%tabSet)
{
	Slayer_Teams_Advanced_bool.setVisible(0);
	Slayer_Teams_Advanced_list.setVisible(0);
	Slayer_Teams_Advanced_string.setVisible(0);
	Slayer_Teams_Advanced_int.setVisible(0);
	Slayer_Teams_Advanced_slide.setVisible(0);
	Slayer_Teams_Advanced_Apply.setVisible(0);

	Slayer_Teams_Advanced_Selector.sort(0);
	Slayer_Teams_Advanced_Selector.clearSelection();

	if(%tabSet)
	{
		%team = %this.editTeam;

		if(isObject(%team))
		{
			%adv = Slayer_Teams_Advanced_Selector;
			for(%i=0; %i < %adv.rowCount(); %i++)
			{
				%text = %adv.getRowTextByID(%i);
				%pref = SlayerClient.TeamPrefs.getPrefSO(getField(%text,0),getField(%text,1));
				%value = %pref.getValue(%team);
	
				switch$(getWord(%pref.type,0))
				{
					case bool:
						if(%value)
							%val = "True";
						else
							%val = "False";

					case list:
						for(%i=0; %i < getWordCount(%pref.type); %i++)
						{
							%w = getWord(%pref.type,%i);
							if(%i % 2 != 0 && %w $= %value)
							{
								%val = getWord(%pref.type,%i+1);
								break;
							}
						}

					default:
						%val = %value;
				}

				%text = setField(%text,2,%val);
				%text = setField(%text,3,%value);
				%adv.setRowByID(%i,%text);
			}

			Slayer_Teams_Advanced_Name.setText("<color:ffffff>Advanced Team Settings:" SPC Slayer_Teams_Edit_Name.getValue());
		}
		else
		{
			Slayer_Main_Tabs.setTab(Slayer_Teams);
			setPopup(Slayer_Main,1,"OK","error","Error","Team not found!");
		}
	}
}

function Slayer_Teams_Advanced_Selector::onSelect(%this,%id)
{
	%entry = Slayer_Teams_Advanced_Selector.getRowTextByID(%id);

	Slayer_Teams_Advanced_bool.setVisible(0);
	Slayer_Teams_Advanced_list.setVisible(0);
	Slayer_Teams_Advanced_string.setVisible(0);
	Slayer_Teams_Advanced_int.setVisible(0);
	Slayer_Teams_Advanced_slide.setVisible(0);

	%canEdit = getField(%entry,6);
	if(!%canEdit && %canEdit != -1 && %canEdit !$= "")
	{
		setPopup("Slayer_Main",0,"OK","exclamation","Access Denied","You don't have permission to edit that setting. Please contact the host for assistance.");
		Slayer_Teams_Advanced_Apply.setVisible(0);
		Slayer_Teams_Advanced_Selector.clearSelection();
		return;
	}

	Slayer_Teams_Advanced_Apply.setVisible(1);

	%control = "Slayer_Teams_Advanced_" @ getWord(getField(%entry,4),0);
	%control.setVisible(1);

	switch$(getWord(getField(%entry,4),0))
	{
		case bool:
			%control.setText(getField(%entry,1));
			%control.setValue(getField(%entry,3));

		case string:
			%control.maxLength = getWord(getField(%entry,4),1);
			%control.setValue(getField(%entry,3));

		case int:
			%control.minValue = getWord(getField(%entry,4),1);
			%control.maxValue = getWord(getField(%entry,4),2);
			%control.setValue(getField(%entry,3));

		case slide:
			%control.range = getWords(getField(%entry,4),1,2);
			%control.ticks = getWord(getField(%entry,4),3);
			%control.snap = getWord(getField(%entry,4),4);
			%control.setValue(getField(%entry,3));

		case list:
			%control.clear();
			%pref = SlayerClient.ServerPrefs.getPrefSO(getField(%entry,0),getField(%entry,1));

			for(%i=1; %i < getFieldCount(%pref.type); %i++)
			{
				%f = getField(%pref.type,%i);
				%control.add(restWords(%f),firstWord(%f));
			}
			%control.setSelected(getField(%entry,3));
	}

}

function Slayer_Teams_Advanced::Apply(%this,%id)
{
	if(%id < 0)
		return;

	%entry = Slayer_Teams_Advanced_Selector.getRowTextByID(%id);

	Slayer_Teams_Advanced_Apply.setVisible(0);
	%type = getWord(getField(%entry,4),0);
	%control = Slayer_Teams_Advanced_ @ %type;
	%control.setVisible(0);

	switch$(%type)
	{
		case list:
			%entry = setField(%entry,3,%control.getSelected());
			%entry = setField(%entry,2,%control.getText());

		case bool:
			%value = %control.getValue();
			%entry = setField(%entry,3,%value);
			if(%value)
				%entry = setField(%entry,2,"True");
			else
				%entry = setField(%entry,2,"False");

		default:
			%entry = setField(%entry,2,%control.getValue());
			%entry = setField(%entry,3,%control.getValue());
	}
	SlayerClient.TeamPrefs.setPref(Slayer_Teams_Selector.getSelectedTeam(),getField(%entry,0),getField(%entry,1),getField(%entry,3));
	Slayer_Teams_Advanced_Selector.setRowByID(%id,%entry);
	Slayer_Teams_Advanced_Selector.clearSelection();
}

//TEAM MEMBER MANAGEMENT
function Slayer_Teams::addMember(%this,%team,%member)
{
	if(isObject(%team))
	{
		if(%team.ID > 0)
		{
			if(%member > 0)
			{
				commandToServer('Teams',"addMember",%member,%team.team_name);
			}
			else
			{
				setPopup(Slayer_Main,0,"OK","exclamation","Hang on a second...","You don't have a player selected.\n\nPlease select a player.");
			}
		}
		else
		{
			setPopup(Slayer_Main,0,"OK","exclamation","Hang on a second...","You can only add members to teams that have already been created.\n\nTo create this team, please update the minigame and then try again.");
		}
	}
	else
	{
		setPopup(Slayer_Main,0,"OK","exclamation","Hang on a second...","You don't have a team selected.\n\nPlease select a team.");
	}
}

function Slayer_Teams::removeMember(%this,%team,%member)
{
	if(isObject(%team))
	{
		if(%team.ID > 0)
		{
			if(%member > 0)
			{
				commandToServer('Teams',"removeMember",%member,%team.team_name);
			}
			else
			{
				setPopup(Slayer_Main,0,"OK","exclamation","Hang on a second...","You don't have a player selected.\n\nPlease select a player.");
			}
		}
		else
		{
			setPopup(Slayer_Main,0,"OK","exclamation","Hang on a second...","You can only remove members from teams that have already been created.");
		}
	}
	else
	{
		setPopup(Slayer_Main,0,"OK","exclamation","Hang on a second...","You don't have a team selected.\n\nPlease select a team.");
	}
}

//TEAM UNIFORMS
function Slayer_Teams::editUniform(%this,%team)
{
	if(!isObject(%team))
		return;

	//save the current avatar
	export("$Pref::Avatar::*",$Slayer::Client::ConfigDir @ "/Temp/avatarBackup.cs");
	//load the team's uniform
	%this.loadUniform(%team);

	AvatarGUI.slyrUniTeam = %team;
	canvas.pushDialog(AvatarGUI);
}

function Slayer_Teams::loadUniform(%this,%team)
{
	if(!isObject(%team))
		return;

	%teamColor = getColorIDTable(SlayerClient.TeamPrefs.getPref(%team,"Team","Color"));

	for(%i=0; %i < SlayerClient.TeamPrefs.getCount(); %i++)
	{
		%p = SlayerClient.TeamPrefs.getObject(%i);
		if(%p.category !$= "Uniform")
			continue;

		%value = %p.getValue(%team);
		if(%value $= "TEAMCOLOR")
			%value = %teamColor;

		$Pref::Avatar["::" @ %p.title] = %value;
	}
}

function Slayer_Teams::saveUniform(%this,%team)
{
	if(!isObject(%team))
		return;

	for(%i=0; %i < SlayerClient.TeamPrefs.getCount(); %i++)
	{
		%p = SlayerClient.TeamPrefs.getObject(%i);
		if(%p.category !$= "Uniform")
			continue;

		%value = $Pref::Avatar["::" @ %p.title];

		%p.setValue(%team,%value);
	}

	Slayer_Teams_Edit_Uniform.setSelected(3);

	//this is just an easy way to tell the server that stuff was updated
	%team.team_uniUpdateTrigger = getRandom(-999999,999999);
}

function Slayer_Teams::resetUniform(%this)
{
	%team = AvatarGUI.slyrUniTeam;
	if(!isObject(%team))
		return;

	%teamColor = getColorIDTable(SlayerClient.TeamPrefs.getPref(%team,"Team","Color"));

	for(%i=0; %i < SlayerClient.TeamPrefs.getCount(); %i++)
	{
		%p = SlayerClient.TeamPrefs.getObject(%i);
		if(%p.category !$= "Uniform")
			continue;

		%value = %p.defaultValue;
		if(%value $= "TEAMCOLOR")
			%value = %teamColor;

		$Pref::Avatar["::" @ %p.title] = %value;
	}

	Avatar_UpdatePreview();
}

// +--------------------+
// | Packaged Functions |
// +--------------------+
package Slayer_Client_GUI_Teams
{
	function AvatarGUI::onWake(%this)
	{
		%team = %this.slyrUniTeam;

		if(isObject(%team))
		{
			if(isPackage(MinigameGUIClient)) //TDM causes problems, deactivate it...
				deActivatePackage(MinigameGUIClient);

			Avatar_Prefix.getGroup().setVisible(0);
			Avatar_Name.getGroup().setVisible(0);

			%this.getObject(0).setText("Slayer | Edit Uniform |" SPC %team.team_name);

			%this.avatarReset = new GuiBitmapButtonCtrl()
			{
				profile = "BlockButtonProfile";
				horizSizing = "right";
				vertSizing = "bottom";
				position = "20 439";
				extent = "151 23";
				minExtent = "8 2";
				visible = "1";
				command = "Slayer_Teams.resetUniform();";
				text = "Reset To Team Default";
				groupNum = "-1";
				buttonType = "PushButton";
				bitmap = "base/client/ui/button1";
				lockAspectRatio = "0";
				alignLeft = "0";
				overflowImage = "0";
				mKeepCached = "0";
				mColor = "255 255 255 255";
			};
			%this.getObject(0).add(%this.avatarReset);
		}

		parent::onWake(%this);
	}

	function AvatarGUI::onSleep(%this)
	{
		parent::onSleep(%this);

		if(isObject(%this.slyrUniTeam))
		{
			Avatar_Prefix.getGroup().setVisible(1);
			Avatar_Name.getGroup().setVisible(1);

			%this.getObject(0).setText("Player Appearance");

			if(isObject(%this.AvatarReset))
				%this.avatarReset.delete();

			%this.slyrUniTeam = "";

			//reload the previous avatar
			exec($Slayer::Client::ConfigDir @ "/Temp/avatarBackup.cs");

			if(isPackage(MinigameGUIClient)) //reactivate TDM...
				activatePackage(MinigameGUIClient);
		}
	}

	function Avatar_Done()
	{
		%team = AvatarGUI.slyrUniTeam;
		if(isObject(%team))
			Slayer_Teams.saveUniform(%team);

		return parent::avatar_done();
	}
};
activatePackage(Slayer_Client_GUI_Teams);

SlayerClient_Support::Debug(2,"GUI Scripts Loaded","Slayer_Teams");