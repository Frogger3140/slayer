// +-------------------------------------------+
// |  ___   _     _____   __  __  ____   ____  |
// | | __| | |   | /_\ |  \ \/ / | ___| | /\ | |
// | |__ | | |_  |  _  |   \  /  | __|  | \/ / |
// | |___| |___| |_| |_|   |__|  |____| |_| \\ |
// |  Greek2me              Blockland ID 11902 |
// +-----------------------------+-------------+
// | DO NOT EDIT BELOW THIS LINE |
// +-----------------------------+

// +---------+
// | General |
// +---------+
function Slayer_General::onWake(%this,%tabSet)
{
	%fName = $Slayer::Client::ServerPrefs::Minigame::Gamemode;
	%mode = SlayerClient.Gamemodes.getModeFromFName(%fName);

	Slayer_General_Mode_Selector.setSelected(%mode);

	//reset the scroll bar to top
	if(!%tabSet)
		Slayer_General_Scroll.scrollToTop();
}

function Slayer_General_Mode_Selector::onSelect(%this,%mode)
{
	if(!isObject(%mode))
		return;

	SlayerClient.Gamemodes.setMode(%mode.fName);

	//RULES
	SlayerClient.ServerPrefs.clearRules("ALL");
	SlayerClient.ServerPrefs.updateRules();

	Slayer_General_Mode_Victory_Disable.setVisible(!%mode.Rounds);
}

SlayerClient_Support::Debug(2,"GUI Scripts Loaded","Slayer_General");