// +-------------------------------------------+
// |  ___   _     _____   __  __  ____   ____  |
// | | __| | |   | /_\ |  \ \/ / | ___| | /\ | |
// | |__ | | |_  |  _  |   \  /  | __|  | \/ / |
// | |___| |___| |_| |_|   |__|  |____| |_| \\ |
// |  Greek2me              Blockland ID 11902 |
// +-----------------------------+-------------+
// | DO NOT EDIT BELOW THIS LINE |
// +-----------------------------+

function Slayer_Update::onWake(%this)
{
	$Slayer::Client::AU::UpdateDisplayed = 1;

	if($Slayer::Client::AU::newVersion $= "") //no new version
	{
		Slayer_Update_newVersion.setText("No new version available.");
		Slayer_Update_DL.setActive(0);
		Slayer_Update_Later.setText("Close");

		if($Slayer::Client::Update)
			%fetchChangeLog = 1;
		else
		{
			Slayer_Update_CL_Text.setText("");
			Slayer_Update_CL_Window_Text.setText("");
		}
	}
	else //new version available!
	{
		Slayer_Update_newVersion.setText("Version" SPC $Slayer::Client::AU::newVersion);
		Slayer_Update_DL.setActive(1);
		Slayer_Update_Later.setText("Later");

		%fetchChangeLog = 1;
	}

	if(%fetchChangeLog)
	{
		if($Slayer::Client::AU::ChangeLog $= "") //download the change log
		{
			Slayer_Update_CL_Text.setText("<just:center>\n\nRetrieving change log...");
			Slayer_Update_CL_Window_Text.setText("<just:center>\n\nRetrieving change log...");
			SlayerClient.CLHTTP.DownloadChangeLog();
		}
		else //change log is already downloaded
		{
			Slayer_Update_CL_Text.setText($Slayer::Client::AU::ChangeLog);
			Slayer_Update_CL_Window_Text.setText($Slayer::Client::AU::ChangeLog);
		}
	}
}

function Slayer_Update::Download(%this)
{
	canvas.popDialog(%this);

	SlayerClient_auDownloadUpdate(0);
}

function Slayer_Update::Later(%this)
{
	canvas.popDialog(%this);
}

function Slayer_Update::pushChangeLog(%this)
{
	Slayer_Update_CL_Window.setVisible(1);
	Slayer_Update_Main.setVisible(0);
	canvas.pushDialog(%this);
}

function Slayer_Update::popChangeLog(%this)
{
	Slayer_Update_CL_Window.setVisible(0);
	Slayer_Update_Main.setVisible(1);
	canvas.popDialog(%this);
}

SlayerClient_Support::Debug(2,"GUI Scripts Loaded","Slayer_Update");