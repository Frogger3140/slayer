//Slayer_Main
exec("./Slayer_Main/Slayer_Main.gui");
exec("./Slayer_Main/scripts.cs");
SlayerClient_Support::LoadFiles($Slayer::Client::Directory @ "/GUI/Slayer_Main/Tabs/Slayer_General/*");
SlayerClient_Support::LoadFiles($Slayer::Client::Directory @ "/GUI/Slayer_Main/Tabs/Slayer_Teams/*");
SlayerClient_Support::LoadFiles($Slayer::Client::Directory @ "/GUI/Slayer_Main/Tabs/Slayer_Advanced/*");
SlayerClient_Support::LoadFiles($Slayer::Client::Directory @ "/GUI/Slayer_Main/Tabs/Slayer_Help/*");

//Slayer_Options
SlayerClient_Support::LoadFiles($Slayer::Client::Directory @ "/GUI/Slayer_Options/*");

//Slayer_Scores
SlayerClient_Support::LoadFiles($Slayer::Client::Directory @ "/GUI/Slayer_Scores/*");

//Slayer_Update
SlayerClient_Support::LoadFiles($Slayer::Client::Directory @ "/GUI/Slayer_Update/*");

//Slayer_Welcome
exec("./Slayer_Welcome/Slayer_Welcome.gui");
exec("./Slayer_Welcome/scripts.cs");

//Other
SlayerClient_Support::LoadFiles($Slayer::Client::Directory @ "/GUI/Other/*");