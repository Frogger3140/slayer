// +-------------------------------------------+
// |  ___   _     _____   __  __  ____   ____  |
// | | __| | |   | /_\ |  \ \/ / | ___| | /\ | |
// | |__ | | |_  |  _  |   \  /  | __|  | \/ / |
// | |___| |___| |_| |_|   |__|  |____| |_| \\ |
// |  Greek2me              Blockland ID 11902 |
// +-----------------------------+-------------+
// | DO NOT EDIT BELOW THIS LINE |
// +-----------------------------+

// +---------+
// | Options |
// +---------+
function Slayer_Options::onWake(%this)
{
	%this.refreshOptions();

	Slayer_Options_bool.setVisible(0);
	Slayer_Options_list.setVisible(0);
	Slayer_Options_string.setVisible(0);
	Slayer_Options_int.setVisible(0);
	Slayer_Options_slide.setVisible(0);
	Slayer_Options_Apply.setVisible(0);

	//UPDATES
	if($Slayer::Client::AU::newVersion $= "")
	{
		Slayer_Options_Updates_Text.setText("<just:center>No new version available.");
		Slayer_Options_Updates_Button.setText("Check");
		Slayer_Options_Updates_Button.command = "SlayerClient.AUHTTP.checkForUpdate();";
	}
	else
	{
		Slayer_Options_Updates_Text.setText("<just:center>New version available:" SPC $Slayer::Client::AU::newVersion);
		Slayer_Options_Updates_Button.setText("Download");
		Slayer_Options_Updates_Button.command = "canvas.pushDialog(Slayer_Update);";
	}
}

function Slayer_Options::RefreshOptions(%this)
{
	Slayer_Options_Selector.clear();

	for(%i=0; %i < SlayerClient.Options.getCount(); %i++)
	{
		%o = SlayerClient.Options.getObject(%i);
		if(%o.list !$= "Options")
			continue;

		%value = SlayerClient.Options.getOption(%o.category,%o.title);

		switch$(%o.type)
		{
			case bool:
				if(%value)
					%val = "True";
				else
					%val = "False";

			case list:
				for(%i=0; %i < getFieldCount(%type); %i++)
				{
					%f = getField(%type,%i);
					if(firstWord(%f) $= %value)
					{
						%val = restWords(%f);
						break;
					}
				}

			default:
				%val = %value;
		}

		%ctrl = Slayer_Options_Selector;
		%ctrl.addRow(%ctrl.rowCount(),%o.category TAB %o.title TAB %val TAB %value TAB %o.type TAB %o.defaultValue);
	}

	Slayer_Options_Selector.sort(0);
}

function Slayer_Options_Selector::onSelect(%this,%id)
{
	%entry = Slayer_Options_Selector.getRowTextByID(%id);

	Slayer_Options_bool.setVisible(0);
	Slayer_Options_list.setVisible(0);
	Slayer_Options_string.setVisible(0);
	Slayer_Options_int.setVisible(0);
	Slayer_Options_slide.setVisible(0);

	Slayer_Options_Apply.setVisible(1);

	%control = "Slayer_Options_" @ getWord(getField(%entry,4),0);
	%control.setVisible(1);

	switch$(getWord(getField(%entry,4),0))
	{
		case bool:
			%control.setText(getField(%entry,1));
			%control.setValue(getField(%entry,3));

		case string:
			%control.maxLength = getWord(getField(%entry,4),1);
			%control.setValue(getField(%entry,3));

		case int:
			%control.minValue = getWord(getField(%entry,4),1);
			%control.maxValue = getWord(getField(%entry,4),2);
			%control.setValue(getField(%entry,3));

		case slide:
			%control.range = getWords(getField(%entry,4),1,2);
			%control.ticks = getWord(getField(%entry,4),3);
			%control.snap = getWord(getField(%entry,4),4);
			%control.setValue(getField(%entry,3));

		case list:
			%control.clear();
			%pref = SlayerClient.getPrefID(getField(%entry,0),getField(%entry,1));

			
			for(%i=1; %i < getFieldCount(%pref.type); %i++)
			{
				%f = getField(%pref.type,%i);
				%control.add(restWords(%f),firstWord(%f));
			}
			%control.setSelected(getField(%entry,3));
	}

}

function Slayer_Options::Apply(%this,%id)
{
	if(%id < 0)
		return;

	%entry = Slayer_Options_Selector.getRowTextByID(%id);

	Slayer_Options_Apply.setVisible(0);
	%type = getWord(getField(%entry,4),0);
	%control = Slayer_Options_ @ %type;
	%control.setVisible(0);

	switch$(%type)
	{
		case list:
			%entry = setField(%entry,3,%control.getSelected());
			%entry = setField(%entry,2,%control.getText());

		case bool:
			%value = %control.getValue();
			%entry = setField(%entry,3,%value);
			if(%value)
				%entry = setField(%entry,2,"True");
			else
				%entry = setField(%entry,2,"False");

		default:
			%entry = setField(%entry,2,%control.getValue());
			%entry = setField(%entry,3,%control.getValue());
	}
	SlayerClient.Options.setOption(getField(%entry,0),getField(%entry,1),getField(%entry,3));
	Slayer_Options_Selector.setRowByID(%id,%entry);
	Slayer_Options_Selector.clearSelection();
}

SlayerClient_Support::Debug(2,"GUI Scripts Loaded","Slayer_Options");