// +-------------------------------------------+
// |  ___   _     _____   __  __  ____   ____  |
// | | __| | |   | /_\ |  \ \/ / | ___| | /\ | |
// | |__ | | |_  |  _  |   \  /  | __|  | \/ / |
// | |___| |___| |_| |_|   |__|  |____| |_| \\ |
// |  Greek2me              Blockland ID 11902 |
// +-----------------------------+-------------+
// | DO NOT EDIT BELOW THIS LINE |
// +-----------------------------+

// +----------------+
// | Slayer_Welcome |
// +----------------+
function Slayer_Welcome::onWake(%this)
{
	%this.loadPanel(0);
}

function Slayer_Welcome::loadPanel(%this,%id)
{
	for(%i = Slayer_Welcome_Content.getCount()-1; %i >= 0; %i--)
		Slayer_Welcome_Content.remove(Slayer_Welcome_Content.getObject(%i));

	%panel = "Slayer_Welcome_Panel" @ %id;
	%file = $Slayer::Client::Directory @ "/GUI/Slayer_Welcome/" @ %panel @ ".gui";

	if(isObject(%panel))
	{
		Slayer_Welcome_Content.add(%panel);
		%loaded = 1;
	}
	else if(isFile(%file))
	{
		exec(%file);
		if(isObject(%panel))
		{
			Slayer_Welcome_Content.add(%panel);
			%loaded = 1;
		}
		else
		{
			SlayerClient_Support::Error("Slayer_Welcome::loadPanel",%panel SPC "not found");
		}
	}
	else
	{
		SlayerClient_Support::Error("Slayer_Welcome::loadPanel",%panel SPC "not found");
	}

	if(%loaded)
	{
		%this.currentPanel = %id;

		Slayer_Welcome_Back.setActive(%this.isPanel(%id - 1));
		Slayer_Welcome_Next.setText(%this.isPanel(%id + 1) ? "Next" : "Close");
	}
}

function Slayer_Welcome::nextPanel(%this)
{
	%next = %this.currentPanel + 1;

	if(%this.isPanel(%next))
		%this.loadPanel(%next);
	else
		canvas.popDialog(%this);
}

function Slayer_Welcome::previousPanel(%this)
{
	%next = %this.currentPanel - 1;

	if(%this.isPanel(%next))
		%this.loadPanel(%next);
}

function Slayer_Welcome::isPanel(%this,%id)
{
	%panel = "Slayer_Welcome_Panel" @ %id;
	%file = $Slayer::Client::Directory @ "/GUI/Slayer_Welcome/" @ %panel @ ".gui";

	if(isObject(%panel) || isFile(%file))
		return 1;
	else
		return 0;
}

function Slayer_Welcome::checkFirstRun(%this)
{
	%firstRun = 0; //$Slayer::Client::FirstRun;
	%updated = SlayerClient_Support::getNewerVersion($Slayer::Client::LastVersion,$Slayer::Client::Version) $= $Slayer::Client::Version;
	%newInstaller = $Slayer::Client::LastInstallerVersion $= $Slayer::Client::Version || SlayerClient_Support::getNewerVersion($Slayer::Client::LastVersion,"3.5") $= "3.5";

	if(%firstRun || (%updated && %newInstaller))
	{
		if(!%this.isAwake())
			canvas.pushDialog(%this);
	}
}

Slayer_Welcome.checkFirstRun();