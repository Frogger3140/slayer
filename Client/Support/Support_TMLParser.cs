// +--------------------------------------+
// | Custom Torque Markup Language Parser |
// |                                      |
// | Author: Greek2me (11902)             |
// +--------------------------------------+
// | Allows for parsing of custom TML     |
// | formatting.                          |
// +--------------------------------------+
// | USAGE:                               |
// | -Create a function called            |
// |  "customTMLParser_IDENTIFIER", where |
// |  IDENTIFIER is a unique name for     |
// |  your parser. See the function,      |
// |  "customTMLParser_default", for an   |
// |  example.                            |
// |                                      |
// | -Parse your text like this:          |
// |  parseCustomTML(%str,%obj,%identi);  |
// | -%str is the string, %obj is the     |
// |  object, %identi is the identifier.  |
// | -%obj is not required but is HIGHLY  |
// |  recommended.                        |
// |                                      |
// | -You may use multiple identifiers by |
// |  separating them by tabs. The string |
// |  will be parsed according to all of  |
// |  them.                               |
// +--------------------------------------+

if($customTMLParser::version >= 1)
	return;
$customTMLParser::version = 1;

$customTMLParser::leftBracket = "<";
$customTMLParser::rightBracket = ">";
$customTMLParser::div = ":";

function customTMLParser_default(%obj,%value0,%value1,%value2,%value3,%value4,%value5,%value6,%value7,%value8,%value9,%value10,%value11,%value12,%value13,%value14,%value15)
{
	switch$(%value[0])
	{
		case "sPush":
			%obj.TML_oldFontTypes = setField(%obj.TML_oldFontTypes,getFieldCount(%obj.TML_oldFontTypes),%obj.TML_fontType);
			%obj.TML_oldFontSizes = setField(%obj.TML_oldFontSizes,getFieldCount(%obj.TML_oldFontSizes),%obj.TML_fontSize);

		case "sPop":
			%obj.TML_fontType = getField(%obj.TML_oldFontTypes,getFieldCount(%obj.TML_oldFontTypes) - 1);
			%obj.TML_fontSize = getField(%obj.TML_oldFontSizes,getFieldCount(%obj.TML_oldFontSizes) - 1);

			%obj.TML_oldFontTypes = removeField(%obj.TML_oldFontTypes,getFieldCount(%obj.TML_oldFontTypes) - 1);
			%obj.TML_oldFontSizes = removeField(%obj.TML_oldFontSizes,getFieldCount(%obj.TML_oldFontSizes) - 1);

		case "b":
			if(isObject(%obj) && striPos(%obj.TML_fontType,"bold") < 0)
				%replace = "<sPush><font:" @ %obj.TML_fontType SPC "bold:" @ %obj.TML_fontSize @ ">";
			else
				%replace = "<sPush><font:arial bold:15>";

		case "/b":
			%replace = "<sPop>";

		case "i":
			if(isObject(%obj) && striPos(%obj.TML_fontType,"italic") < 0)
				%replace = "<sPush><font:" @ %obj.TML_fontType SPC "italic:" @ %obj.TML_fontSize @ ">";
			else
				%replace = "<sPush><font:arial italic:15>";

		case "/i":
			%replace = "<sPop>";

		case "font":
			%obj.TML_fontType = %value[1];
			%obj.TML_fontSize = %value[2];

		case "colorHex":
			%replace = "<sPush><color:" @ %value[1] @ ">";

		case "/color":
			%replace = "<sPop>";

		case "/just":
			%replace = "<just:left>";

		case "h1":
			%replace = "<sPush><font:arial bold:24>";

		case "/h1":
			%replace = "<sPop><br>";

		case "h2":
			%replace = "<sPush><font:arial bold:20>";

		case "/h2":
			%replace = "<sPop><br>";

		case "h3":
			%replace = "<sPush><font:arial bold:17>";

		case "/h3":
			%replace = "<sPop><br>";

		case "bullet":
			switch$(%value[1])
			{
				case "bug":
					%replace = "<bitmap:" @ $Slayer::Client::Directory @ "/Images/Bullets/bullet_bug.png>\t";
					
				case "add":
					%replace = "<bitmap:" @ $Slayer::Client::Directory @ "/Images/Bullets/bullet_add.png>\t";

				case "remove":
					%replace = "<bitmap:" @ $Slayer::Client::Directory @ "/Images/Bullets/bullet_delete.png>\t";

				case "gamemode":
					%replace = "<bitmap:" @ $Slayer::Client::Directory @ "/Images/Bullets/bullet_controller.png>\t";

				case "pref":
					%replace = "<bitmap:" @ $Slayer::Client::Directory @ "/Images/Bullets/bullet_cog.png>\t";

				case "module":
					%replace = "<bitmap:" @ $Slayer::Client::Directory @ "/Images/Bullets/bullet_plugin.png>\t";

				case "gui":
					%replace = "<bitmap:" @ $Slayer::Client::Directory @ "/Images/Bullets/bullet_application.png>\t";

				default:
					%replace = "<bitmap:" @ $Slayer::Client::Directory @ "/Images/Bullets/bullet_black.png>\t";
			}

		case "/bullet":
			%replace = "<br><br>";

		case "*":
			%replace = "<bullet>";

		case "/*":
			%replace = "</bullet>";
	}

	return %replace;
}

function parseCustomTML(%string,%obj,%parserFunction)
{
	if(%parserFunction $= "")
		%parserFunction = "default";

	if(isObject(%obj) && %obj.getClassName() $= "GuiMLTextCtrl")
	{
		%text = %obj.getText();
		if(%text $= "" || %text $= %obj.text)
		{
			%obj.TML_fontType = %obj.profile.fontType;
			%obj.TML_fontSize = %obj.profile.fontSize;

			%obj.TML_oldFontTypes = "";
			%obj.TML_oldFontSizes = "";
		}
	}

	for(%i=0; %i < strLen(%string); %i++)
	{
		%char = getSubStr(%string,%i,1);

		if(%char $= $customTMLParser::leftBracket)
			%start = %i;

		if(%char $= $customTMLParser::rightBracket && %start !$= "")
		{
			%end = %i;

			%full = getSubStr(%string,%start,%end-%start+1);
			%contents = getSubStr(%full,1,strLen(%full) - 2);

			%search = %contents;
			%pos = -1;
			%numValues = 0;
			while(strPos(%search,$customTMLParser::div) >= 0)
			{
				%search = getSubStr(%search,%pos+1,strLen(%search));

				%pos = strPos(%search,$customTMLParser::div);
				if(%pos >= 0)
				{
					%value[%numValues] = getSubStr(%search,0,%pos);
				}
				else
				{
					%value[%numValues] = %search;
				}
				%numValues ++;
			}

			if(%numValues <= 0)
			{
				if(%contents !$= "")
				{
					%value[0] = %contents;
					%numValues = 1;
				}
			}

			for(%e = 0; %e < getFieldCount(%parserFunction); %e ++)
			{
				%replace = call("customTMLParser_" @ getField(%parserFunction,%e),%obj,%value0,%value1,%value2,%value3,%value4,%value5,%value6,%value7,%value8,%value9,%value10,%value11,%value12,%value13,%value14,%value15);
				if(%replace !$= "")
				{
					%string = strReplace(%string,%full,%replace);
					%replaced = 1;
				}
			}
			if(%replaced)
				%i = %start-1;

			%start = "";
			%end = "";
			%full = "";
			%contents = "";
			%search = "";
			%replace = "";
			%replaced = "";
			for(%e=0; %e < %numValues; %e++)
				%value[%e] = "";
			%numValues = "";
		}
	}

	return %string;
}