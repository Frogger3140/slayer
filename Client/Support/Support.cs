// +-------------------------------------------+
// |  ___   _     _____   __  __  ____   ____  |
// | | __| | |   | /_\ |  \ \/ / | ___| | /\ | |
// | |__ | | |_  |  _  |   \  /  | __|  | \/ / |
// | |___| |___| |_| |_|   |__|  |____| |_| \\ |
// |  Greek2me              Blockland ID 11902 |
// +-----------------------------+-------------+
// | DO NOT EDIT BELOW THIS LINE |
// +-----------------------------+

$Slayer::Client::Support::Main = 1;

// +-------------------------+
// | Stuff for finding stuff |
// +-------------------------+
function SlayerClient_Support::getIDFromUiName(%uiName,%className)
{
	if(serverConnection.isLocal())
		%group = datablockGroup;
	else
		%group = serverConnection;

	%count = %group.getCount();
	for(%i=0; %i < %count; %i++)
	{
		%obj = %group.getObject(%i);
		%cn = %obj.getClassName();
		%ui = %obj.uiName;
		if(%ui $= "")
			continue;
		if(%cn $= "MissionArea")
			break;
		if(%ui $= %uiName)
		{
			if(%className $= "" || %cn $= %className)
			{
				return %obj;
			}
		}
	}
	return 0;
}

function SlayerClient_Support::getNewerVersion(%version1,%version2)
{
	%num1 = strReplace(firstWord(%version1),"."," ");
	%num2 = strReplace(firstWord(%version2),"."," ");

	%count = getMax(getWordCount(%num1),getWordCount(%num2));
	for(%i = 0; %i < %count; %i ++)
	{
		%n1 = getWord(%num1,%i);
		%n2 = getWord(%num2,%i);

		if(%n1 > %n2)
			return %version1;
		else if(%n2 > %n1)
			return %version2;
	}

	%tags = "\tAlpha\tBeta\tRC\t\t";
	%tag1 = striPos(%tags,"\t" @ restWords(%version1) @ "\t");
	%tag2 = striPos(%tags,"\t" @ restWords(%version2) @ "\t");

	if(%tag1 > %tag2)
		return %version1;
	else if(%tag2 > %tag1)
		return %version2;

	return -1;
}

function SlayerClient_Support::isNewerVersion(%version1,%version2) //check if %version1 is newer than %version2
{
	return SlayerClient_Support::getNewerVersion(%version1,%version2) $= %version1;
}

// +--------+
// | SimSet |
// +--------+
function SimSet::getGroupID(%this)
{
	%parent = %this.getGroup();
	if(!isObject(%parent))
		return -1;

	for(%i=0; %i < %parent.getCount(); %i++)
	{
		%obj = %parent.getObject(%i);
		if(%obj == %this)
			return %i;
	}

	return -1;
}

// +-----+
// | GUI |
// +-----+
function SlayerClient_Support::addKeyBind(%div,%name,%cmd,%device,%action,%overWrite)
{
	%divIndex = -1;
	for(%i = 0; %i < $RemapCount; %i ++)
	{
		if($RemapDivision[%i] $= %div)
		{
			for(%e = %i; %e < $RemapCount; %e ++)
			{
				if($RemapDivision[%e] !$= $RemapDivision[%i] && $RemapDivision[%e] !$= "")
				{
					break;
				}

				if($RemapName[%e] $= %name)
				{
					return;
				}
			}
			%divIndex = %i;
			break;
		}
	}

	if(%divIndex >= 0)
	{
		for(%i=$RemapCount-1; %i > %divIndex; %i--)
		{
			$RemapDivision[%i+1] = $RemapDivision[%i];
			$RemapName[%i+1] = $RemapName[%i];
			$RemapCmd[%i+1] = $RemapCmd[%i];
		}

		$RemapDivision[%divIndex+1] = "";
		$RemapName[%divIndex+1] = %name;
		$RemapCmd[%divIndex+1] = %cmd;
		$RemapCount ++;
	}
	else
	{
		$RemapDivision[$RemapCount] = %div;
		$RemapName[$RemapCount] = %name;
		$RemapCmd[$RemapCount] = %cmd;
		$RemapCount ++;
	}

	if(%device !$= "" && %action !$= "")
	{
		if(moveMap.getCommand(%device,%action) $= "" || %overWrite)
			moveMap.bind(%device,%action,%cmd);
	}
}

function SlayerClient_Support::removeKeyBind(%div,%name,%cmd)
{
	%binding = moveMap.getBinding(%cmd);
	%device = getField(%binding,0);
	%action = getField(%binding,1);

	if(%device !$= "" && %action !$= "")
		moveMap.unBind(%device,%action);

	for(%i=0; %i < $RemapCount; %i++)
	{
		%d = $RemapDivision[%i];
		%n = $RemapName[%i];
		%c = $RemapCmd[%i];

		%start = 0;

		if(%n $= %name && %c $= %cmd && (%d $= %div || %lastDiv $= %div))
		{
			%start = %i + 1;
			break;
		}

		if(%d !$= "")
			%lastDiv = %d;
	}

	if(%start > 0)
	{
		for(%i=%start; %i < $RemapCount; %i++)
		{
			$RemapDivision[%i-1] = $RemapDivision[%i];
			$RemapName[%i-1] = $RemapName[%i];
			$RemapCmd[%i-1] = $RemapCmd[%i];
		}

		%d = $RemapDivision[%start-1];
		if(%d $= "")
			$RemapDivision[%start-1] = %div;

		$RemapDivision[$RemapCount-1] = "";
		$RemapName[$RemapCount-1] = "";
		$RemapCmd[$RemapCount-1] = "";

		$RemapCount --;
	}
}

// +-------+
// | Debug |
// +-------+
function SlayerClient_Support::Debug(%level,%title,%value)
{
	if(%value $= "")
		%val = %title;
	else
		%val = %title @ ":" SPC %value; 

	if(%level <= $Slayer::Client::Debug)
		echo("\c4Slayer (Client):" SPC %val);

	%path = $Slayer::Client::ConfigDir @ "/debug_client.log";
	if(isWriteableFileName(%path))
	{
		if(!isObject(SlayerClient.DebugFO))
		{
			SlayerClient.debugFO = new fileObject();
			SlayerClient.add(SlayerClient.debugFO);

			SlayerClient.debugFO.openForWrite(%path);
			SlayerClient.debugFO.writeLine("//Slayer Version" SPC $Slayer::Client::Version SPC "-----" SPC getDateTime());
			SlayerClient.debugFO.writeLine("//By Greek2me, Blockland ID 11902");
		}

		SlayerClient.debugFO.writeLine(%val);
	}
}

function SlayerClient_Support::Error(%title,%value)
{
	if(%value $= "")
		%val = %title;
	else
		%val = %title @ ":" SPC %value; 

	error("Slayer Error (Client):" SPC %val);

	%path = $Slayer::Client::ConfigDir @ "/debug_client.log";
	if(isWriteableFileName(%path))
	{
		if(!isObject(SlayerClient.debugFO))
		{
			SlayerClient.debugFO = new fileObject();
			SlayerClient.add(SlayerClient.debugFO);

			SlayerClient.debugFO.openForWrite(%path);
			SlayerClient.debugFO.writeLine("//Slayer Version" SPC $Slayer::Client::Version SPC "-----" SPC getDateTime());
			SlayerClient.debugFO.writeLine("//By Greek2me, Blockland ID 11902");
		}

		SlayerClient.debugFO.writeLine("ERROR:" SPC %val);
	}
}

function SlayerClient_Support::Benchmark(%times,%function,%a,%b,%c,%d,%e,%f,%g,%h,%i,%j,%k,%l,%m,%n,%o)
{
	if(!isFunction(%function))
	{
		warn("BENCHMARKING RESULT FOR" SPC %function @ ":" NL "Function does not exist.");
		return -1;
	}

	%start = getRealTime();

	for(%i=0; %i < %times; %i++)
	{
		call(%function,%a,%b,%c,%d,%e,%f,%g,%h,%i,%j,%k,%l,%m,%n,%o);
	}

	%end = getRealTime();

	%result = (%end-%start) / %times;

	warn("BENCHMARKING RESULT FOR" SPC %function @ ":" NL %result);

	return %result;
}

// +--------------+
// | File Loading |
// +--------------+
function SlayerClient_Support::LoadFiles(%path,%ignoreFile) //load all files in the specified path
{
	if(%ignoreFile !$= "")
		%ignorePath = filePath(%path) @ "/" @ %ignoreFile;
	%count = getFileCount(%path);
	for(%i=0; %i < %count; %i++)
	{
		%f = findNextFile(%path);
		if(%f $= %ignorePath)
			continue;
		if(isFile(%f))
			exec(%f);
	}
}

// +--------+
// | Colors |
// +--------+
function SlayerClient_Support::RgbToHex(%rgb)
{
	//use % to find remainder
	%r = getWord(%rgb,0);
	if(%r <= 1)
		%r = %r * 255;
	%g = getWord(%rgb,1);
	if(%g <= 1)
		%g = %g * 255;
	%b = getWord(%rgb,2);
	if(%b <= 1)
		%b = %b * 255;
	%a = "0123456789ABCDEF";

	%r = getSubStr(%a,(%r-(%r % 16))/16,1) @ getSubStr(%a,(%r % 16),1);
	%g = getSubStr(%a,(%g-(%g % 16))/16,1) @ getSubStr(%a,(%g % 16),1);
	%b = getSubStr(%a,(%b-(%b % 16))/16,1) @ getSubStr(%a,(%b % 16),1);

	return %r @ %g @ %b;
}

function SlayerClient_Support::getClosestPaintColor(%rgba)
{
	%prevdist = 100000;
	%colorMatch = 0;
	for(%i = 0; %i < 64; %i++)
	{
		%color = getColorIDTable(%i);
		if(vectorDist(%rgba,getWords(%color,0,2)) < %prevdist && getWord(%rgba,3) - getWord(%color,3) < 0.3 && getWord(%rgba,3) - getWord(%color,3) > -0.3)
		{
			%prevdist = vectorDist(%rgba,%color);
			%colormatch = %i;
		}
	}
	return %colormatch;
}

// +------------+
// | Math Stuff |
// +------------+
function SlayerClient_Support::isNumber(%flag)
{
	if(%flag $= "")
		return 0;

	%check = "-.0123456789";

	%lastPos = -1;
	%length = strLen(%flag);
	for(%i=0; %i < %length; %i++)
	{
		%char = getSubStr(%flag,%i,1);

		%pos = strPos(%check,%char);
		if(%pos < 0)
			return 0;
		if(%pos == 0)
		{
			if(%neg)
				return 0;
			if(%i != 0)
				return 0;
			if(%length == 1)
				return 0;
			%neg = 1;
		}
		if(%pos == 1)
		{
			if(%dec)
				return 0;
			if(%i == %length - 1)
				return 0;
			%dec = 1;
		}

		%lastPos = %pos;
	}

	return 1;
}

function SlayerClient_Support::stripTrailingZeros(%flag)
{
	if(!SlayerClient_Support::isNumber(%flag))
		return %flag;

	%pos = -1;
	for(%i=0; %i < strLen(%flag); %i++)
	{
		%char = getSubStr(%flag,%i,1);

		if(%char $= ".")
		{
			%dec = 1;
			%pos = %i;
			%zero = 1;
			continue;
		}
		if(%char $= "0" && %dec)
		{
			if(!%zero)
				%pos = %i;
			%zero = 1;
		}
		else
		{
			%zero = 0;
			%pos = -1;
		}
	}

	if(%pos > 0)
		%flag = getSubStr(%flag,0,%pos);

	return %flag;
}

function SlayerClient_Support::mRestrict(%val,%min,%max)
{
	%stripChars = "~`!@#$%^&*()_+=qwertyuiop{[}]|\asdfghjkl;:\"\'zxcvbnm<,>?/ ";
	%val = stripChars(%val,%stripChars);

	if(%val < %min)
		%val = %min;
	if(%val > %max)
		%val = %max;

	return %val;
}

// +-------+
// | Other |
// +-------+
function SlayerClient_Support::setDynamicVariable(%var,%val)
{
	eval(%var SPC "= \"" @ %val @ "\";");
	return %val;
}

function SlayerClient_Support::getDynamicVariable(%var)
{
	%val = eval("return" SPC %var @ ";");
	return %val;
}