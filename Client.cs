// +-------------------------------------------+
// |  ___   _     _____   __  __  ____   ____  |
// | | __| | |   | /_\ |  \ \/ / | ___| | /\ | |
// | |__ | | |_  |  _  |   \  /  | __|  | \/ / |
// | |___| |___| |_| |_|   |__|  |____| |_| \\ |
// |  Greek2me              Blockland ID 11902 |
// +-------------------------------------------+

$Slayer::Client::Debug = 0;

// +-----------------------------+
// | DO NOT EDIT BELOW THIS LINE |
// +-----------------------------+

if(isObject(SlayerClient))
	SlayerClient.delete();

$Slayer::Client::Version = "3.7.3";
$Slayer::Client::CompatibleVersion = "3.6";

$Slayer::Client::ParentDir = filePath(expandFileName("./Client.cs"));
$Slayer::Client::Directory = $Slayer::Client::ParentDir @ "/Client";
$Slayer::Client::ConfigDir = "config/client/Slayer";

$Slayer::Client::FirstRun = !isFile($Slayer::Client::ConfigDir @ "/config_last.cs");

// +------+
// | Core |
// +------+
exec($Slayer::Client::Directory @ "/Main.cs"); //load main scripts

// +----------+
// | KeyBinds |
// +----------+
//remove the old keybind from pre v3.0
SlayerClient_Support::removeKeyBind("Slayer","Setup","SlayerClient_pushSetup");

//add keybinds
SlayerClient_Support::addKeyBind("Slayer","Edit Minigame","SlayerClient_pushMain");
SlayerClient_Support::addKeyBind("Slayer","Options","SlayerClient_pushOptions");

// +-----------------+
// | Blockland Glass |
// +-----------------+
if(isObject(BLG_DT))
{
	BLG_DT.registerImageIcon("Slayer Options","canvas.pushDialog(Slayer_Options);",$Slayer::Client::Directory @ "/Images/logo_icon.png");
}

// +--------------+
// | Fully Loaded |
// +--------------+
warn("Slayer Version" SPC $Slayer::Client::Version);