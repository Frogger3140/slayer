// +-------------------------------------------+
// |  ___   _     _____   __  __  ____   ____  |
// | | __| | |   | /_\ |  \ \/ / | ___| | /\ | |
// | |__ | | |_  |  _  |   \  /  | __|  | \/ / |
// | |___| |___| |_| |_|   |__|  |____| |_| \\ |
// |  Greek2me              Blockland ID 11902 |
// +-----------------------------+-------------+
// | DO NOT EDIT BELOW THIS LINE |
// +-----------------------------+

$Slayer::Server::Dependencies::Brick = 1;

if(!isObject(Slayer.Spawns))
{
	if(!isObject(Slayer))
		exec("Add-ons/Gamemode_Slayer/Main.cs");

	Slayer.Spawns = new simSet();
	Slayer.add(Slayer.Spawns);
}

if(!isObject(Slayer.capturePoints))
{
	if(!isObject(Slayer))
		exec("Add-ons/Gamemode_Slayer/Main.cs");

	Slayer.capturePoints = new simSet();
	Slayer.add(Slayer.capturePoints);
}

//Restricted Events
//KEY: -1=anyone with edit rights; 0=host; 1=super admin; 2=admin; 3=creator; 4=full trust; 5=build trust
$Slayer::Server::Events::RestrictedEvent__["Minigame","BottomPrintAll"] = -1;
$Slayer::Server::Events::RestrictedEvent__["Minigame","CenterPrintAll"] = -1;
$Slayer::Server::Events::RestrictedEvent__["Minigame","ChatMsgAll"] = -1;
$Slayer::Server::Events::RestrictedEvent__["Minigame","incTime"] = -1;
$Slayer::Server::Events::RestrictedEvent__["Minigame","Reset"] = -1;
$Slayer::Server::Events::RestrictedEvent__["Minigame","RespawnAll"] = -1;
$Slayer::Server::Events::RestrictedEvent__["Minigame","setTime"] = -1;
$Slayer::Server::Events::RestrictedEvent__["Minigame","Slayer_addTimeRemaining"] = -1;
$Slayer::Server::Events::RestrictedEvent__["Minigame","Slayer_setTimeRemaining"] = -1;
$Slayer::Server::Events::RestrictedEvent__["Minigame","Slayer_setPref"] = 3;
$Slayer::Server::Events::RestrictedEvent__["Minigame","Win"] = -1;
$Slayer::Server::Events::RestrictedEvent__["Slayer_TeamSO","BottomPrintAll"] = -1;
$Slayer::Server::Events::RestrictedEvent__["Slayer_TeamSO","CenterPrintAll"] = -1;
$Slayer::Server::Events::RestrictedEvent__["Slayer_TeamSO","ChatMsgAll"] = -1;
$Slayer::Server::Events::RestrictedEvent__["Slayer_TeamSO","RespawnAll"] = -1;
$Slayer::Server::Events::RestrictedEvent__["Slayer_TeamSO","IncScore"] = -1;

// +------------+
// | fxDtsBrick |
// +------------+
function fxDtsBrick::setSlayerBrick(%this,%displayPlantMsg)
{
	if(!%this.isPlanted)
		return;

	%db = %this.getDatablock();

	switch$(%db.specialBrickType)
	{
		case "SpawnPoint":
			Slayer.Spawns.add(%this);

		case "vehicleSpawn":
			vehicleSpawnGroup.add(%this);
	}

	if(%db.isSlyrBrick)
	{
		%type = %db.slyrType;
		%color = %this.getColorID();

		%client = %this.getGroup().client;

		%miniA = getMinigameFromObject(%client);
		%miniB = getMinigameFromObject(%this);

		if(isObject(%miniA) && !isObject(%miniB))
			%mini = %miniA;
		else if(!isObject(%miniA) && isObject(%miniB))
			%mini = %miniB;
		else if(%miniA == %miniB)
			%mini = %miniA;

		switch$(%type)
		{
			case TeamSpawn:
				if(isSlayerMinigame(%mini))
				{
					%teams = %mini.Teams.getTeamsFromColor(%color,"COM",1);
				}

			case TeamVehicle:
				if(isSlayerMinigame(%mini))
				{
					%teams = %mini.Teams.getTeamsFromColor(%color,"COM",1);
				}

			case CP:
				if(isObject(%this.trigger))
					%this.trigger.delete();

				Slayer.capturePoints.add(%this);

				%this.createTrigger(Slayer_CPTriggerData,"0 0 0 1 0 0 0 -1 0 0 0 1");
				%this.origColor = %color;
				%this.controlColor = %color;

				if(isSlayerMinigame(%mini))
				{
					%teams = %mini.Teams.getTeamsFromColor(%color,"COM",1);
				}

			default:
				%count = Slayer.Minigames.getCount();
				if(%count > 0)
				{
					for(%i=0; %i < %count; %i++)
					{
						%m = Slayer.Minigames.getObject(%i);

						for(%e=0; %e < Slayer.Gamemodes.getCount(); %e++)
						{
							%mode = Slayer.Gamemodes.getObject(%e);

							if(isFunction("Slayer_" @ %mode.fName @ "_onSlyrBrickAdd"))
								%special = call("Slayer_" @ %mode.fName @ "_onSlyrBrickAdd",%m,%this,%type);
							if(%special !$= "" && %special !$= 0)
								%msg = %special;
						}
					}
				}
				else
				{
					for(%i = 0; %i  < Slayer.Gamemodes.getCount(); %i++)
					{
						%mode = Slayer.Gamemodes.getObject(%i);

						if(isFunction("Slayer_" @ %mode.fName @ "_onSlyrBrickAdd"))
							%special = call("Slayer_" @ %mode.fName @ "_onSlyrBrickAdd",0,%this,%type);
						if(%special !$= "" && %special !$= 0)
							%msg = %special;
					}
				}
		}

		if(%displayPlantMsg && isObject(%client))
		{
			%uiName = %db.uiName;
			%teams = (%teams $= "" ? "." : "<color:ff00ff> for" SPC %teams @ ".");

			if(%msg $= "")
				%msg = buildTaggedString('<just:center>\c5%1 set%2',%uiName,%teams);

			%client.bottomPrint(%msg,4);
		}
	}
}

function fxDtsBrick::setControl(%this,%color,%client)
{
	if(isObject(%client))
	{
		%mini = getMinigameFromObject(%client);
		if(!isSlayerMinigame(%mini))
			return;
		if(!minigameCanUse(%client,%this))
			return;
	}

	%db = %this.getDatablock();
	if(!%db.isSlyrBrick)
		return;

	switch$(%db.slyrType)
	{
		case TeamSpawn:
			%this.controlColor = %color;

		case TeamVehicle:
			%this.controlColor = %color;

		case CP:
			%this.setCPControl(%color,0,%client);
	}
}

function fxDtsBrick::getControl(%this)
{
	if(%this.controlColor $= "")
		return %this.getColorID();
	else
		return %this.controlColor;
}

function fxDtsBrick::getControlTeams(%this)
{
	%mini = getMinigameFromObject(%this);
	if(isObject(%mini.Teams))
		return %mini.Teams.getTeamsFromColor(%this.getControl(),"TAB");
}

function fxDtsBrick::setControlLocked(%this,%mode,%color,%flag,%client)
{
	if(isObject(%client))
	{
		if(!minigameCanUse(%client,%this))
			return;
	}

	%brColor = %this.getControl();

	switch(%mode)
	{
		case 0: //TriggerTeam
			if(isObject(%client))
			{
				%team = %client.getTeam();
				if(isObject(%team))
				{
					if(%this.isLocked[%team.color] != %flag)
					{
						%this.isLocked[%team.color] = %flag;
						if(%team.color != %brColor)
							%this.capture[%team.color] = 0;
					}
				}
			}

		case 1: //TeamColor
			if(%color !$= "")
			{
				if(%this.isLocked[%color] != %flag)
				{
					%this.isLocked[%color] = %flag;
					if(%color != %brColor)
						%this.capture[%color] = 0;
				}
			}

		case 2: //ALL
			for(%i=0; %i < 64; %i++)
			{
				if(%this.isLocked[%i] !=  %flag)
				{
					%this.isLocked[%i] = %flag;
					if(%i != %brColor)
						%this.capture[%i] = 0;
				}
			}
	}
}

//returns a comma separated list of teams controlling the brick - for VCE vars
function fxDtsBrick::getControllingTeam(%this,%returnColor) 
{
	%mini = getMinigameFromObject(%this);
	if(!isSlayerMinigame(%mini))
		return;

	%color = %this.getControl();
	%teams = %mini.Teams.getTeamsFromColor(%color,"COM",%returnColor);

	return (%teams $= "" ? "None" : %teams);
}

// +----------------+
// | Capture Points |
// +----------------+
function fxDtsBrick::createTrigger(%this,%data,%polyhedron)
{
	//credits to Space Guy for showing how to create triggers
	if(!isObject(%data))
	{
		Slayer_Support::Error("fxDtsBrick::createTrigger","Trigger datablock not found.");
		return 0;
	}

	if(%polyhedron $= "")
		%polyhedron = "0 0 0 1 0 0 0 -1 0 0 0 1";

	if(isObject(%this.trigger))
		%this.trigger.delete();

	%t = new Trigger()
	{
		datablock = %data;
		polyhedron = %polyhedron;
	};
	missionCleanup.add(%t);
	
	%boxMax = getWords(%this.getWorldBox(), 3, 5);
	%boxMin = getWords(%this.getWorldBox(), 0, 2);
	%boxDiff = vectorSub(%boxMax,%boxMin);
	%boxDiff = vectorAdd(%boxDiff,"0 0 0.2"); 
	%t.setScale(%boxDiff);
	%posA = %this.getWorldBoxCenter();
	%posB = %t.getWorldBoxCenter();
	%posDiff = vectorSub(%posA, %posB);
	%posDiff = vectorAdd(%posDiff, "0 0 0.1");
	%t.setTransform(%posDiff);

	%this.trigger = %t;
	%t.brick = %this;

	return %t;
}

function fxDtsBrick::setCPControl(%this,%color,%reset,%client)
{
	if(isObject(%client))
	{
		%mini = getMinigameFromObject(%client);
		if(!isSlayerMinigame(%mini))
			return;
		if(!minigameCanUse(%client,%this))
			return;

		if(!%reset)
			%client.incScore(%mini.points_cp);
	}
	else
	{
		%mini = getMinigameFromObject(%this);
		if(!isSlayerMinigame(%mini))
			return;
		%client = %mini.getFakeClient();
	}

	%oldColor = %this.getControl();
	%this.setColor(%color);
	%this.controlColor = %color;

	for(%i=0; %i < 64; %i++)
	{
		if(%i != %color)
			%this.capture[%i] = "";
	}

	//onCapture Input Event
	$InputTarget_["Self"] = %this;
	$InputTarget_["Player"] = %client.player;
	$InputTarget_["Client"] = %client;
	$InputTarget_["MiniGame"] = %mini;
	if(%reset)
	{
		if(isFunction("Slayer_" @ %mini.mode @ "_onCPReset"))
			call("Slayer_" @ %mini.mode @ "_onCPReset",%mini,%this,%color,%oldColor,%client);
		%this.processInputEvent("onCPReset",%client);
	}
	else
	{
		if(isFunction("Slayer_" @ %mini.mode @ "_onCPCapture"))
			call("Slayer_" @ %mini.mode @ "_onCPCapture",%mini,%this,%color,%oldColor,%client);
		%this.processInputEvent("onCPCapture",%client);

		for(%i=0; %i < %mini.Teams.getCount(); %i++)
		{
			%t = %mini.Teams.getObject(%i);
			if(%t.color == %color)
				%this.processInputEvent("onCPCapture(Team" @ %i + 1 @ ")",%client);
		}
	}
}

function Slayer_CPTriggerData::onEnterTrigger(%this,%trigger,%player)
{
	if(%player.getClassName() !$= "Player")
		return;

	%client = %player.client;
	if(!isObject(%client))
		return;

	%mini = getMinigameFromObject(%client);
	%team = %client.getTeam();

	if(!isSlayerMinigame(%mini) || !isObject(%team))
		return;

	Slayer_Support::Debug(3,"CP Enter",%trigger TAB %player TAB %client);

	%client.doNotDisplay = 1; //compatibility with Boom's DM+ script
}

function Slayer_CPTriggerData::onTickTrigger(%this,%trigger,%player)
{
	if(%player.getClassName() !$= "Player")
		return;

	%client = %player.client;
	if(!isObject(%client))
		return;

	%mini = getMinigameFromObject(%client);
	%team = %client.getTeam();

	if(!isSlayerMinigame(%mini) || !isObject(%team))
		return;

	Slayer_Support::Debug(3,"CP Tick",%trigger TAB %player TAB %client);

	%brick = %trigger.brick;
	%db = %brick.getDatablock();
	%attCol = %team.color;
	%defCol = %brick.controlColor;
	%maxTicks = %db.CPMaxTicks;

	cancel(%trigger.decreaseTimer[%attCol]);

	if(%attCol != %defCol)
	{
		if(%brick.capture[%attCol] < %maxTicks)
		{
			if(%brick.isLocked[%attCol])
				%client.bottomPrint("<just:center>\c5This capture point is locked. You cannot capture it yet.",1);
			else
			{
				%brick.capture[%attCol] ++;

				for(%i=0; %i < %brick.capture[%attCol]; %i++)
					%a = "_" @ %a;
				for(%i=0; %i < %maxTicks-%brick.capture[%attCol]; %i++)
					%d = "_" @ %d;

				%rgb = getColorIDTable(%defCol);
				%hex = "<color:" @ Slayer_Support::RgbToHex(%rgb) @ ">";

				%a = %team.getColorHex() @ %a;
				%d = %hex @ %d;

				%client.bottomPrint("<just:center>" @ %a @ %d,1);

				if(%mini.CPTransitionColors)
				{
					%avgColor = Slayer_Support::getAverageColor(%rgb,%team.colorRGB);
					%avgColor = Slayer_Support::getClosestPaintColor(%avgColor);

					%brick.setColor(%avgColor);
				}
			}
		}
		else
		{
			%brick.setCPControl(%attCol,0,%client);

			for(%i=0; %i < %brick.capture[%attCol]; %i++)
				%a = "_" @ %a;
			%client.bottomPrint("<just:center>" @ %team.getColorHex() @ %a,1);
		}
	}
	else
	{
		for(%i=0; %i < %maxTicks; %i++)
			%a = "_" @ %a;
		%client.bottomPrint("<just:center>" @ %team.getColorHex() @ %a,1);
	}
}

function Slayer_CPTriggerData::onLeaveTrigger(%this,%trigger,%player)
{
	if(%player.getClassName() !$= "Player")
		return;

	%client = %player.client;
	if(!isObject(%client))
		return;

	%mini = getMinigameFromObject(%client);
	%team = %client.getTeam();

	if(!isSlayerMinigame(%mini) || !isObject(%team))
		return;

	Slayer_Support::Debug(3,"CP Leave",%trigger TAB %player TAB %client);

	%brick = %trigger.brick;
	%attCol = %team.color;
	%defCol = %brick.controlColor;

	if(%attCol == %defCol)
		return;

	for(%i=0; %i < %trigger.getNumObjects(); %i++)
	{
		%pl = %trigger.getObject(%i);
		if(%pl.getClassName() $= "Player")
		{
			%cl = %pl.client;
			if(isObject(%cl) && %cl != %client)
			{
				if(%cl.getTeam().color == %attCol)
				{
					%isTeam = 1;
					break;
				}
			}
		}
	}
	if(!%isTeam)
		%trigger.decreaseTimer[%attCol] = %this.scheduleNoQuota(%this.tickPeriodMS,decreaseCapture,%trigger,%attCol);

	%client.doNotDisplay = 0; //compatibility with Boom's DM+ script
}

function Slayer_CPTriggerData::decreaseCapture(%this,%trigger,%color)
{
	cancel(%trigger.decreaseTimer[%color]);

	%brick = %trigger.brick;
	%defCol = %brick.controlColor;

	if(%color == %defCol)
		return;

	for(%i=0; %i < %trigger.getNumObjects(); %i++)
	{
		%pl = %trigger.getObject(%i);
		if(%pl.getClassName() $= "Player")
		{
			%cl = %pl.client;
			if(%cl != %client)
			{
				if(%cl.getTeam().color == %color)
				{
					return;
				}
			}
		}
	}

	%brick.capture[%color] --;
	if(%brick.capture[%color] <= 0)
	{
		%brick.setColor(%defCol);
		return;
	}

	%trigger.decreaseTimer[%color] = %this.scheduleNoQuota(%this.tickPeriodMS,decreaseCapture,%trigger,%color);
}

// +----------+
// | Vehicles |
// +----------+
if(!isObject(vehicleSpawnGroup))
	new simSet(vehicleSpawnGroup);

function respawnAllVehicles()
{
	for(%i = 0; %i < vehicleSpawnGroup.getCount(); %i ++) //respawn all vehicles
		vehicleSpawnGroup.getObject(%i).respawnVehicle();
}

function recoverAllVehicles()
{
	for(%i = 0; %i < vehicleSpawnGroup.getCount(); %i ++) //respawn all vehicles
		vehicleSpawnGroup.getObject(%i).recoverVehicle();
}

// +-------+
// | Other |
// +-------+
function Slayer_checkBricks()
{
	if(!isObject(mainBrickGroup))
		return;

	for(%i=0; %i < mainBrickGroup.getCount(); %i++)
	{
		%bg = mainBrickGroup.getObject(%i);
		for(%b=0; %b < %bg.getCount(); %b++)
		{
			%bg.getObject(%b).setSlayerBrick(0);
		}
	}
}

// +--------------------+
// | Packaged Functions |
// +--------------------+
package Slayer_Dependencies_Brick
{
	function fxDtsBrick::onPlant(%this) //this only is called on manual plant
	{
		parent::onPlant(%this);

		%this.scheduleNoQuota(15,setSlayerBrick,1);
	}

	function fxDtsBrick::onLoadPlant(%this) //called on load plant
	{
		parent::onLoadPlant(%this);

		%this.scheduleNoQuota(15,setSlayerBrick,0);
	}

	function fxDtsBrick::onRemove(%this)
	{
		if(isObject(%this.trigger))
			%this.trigger.delete();

		%mini = getMinigameFromObject(%this);
		%db = %this.getDatablock();
		%type = %db.slyrType;

		if(%db.isSlyrBrick)
		{
			switch$(%type)
			{
				case "315u49y54689418691469h4":
					
				default:
					if(isObject(Slayer.Gamemodes))
					{
						for(%i=0; %i < Slayer.Gamemodes.getCount(); %i++)
						{
							%m = Slayer.Gamemodes.getObject(%i);
							if(isFunction("Slayer_" @ %m.fName @ "_onSlyrBrickRemove"))
								call("Slayer_" @ %m.fName @ "_onSlyrBrickRemove",%mini,%this,%type);
						}
					}
			}
		}

		parent::onRemove(%this);
	}

	function serverCmdAddEvent(%client,%enabled,%inputEventIdx,%delay,%targetIdx,%namedTargetNameIdx,%outputEventIdx,%par1,%par2,%par3,%par4,%par5,%par6,%par7,%par8,%par9,%par10,%par11,%par12,%par13)
	{
		%mini = getMinigameFromObject(%client);
		if(isSlayerMinigame(%mini) && %mini.restrictOutputEvents)
		{
			%target = inputEvent_getTargetClass("fxDtsBrick",%inputEventIdx,%targetIdx);
			if(%target $= "")
				%target = "fxDtsBrick";
			%outputEvent = outputEvent_getOutputName(%target,%outputEventIdx);

			if($Slayer::Server::Events::RestrictedEvent__[%target,%outputEvent] !$= "")
			{
				if(!%mini.canEdit(%client,$Slayer::Server::Events::RestrictedEvent__[%target,%outputEvent]))
				{
					messageClient(%client,'',"You do not have permission to use the [" @ %target @ "," SPC %outputEvent @ "] event.");
					Slayer_Support::Debug(2,"serverCmdAddEvent",%client.getSimpleName() SPC "does not have permission to use [" @ %target @ "," SPC %outputEvent @ "]");
					return;
				}
			}
		}

		parent::serverCmdAddEvent(%client,%enabled,%inputEventIdx,%delay,%targetIdx,%NamedTargetNameIdx,%outputEventIdx,%par1,%par2,%par3,%par4,%par5,%par6,%par7,%par8,%par9,%par10,%par11,%par12,%par13);
	}

	function fxDtsBrick::onColorChange(%this)
	{
		%parent = parent::onColorChange(%this);

		if(!%this.getDatablock().setOnceOnly)
			%this.setSlayerBrick(1);

		return %parent;
	}

	function fxDtsBrick::onPlayerTouch(%this,%player)
	{
		parent::onPlayerTouch(%this,%player);

		if(%player.getClassName() !$= "Player")
			return;

		%client = %player.client;

		%mini = getMinigameFromObject(%client);
		if(!isSlayerMinigame(%mini))
			return;

		%team = %client.getTeam();
		if(!isObject(%team))
			return;

		$InputTarget_["Self"] = %this;
		$InputTarget_["Player"] = %player;
		$InputTarget_["Client"] = %client;
		$InputTarget_["MiniGame"] = %mini;
		%this.processInputEvent("onPlayerTouch(Team" @ %team.getGroupID() + 1 @ ")",%client);
	}

	function fxDtsBrick::onActivate(%this,%player,%client)
	{
		parent::onActivate(%this,%player,%client);

		if(%player.getClassName() !$= "Player")
			return;

		%client = %player.client;

		%mini = getMinigameFromObject(%client);
		if(!isSlayerMinigame(%mini))
			return;

		%team = %client.getTeam();
		if(!isObject(%team))
			return;

		$InputTarget_["Self"] = %this;
		$InputTarget_["Player"] = %player;
		$InputTarget_["Client"] = %client;
		$InputTarget_["MiniGame"] = %mini;
		%this.processInputEvent("onActivate(Team" @ %team.getGroupID() + 1 @ ")",%client);
	}

	function fxDtsBrick::setNTObjectName(%this,%flag)
	{
		%name = lTrim(strReplace(%flag,"_"," "));
		%pre = getWord(%name,0);

		%oldName = lTrim(strReplace(%this.getName(),"_"," "));
		%oldPre = getWord(%oldName,0);

		%prefixList = "Team\tTeamVehicle";

		switch$(%pre)
		{
			case "Team":
				%value = getWords(%name,1);
				%this.setControl(%value);

			case "TeamVehicle":
				%value = getWords(%name,1);
				%this.setControl(%value);

			default:
				if(%this.controlColor !$= "" && Slayer_Support::isItemInList(%prefixList,%oldPre))
					%this.setControl("");
		}

		return parent::setNTObjectName(%this,%flag);
	}

	function paintProjectile::onCollision(%this,%obj,%brick,%fade,%pos,%normal)
	{
		%parent = parent::onCollision(%this,%obj,%brick,%fade,%pos,%normal);

		if(%brick.getClassName() $= "fxDtsBrick")
		{
			if(getTrustLevel(%obj,%brick) >= $TrustLevel::FXPaint)
			{
				%client = %obj.client;
				%db = %brick.getDatablock();

				if(%db.isSlyrBrick)
				{
					%color = %brick.getColorID();
	
					switch$(%db.slyrType)
					{
						case CP:
							%brick.origColor = %color;
							%brick.setCPControl(%color,1,%client);
					}
				}
			}
		}

		return %parent;
	}

	//JVS CONTENT RESTRICTIONS
	function fxDTSBrick::contentTDMTeamAllowUse(%this,%action,%direction,%text,%client)
	{
		%id = %this.contentTypeID();

		if(%id >= 0)
		{
			%mini = getMinigameFromObject(%this.getGroup());

			if(isSlayerMinigame(%mini))
			{
				if(!miniGameCanUse(%client,%this))
					return 0;

				%team = %client.getTeam();

				for(%i=0; %i < getWordCount(%text); %i++)
				{
					%w = getWord(%text,%i);

					if(%w == 0 && isObject(%team))
						return 1;
					if(%w == -1 && !isObject(%team))
						return 1;
					else if(isObject(%team) && %team.getGroupID() + 1 == %w)
						return 1;
				}

				return 0;
			}
			else if(isObject(%mini) && $AddOn__Gamemode_TeamDeathmatch == 1 && isObject(GameModeStorerSO))
			{
				//use the original TDM functionality
				return parent::contentTDMTeamAllowUse(%this,%action,%direction,%text,%client);
			}
			else
			{
				for(%i=0; %i < getWordCount(%text); %i++)
				{
					%w = getWord(%text,%i);

					if(%w == -1)
						return 1;
				}

				return 0;
			}
		}
		else
		{
			return -1;
		}
	}

	//JVS CONTENT RESTRICTIONS
	function fxDTSBrick::contentTDMTeamDenyUse(%this,%action,%direction,%text,%client)
	{
		%id = %this.contentTypeID();

		if(%id >= 0)
		{
			%mini = getMinigameFromObject(%this.getGroup());

			if(isSlayerMinigame(%mini))
			{
				if(!miniGameCanUse(%client,%this))
					return 0;

				%team = %client.getTeam();

				for(%i=0; %i < getWordCount(%text); %i++)
				{
					%w = getWord(%text,%i);

					if(%w == 0 && isObject(%team))
						return 0;
					if(%w == -1 && !isObject(%team))
						return 0;
					else if(isObject(%team) && %team.getGroupID() + 1 == %w)
						return 0;
				}

				return 1;
			}
			else if(isObject(%mini) && $AddOn__Gamemode_TeamDeathmatch == 1 && isObject(GameModeStorerSO))
			{
				//use the original TDM functionality
				return parent::contentTDMTeamDenyUse(%this,%action,%direction,%text,%client);
			}
			else
			{
				for(%i=0; %i < getWordCount(%text); %i++)
				{
					%w = getWord(%text,%i);

					if(%w == -1)
						return 0;
				}

				return 1;
			}
		}
		else
		{
			return -1;
		}
	}

	function onMissionLoaded()
	{
		Slayer.schedule(0,createBrickEvents);

		return parent::onMissionLoaded();
	}

	function Slayer::createBrickEvents(%this)
	{
		// +--------------+
		// | Input Events |
		// +--------------+
		registerInputEvent(fxDtsBrick,onCPReset,"Self fxDTSBrick\tMiniGame MiniGame");
		registerInputEvent(fxDtsBrick,onCPCapture,"Self fxDTSBrick\tPlayer Player\tClient GameConnection\tMiniGame MiniGame");
		for(%i=1; %i <= $Slayer::Server::Preload::Teams::maxEvents; %i++)
		{
			registerInputEvent(fxDtsBrick,"onCPCapture(Team" @ %i @ ")","Self fxDTSBrick\tPlayer Player\tClient GameConnection\tMiniGame MiniGame");
			registerInputEvent(fxDtsBrick,"onPlayerTouch(Team" @ %i @ ")","Self fxDTSBrick\tPlayer Player\tClient GameConnection\tMiniGame MiniGame");
			registerInputEvent(fxDtsBrick,"onActivate(Team" @ %i @ ")","Self fxDTSBrick\tPlayer Player\tClient GameConnection\tMiniGame MiniGame");
		}

		// +---------------+
		// | Outout Events |
		// +---------------+
		registerOutputEvent(fxDtsBrick,"setControl","paintColor 0",1);
		registerOutputEvent(fxDtsBrick,"setControlLocked","list TriggerTeam 0 TeamColor 1 ALL 2" TAB "paintColor 0" TAB "bool",1);
		//registerOutputEvent(fxDtsBrick,setTeamSpawn,"paintColor 0",1);
		registerOutputEvent(GameConnection,"addLives","int 0 200 1");
		registerOutputEvent(GameConnection,"addDeaths","int 0 200 1");
		registerOutputEvent(GameConnection,"addKills","int 0 200 1");
		registerOutputEvent(GameConnection,"setLives","int 0 200 1");
		registerOutputEvent(GameConnection,"setDeaths","int 0 200 1");
		registerOutputEvent(GameConnection,"setKills","int 0 200 1");
		registerOutputEvent(GameConnection,"joinTeam","string 50 80");
		registerOutputEvent(Minigame,"incTime","int -999 999 1" TAB "bool 1");
		registerOutputEvent(Minigame,"setTime","int 1 999 1" TAB "bool 1");
		registerOutputEvent(Minigame,"Win","list TriggerPlayer 0 TriggerTeam 1 CustomPlayer 2 CustomTeam 3 CustomString 4 NONE 5" TAB "string 80 100",1);

		registerEventTarget("Team(Client) Slayer_TeamSO","GameConnection","%client.getTeam()");
		registerEventTarget("Team(Brick) Slayer_TeamSO","fxDtsBrick","%this.getControlTeams()");
		registerOutputEvent(Slayer_TeamSO,"BottomPrintAll","string 200 156" TAB "int 1 10 3" TAB "bool 0",1);
		registerOutputEvent(Slayer_TeamSO,"CenterPrintAll","string 200 156" TAB "int 1 10 3",1);
		registerOutputEvent(Slayer_TeamSO,"ChatMsgAll","string 200 176",1);
		registerOutputEvent(Slayer_TeamSO,"RespawnAll","",1);
		registerOutputEvent(Slayer_TeamSO,"IncScore","int -999999 999999 1",1);

		if(isFile($Slayer::Server::ConfigDir @ "/.advancedEvents.txt"))
		{
			Slayer_Support::Debug(0,"WARNING: Registering Advanced Events - INSECURE");

			registerOutputEvent(Minigame,"Slayer_setPref","string 50 80" TAB "string 50 80" TAB "string 50 80",1);
		}

		// +--------------------------+
		// | JVS Content Restrictions |
		// +--------------------------+
		if($AddOn__JVS_Content == 1 && isFunction(ContentRestrictionsSO,addRestriction))
		{
			//we are using the exact same restrictions as Space Guy's TDM mod
			//this makes things easier for users
			ContentRestrictionsSO.addRestriction("TeamAllow","contentTDMTeamAllowUse","10100130");
			ContentRestrictionsSO.addRestriction("TeamDeny","contentTDMTeamDenyUse","10200130");
		}

		// +-----------------------+
		// | VCE Special Variables |
		// +-----------------------+
		if($AddOn__Event_Variables == 1 && isFunction(registerSpecialVar))
		{
			Slayer_Support::Debug(2,"VCE","Registering variables");

			registerSpecialVar(fxDtsBrick,"teamName","%this.getControllingTeam(0)");
			registerSpecialVar(fxDtsBrick,"teamNameColored","%this.getControllingTeam(1)");

			registerSpecialVar(GameConnection,"lives","%this.getLives()");
			registerSpecialVar(GameConnection,"kills","%this.getKills()");
			registerSpecialVar(GameConnection,"deaths","%this.getDeaths()");
			registerSpecialVar(GameConnection,"team","%this.getTeam().getGroupID() + 1");
			registerSpecialVar(GameConnection,"teamName","%this.getTeam().name");
			registerSpecialVar(GameConnection,"teamNameColored","\"<sPush>\" @ %this.getTeam().getColorHex() @ %this.getTeam().name @ \"<sPop>\"");
			registerSpecialVar(GameConnection,"teamColor","%this.getTeam().getColorHex()");
			registerSpecialVar(GameConnection,"teamColorID","%this.getTeam().color");
			registerSpecialVar(GameConnection,"teamScore","%this.getTeam().getScore()");
			registerSpecialVar(GameConnection,"teamMembers","%this.getTeam().numMembers");

			registerSpecialVar(MinigameSO,"gamemode","%this.mode");
			registerSpecialVar(MinigameSO,"resetting","%this.isResetting()");
			registerSpecialVar(MinigameSO,"useRounds","%this.Gamemode.rounds");
			registerSpecialVar(MinigameSO,"useTeams","%this.Gamemode.teams");
			registerSpecialVar(MinigameSO,"lives","%this.lives");
			registerSpecialVar(MinigameSO,"points","%this.points");
			registerSpecialVar(MinigameSO,"time","%this.time");
			registerSpecialVar(MinigameSO,"teamCount","%this.Teams.getCount()");

			for(%i=1; %i <= $Slayer::Server::Preload::Teams::maxEvents; %i++)
			{
				registerSpecialVar(MinigameSO,"teamScore" @ %i,"%this.Teams.getObject(" @ %i - 1 @ ").getScore()");
				registerSpecialVar(MinigameSO,"teamLiving" @ %i,"%this.Teams.getObject(" @ %i - 1 @ ").getLiving()");
				registerSpecialVar(MinigameSO,"teamSpawned" @ %i,"%this.Teams.getObject(" @ %i - 1 @ ").getSpawned()");
				registerSpecialVar(MinigameSO,"teamIsDead" @ %i,"%this.Teams.getObject(" @ %i - 1 @ ").isTeamDead()");
				registerSpecialVar(MinigameSO,"teamKills" @ %i,"%this.Teams.getObject(" @ %i - 1 @ ").getKills()");
				registerSpecialVar(MinigameSO,"teamDeaths" @ %i,"%this.Teams.getObject(" @ %i - 1 @ ").getDeaths()");
				registerSpecialVar(MinigameSO,"teamMembers" @ %i,"%this.Teams.getObject(" @ %i - 1 @ ").numMembers");
			}
		}
	}
};
activatePackage(Slayer_Dependencies_Brick);

Slayer_Support::Debug(2,"Dependency Loaded","Brick");