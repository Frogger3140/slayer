// +-------------------------------------------+
// |  ___   _     _____   __  __  ____   ____  |
// | | __| | |   | /_\ |  \ \/ / | ___| | /\ | |
// | |__ | | |_  |  _  |   \  /  | __|  | \/ / |
// | |___| |___| |_| |_|   |__|  |____| |_| \\ |
// |  Greek2me              Blockland ID 11902 |
// +-----------------------------+-------------+
// | DO NOT EDIT BELOW THIS LINE |
// +-----------------------------+

$Slayer::Server::Dependencies::Preferences = 1;

// +----------------------+
// | Slayer_PrefHandlerSO |
// +----------------------+
function Slayer_PrefHandlerSO::onAdd(%this)
{
	Slayer.Prefs = %this;

	//CREATE PREFERENCES
	%this.createPrefs();
	%this.schedule(0,"loadInitialPrefs");
}

function Slayer_PrefHandlerSO::addPref(%this,%category,%title,%variable,%type,%defaultValue,%requiresReset,%notify,%editRights,%list,%callback)
{
	%p = %this.getPrefSO(%category,%title);
	if(isObject(%p))
	{
		%p.delete();
		Slayer_Support::Debug(1,"Overwriting Preference",%category TAB %title);
	}

	%var = getField(%variable,0);
	%className = getField(%variable,1);
	%preload = (striPos(%var,"$Slayer::Server::Preload::") == 0);

	%pref = new scriptObject()
	{
		class = Slayer_PrefSO;

		title = %title;
		category = %category;
		type = %type;
		variable = %var;
		defaultValue = %defaultValue;
		requiresReset = %requiresReset;
		notify = %notify;
		editRights = %editRights;
		isObject = (%className !$= "0" && %className !$= "");
		objectClassName = %className;
		objectCannotBeNull = getField(%variable,2);
		list = %list;
		callback = %callback;
		preload = %preload;
	};
	%this.add(%pref);
	if(isObject(missionCleanup) && missionCleanup.isMember(%pref))
		missionCleanup.remove(%pref);

	if(striPos(%variable,"%mini.") < 0)
	{
		%val = %pref.getValue();
		if(!%preload || %val $= "")
			%pref.setValue(%defaultValue);
	}

	return %pref;
}

function Slayer_PrefHandlerSO::addNonNetworkedPref(%this,%category,%title,%variable,%type,%defaultValue,%callback)
{
	%pref = %this.addPref(%category,%title,%variable,%type,%defaultValue,"","","","",%callback);
	%pref.nonNetworked = 1;
	return %pref;
}

function Slayer_PrefHandlerSO::addTransientPref(%this,%category,%title,%variable,%type,%defaultValue,%requiresReset,%notify,%editRights,%list,%callback)
{
	%pref = %this.addPref(%category,%title,%variable,%type,%defaultValue,%requiresReset,%notify,%editRights,%list,%callback);
	%pref.doNotSave = 1;
	%pref.doNotReset = 1;
	return %pref;
}

function Slayer_PrefHandlerSO::setPref(%this,%category,%title,%value,%mini) //set the value of a pref
{
	%pref = %this.getPrefSO(%category,%title);
	if(!isObject(%pref))
		return false;

	return %pref.setValue(%value,%mini);
}

function Slayer_PrefHandlerSO::getPref(%this,%category,%title,%mini) //get the value of a pref
{
	%pref = %this.getPrefSO(%category,%title);
	if(!isObject(%pref))
		return false;

	return %pref.getValue(%mini);
}

function Slayer_PrefHandlerSO::getPrefSO(%this,%category,%title) //get the SO of a pref
{
	for(%i=0; %i < %this.getCount(); %i++)
	{
		%pref = %this.getObject(%i);
		if(%pref.category $= %category && %pref.title $= %title)
		{
			return %pref;
		}
	}

	return false;
}

function Slayer_PrefHandlerSO::resetPrefs(%this,%mini) //return all prefs to default values
{
	for(%i=0; %i < %this.getCount(); %i++)
	{
		%pref = %this.getObject(%i);
		if(!%pref.doNotReset)
			%pref.setValue(%pref.defaultValue,%mini);
	}
}

function Slayer_PrefHandlerSO::exportPrefs(%this,%path,%mini)
{
	%isSlayerMinigame = isSlayerMinigame(%mini);

	%file = new fileObject();
	%file.openForWrite(%path);

	%file.writeLine("//Slayer -----" SPC getDateTime());
	%file.writeLine("//By Greek2me, Blockland ID 11902");
	%file.writeLine("");
	%file.writeLine("//Version:" SPC $Slayer::Server::Version);
	%file.writeLine("//Debug Mode:" SPC $Slayer::Server::Debug);
	%file.writeLine("//Core Directory:" SPC $Slayer::Server::Directory);
	%file.writeLine("//Config Directory:" SPC $Slayer::Server::ConfigDir);
	%file.writeLine("");
	%file.writeLine("if(isObject(%mini))");
	%file.writeLine("\t%mini.preConfigLoad(\"" @ %path @ "\");");
	%file.writeLine("");

	%file.writeLine("//PREFERENCES //USAGE: (category,title,value)");

	for(%i=0; %i < %this.getCount(); %i++)
	{
		%p = %this.getObject(%i);
		if(%p.preload || %p.doNotSave)
			continue;
		if(!%isSlayerMinigame && striPos(%p.variable,"%mini") >= 0)
			continue;

		%v = %p.getValue(%mini);

		if(%p.isObject)
		{
			if(isObject(%v))
				%var = "nameToID(" @ %v.getName() @ ")";
			else
				%var = 0;
			%file.writeLine("Slayer.Prefs.setPref(\"" @ %p.category @ "\",\"" @ %p.title @ "\"," @ %var @ ",%mini);");
		}
		else
		{
			%file.writeLine("Slayer.Prefs.setPref(\"" @ %p.category @ "\",\"" @ %p.title @ "\",\"" @ %v @ "\",%mini);");
		}
	}

	%teams = %mini.Teams;
	if(isObject(%teams))
	{
		if(%teams.getCount() > 0)
		{
			%file.writeLine("");
			%file.writeLine("//TEAMS");
			%file.writeLine("if(isObject(%mini.Teams))");
			%file.writeLine("{");
			for(%i=0; %i < %teams.getCount(); %i++)
			{
				%t = %teams.getObject(%i);
	
				if(%t.isSpecialTeam)
					continue;
	
				%file.writeLine("\t%team = %mini.Teams.addTeam();");
				for(%e=0; %e < %teams.Prefs.getCount(); %e++)
				{
					%p = %teams.Prefs.getObject(%e);
					%v = %p.getValue(%t);
	
					if(%p.isObject)
					{
						if(isObject(%v))
							%var = "nameToID(" @ %v.getName() @ ")";
						else
							%var = 0;
						%file.writeLine("\t\t%mini.TeamPrefs.setPref(%team,\"" @ %p.category @ "\",\"" @ %p.title @ "\"," @ %var @ ");");
					}
					else
						%file.writeLine("\t\t%mini.TeamPrefs.setPref(%team,\"" @ %p.category @ "\",\"" @ %p.title @ "\",\"" @ %v @ "\");");
				}
			}
			%file.writeLine("}");
		}
	}

	%file.writeLine("");
	%file.writeLine("if(isObject(%mini))");
	%file.writeLine("\t%mini.postConfigLoad(\"" @ %path @ "\");");

	%file.close();
	%file.delete();

	for(%i=0; %i < Slayer.Gamemodes.getCount(); %i++)
	{
		%m = Slayer.Gamemodes.getObject(%i);
		if(isFunction("Slayer_" @ %m.fName @ "_onExportPrefs"))
			call("Slayer_" @ %m.fName @ "_onExportPrefs",%path);
	}

	Slayer_Support::Debug(1,"Exporting Preferences",%path);
}

function Slayer_PrefHandlerSO::exportPreload(%this,%path)
{
	%file = new fileObject() {};
	%file.openForWrite(%path);

	%file.writeLine("//Slayer -----" SPC getDateTime());
	%file.writeLine("//By Greek2me, Blockland ID 11902");
	%file.writeLine("");
	%file.writeLine("//Version:" SPC $Slayer::Server::Version);
	%file.writeLine("//Debug Mode:" SPC $Slayer::Server::Debug);
	%file.writeLine("//Core Directory:" SPC $Slayer::Server::Directory);
	%file.writeLine("//Config Directory:" SPC $Slayer::Server::ConfigDir);
	%file.writeLine("");
	%file.writeLine("$Slayer::Server::Debug =" SPC $Slayer::Server::Debug @ "; //For advanced users only. int 0-3");
	%file.writeLine("$Slayer::Server::LastVersion = \"" @ $Slayer::Server::Version @ "\"; //DO NOT EDIT!");
	%file.writeLine("");

	%file.writeLine("//PREFERENCES");
	%file.close();
	%file.delete();

	export("$Slayer::Server::Preload::*",%path,1);

	for(%i=0; %i < Slayer.Gamemodes.getCount(); %i++)
	{
		%m = Slayer.Gamemodes.getObject(%i);
		if(isFunction("Slayer_" @ %m.fName @ "_onExportPreload"))
			call("Slayer_" @ %m.fName @ "_onExportPreload",%path);
	}

	Slayer_Support::Debug(1,"Exporting Preload",%path);
}

function Slayer_PrefHandlerSO::createPrefs(%this)
{
	//This is how you add preferences.
	//Slayer.Prefs.addPref(category,title,variable,control type,default,requires reset,notify,editRights,list,callback,preload);
	%this.addPref("Permissions","Create Minigame Rights","Slayer.Minigames.createRights","list" TAB "0 Host" TAB "1 Super Admin" TAB "2 Admin" TAB "3 Everyone",2,0,1,0,"Advanced");
	%this.addPref("Permissions","Edit Rights","%mini.editRights","list" TAB "3 Creator" TAB "4 Full Trust" TAB "5 Build Trust" TAB "0 Host" TAB "1 Super Admin" TAB "2 Admin",3,0,1,3,"Advanced");
	%this.addPref("Permissions","Leave Rights","%mini.leaveRights","list" TAB "3 Creator" TAB "4 Full Trust" TAB "5 Build Trust" TAB "0 Host" TAB "1 Super Admin" TAB "2 Admin" TAB "6 Everyone",2,0,1,3,"Advanced");
	%this.addPref("Permissions","Reset Rights","%mini.resetRights","list" TAB "3 Creator" TAB "4 Full Trust" TAB "5 Build Trust" TAB "0 Host" TAB "1 Super Admin" TAB "2 Admin",3,0,1,3,"Advanced");

	%this.addPref("Minigame","Auto Start With Server","$Slayer::Server::Preload::Minigame::AutoStartMinigame","bool",0,0,0,0,"Advanced");

	%this.addPref("Minigame","Custom Rule","%mini.customRule","string 150","",0,0,-1,"Advanced");
	%this.addPref("Minigame","Host Name","%mini.hostName","string 25","HOST",0,0,3,"Advanced");
	%this.addPref("Minigame","Enable Building","%mini.enableBuilding","bool",1,0,1,-1,"","%mini.updateEnableBuilding();");
	%this.addPref("Minigame","Enable Painting","%mini.enablePainting","bool",1,0,1,-1,"","%mini.updateEnablePainting();");
	%this.addPref("Minigame","Enable Wand","%mini.enableWand","bool",1,0,1,-1);
	%this.addPref("Minigame","Late Join Time","%mini.lateJoinTime","slide -1 5 5 1",-1,0,1,-1,"Advanced");
	%this.addPref("Minigame","Use All Players' Bricks","%mini.useAllPlayersBricks","bool",0,0,1,-1);
	%this.addPref("Minigame","Players Use Own Bricks","%mini.playersUseOwnBricks","bool",0,0,1,-1);
	%this.addPref("Minigame","Invite-Only","%mini.inviteOnly","bool",0,0,1,4);
	%this.addPref("Minigame","Title","%mini.Title","string 50","Slayer",0,1,-1);
	%this.addPref("Minigame","Use Spawn Bricks","%mini.useSpawnBricks","bool",1,0,1,-1);
	%this.addPref("Minigame","Spawn Brick Color","%mini.spawnBrickColor","int -1 63",-1,0,0,-1,"Advanced");

	%this.addPref("Damage","Bot","%mini.botDamage","bool",1,0,1,-1);
	%this.addPref("Damage","Brick","%mini.brickDamage","bool",1,0,1,-1);
	%this.addPref("Damage","Falling","%mini.fallingDamage","bool",1,0,1,-1);
	%this.addPref("Damage","Self","%mini.selfDamage","bool",1,0,1,-1);
	%this.addPref("Damage","Vehicle","%mini.vehicleDamage","bool",1,0,1,-1);
	%this.addPref("Damage","Weapon","%mini.weaponDamage","bool",1,0,1,-1);

	%this.addPref("Respawn Time","Player","%mini.respawnTime_player","int 1 999",5,0,1,-1,"","%mini.updateRespawnTime(\"player\",%1,%2);");
	%this.addPref("Respawn Time","Bot","%mini.respawnTime_bot","int -1 999",5,0,1,-1,"","%mini.updateRespawnTime(\"bot\",%1,%2);");
	%this.addPref("Respawn Time","Brick","%mini.respawnTime_brick","int -1 999",30,0,1,-1,"","%mini.updateRespawnTime(\"brick\",%1,%2);");
	%this.addPref("Respawn Time","Vehicle","%mini.respawnTime_vehicle","int 0 999",10,0,1,-1,"","%mini.updateRespawnTime(\"vehicle\",%1,%2);");
	%this.addPref("Respawn Time","Friendly Fire Penalty","%mini.respawnPenalty_FriendlyFire","int 0 999",5,0,1,-1,"Rules Teams Respawn","%mini.updateRespawnTime(\"penalty_FF\",%1,%2);");

	%this.addPref("Player","Playertype","%mini.playerDatablock" TAB "PlayerData" TAB 1,"string 100",nameToID(playerStandardArmor),0,0,-1,"","%mini.updatePlayerDatablock();");
	%this.addPref("Player","Starting Equipment 0","%mini.startEquip0" TAB "ItemData","string 100",nameToID(hammerItem),0,0,-1,"","%mini.updateEquip(0,%1,%2);");
	%this.addPref("Player","Starting Equipment 1","%mini.startEquip1" TAB "ItemData","string 100",nameToID(wrenchItem),0,0,-1,"","%mini.updateEquip(1,%1,%2);");
	%this.addPref("Player","Starting Equipment 2","%mini.startEquip2" TAB "ItemData","string 100",nameToID(printGun),0,0,-1,"","%mini.updateEquip(2,%1,%2);");
	%this.addPref("Player","Starting Equipment 3","%mini.startEquip3" TAB "ItemData","string 100",0,0,0,-1,"","%mini.updateEquip(3,%1,%2);");
	%this.addPref("Player","Starting Equipment 4","%mini.startEquip4" TAB "ItemData","string 100",0,0,0,-1,"","%mini.updateEquip(4,%1,%2);");
	%this.addPref("Player","Name Distance","%mini.nameDistance","slide 0 600 29 1",140,0,1,-1,"","%mini.updateNameDistance(%1);");

	%this.addPref("Points","Break Brick","%mini.points_breakBrick","int -999 999",0,0,1,-1);
	%this.addPref("Points","Die","%mini.points_die","int -999 999",0,0,1,-1);
	%this.addPref("Points","Kill Player","%mini.points_killPlayer","int -999 999",1,0,1,-1);
	%this.addPref("Points","Kill Bot","%mini.points_killBot","int -999 999",1,0,1,-1);
	%this.addPref("Points","Plant Brick","%mini.points_plantBrick","int -999 999",0,0,1,-1);
	%this.addPref("Points","Suicide","%mini.points_killSelf","int -999 999",-1,0,1,-1);
	%this.addPref("Points","Friendly Fire","%mini.points_friendlyFire","int -999 999",-5,0,1,-1,"Rules Teams Points");
	%this.addPref("Points","CP Capture","%mini.points_CP","int -999 999",10,0,1,-1,"Rules Teams Points");

	%this.addPref("Minigame","Pre-Round Countdown","%mini.preRoundSeconds","int 0 10",0,0,1,-1,"Advanced");
	%this.addPref("Minigame","Clear Scores on Reset","%mini.clearScores","bool",1,0,1,-1,"Advanced");
	%this.addPref("Minigame","Clear Stats on Reset","%mini.clearStats","bool",1,0,1,-1,"Advanced");
	%this.addPref("Minigame","Time Between Rounds","%mini.timeBetweenRounds","int 5 300",10,0,1,4,"Advanced");
	%this.addPref("Minigame","Allow Movement During Reset","%mini.allowMoveWhileResetting","bool",0,0,1,-1,"Advanced");
	%this.addPref("EoRR","Display End of Round Report","%mini.eorrEnable","bool",1,0,1,-1,"Advanced");
	%this.addPref("EoRR","Display Team Scores","%mini.eorrDisplayTeamScores","bool",1,0,1,-1,"Advanced");

	%this.addPref("Victory Method","Lives","%mini.lives","int 0 99",0,0,1,-1,"","%mini.updateLives(%1,%2);");
	%this.addPref("Victory Method","Points","%mini.points","int 0 2000",0,0,1,-1,"","%mini.updatePoints(%1,%2);");
	%this.addPref("Victory Method","Time","%mini.time","int 0 999999",0,0,1,-1,"","%mini.updateTime(%1,%2);");

	%this.addPref("Teams","Friendly Fire","%mini.Teams.friendlyFire","bool",0,0,1,-1);
	%this.addPref("Teams","Lock Teams","%mini.Teams.lock","bool",0,0,1,-1);
	%this.addPref("Teams","Auto Sort","%mini.Teams.autoSort","bool",1,0,1,-1);
	%this.addPref("Teams","Balance Teams","%mini.Teams.balanceTeams","bool",1,0,1,-1);
	%this.addPref("Teams","Shuffle Teams","%mini.Teams.shuffleTeams","bool",0,0,1,-1);
	%this.addPref("Teams","Rounds Between Shuffle","%mini.Teams.roundsBetweenShuffles","int 1 999",1,0,1,-1);
	%this.addPref("Teams","Ally Same Colors","%mini.Teams.allySameColors","bool",0,0,1,-1);
	%this.addPref("Teams","Punish Friendly Fire","%mini.Teams.punishFF","bool",1,0,1,3,"Advanced");
	%this.addPref("Teams","Balance New Teams","%mini.Teams.balanceOnNew","bool",1,0,1,3);
	%this.addPref("Teams","Team-Only DeadCam","%mini.Teams.teamOnlyDeadCam","bool",0,0,1,-1,"Advanced");
	%this.addPref("Teams","Display Join/Leave Messages","%mini.Teams.notifyMemberChanges","bool",1,0,0,-1,"Advanced");

	%this.addPref("Capture Points","Tick Time","Slayer_CPTriggerData.tickPeriodMS","int 20 500",150,0,1,-1,"Advanced");
	%this.addPref("Capture Points","Use Transitional Colors","%mini.CPTransitionColors","bool",1,0,1,-1,"Advanced");

	%this.addPref("Events","Max Team Events","$Slayer::Server::Preload::Teams::maxEvents","int 0 64",6,0,0,0,"Advanced");
	%this.addPref("Events","Restrict Output Events","%mini.restrictOutputEvents","bool",1,0,1,0,"Advanced");

	%this.addPref("Chat","Allow Dead Talking","%mini.chat_deadChatMode","list" TAB "0 Disabled" TAB "1 Enabled" TAB "2 Enabled - Global Only" TAB "3 Enabled - Team Only",1,0,1,-1,"Advanced");
	%this.addPref("Chat","Enable Team Chat","%mini.Teams.enableTeamChat","bool",1,0,1,3,"Advanced");
	%this.addPref("Chat","Team Display Mode","%mini.chat_teamDisplayMode","list" TAB "0 Disabled" TAB "1 Color Tag" TAB "2 Color Name" TAB "3 Add Name to Tag",2,0,1,-1,"Advanced");
	%this.addPref("Chat","Death Message Mode","%mini.deathMsgMode","list" TAB "0 Do Not Display" TAB "1 Colored Names" TAB "2 Non-Colored Names",1,0,0,-1,"Advanced");

	//these need to be the last minigame prefs for various reasons
	%this.addPref("Minigame","Gamemode","%mini.mode","string 100","Slyr",1,1,-1,"","%mini.updateGamemode(%1,%2);");
	%this.addPref("Minigame","Default Minigame","%mini.isDefaultMinigame","bool",0,0,1,2,"","%mini.updateDefaultMinigame(%1);");

	%this.addPref("Server","Announcements","Slayer.announcements","string 500","",0,0,1,"Advanced");
	%this.addPref("Server","Save Settings on Update","$Slayer::Server::Preload::SavePrefsOnUpdate","bool",0,0,0,0,"Advanced");
}

function Slayer_PrefHandlerSO::loadInitialPrefs(%this)
{
	//LOAD ANY SAVED NON-MINIGAME PREFS
	if(isFile($Slayer::Server::ConfigDir @ "/config_last.cs"))
		exec($Slayer::Server::ConfigDir @ "/config_last.cs");
	%this.setNonMinigamePrefs = 1;

	//LOAD EXISTING PRELOAD CONFIG
	if(isFile($Slayer::Server::ConfigDir @ "/config_preload.cs"))
		exec($Slayer::Server::ConfigDir @ "/config_preload.cs");
}

function Slayer_PrefHandlerSO::sendPrefs(%this,%client,%mini,%category)
{
	if(!isObject(%mini) && %mini != -1)
		%mini = getMinigameFromObject(%client);
	if(isObject(%mini) && !isSlayerMinigame(%mini))
		return;

	Slayer_Support::Debug(2,"Sending Preferences",%client.getSimpleName());

	//START THE PREF TRANSFER
	commandToClient(%client,'Slayer_getPrefs_Start',%this.getCount());

	for(%i=0; %i < %this.getCount(); %i++)
	{
		%p = %this.getObject(%i);

		if(%p.nonNetworked)
			continue;
		if(%category !$= "" && %category !$= %p.category)
			continue;

		%val = %p.getValue(%mini);

		if(isObject(%mini))
			%canEdit = %mini.canEdit(%client,%p.editRights);
		else
		{
			if(%p.editRights <= 2)
				%canEdit = (%p.editRights <= %client.getAdminLvl());
			else
				%canEdit = 1;
		}

		commandToClient(%client,'Slayer_getPrefs_Tick',%p.category,%p.title,%p.type,%p.objectClassName,%p.defaultValue,%val,%canEdit,%p.list);

		Slayer_Support::Debug(2,"Sending pref",%p.category TAB %p.title TAB %p.type TAB %p.defaultValue TAB %val TAB %p.editRights TAB %p.list);
	}

	//END THE PREF TRANSFER
	commandToClient(%client,'Slayer_getPrefs_End',%this.getCount());
}

// +---------------+
// | Slayer_PrefSO |
// +---------------+
function Slayer_PrefSO::setValue(%this,%value,%mini)
{
	%handler = %this.getGroup();
	%var = %this.variable;
	%isMinigameVar = (striPos(%var,"%mini.") >= 0);

	if(isObject(%mini))
	{
		if(!isSlayerMinigame(%mini))
			return false;

		if(%this.preload || %this.editRights == 0)
		{
			if(%mini != Slayer.Minigames.getHostMinigame())
				return false;
		}

		if(%isMinigameVar)
			%var = strReplace(%var,"%mini",%mini);
	}
	else if(%isMinigameVar)
	{
		return false;
	}

	if(Slayer.isStarting || (isObject(%mini) && %mini.isStarting && %mini.isDedicatedStart))
	{
		if(%isMinigameVar)
		{
			if(Slayer.isStarting)
			{
				return false;
			}
		}
		else
		{
			if(%handler.setNonMinigamePrefs)
			{
				return false;
			}
		}
	}

	%proof = %this.idiotProof(%value);
	if(getField(%proof,0))
		%value = getField(%proof,1);
	else
		return false;

	%callback = %this.callback;
	if(%callback !$= "")
	{
		%callback = strReplace(%callback,"%1","\"" @ %value @ "\"");
		%callback = strReplace(%callback,"%2","\"" @ %this.getValue(%mini) @ "\"");

		if(strPos(%callback,"%mini.") >= 0 && !isObject(%mini))
			%callback = "";
		else
			%callback = strReplace(%callback,"%mini",%mini);
	}

	Slayer_Support::setDynamicVariable(%var,%value);

	Slayer_Support::Debug(2,"Preference Set",%this.category TAB %this.title TAB %value);

	if(%callback !$= "")
		eval(%callback);

	return true;
}

function Slayer_PrefSO::getValue(%this,%mini)
{
	%var = %this.variable;
	if(striPos(%var,"%mini") >= 0)
	{
		if(isSlayerMinigame(%mini))
			%var = strReplace(%var,"%mini",%mini);
		else
			return %this.defaultValue;
	}

	return Slayer_Support::getDynamicVariable(%var);
}

function Slayer_PrefSO::idiotProof(%this,%value)
{
	if(%this.isObject)
	{
		if(!isObject(%value))
		{
			if(%this.objectCannotBeNull)
				return false;
			%value = 0;
		}
		else
		{
			%className = %value.getClassName();
			if(%this.objectClassName !$= %className && %this.objectClassName !$= "ALL")
				return false;
		}
	}

	%type = %this.type;
	switch$(getWord(%type,0))
	{
		case bool:
			if(%value !$= false && %value !$= true)
				return false;

		case string:
			%value = getSubStr(%value,0,getWord(%type,1));

		case int:
			if(!Slayer_Support::isNumber(%value))
				return false;
			%value = Slayer_Support::StripTrailingZeros(%value);
			%value = Slayer_Support::mRestrict(%value,getWord(%type,1),getWord(%type,2));

		case slide:
			if(!Slayer_Support::isNumber(%value))
				return false;
			%value = Slayer_Support::StripTrailingZeros(%value);
			%value = Slayer_Support::mRestrict(%value,getWord(%type,1),getWord(%type,2));

		case list:
			%count = getFieldCount(%type);
			if(%count > 1)
			{
				for(%i = 1; %i < %count; %i ++)
				{
					%f = getField(%type,%i);
					%w = firstWord(%f);
					if(%w == %value)
					{
						%ok = true;
						break;
					}
				}
				if(!%ok)
					return false;
			}
	}

	return true TAB %value;
}

// +------------+
// | serverCmds |
// +------------+
function serverCmdSlayer_getPrefs_Start(%client,%reset,%count,%notify)
{
	%mini = %client.editingMinigame;
	if(!isObject(%mini))
		%mini = getMinigameFromObject(%client);

	if(isSlayerMinigame(%mini) && !%mini.canEdit(%client))
	{
		return;
	}
	else if(!isObject(%mini)) //minigame creation
	{
		if(Slayer.Minigames.canCreate(%client))
		{
			%mini = Slayer.Minigames.addMinigame(%client);
			if(!isObject(%mini))
				return;
			%client.editingMinigame = %mini;
		}
		else
		{
			return;
		}
	}

	%mini.requiresReset = %reset;
	%mini.notifyOnChange = %notify;

	if(%count > 0)
	{
		%message = '\c3%1 \c5updated the \c3%2 \c5minigame.';
		%mini.messageAll('',%message,%client.getPlayerName(),%mini.title);
	}
}

function serverCmdSlayer_getPrefs_Tick(%client,%category,%title,%value)
{
	%mini = %client.editingMinigame;
	if(!isObject(%mini))
		%mini = getMinigameFromObject(%client);

	if(!isSlayerMinigame(%mini))
		return;

	%pref = Slayer.Prefs.getPrefSO(%category,%title);

	if(%pref.nonNetworked)
		return;

	//make sure that nobody can change things without permission
	if(!%mini.canEdit(%client,%pref.editRights))
		return;

	Slayer_Support::Debug(2,"Preference Recieved",%category TAB %title TAB %value);

	if(%pref.getValue(%mini) $= %value)
		return;

	%success = %pref.setValue(%value,%mini);

	if(%success)
	{
		//does the minigame need to be reset once all prefs are loaded?
		if(%pref.requiresReset)
			%mini.requiresReset = 1;

		//tell players that the pref was updated
		if(%pref.notify && %mini.notifyOnChange)
		{
			%type = %pref.type;
			switch$(getWord(%type,0))
			{
				case bool:
					if(%value)
						%val = "True";
					else
						%val = "False";

				case list:
					for(%i=0; %i < getFieldCount(%type); %i++)
					{
						%f = getField(%type,%i);
						if(firstWord(%f) $= %value)
						{
							%val = restWords(%f);
							break;
						}
					}

				default:
					%val = %value;
			}

			%message = '\c3 + \c5[\c3%1\c5|\c3%2\c5] is now \c3%3';
			%mini.messageAll('',%message,%category,%title,%val);
		}
	}
}

function serverCmdSlayer_getPrefs_End(%client)
{
	%mini = %client.editingMinigame;
	if(!isObject(%mini))
		%mini = getMinigameFromObject(%client);

	if(!isSlayerMinigame(%mini))
		return;

	if(!%mini.canEdit(%client))
		return;

	//new teams
	if(%mini.notifyOnChange && %mini.notifyNewTeam > 0)
	{
		%newTeams = (%mini.notifyNewTeam == 1 ? "team" : "teams");
		%mini.messageAll('',"\c3 + \c5Added\c3" SPC %mini.notifyNewTeam SPC "\c5new" SPC %newTeams @ ".");
		%mini.messageAll('',"\c3 + + \c5Type \c3/listTeams \c5to view a list of teams and \c3/joinTeam [Team Name] \c5to join a team.");
		if(%mini.Teams.balanceOnNew)
		{
			%mini.messageAll('',"\c3 + + \c5Teams have been \c3balanced\c5.");
		}
	}

	//check if the minigame should be reset
	if(%mini.requiresReset)
		%mini.reset(%client);

	%mini.requiresReset = 0;
	%mini.notifyNewTeam = 0;
	%mini.notifyOnChange = 0;

	//update the Join Minigame menu
	%mini.broadcastMinigame();

	%client.editingMinigame = "";

	//save settings?
	if($Slayer::Server::Preload::SavePrefsOnUpdate)
	{
		%blid = Slayer_Support::getBlocklandID();
		if(%mini.creatorBLID == %blid && ($Slayer::Server::CurGameModeArg $= "" || $Slayer::Server::CurGameModeArg $= $Slayer::Server::GameModeArg))
			Slayer.Prefs.exportPrefs($Slayer::Server::ConfigDir @ "/config_last.cs",%mini);
		Slayer.Prefs.exportPreload($Slayer::Server::ConfigDir @ "/config_preload.cs");
	}
}

function serverCmdSlayer_saveConfig(%client,%fileName)
{
	if(%client.isSpamming())
		return;

	%fileName = strReplace(%fileName," ","");
	if(%fileName $= "")
		return;

	%mini = getMinigameFromObject(%client);
	if(!isSlayerMinigame(%mini))
		return;
	if(!%mini.canEdit(%client))
		return;

	%path = $Slayer::Server::ConfigDir @ "/config_saved/config_" @ %fileName @ ".cs";
	Slayer.Prefs.exportPrefs(%path,%mini);

	messageClient(%client,'',"Config saved as \"" @ %fileName @ "\"");
}

function serverCmdSlayer_loadConfig(%client,%filename)
{
	if(%client.isSpamming())
		return;

	%mini = getMinigameFromObject(%client);
	if(!isSlayerMinigame(%mini))
		return;
	if(!%mini.canEdit(%client))
		return;

	%path = $Slayer::Server::ConfigDir @ "/config_saved/config_" @ %fileName @ ".cs";
	if(!isFile(%path))
		return;

	exec(%path);

	//hide the GUI
	commandToClient(%client,'Slayer_forceGUI',"ALL",0);

	%mini.reset(%client);
	%mini.messageAll('',%client.getPlayerName() SPC "loaded a minigame config file.");
}

// +----------------------+
// | Initialize Component |
// +----------------------+
if(!isObject(Slayer.Prefs))
{
	if(!isObject(Slayer))
		exec("Add-ons/Gamemode_Slayer/Main.cs");

	Slayer.Prefs = new scriptGroup(Slayer_PrefHandlerSO);
	Slayer.add(Slayer.Prefs);
}

Slayer_Support::Debug(2,"Dependency Loaded","Preferences");