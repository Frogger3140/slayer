// +-------------------------------------------+
// |  ___   _     _____   __  __  ____   ____  |
// | | __| | |   | /_\ |  \ \/ / | ___| | /\ | |
// | |__ | | |_  |  _  |   \  /  | __|  | \/ / |
// | |___| |___| |_| |_|   |__|  |____| |_| \\ |
// |  Greek2me              Blockland ID 11902 |
// +-----------------------------+-------------+
// | DO NOT EDIT BELOW THIS LINE |
// +-----------------------------+

$Slayer::Server::Dependencies::Gamemodes = 1;

// +--------------------------+
// | Slayer_GamemodeHandlerSO |
// +--------------------------+
function Slayer_GamemodeHandlerSO::onAdd(%this)
{
	//("name","FName - shortened name (one word)","use teams (bool)","use rounds (bool)")
	%this.addMode("Slayer","Slyr",0,1);
	%this.addMode("Team Slayer","TSlyr",1,1);
}

function Slayer_GamemodeHandlerSO::addMode(%this,%name,%fname,%teams,%rounds)
{
	if(%name $= "" || %fname $= "" || %teams $= "" || %rounds $= "")
		return;

	if(isObject(%this.getModeFromFName(%fName)) || isObject(%this.getModeFromUIName(%name)))
	{
		Slayer_Support::Error("Gamemode Already Exists",%fName TAB %name);
		return;
	}

	%mode = new scriptObject()
	{
		class = Slayer_GamemodeSO;

		uiName = %name;
		fName = %fName;
		Teams = %teams;
		Rounds = %rounds;
	};
	%this.add(%mode);

	if(isObject(missionCleanup) && missionCleanup.isMember(%mode))
		missionCleanup.remove(%mode);
}

function Slayer_GamemodeHandlerSO::getModeFromFName(%this,%flag)
{
	for(%i=0; %i < %this.getCount(); %i++)
	{
		%m = %this.getObject(%i);
		if(%m.fName $= %flag)
		{
			return %m;
		}
	}

	return 0;
}

function Slayer_GamemodeHandlerSO::getModeFromUIName(%this,%flag)
{
	for(%i=0; %i < %this.getCount(); %i++)
	{
		%m = %this.getObject(%i);
		if(%m.uiName $= %flag)
		{
			return %m;
		}
	}

	return 0;
}

function Slayer_GamemodeHandlerSO::getFNameFromUIName(%this,%flag)
{
	for(%i=0; %i < %this.getCount(); %i++)
	{
		%m = %this.getObject(%i);

		if(%m.uiName $= %flag)
		{
			return %m.fName;
		}
	}

	return "";
}

function Slayer_GamemodeHandlerSO::getUINameFromFName(%this,%flag)
{
	for(%i=0; %i < %this.getCount(); %i++)
	{
		%m = %this.getObject(%i);

		if(%m.fName $= %flag)
		{
			return %m.uiName;
		}
	}

	return "";
}

function Slayer_GamemodeHandlerSO::sendGamemodes(%this,%client)
{
	//START THE GAMEMODE TRANSFER
	commandToClient(%client,'Slayer_getGamemodes_Start');

	for(%i=0; %i < %this.getCount(); %i++)
	{
		%mode = %this.getObject(%i);
		commandToClient(%client,'Slayer_getGamemodes_Tick',%mode.uiName,%mode.fName,%mode.Teams,%mode.Rounds);
	}

	//END THE GAMEMODE TRANSFER
	commandToClient(%client,'Slayer_getGamemodes_End');
}

// +----------------------+
// | Initialize Component |
// +----------------------+
if(!isObject(Slayer.Gamemodes))
{
	if(!isObject(Slayer))
		exec("Add-ons/Gamemode_Slayer/Main.cs");

	Slayer.Gamemodes = new scriptGroup(Slayer_GamemodeHandlerSO);
	Slayer.add(Slayer.Gamemodes);
}

Slayer_Support::Debug(2,"Dependency Loaded","Gamemode");