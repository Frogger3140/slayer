// +-------------------------------------------+
// |  ___   _     _____   __  __  ____   ____  |
// | | __| | |   | /_\ |  \ \/ / | ___| | /\ | |
// | |__ | | |_  |  _  |   \  /  | __|  | \/ / |
// | |___| |___| |_| |_|   |__|  |____| |_| \\ |
// |  Greek2me              Blockland ID 11902 |
// +-----------------------------+-------------+
// | DO NOT EDIT BELOW THIS LINE |
// +-----------------------------+

$Slayer::Server::Dependencies::GameConnection = 1;

$Slayer::Server::spamTimeout = 0.5; //time, in seconds

// +----------------+
// | GameConnection |
// +----------------+
function GameConnection::joinTeam(%this,%flag) //for events
{
	%mini = getMinigameFromObject(%this);
	if(!isSlayerMinigame(%mini))
		return;

	%team = %mini.Teams.getTeamFromName(%flag);
	if(isObject(%team))
		%team.addMember(%this);
}

function GameConnection::getTeam(%this)
{
	%mini = getMinigameFromObject(%this);
	if(!isSlayerMinigame(%mini))
		return -1;
	if(!%mini.Gamemode.teams)
		return -1;
	if(!isObject(%this.SlyrTeam))
		return -1;

	return %this.SlyrTeam;
}

function GameConnection::getKills(%this)
{
	return %this.Slyr_Kills;
}

function GameConnection::setKills(%this,%flag)
{
	%this.Slyr_Kills = %flag;
}

function GameConnection::addKills(%this,%flag)
{
	%this.setKills(%this.Slyr_Kills + %flag);
}

function GameConnection::getDeaths(%this)
{
	return %this.Slyr_Deaths;
}

function GameConnection::setDeaths(%this,%flag)
{
	%this.Slyr_Deaths = %flag;
}

function GameConnection::addDeaths(%this,%flag)
{
	%this.setDeaths(%this.Slyr_Deaths + %flag);
}

function GameConnection::getLives(%this)
{
	return %this.Lives;
}

function GameConnection::setLives(%this,%flag)
{
	%this.Lives = %flag;
}

function GameConnection::addLives(%this,%flag)
{
	%this.setLives(%this.Lives + %flag);
}

function GameConnection::Dead(%this)
{
	return %this.Dead;
}

function GameConnection::setDead(%this,%flag)
{
	%this.Dead = %flag;
}

function GameConnection::getScore(%this)
{
	return %this.score;
}

function GameConnection::addDynamicRespawnTime(%this,%flag)
{
	%this.setDynamicRespawnTime(%this.dynamicRespawnTime + %flag);
}

function GameConnection::setDynamicRespawnTime(%this,%flag)
{
	%flag = Slayer_Support::mRestrict(%flag,-1,999999);

	%this.dynamicRespawnTime = %flag;
}

function GameConnection::applyUniform(%this)
{
	%player = %this.player;
	if(!isObject(%player))
		return;
	%team = %this.getTeam();
	if(!isObject(%team))
		return;

	%color = %team.colorRGB;

	%player.setShapeNameColor(%color);

	//check if this playertype uses the default model
	if(fileName(%player.getDatablock().shapeFile) $= "m.dts" || %team.uniform == 0)
	{
		switch(%team.uniform)
		{
			case 0: //no uniform
				%this.overrideUniform = 1;
				%this.applyBodyParts();
				%this.overrideUniform = 1;
				%this.applyBodyColors();

			case 1: //shirt only
				%this.overrideUniform = 1;
				%this.applyBodyParts(1);
				%this.overrideUniform = 1;
				%this.applyBodyColors(1);

				%player.setNodeColor(chest,%color);
				%player.setNodeColor(femChest,%color);
				%player.setNodeColor(armor,%color);
				%player.setNodeColor(bucket,%color);
				%player.setNodeColor(cape,%color);
				%player.setNodeColor(pack,%color);
				%player.setNodeColor(quiver,%color);
				%player.setNodeColor(tank,%color);

			case 2: //full uniform
				hideAllNodes(%player);

				%player.unHideNode(helmet);
				%player.unHideNode(headSkin);
				%player.unHideNode(chest);
				%player.unHideNode(pants);
				%player.unHideNode(lShoe);
				%player.unHideNode(rShoe);
				%player.unHideNode(lArm);
				%player.unHideNode(rArm);
				%player.unHideNode(lHand);
				%player.unHideNode(rHand);

				%player.setNodeColor(helmet,%color);
				%player.setNodeColor(chest,%color);
				%player.setNodeColor(pants,%color);
				%player.setNodeColor(lShoe,%color);
				%player.setNodeColor(rShoe,%color);
				%player.setNodeColor(larm,%color);
				%player.setNodeColor(rarm,%color);
				%player.setNodeColor(lHand,"1 0.878431 0.611765 1");
				%player.setNodeColor(rHand,"1 0.878431 0.611765 1");
				%player.setNodeColor(headSkin,"1 0.878431 0.611765 1");

			case 3: //custom uniform
				for(%i=0; %i < Slayer.TeamPrefs.getCount(); %i++)
				{
					%p = Slayer.TeamPrefs.getObject(%i);
					if(%p.category !$= "Uniform")
						continue;

					%val = %team.uni_[%p.title];
					if(%val $= "TEAMCOLOR")
						%val = %color;

					%uni_[%p.title] = %val;
				}

				hideAllNodes(%player);

				//head
				%player.unHideNode(headSkin);
				%player.setNodeColor(headSkin,%uni_headColor);

				//chest
				switch(%uni_chest)
				{
					case 0:
						%player.unHideNode(chest);
						%player.setNodeColor(chest,%uni_torsoColor);
					case 1:
						%player.unHideNode(femChest);
						%player.setNodeColor(femChest,%uni_torsoColor);
				}

				//arms
				switch(%uni_lArm)
				{
					case 0:
						%player.unHideNode(lArm);
						%player.setNodeColor(lArm,%uni_lArmColor);
					case 1:
						%player.unHideNode(lArmSlim);
						%player.setNodeColor(lArmSlim,%uni_lArmColor);
				}
				switch(%uni_rArm)
				{
					case 0:
						%player.unHideNode(rArm);
						%player.setNodeColor(rArm,%uni_rArmColor);
					case 1:
						%player.unHideNode(rArmSlim);
						%player.setNodeColor(rArmSlim,%uni_rArmColor);
				}

				//hands
				switch(%uni_lHand)
				{
					case 0:
						%player.unHideNode(lHand);
						%player.setNodeColor(lHand,%uni_lHandColor);
					case 1:
						%player.unHideNode(lHook);
						%player.setNodeColor(lHook,%uni_lHandColor);
				}
				switch(%uni_rHand)
				{
					case 0:
						%player.unHideNode(rHand);
						%player.setNodeColor(rHand,%uni_rHandColor);
					case 1:
						%player.unHideNode(rHook);
						%player.setNodeColor(rHook,%uni_rHandColor);
				}

				//legs and pants/hips
				switch(%uni_hip)
				{
					case 0:
						%player.unHideNode(pants);
						%player.setNodeColor(pants,%uni_hipColor);
						switch(%uni_lLeg)
						{
							case 0:
								%player.unHideNode(lShoe);
								%player.setNodeColor(lShoe,%uni_lLegColor);
							case 1:
								%player.unHideNode(lPeg);
								%player.setNodeColor(lPeg,%uni_lLegColor);
						}
						switch(%uni_rLeg)
						{
							case 0:
								%player.unHideNode(rShoe);
								%player.setNodeColor(rShoe,%uni_rLegColor);
							case 1:
								%player.unHideNode(rPeg);
								%player.setNodeColor(rPeg,%uni_rLegColor);
						}
					case 1:
						%player.unHideNode(skirtHip);
						%player.setNodeColor(skirtHip,%uni_hipColor);
						%player.unHideNode(skirtTrimLeft);
						%player.setNodeColor(skirtTrimLeft,%uni_lLegColor);
						%player.unHideNode(skirtTrimRight);
						%player.setNodeColor(skirtTrimRight,%uni_rLegColor);
				}

				//hat
				switch(%uni_hat)
				{
					case 1:
						%player.unHideNode(helmet);
						%player.setNodeColor(helmet,%uni_hatColor);
					case 2:
						%player.unHideNode(pointyHelmet);
						%player.setNodeColor(pointyHelmet,%uni_hatColor);
					case 3:
						%player.unHideNode(flareHelmet);
						%player.setNodeColor(flareHelmet,%uni_hatColor);
					case 4:
						%player.unHideNode(scoutHat);
						%player.setNodeColor(scoutHat,%uni_hatColor);
					case 5:
						%player.unHideNode(bicorn);
						%player.setNodeColor(bicorn,%uni_hatColor);
					case 6:
						%player.unHideNode(copHat);
						%player.setNodeColor(copHat,%uni_hatColor);
					case 7:
						%player.unHideNode(knitHat);
						%player.setNodeColor(knitHat,%uni_hatColor);
				}

				//pack
				switch(%uni_pack)
				{
					case 1:
						%player.unHideNode(armor);
						%player.setNodeColor(armor,%uni_packColor);
					case 2:
						%player.unHideNode(bucket);
						%player.setNodeColor(bucket,%uni_packColor);
					case 3:
						%player.unHideNode(cape);
						%player.setNodeColor(cape,%uni_packColor);
					case 4:
						%player.unHideNode(pack);
						%player.setNodeColor(pack,%uni_packColor);
					case 5:
						%player.unHideNode(quiver);
						%player.setNodeColor(quiver,%uni_packColor);
					case 6:
						%player.unHideNode(tank);
						%player.setNodeColor(tank,%uni_packColor);
				}
				%player.setHeadUp(%uni_pack > 0);

				//secondPack
				switch(%uni_secondPack)
				{
					case 1:
						%player.unHideNode(epaulets);
						%player.setNodeColor(epaulets,%uni_secondPackColor);
					case 2:
						%player.unHideNode(epauletsRankA);
						%player.setNodeColor(epauletsRankA,%uni_secondPackColor);
					case 3:
						%player.unHideNode(epauletsRankB);
						%player.setNodeColor(epauletsRankB,%uni_secondPackColor);
					case 4:
						%player.unHideNode(epauletsRankC);
						%player.setNodeColor(epauletsRankC,%uni_secondPackColor);
					case 5:
						%player.unHideNode(epauletsRankD);
						%player.setNodeColor(epauletsRankD,%uni_secondPackColor);
					case 6:
						%player.unHideNode(shoulderPads);
						%player.setNodeColor(shoulderPads,%uni_secondPackColor);
				}

				//accent
				switch(%uni_hat)
				{
					case 1:
						switch(%uni_accent)
						{
							case 1:
								%player.unHideNode(visor);
								%player.setNodeColor(visor,%uni_accentColor);
						}
					default:
						switch(%uni_accent)
						{
							case 1:
								%player.unHideNode(plume);
								%player.setNodeColor(plume,%uni_accentColor);
							case 2:
								%player.unHideNode(triPlume);
								%player.setNodeColor(triPlume,%uni_accentColor);
							case 3:
								%player.unHideNode(septPlume);
								%player.setNodeColor(septPlume,%uni_accentColor);
						}
				}

				//decals
				%player.setFaceName(fileBase(%uni_faceName));
				%player.setDecalName(fileBase(%uni_decalName));
		}
	}
	else //using a custom model, like the Horse
		%player.setNodeColor("ALL",%color);

	Slayer_Support::Debug(2,"GameConnection::applyUniform",%this);
}

function GameConnection::forceEquip(%this,%slot,%item)
{
	%player = %this.player;
	if(!isObject(%player))
		return;

	if(!isObject(%item))
		%item = 0;

	%oldTool = %player.tool[%slot];
	%player.tool[%slot] = %item;

	messageClient(%this,'MsgItemPickup','',%slot,%item);

	if(!isObject(%oldTool))
		%player.weaponCount ++;
}

//only gives the player the item if they haven't changed it from %old
function GameConnection::updateEquip(%this,%slot,%new,%old)
{
	%player = %this.player;
	if(!isObject(%player))
		return;

	if(%player.tool[%slot] != %old && (isObject(%player.tool[%slot]) || isObject(%old)))
		return;

	if(%player.tool[%slot] == %new)
		return;

	%this.forceEquip(%slot,%new);
}

function GameConnection::spectateInit(%this)
{
	%mini = getMinigameFromObject(%this);

	%target = %this.spectateNextTarget();
	if(!isObject(%target))
		%this.spectateFree();

	return %target;
}

function GameConnection::spectateFree(%this)
{
	%cam = %this.camera;
	%mini = getMinigameFromObject(%this);
	if(!isObject(%cam) || !isSlayerMinigame(%mini))
		return;

	%this.camera.setMode(observer);
	%this.setControlObject(%this.camera);
	if(!%mini.isResetting())
	{
		%this.bottomPrint("<just:right>\c3Free "
			NL "<font:Palatino Linotype:14>\c5[\c3Change Mode\c5|\c3Jump\c5] ",-1,1);
	}
}

function GameConnection::spectateNextTarget(%this)
{
	%cam = %this.camera;
	%tar = %this.camTarget;
	%mini = getMinigameFromObject(%this);
	%team = %this.getTeam();
	if(!isObject(%cam) || !isSlayerMinigame(%mini))
		return;

	if(%tar >= %mini.numMembers-1)
		%tar = -1;
	for(%i = %tar+1; %i < %mini.numMembers; %i ++)
	{
		%cl = %mini.member[%i];
		%t = %cl.getTeam();
		if(!isObject(%cl.player) || (%mini.Teams.teamOnlyDeadCam && isObject(%team) && %team != %t))
		{
			if(!%repeat && %i >= %mini.numMembers-1)
			{
				%repeat = 1;
				%i = -1;
			}
			continue;
		}

		%cam.setMode("corpse",%cl.player);
		%this.setControlObject(%cam);
		%this.camTarget = %i;
		if(!%mini.isResetting())
		{
			%t = %cl.getTeam();
			%col = isObject(%t) ? %t.getColorHex() : "\c3";
			%this.bottomPrint("<just:right>" @ %col @ %cl.getPlayerName() @ " "
				NL "<font:Palatino Linotype:14>\c5[\c3Change Mode\c5|\c3Jump\c5] "
				NL "\c5[\c3Next\c5|\c3Fire\c5] [\c3Previous\c5|\c3Alt-Fire\c5] ",-1,1);
		}

		return %cl;
	}

	if(!%mini.isResetting())
	{
		%this.bottomPrint("<just:right>\c3No Target "
			NL "<font:Palatino Linotype:14>\c5[\c3Change Mode\c5|\c3Jump\c5] "
			NL "\c5[\c3Next\c5|\c3Fire\c5] [\c3Previous\c5|\c3Alt-Fire\c5] ",-1,1);
	}

	return 0;
}

function GameConnection::spectatePrevTarget(%this)
{
	%cam = %this.camera;
	%tar = %this.camTarget;
	%mini = getMinigameFromObject(%this);
	%team = %this.getTeam();
	if(!isObject(%cam) || !isSlayerMinigame(%mini))
		return;

	if(%tar <= 0)
		%tar = %mini.numMembers;
	for(%i = %tar-1; %i >= 0; %i --)
	{
		%cl = %mini.member[%i];
		%t = %cl.getTeam();
		if(!isObject(%cl.player) || (%mini.Teams.teamOnlyDeadCam && isObject(%team) && %team != %t))
		{
			if(!%repeat && %i <= 0)
			{
				%repeat = 1;
				%i = %mini.numMembers;
			}
			continue;
		}

		%cam.setMode("corpse",%cl.player);
		%this.setControlObject(%cam);
		%this.camTarget = %i;
		if(!%mini.isResetting())
		{
			%t = %cl.getTeam();
			%col = isObject(%t) ? %t.getColorHex() : "\c3";
			%this.bottomPrint("<just:right>" @ %col @ %cl.getPlayerName() @ " "
				NL "<font:Palatino Linotype:14>\c5[\c3Change Mode\c5|\c3Jump\c5] "
				NL "\c5[\c3Next\c5|\c3Fire\c5] [\c3Previous\c5|\c3Alt-Fire\c5] ",-1,1);
		}

		return %cl;
	}

	if(!%mini.isResetting())
	{
		%this.bottomPrint("<just:right>\c3No Target "
			NL "<font:Palatino Linotype:14>\c5[\c3Change Mode\c5|\c3Jump\c5] "
			NL "\c5[\c3Next\c5|\c3Fire\c5] [\c3Previous\c5|\c3Alt-Fire\c5] ",-1,1);
	}

	return 0;
}

function GameConnection::getAdminLvl(%this)
{
	if(%this.isHost)
		return 0;
	if(%this.isSuperAdmin)
		return 1;
	if(%this.isAdmin)
		return 2;

	return 3;
}

function GameConnection::getMiniState(%this)
{
	if(Slayer.Minigames.canCreate(%this) && !isObject(Slayer.Minigames.getMinigameFromBLID(%this.getBLID())))
		%miniState = "CREATE";
	else
		%miniState = "NONE";

	return %miniState;
}

function GameConnection::isSpamming(%this)
{
	%time = $Sim::Time;
	%spam = (%time - %this.lastSlyrCmdTime < $Slayer::Server::spamTimeout);

	%this.lastSlyrCmdTime = %time;

	if(%this.ignoreSpam)
		%spam = 0;
	%this.ignoreSpam = 0;

	return %spam;
}

function isSpecialKill_Slayer_Teams(%client,%sourceObject,%killer,%mini)
{
	if(isSlayerMinigame(%mini))
	{
		if(!%mini.deathMsgMode)
		{
			%client.doNotDisplayDeath = 1;
		}
		else if(%mini.Gamemode.teams)
		{
			%isKill = (isObject(%killer) && %killer != %client);

			%clientTeam = %client.getTeam();
			if(isObject(%clientTeam) && %mini.deathMsgMode == 1)
				%clientColor = %clientTeam.getColorHex();
			else
				%clientColor = "<color:ff0040>"; //default color

			if(%isKill)
			{
				%killerTeam = %killer.getTeam();
				if(isObject(%killerTeam))
				{
					if(%mini.deathMsgMode == 1)
						%killerColor = %killerTeam.getColorHex();
					if(%killerTeam == %clientTeam && %mini.Teams.punishFF)
					{
						%friendlyFire = "\c3(Teamkill)";
					}
				}
				else
					%killerColor = "<color:ff0040>"; //default color
			}

			%value = 2 TAB %clientColor TAB %killerColor TAB %friendlyFire;

			if(isFunction("Slayer_" @ %mini.mode @ "_onDeathMessage"))
				%special = call("Slayer_" @ %mini.mode @ "_onDeathMessage",%mini,%client,%killer,%value);
			if(getField(%special,0) == 1)
				%value = getFields(%special,1);

			Slayer_Support::Debug(2,"isSpecialKill_Slayer_Teams",%value);

			return %value;
		}
	}

	return 0;
}
addSpecialDamageMsg("Slayer_Teams","%5%2%3%4%1 %6","%3%4%1");

// +------------+
// | serverCmds |
// +------------+
function serverCmdAddLives(%client,%flag,%a,%b,%c,%d,%e,%f,%g,%h,%i,%j,%k,%l)
{
	if(%client.isSpamming())
		return;

	%mini = getMinigameFromObject(%client);

	if(!%mini.canEdit(%client))
		return;

	if(!Slayer_Support::isNumber(%flag))
	{
		messageClient(%client,'',"\c5Please use the following format: \c3/addLives Amount Player Name");
		return;
	}

	%name = trim(%a SPC %b SPC %c SPC %d SPC %e SPC %f SPC %g SPC %h SPC %i SPC %j SPC %k SPC %l);
	%target = findClientByName(%name);

	if(!isObject(%target) || getMinigameFromObject(%target) != %mini)
	{
		messageClient(%client,'',"\c5Invalid target.");
		return;
	}

	if(!%mini.Gamemode.rounds || %mini.Gamemode.lives <= 0)
		return;

	%flag = Slayer_Support::mRestrict(%flag,-99,99);

	%target.addLives(%flag);
	%mini.messageAll('',"\c3" @ %client.getPlayerName() SPC "\c5gave\c3" SPC %flag SPC "\c5lives to\c3" SPC %target.getPlayerName() @ "\c5.");

	%lives = %target.getLives();
	if(%lives < 0)
		%target.setLives(0);

	if(%lives > 0 && %target.dead())
	{
		%target.setDead(0);
		if(!isObject(%target.player))
			%target.instantRespawn();
	}
	if(%lives <= 0 && !%target.dead())
	{
		%target.setDead(1);
		if(isObject(%target.player))
		{
			%target.camera.setMode(observer);
			%target.setControlObject(%target.camera);
			%target.player.delete();
		}
		%target.bottomPrint("<just:center>\c5The number of lives was reduced. You are now out of lives.",5);
	}
}

//overwriting the default chat function, nbd
function servercmdMessageSent(%client,%msg)
{
	serverCmdStopTalking(%client);

	%msg = stripMLControlChars(trim(%msg));
	%time = getSimTime();

	if(%msg $= "")
		return;

	%mini = getMinigameFromObject(%client);
	%slayer = isSlayerMinigame(%mini);
	%team = %client.getTeam();

	%name = %client.getPlayerName();
	%pre  = %client.clanPrefix;
	%suf  = %client.clanSuffix;

	%length = strlen(%msg);

	//did they repeat the same message recently?
	if(%msg $= %client.lastMsg && %time-%client.lastMsgTime < $SPAM_PROTECTION_PERIOD)
	{
		if(!%client.isSpamming)
		{
			messageClient(%client,'',"\c5Do not repeat yourself.");
			if(!%client.isAdmin)
			{

				%client.isSpamming = 1;
				%client.spamProtectStart = %time;
				%client.schedule($SPAM_PENALTY_PERIOD,spamReset);
			}
		}
	}

	//are they sending messages too quickly?
	if(!%client.isAdmin && !%client.isSpamming)
	{
		if(%client.spamMessageCount >= $SPAM_MESSAGE_THRESHOLD)
		{
			%client.isSpamming = 1;
			%client.spamProtectStart = %time;
			%client.schedule($SPAM_PENALTY_PERIOD,spamReset);
		}
		else
		{
			%client.spamMessageCount ++;
			%client.schedule($SPAM_PROTECTION_PERIOD,spamMessageTimeout);
		}
	}

	//tell them they're spamming and block the message
	if(%client.isSpamming)
	{
		spamAlert(%client);
		return;
	}

	//eTard Filter, which I hate, but have to include
	if($Pref::Server::eTardFilter)
	{
		%list = strReplace($Pref::Server::eTardList,",","\t");

		for(%i=0; %i < getFieldCount(%list); %i++)
		{
			%wrd = trim(getField(%list,%i));
			if(%wrd $= "")
				continue;
			if(striPos(" " @ %msg @ " "," " @ %wrd @ " ") >= 0)
			{
				messageClient(%client,'',"\c5This is a civilized game. Please use full words.");
				return;
			}
		}
	}

	//URLs
	for(%i=0; %i < getWordCount(%msg); %i++)
	{
		%word = getWord(%msg,%i);
		%url  = getSubStr(%word,7,strLen(%word));

		if(getSubStr(%word,0,7) $= "http://" && strPos(%url,":") == -1)
		{
			%word = "<sPush><a:" @ %url @ ">" @ %url @ "</a><sPop>";
			%msg = setWord(%msg,%i,%word);
		}
	}

	//MESSAGE FORMAT
	if(%slayer)
	{
		if(isObject(%team))
		{
			%tColor = %team.getPref("Chat","Name Color");
			if(%tColor $= "" || strLen(%tColor) != 6)
				%color = %team.getColorHex();
			else
				%color = "<color:" @ %tColor @ ">";

			switch(%mini.chat_teamDisplayMode)
			{
				case 1:
					%all  = '%5%1\c3%2%5%3%7: %4';
	
				case 2:
					%all  = '\c7%1%5%2\c7%3%7: %4';
	
				case 3:
					%all  = '\c7[%5%6\c7] %1\c3%2\c7%3%7: %4';
			}
		}
	}
	if(%all $= "")
		%all  = '\c7%1\c3%2\c7%3%7: %4';

	if(%slayer && %client.dead() && (%mini.chat_deadChatMode == 0 || %mini.chat_deadChatMode == 3) && !%mini.isResetting())
		%mini.messageAllDead('chatMessage',%all,%pre,%name,%suf,%msg,%color,%team.name," \c7[DEAD]<color:c0c0c0>");
	else
		commandToAll('chatMessage',%client,'','',%all,%pre,%name,%suf,%msg,%color,%team.name,"<color:ffffff>");

	echo(%client.getSimpleName() @ ":" SPC %msg);

	%client.lastMsg = %msg;
	%client.lastMsgTime = %time;

	if(isObject(%client.player))
	{
		%client.player.playThread(3,"talk");
		%client.player.schedule(%length * 50,playThread,3,"root");
	}

	if(%slayer)
	{
		if(isFunction("Slayer_" @ %mini.mode @ "_onChat"))
			call("Slayer_" @ %mini.mode @ "_onChat",%mini,%client,%msg);
	}
}

// +--------------------+
// | Packaged Functions |
// +--------------------+
package Slayer_Dependencies_GameConnection
{
	function GameConnection::autoAdminCheck(%this)
	{
		%this.isHost = (%this.isLocalConnection() || %this.getBLID() == Slayer_Support::getBlocklandID());

		Slayer.schedule(0,initClientAuth,%this);

		return parent::autoAdminCheck(%this);
	}

	function GameConnection::onClientEnterGame(%this)
	{
		//PREVENT VEHICLE RESPAWN
		%oldClearVehicles = %this.doNotResetVehicles;

		%blid = %this.getBLID();
		for(%i=0; %i < Slayer.Minigames.getCount(); %i ++)
		{
			%mini = Slayer.Minigames.getObject(%i);
			if(%mini.useAllPlayersBricks || %mini.creatorBLID == %blid)
			{
				%this.doNotResetVehicles = 1; //hack to make vehicles not respawn
				break;
			}
		}

		%parent = parent::onClientEnterGame(%this);

		%this.doNotResetVehicles = %oldClearVehicles;

		//TRANSMIT MINIGAME LIST
		Slayer.Minigames.broadcastAllMinigames(%this);

		//DEFAULT MINIGAME
		%defaultMini = Slayer.Minigames.defaultMinigame;
		if(isObject(%defaultMini))
			%defaultMini.addMember(%this);

		return %parent;
	}

	function GameConnection::onClientLeaveGame(%this)
	{
		%oldClearVehicles = %this.doNotResetVehicles;

		if(isObject(Slayer.Minigames))
		{
			%blid = %this.getBLID();

			for(%i=0; %i < Slayer.Minigames.getCount(); %i ++)
			{
				%mini = Slayer.Minigames.getObject(%i);
				if(%mini.useAllPlayersBricks || %mini.creatorBLID == %blid)
				{
					%this.doNotResetVehicles = 1; //hack to make vehicles not respawn
					break;
				}
			}
		}

		%parent = parent::onClientLeaveGame(%this);

		%this.doNotResetVehicles = %oldClearVehicles;

		return %parent;
	}

	function GameConnection::spawnPlayer(%this)
	{
		return parent::spawnPlayer(%this); //we don't need this function right now
	
		%mini = getMinigameFromObject(%this);

		if(isSlayerMinigame(%mini))
		{
			%team = %this.getTeam();

			if(%this.dead())
			{

			}
			else
			{
				if(isObject(%team))
				{
					if(%team.isSpectatorTeam)
					{
						%this.setDead(1);
						%this.initSpectatorCam();
						commandToClient(%this,'MsgYourSpawn');
						return;
					}
				}

				return parent::spawnPlayer(%this);
			}
		}
		else
		{
			return parent::spawnPlayer(%this);
		}
	}

	function GameConnection::createPlayer(%this,%pos)
	{
		%mini = getMinigameFromObject(%this);
		%team = %this.getTeam();

		if(isSlayerMinigame(%mini))
		{
			%parent = parent::createPlayer(%this,%pos);

			//apply team settings
			if(isObject(%team))
			{
				if(isObject(%team.playerDatablock))
					%this.player.changeDatablock(%team.playerDatablock);

				for(%i=0; %i < %this.player.getDatablock().maxTools; %i++)
					%this.forceEquip(%i,%team.startEquip[%i]);

				%ps = %team.playerScale;
				if(%ps != 1)
					%this.player.setScale(%ps SPC %ps SPC %ps);
	
				%this.player.setShapeNameColor(%team.colorRGB);
			}

			//Name distance
			%this.player.setShapeNameDistance(%mini.nameDistance);

			//display remaining lives
			%lives = %this.getLives();
			if(%mini.Gamemode.rounds && %lives > 0)
			{
				if(!%mini.isResetting())
				{
					%msg = "\c5You have \c3" @ %lives SPC (%lives == 1 ? "\c5life" : "\c5lives") SPC "left.";
					%this.schedule(100,centerPrint,%msg,3);
					messageClient(%this,'',%msg);
				}
			}

			//dynamic respawn time
			if(isObject(%team) && %team.respawnTime >= 0)
				%this.setDynamicRespawnTime(%team.respawnTime);
			else
				%this.setDynamicRespawnTime(%mini.respawnTime);
			cancel(%this.dynamicRespawnSchedule);
			%this.dynamicRespawnSchedule = "";
	
			if(isFunction("Slayer_" @ %mini.mode @ "_onSpawn"))
				call("Slayer_" @ %mini.mode @ "_onSpawn",%mini,%this);
		}
		else
		{
			%parent = parent::createPlayer(%this,%pos);
		}

		return %parent;
	}

	function GameConnection::applyBodyParts(%this)
	{
		%mini = getMinigameFromObject(%this);

		if(isSlayerMinigame(%mini) && !%this.overrideUniform)
		{
			if(%this.getClassName() $= "GameConnection")
			{
				if(isObject(%this.getTeam()))
				{
					return;
				}
			}
		}

		%this.overrideUniform = "";

		return parent::applyBodyParts(%this);
	}
	
	function GameConnection::applyBodyColors(%this,%overRide)
	{
		%mini = getMinigameFromObject(%this);

		if(isSlayerMinigame(%mini) && !%this.overrideUniform)
		{
			if(%this.getClassName() $= "GameConnection")
			{
				if(isObject(%this.getTeam()))
				{
					%this.applyUniform();
					return;
				}
			}
		}

		%this.overrideUniform = "";

		return parent::applyBodyColors(%this);
	}

	function GameConnection::onDeath(%this,%obj,%killer,%type,%area)
	{
		%mini = getMinigameFromObject(%this);

		if(isSlayerMinigame(%mini))
		{
			//friendly fire, kill and death counts
			if(isObject(%killer))
			{
				%killTeam = %killer.getTeam();
				%clientTeam = %this.getTeam();
	
				if(%killTeam == %clientTeam && isObject(%killTeam) && %killer != %this && %mini.getPref("Teams","Punish Friendly Fire"))
				{
					%killer.incScore(%mini.points_friendlyFire);
					%killer.incScore(-%mini.points_killPlayer);
					%this.incScore(-%mini.points_die);
	
					%killer.addDynamicRespawnTime(%mini.friendlyFireRespawnPenalty);
				}
				else
				{
					%this.addDeaths(1);
					if(%killer != %this)
						%killer.addKills(1);
				}
			}

			if(isFunction("Slayer_" @ %mini.mode @ "_preDeath"))
				%special = call("Slayer_" @ %mini.mode @ "_preDeath",%mini,%this,%obj,%killer,%type,%area);
			if(getField(%special,0) == 1)
				%type = getField(%special,1);

			//lives
			if(%this.getLives() > 0 && %mini.Gamemode.rounds)
			{
				%this.addLives(-1);
				if(%this.getLives() <= 0)
					%this.setDead(1);
			}

			//dynamic respawn time -- this is hacky
			%drt = %this.dynamicRespawnTime;
			%respawnTime = %mini.respawnTime; //make sure we hold onto the old respawn time
			if(%drt >= 0 && %drt !$= "" && %drt != %respawnTime && !%this.dead())
			{
				%this.dynamicRespawnSchedule = scheduleNoQuota(%drt,0,eval,%this @ ".dynamicRespawnTime = 0;");
				%mini.respawnTime = %drt;
			}
	
			parent::onDeath(%this,%obj,%killer,%type,%area);
	
			//restore the minigame respawn time
			%mini.respawnTime = %respawnTime;
	
			if(isFunction("Slayer_" @ %mini.mode @ "_postDeath"))
				%special = call("Slayer_" @ %mini.mode @ "_postDeath",%mini,%this,%obj,%killer,%type,%area);

			if(%special == -1)
				return;

			if(%this.dead())
			{
				//remove the "Click to Respawn" message and replace it with a lives message
				messageClient(%this,'MsgYourSpawn');
				%this.centerPrint("\c5You have \c30 \c5lives left.",3);

				//check to see if anyone won
				%winner = %mini.victoryCheck_Lives();
				if(%winner >= 0)
					%mini.endRound(%winner);
				else
				{
					%this.bottomPrint("<just:right>\c3Spectating "
						NL "<font:Palatino Linotype:14>\c5[\c3Change Mode\c5|\c3Jump\c5] "
						NL "\c5[\c3Next\c5|\c3Fire\c5] [\c3Previous\c5|\c3Alt-Fire\c5] ",-1,1);
				}
			}
		}
		else
		{
			parent::onDeath(%this,%obj,%killer,%type,%area);
		}
	}

	function GameConnection::setScore(%this,%flag)
	{
		%mini = getMinigameFromObject(%this);
		if(isSlayerMinigame(%mini))
		{
			if(!(%mini.isResetting() && !%mini.clearScores && %this.points <= 0))
			{
				%parent = parent::setScore(%this,%flag);

				if(isFunction("Slayer_" @ %mini.mode @ "_onScore"))
					call("Slayer_" @ %mini.mode @ "_onScore",%mini,%this,%flag);
	
				%winner = %mini.victoryCheck_Points();
				if(%winner >= 0)
					%mini.endRound(%winner);
			}
		}
		else
			%parent = parent::setScore(%this,%flag);

		return %parent;
	}

	function GameConnection::resetVehicles(%this) //hack
	{
		if(!%this.doNotResetVehicles)
			parent::resetVehicles(%this);
	}

	function GameConnection::clearEventObjects(%this,%a) //hack
	{
		if(!%this.doNotClearEventObjects)
			return parent::clearEventObjects(%this,%a);
	}

	function Observer::onTrigger(%this,%cam,%btn,%state)
	{
		if(!%state)
			return parent::onTrigger(%this,%cam,%btn,%state);
		if(!isObject(%cam))
			return parent::onTrigger(%this,%cam,%btn,%state);
		%client = %cam.getControllingClient();
		if(!isObject(%client))
			return parent::onTrigger(%this,%cam,%btn,%state);

		%mini = getMinigameFromObject(%client);

		if(isSlayerMinigame(%mini))
		{
			if(%client.dead())
			{
				switch(%btn)
				{
					case 0:
						switch$(%cam.mode)
						{
							case corpse:
								%client.spectateNextTarget();
						}

					case 4:
						switch$(%cam.mode)
						{
							case corpse:
								%client.spectatePrevTarget();
						}
	
					case 2:
						switch$(%cam.mode)
						{
							case corpse:
								if(!(%mini.Teams.teamOnlyDeadCam && isObject(%client.getTeam())))
									%client.spectateFree();
	
							case observer:
								%client.spectateNextTarget();
						}
				}
	
				return;
			}
			else if(!isObject(%client.player))
			{
				//dynamic respawn time
				%dnr = %client.dynamicRespawnTime;
				if(%dnr >= 0 && %dnr !$= "")
				{
					if(%dnr != %mini.respawnTime)
					{
						if(%dnr < %mini.respawnTime)
							%client.instantRespawn();
						return;
					}
				}
			}

			if(isFunction("Slayer_" @ %mini.mode @ "_onObserverTrigger"))
				call("Slayer_" @ %mini.mode @ "_onObserverTrigger",%mini,%this,%cam,%btn,%state,%client);
		}

		return parent::onTrigger(%this,%cam,%btn,%state);
	}

	function serverCmdDropPlayerAtCamera(%client)
	{
		%mini = getMinigameFromObject(%client);
		if(isSlayerMinigame(%mini))
		{
			if(%client.dead())
			{
				if(!%mini.canEdit(%client))
				{
					%client.centerPrint("\c5You're out of lives! Don't cheat!",3);
					return;
				}
			}
		}

		parent::serverCmdDropPlayerAtCamera(%client);
	}

	function serverCmdDropCameraAtPlayer(%client)
	{
		%mini = getMinigameFromObject(%client);
		if(isSlayerMinigame(%mini))
		{
			if(%client.dead())
			{
				if(!%mini.canEdit(%client))
				{
					%client.centerPrint("\c5You're out of lives! Don't cheat!",3);
					return;
				}
			}
		}

		parent::serverCmdDropCameraAtPlayer(%client);
	}
};
activatePackage(Slayer_Dependencies_GameConnection);

Slayer_Support::Debug(2,"Dependency Loaded","GameConnection");