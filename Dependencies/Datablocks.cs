// +-------------------------------------------+
// |  ___   _     _____   __  __  ____   ____  |
// | | __| | |   | /_\ |  \ \/ / | ___| | /\ | |
// | |__ | | |_  |  _  |   \  /  | __|  | \/ / |
// | |___| |___| |_| |_|   |__|  |____| |_| \\ |
// |  Greek2me              Blockland ID 11902 |
// +-----------------------------+-------------+
// | DO NOT EDIT BELOW THIS LINE |
// +-----------------------------+

datablock TriggerData(Slayer_CPTriggerData)
{
	tickPeriodMS = 150;
};

datablock fxDtsBrickData(brickSlyrSpawnPointData : brickSpawnPointData)
{
	uiName = "Team Spawn Point";
	category = "Slayer";
	subCategory = "General";

	indestructable = 1;

	isSlyrBrick = 1;
	slyrType = "TeamSpawn";

	setOnceOnly = 0;
};

datablock fxDtsBrickData(brickSlyrVehicleSpawnData : brickVehicleSpawnData)
{
	uiName = "Team Vehicle Spawn";
	category = "Slayer";
	subCategory = "General";

	isSlyrBrick = 1;
	slyrType = "TeamVehicle";

	setOnceOnly = 0;
};

datablock fxDtsBrickData(brickSlyrCPData : brick8x8FData)
{
	uiName = "8x8 Capture Point";
	category = "Slayer";
	subCategory = "General";

	indestructable = 1;

	isSlyrBrick = 1;
	slyrType = "CP";
	CPMaxTicks = 40;

	setOnceOnly = 1;
};

datablock fxDtsBrickData(brickSlyrLrgCPData : brick16x16FData)
{
	uiName = "16x16 Capture Point";
	category = "Slayer";
	subCategory = "General";

	indestructable = 1;

	isSlyrBrick = 1;
	slyrType = "CP";
	CPMaxTicks = 66;

	setOnceOnly = 1;
};

Slayer_Support::Debug(2,"Datablocks Loaded","Main");