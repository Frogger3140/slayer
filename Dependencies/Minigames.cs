// +-------------------------------------------+
// |  ___   _     _____   __  __  ____   ____  |
// | | __| | |   | /_\ |  \ \/ / | ___| | /\ | |
// | |__ | | |_  |  _  |   \  /  | __|  | \/ / |
// | |___| |___| |_| |_|   |__|  |____| |_| \\ |
// |  Greek2me              Blockland ID 11902 |
// +-----------------------------+-------------+
// | DO NOT EDIT BELOW THIS LINE |
// +-----------------------------+

$Slayer::Server::Dependencies::Minigames::ResetTimeOut = 5000;

// +--------------------------+
// | Slayer_MinigameHandlerSO |
// +--------------------------+
function Slayer_MinigameHandlerSO::onAdd(%this)
{

}

function Slayer_MinigameHandlerSO::onRemove(%this)
{
	%this.endAllMinigames();
}

function Slayer_MinigameHandlerSO::addMinigame(%this,%client,%configFile)
{
	if(isObject(%client))
	{
		if(!%this.canCreate(%client))
			return;

		%blid = %client.getBLID();

		if(isObject(%this.getMinigameFromBLID(%blid)))
		{
			messageClient(%client,'',"\c5A minigame has already been created by Blockland ID" SPC %blid);
			return;
		}
	}
	else
	{
		if(isObject(%this.getHostMinigame()))
		{
			Slayer_Support::Debug(1,"Create Minigame","A server-created minigame has already been set.");
			return;
		}

		%blid = Slayer_Support::getBlocklandID();
	}

	Slayer_Support::Debug(1,"Minigame","Starting...");
		
	%mini = new scriptObject()
	{
		class = Slayer_MinigameSO;
		superClass = MinigameSO;

		creatorBLID = %blid;
		owner = 0;

		isDedicatedStart = !isObject(%client);

		configFile = %configFile;
	};

	if(isObject(%client))
	{
		if(%mini.hostName $= "HOST")
		{
			%mini.schedule(0,setPref,"Minigame","Host Name",%client.getPlayerName());
			%mini.schedule(0,broadcastMinigame);
		}

		%mini.addMember(%client);
	}

	return %mini;
}

function Slayer_MinigameHandlerSO::endAllMinigames(%this)
{
	for(%i=0; %i < %this.getCount(); %i++)
		%this.getObject(%i).endGame();
}

function Slayer_MinigameHandlerSO::getMinigameFromBLID(%this,%blid)
{
	if(!Slayer_Support::isNumber(%blid))
		return 0;

	for(%i=0; %i < %this.getCount(); %i++)
	{
		%mini = %this.getObject(%i);
		if(%mini.creatorBLID == %blid)
		{
			return %mini;
		}
	}

	return 0;
}

function Slayer_MinigameHandlerSO::getHostMinigame(%this)
{
	%blid = Slayer_Support::getBlocklandID();

	return %this.getMinigameFromBLID(%blid);
}

function Slayer_MinigameHandlerSO::getHighestPriorityMinigame(%this,%blid)
{
	%m = %this.getMinigameFromBLID(%blid);
	if(isObject(%m))
	{
		%mini = %m; //owner has #1 priority
	}
	else
	{
		%hostID = Slayer_Support::getBlocklandID();

		for(%i=0; %i < %this.getCount(); %i++)
		{
			%m = %this.getObject(%i);
			if(!%m.useAllPlayersBricks)
				continue;

			if(%m.creatorBLID == %hostID) //host has #2 priority
				return %m;

			%mini = %m; //this is a crappy way of doing things
		}
	}

	return (isObject(%mini) ? %mini : -1);
}

function Slayer_MinigameHandlerSO::setDefaultMinigame(%this,%mini)
{
	%this.defaultMinigame = %mini;

	for(%i=0; %i < %this.getCount(); %i++)
	{
		%m = %this.getObject(%i);
		%m.isDefaultMinigame = (%mini == %m);
		%m.broadcastMinigame();
	}

	if(isObject(%mini))
	{
		for(%i=0; %i < clientGroup.getCount(); %i++)
		{
			%cl = clientGroup.getObject(%i);
			if(%cl.hasSpawnedOnce)
			{
				%m = getMinigameFromObject(%cl);
				if(!isObject(%m))
					%mini.addMember(%cl);
			}
		}
	}
}

function Slayer_MinigameHandlerSO::broadcastAllMinigames(%this,%client,%onlyChanged)
{
	for(%i=0; %i < %this.getCount(); %i++)
		%this.getObject(%i).broadcastMinigame(%client,%onlyChanged);
}

function Slayer_MinigameHandlerSO::canCreate(%this,%client,%overRide)
{
	if(%client.isHost)
		return true;

	%mini = getMinigameFromObject(%client);
	if(isSlayerMinigame(%mini))
	{
		if(!%mini.canLeave(%client))
		{
			return false;
		}
	}

	%create = %this.createRights;
	%level = %client.getAdminLvl();

	if(%level <= %create || (%level <= %overRide && %overRide !$= "" && %overRide != -1))
		return true;

	return false;
}

// +-------------------+
// | Slayer_MinigameSO |
// +-------------------+
function Slayer_MinigameSO::onAdd(%this) //minigame initial setup
{
	if(isObject(minigameGroup))
		minigameGroup.add(%this); //not sure what benefit this has, but standard minigames use it

	%this.isStarting = 1; //some functions perform diferently on startup

	%this.isSlayerMinigame = 1;
	%this.colorIdx = 3; //change this for a different minigame color

	%this.isResetting = 0;
	%this.numMembers = 0;

	//Create the needed stuffs
	%this.Prefs = Slayer.Prefs;

	%this.TeamPrefs = Slayer.TeamPrefs;

	%this.Teams = new scriptGroup()
	{
		class = Slayer_TeamHandlerSO;
		minigame = %this;
	};

	%this.onStart();
}

function Slayer_MinigameSO::onRemove(%this)
{
	%blid = Slayer_Support::getBlocklandID();
	if(%this.creatorBLID == %blid && ($Slayer::Server::CurGameModeArg $= "" || $Slayer::Server::CurGameModeArg $= $Slayer::Server::GameModeArg))
		Slayer.Prefs.exportPrefs($Slayer::Server::ConfigDir @ "/config_last.cs",%this);

	if(isObject(%this.Teams))
		%this.Teams.delete();

	if(isObject(%this.fakeClient))
		%this.fakeClient.delete();
}

function Slayer_MinigameSO::onStart(%this)
{
	Slayer.Minigames.add(%this);

	//PREFERENCES
	%this.resetPrefs(); //apply defaults

	//AUTOMATIC STARTUP
	if(%this.isDedicatedStart)
	{
		if(isFile(%this.configFile))
		{
			%mini = %this; //needed to load prefs
			exec(%this.configFile);

			//in case the gamemode they were using before was disabled
			if(!isObject(%this.Gamemode))
				%this.setPref("Minigame","Gamemode","Slyr");

			//corruption protection
			//if these prefs aren't set, something went wrong with the config file
			//reset to defaults
			if(%this.getPref("Permissions","Edit Rights") $= "" && %this.getPref("Permissions","Reset Rights") $= "" && %this.getPref("Permissions","Leave Rights") $= "")
				%this.resetPrefs();
		}

		//Are we using the BL Slayer game mode? (from the main menu)
		if($Slayer::Server::CurGameModeArg $= $Slayer::Server::GameModeArg)
		{
			%this.isDefaultMinigame = 1;
		}
		else if($Slayer::Server::CurGameModeArg !$= "")
		{
			//"I reject your minigame and substitute my own" - replace the BL default minigame
			if(isObject($DefaultMiniGame))
				$DefaultMiniGame.delete();
		}
	}

	//add to the join minigame GUI
	%this.broadcastMinigame();

	%this.isStarting = 0;

	%this.updateDefaultMinigame(%this.isDefaultMinigame);

	%this.reset();

	Slayer_Support::Debug(1,"Minigame","Started");
}

function Slayer_MinigameSO::preConfigLoad(%this,%file)
{
	Slayer_Support::Debug(2,"Loading config file",%file);

	if(isObject(%this.Teams))
		%this.Teams.deleteAll();
}

function Slayer_MinigameSO::postConfigLoad(%this,%file)
{
	Slayer_Support::Debug(2,"Config file loaded",%file);

	if(%this.gamemode.teams && isObject(%this.Teams) && %this.Teams.getAutoSortTeamCount() > 0)
	{
		%this.Teams.autoSortAll(1);
	}
}

function Slayer_MinigameSO::broadcastMinigame(%this,%client,%onlyIfChanged) //add to the join minigame GUI
{
	%blid = %this.creatorBLID;

	%defaultMini = (Slayer.Minigames.defaultMinigame == %this ? "Yes" : "No");

	if(isObject(%client))
	{
		%canEdit = %this.canEdit(%client);
		%inviteOnly = (%this.inviteOnly && !%canEdit);
		%data = %this.hostName TAB %blid TAB %this.title TAB %inviteOnly TAB %defaultMini TAB %canEdit;

		if(%onlyIfChanged && %client.lastMinigameBroadcast[%this] $= %data)
			return;

		%client.lastMinigameBroadcast[%this] = %data;

		commandToClient(%client,'addMinigameLine',%data,%this.getID(),%this.colorIDX);
	}
	else
	{
		for(%i=0; %i < clientGroup.getCount(); %i++)
		{
			%cl = clientGroup.getObject(%i);

			%canEdit = %this.canEdit(%cl);
			%inviteOnly = (%this.inviteOnly && !%canEdit);
			%data = %this.hostName TAB %blid TAB %this.title TAB %inviteOnly TAB %defaultMini TAB %canEdit;

			if(%onlyIfChanged && %cl.lastMinigameBroadcast[%this] $= %data)
				continue;

			%cl.lastMinigameBroadcast[%this] = %data;

			commandToClient(%cl,'addMinigameLine',%data,%this.getID(),%this.colorIDX);
		}
	}
}

function Slayer_MinigameSO::victoryCheck_Lives(%this)
{
	if(%this.isResetting())
		return -1;

	if(!%this.Gamemode.rounds)
		return -1;

	if(isFunction("Slayer_" @ %this.mode @ "_victoryCheck_Lives"))
		%special = call("Slayer_" @ %this.mode @ "_victoryCheck_Lives",%this);

	if(%special >= 0 && %special !$= "")
		return %special;

	//check if any teams have remaining players
	if(%this.Gamemode.teams && %this.Teams.getCount() > 0)
	{
		%winnerColor = -1;
		for(%i=0; %i < %this.Teams.getCount(); %i++)
		{
			%t = %this.Teams.getObject(%i);
			if(!%t.isTeamDead())
			{
				if(%this.Teams.allySameColor && %t.color == %winnerColor)
				{
					%winner = setField(%winner,getFieldCount(%winner),%t);
				}
				else
				{
					%winner = %t;
					%winnerColor = %t.color;
					%count ++;
				}
			}
		}
		if(%count > 1)
			return -1;
	}

	//check if anyone without a team is still alive
	for(%i=0; %i < %this.numMembers; %i++)
	{
		%cl = %this.member[%i];
		if(!%cl.Dead() && !isObject(%cl.getTeam()))
		{
			%winner = %cl;
			%count ++;
		}
	}

	if(%count == 1)
		return %winner;
	else if(%count > 1)
		return -1;
	else
		return 0;
}

function Slayer_MinigameSO::victoryCheck_Points(%this)
{
	if(%this.isResetting())
		return -1;

	if(!%this.Gamemode.rounds || %this.points <= 0)
		return -1;

	if(isFunction("Slayer_" @ %this.mode @ "_victoryCheck_Points"))
		%special = call("Slayer_" @ %this.mode @ "_victoryCheck_Points",%this);

	if(%special >= 0 && %special !$= "")
		return %special;

	if(%this.Gamemode.teams && %this.Teams.getCount() > 0)
	{
		for(%i=0; %i < %this.Teams.getCount(); %i++)
		{
			%t = %this.Teams.getObject(%i);
			if(%t.getScore() >= %this.points)
			{
				%winner = %t;
				break;
			}
		}
	}

	if(!isObject(%winner))
	{
		for(%i=0; %i < %this.numMembers; %i++)
		{
			%cl = %this.member[%i];
			if(!isObject(%cl.slyrTeam) && %cl.score >= %this.points)
			{
				%winner = %cl;
				break;
			}
		}
	}

	if(isObject(%winner))
		return %winner;
	else
		return -1;
}

function Slayer_MinigameSO::victoryCheck_Time(%this,%ticks)
{
	if(!%this.Gamemode.rounds || %this.time <= 0 || %this.isResetting())
		return;

	//round time - total
	%time = %this.time * 60000;
	%this.timeTicks = %ticks;

	//round time - tick size
	%this.timeTickLength = (%ticks <= 0 ? 60000 : %this.timeTickLength);
	%timeTickLength = %this.timeTickLength;

	//round time - remaining
	%this.timeRemaining = (%ticks <= 0 ? %time : %this.timeRemaining - %timeTickLength);
	%timeRemaining = %this.timeRemaining;

	//round time - elapsed
	%this.timeElapsed = %time - %this.timeRemaining;
	%timeElapsed = %this.timeElapsed;

	Slayer_Support::Debug(2,"Slayer_MinigameSO::victoryCheck_Time","Time remaining:" SPC %timeRemaining);

	//CHECK IF THE GAMEMODE HAS ANYTHING TO SAY ABOUT THIS
	if(isFunction("Slayer_" @ %this.mode @ "_victoryCheck_Time"))
		%special = call("Slayer_" @ %this.mode @ "_victoryCheck_Time",%this,%ticks);
	if(%special > 0 && %special !$= "")
	{
		%this.endRound(%special);
		return;
	}
	if(%special < 0 && %special !$= "")
		return;

	if(%timeElapsed >= %time)
	{
		%least = 0;
		%count = 0;
		if(%this.Gamemode.teams && %this.Teams.getCount() > 0)
		{
			for(%i = 0; %i < %this.Teams.getCount(); %i ++)
			{
				%t = %this.Teams.getObject(%i);
				%sc = %t.getScore();

				if((%sc > %least || %t.winOnTimeUp) && !%winOnTimeUp)
				{
					%winner = %t;
					%least = %sc;
					%count = 1;

					if(%t.winOnTimeUp)
						%winOnTimeUp = 1;
				}
				else if((%sc == %least && %least > 0 && !%winOnTimeUp) || (%winOnTimeUp && %t.winOnTimeUp))
				{
					%winner = setField(%winner,%count,%t);
					%count ++;
				}
			}
		}
		for(%i = 0; %i < %this.numMembers; %i++)
		{
			%cl = %this.member[%i];
			%sc = %cl.score;

			if(isObject(%cl.getTeam()))
				continue;

			if(%sc > %least)
			{
				%winner = %cl;
				%least = %sc;
				%count = 1;
			}
			else if(%sc == %least && %least > 0)
			{
				%winner = setField(%winner,%count,%cl);
				%count ++;
			}
		}

		%this.endRound(%winner);
		return;
	}
	else
	{
		if(%timeRemaining % 60000 == 0) //only on the minute
		{
			if(%timeRemaining <= 60000) //start ticking every 10 seconds
				%this.timeTickLength = 30000;

			if(%timeRemaining % 600000 == 0) //every 10 minutes
				%announce = 1;
			else if(%timeRemaining <= 300000) // <= 5 minutes remaining
				%announce = 1;

			%minutes = %timeRemaining / 60000;
			%remain = %minutes SPC "\c5" @ (%minutes == 1 ? "minute" : "minutes");
		}
		else
		{
			if(%timeRemaining % 10000 == 0 && %timeRemaining <= 30000) //30, 20, 10 seconds remaining
			{
				if(%timeRemaining <= 10000) // <= 10 seconds remaining
					%this.timeTickLength = 5000;
				else
					%this.timeTickLength = 10000;
				%announce = 1;
			}
			else if(%timeRemaining <= 5000) // <= 5 seconds remaining
			{
				%this.timeTickLength = 1000;
				%announce = 1;
			}

			%seconds = %timeRemaining / 1000;
			%remain = %seconds SPC "\c5" @ (%seconds == 1 ? "second" : "seconds");
		}

		if(%announce && %ticks > 0)
			%this.messageAll('','\c3%1 \c5remaining.',%remain);

		cancel(%this.timeSchedule);
		%this.timeSchedule = %this.scheduleNoQuota(%this.timeTickLength,victoryCheck_Time,%ticks++);
	}
}

function Slayer_MinigameSO::onReset(%this)
{
	//START THE ROUND
	if(%this.preRoundSeconds > 0)
		%this.preRoundCountdownTick(0);
	else
		%this.startRound();
}

function Slayer_MinigameSO::preRoundCountdownTick(%this,%ticks)
{
	cancel(%this.preRoundCountdownTimer);

	if(%this.isResetting())
		return;

	%remain = %this.preRoundSeconds - %ticks;

	Slayer_Support::Debug(2,"Slayer_MinigameSO::preRoundCountdownTick",%remain);

	if(%remain <= 0)
	{
		%this.startRound();

		%this.play2dAll(Slayer_Begin_Sound);
		%this.centerPrintAll("<font:Arial Bold:100><color:ff00ff>GO!",2);
	}
	else
	{
		if(%ticks == 0)
		{
			for(%i = 0; %i < %this.numMembers; %i ++)
			{
				%cl = %this.member[%i];
				if(isObject(%cl.player))
					%cl.player.changeDatablock(PlayerFrozenArmor);
			}
		}

		%sound = "Slayer_" @ %remain @ "_Seconds_Sound";
		if(isObject(%sound))
			%this.play2dAll(%sound);
		%this.centerPrintAll("<font:Arial Bold:100><color:ff00ff>" @ %remain);

		%this.preRoundCountdownTimer = %this.scheduleNoQuota(1000,preRoundCountdownTick,%ticks ++);
	}
}

function Slayer_MinigameSO::startRound(%this)
{
	Slayer_Support::Debug(2,"Slayer_MinigameSO::startRound");
	%this.roundStarted = $Sim::Time;

	for(%i = 0; %i < %this.numMembers; %i ++)
	{
		%cl = %this.member[%i];
		if(isObject(%cl.player))
		{
			if(%cl.player.getDatablock().getID() == PlayerFrozenArmor.getID())
			{
				%t = %cl.getTeam();
				%db = (isObject(%t) ? %t.playerDatablock : %this.playerDatablock);
				%cl.player.changeDatablock(%db);
			}
		}
		else if(!%cl.dead())
			%cl.instantRespawn();
	}

	//begin the winTick
	if(%this.Gamemode.rounds && %this.Time > 0)
		%this.victoryCheck_Time(0);
}

function Slayer_MinigameSO::endRound(%this,%winner,%resetTime)
{
	if(%this.isResetting())
		return;
	%this.setResetting(1);

	if(%resetTime $= "")
	{
		%resetTime = %this.timeBetweenRounds * 1000;
	}

	%seconds = mCeil(%resetTime / 1000);

	Slayer_Support::Debug(1,"Round won",%winner TAB "resetTime:" SPC %resetTime);

	if(getField(%winner,0) $= "CUSTOM")
	{
		%count = 1;
		%nameList = getField(%winner,1);
	}
	else //this generates the list of winner names
	{
		%count = 0;
		for(%i=0; %i < getFieldCount(%winner); %i++)
		{
			%w = getField(%winner,%i);
			if(!isObject(%w))
				continue;
			if(getMinigameFromObject(%w) != %this)
				continue;

			if(%w.class $= "Slayer_TeamSO")
			{
				%name = "<sPush>" @ %w.getColoredName() @ "<sPop>";

				for(%e=0; %e < %w.numMembers; %e++)
				{
					%cl = %w.member[%e];
					if(isObject(%cl.player))
						%cl.player.emote(winStarProjectile);
				}
			}
			else if(%w.getClassName() $= "GameConnection")
			{
				%name = "<sPush><color:ffff00>" @ %w.getPlayerName() @ "<sPop>";

				if(isObject(%w.player))
					%w.player.emote(winStarProjectile);
			}
			else
				continue;

			%w.wins ++;

			%winner[%count] = %w;
			%count ++;

			if(%nameList $= "")
				%nameList = %name;
			else
				%nameList = %nameList @ "," SPC %name;
		}
	}

	if(isFunction("Slayer_" @ %this.mode @ "_preVictory"))
		call("Slayer_" @ %this.mode @ "_preVictory",%this,%winner,%nameList);

	if(%count <= 0)
		%msg = '\c5Nobody won this round. Resetting in %4 seconds.';
	else
	{
		if(%count > 1)
			%msg = '<color:ff00ff>%1 tied this round. Resetting in %4 seconds.';
		else if(isObject(%winner))
		{
			%score = %winner.getScore();
			if(%score > 0)
				%msg = '<color:ff00ff>%1 won this round with a score of \c3%2\c5. %1 has won \c3%3\c5. Resetting in %4 seconds.';
			else
				%msg = '<color:ff00ff>%1 won this round. %1 has won \c3%3\c5. Resetting in %4 seconds.';
		}
		else
			%msg = '<color:ffff00>%1 \c5won this round. Resetting in %4 seconds.';
	}

	%winnerCam = (%count == 1 && isObject(%winner) && %winner.getClassName() $= "GameConnection" && isObject(%winner.player));
	for(%i=0; %i < %this.numMembers; %i++)
	{
		%cl = %this.member[%i];
		%cl.setDead(1);

		if(!%this.allowMoveWhileResetting)
		{
			if(%winnerCam)
				%cl.camera.setMode(corpse,%winner.player);
			else
			{
				if(isObject(%cl.player))
					%cl.camera.setMode(corpse,%cl.player);
			}
			%cl.setControlObject(%cl.camera);
		}

		//END OF ROUND REPORT
		if(%this.eorrEnable)
		{
			if(%cl.slayer)
				commandToClient(%cl,'Slayer_ForceGUI',"Slayer_Scores",1);
			else
				messageClient(%cl,'',"You cannot see the End of Round Report because you don't have <a:forum.returntoblockland.com/dlm/viewFile.php?id=3755>Slayer</a> installed.");
		}
	}

	%points = %score SPC (%score == 1 ? "\c5point" : "\c5points");
	%wins = (%winner.wins == 1 ? "once" : %winner.wins SPC "\c5times");

	//tell everybody who won
	%this.messageAll('',%msg,%nameList,%points,%wins,%seconds);

	//populate end of round report
	if(%this.eorrEnable)
		%this.sendScoreListAll();

	for(%i=0; %i < %seconds; %i++)
	{
		%remaining = %seconds - %i;
		%timeLeft = %remaining SPC (%remaining == 1 ? "second" : "seconds");
		%this.resetCountdownTimer[%i] = %this.schedule(%i*1000,bottomPrintAll,"<just:center>\c5Resetting in" SPC %timeLeft @ ".",2,1);
	}
	%this.resetCountdownTimerCount = %seconds;

	%this.resetTimer = %this.schedule(%resetTime,Reset);

	if(isFunction("Slayer_" @ %this.mode @ "_postVictory"))
		call("Slayer_" @ %this.mode @ "_postVictory",%this,%winner,%nameList);
}

//this is a stub function
function Slayer_MinigameSO::Cycle(%this,%winner)
{
	%this.endRound(%winner);
}

function Slayer_MinigameSO::sendScoreListAll(%this)
{
	//SORT IN SCORE ORDER
	%memberList = %this.getMemberListSortedScore();

	%tabBreaks = "150,250,350,450";
	%header = '<font:arial:16><h2>%1\t%2\t%3\t%4\t%5</h2>';

	%var1 = "Name";
	%var2 = "Score";
	%var3 = "Kills";
	%var4 = "Deaths";
	%var5 = "Rounds Won";

	if(isFunction("Slayer_" @ %this.mode @ "_scoreListInit"))
		%custom = call("Slayer_" @ %this.mode @ "_scoreListInit",%this,%tabBreaks,%header,%var1,%var2,%var3,%var4,%var5,%var6,%var7,%var8,%var9,%var10,%var11,%var12,%var13,%var14,%var15);
	if(getField(%custom,0))
	{
		%tabBreaks = getField(%custom,1);
		%header = getField(%custom,2);
		for(%e = 3; %e < getFieldCount(%custom); %e ++)
			%var[%e - 2] = getField(%custom,%e);
	}

	//INIT LISTS ALL
	%this.commandToAllSlayerClients('Slayer_scoreListInit',%tabBreaks,%header,%var1,%var2,%var3,%var4,%var5,%var6,%var7,%var8,%var9,%var10,%var11,%var12,%var13,%var14,%var15);

	%isScoreListAdd = isFunction("Slayer_" @ %this.mode @ "_scoreListAdd");

	if(isFunction("Slayer_" @ %this.mode @ "_scoreListCheckSendTeams"))
		%sendTeams = call("Slayer_" @ %this.mode @ "_scoreListCheckSendTeams",%this);
	%sendTeams = (%sendTeams $= "" || %sendTeams);

	//SEND TEAMS ALL
	if(%this.Gamemode.teams && %this.Teams.getCount() > 0 && %this.eorrDisplayTeamScores && %sendTeams)
	{
		%this.commandToAllSlayerClients('Slayer_scoreListAdd',"<b>Teams:</b>");

		%teamList = %this.Teams.getTeamListSortedScore();

		for(%i = 0; %i < %this.Teams.getCount(); %i ++)
		{
			%t = getField(%teamList,%i);
			%var1 = %t.colorHex;
			%var2 = %t.name;
			%var3 = %t.getScore();
			%var4 = %t.getKills();
			%var5 = %t.getDeaths();
			%var6 = %t.wins;

			%line = '<colorHex:%1><b>%2</b></color>\t%3\t%4\t%5\t%6';

			if(%isScoreListAdd)
				%custom = call("Slayer_" @ %this.mode @ "_scoreListAdd",%this,%t,%line,%var1,%var2,%var3,%var4,%var5,%var6,%var7,%var8,%var9,%var10,%var11,%var12,%var13,%var14,%var15);
			if(getField(%custom,0))
			{
				%line = getField(%custom,1);
				for(%e = 2; %e < getFieldCount(%custom); %e ++)
					%var[%e - 1] = getField(%custom,%e);
			}

			%this.commandToAllSlayerClients('Slayer_scoreListAdd',%line,%var1,%var2,%var3,%var4,%var5,%var6,%var7,%var8,%var9,%var10,%var11,%var12,%var13,%var14,%var15);
		}

		%this.commandToAllSlayerClients('Slayer_scoreListAdd',"<br><b>Players:</b>");
	}

	//SEND PLAYERS ALL
	for(%i = 0; %i < %this.numMembers; %i ++)
	{
		%cl = getField(%memberList,%i);
		%team = %cl.getTeam();
		%var1 = (isObject(%team) ? %team.colorHex : "ffffff");
		%var2 = %cl.getPlayerName();
		%var3 = %cl.getScore();
		%var4 = %cl.getKills();
		%var5 = %cl.getDeaths();
		%var6 = %cl.wins;

		%line = '<colorHex:%1><b>%2</b></color>\t%3\t%4\t%5\t%6';

		if(%isScoreListAdd)
			%custom = call("Slayer_" @ %this.mode @ "_scoreListAdd",%this,%cl,%line,%var1,%var2,%var3,%var4,%var5,%var6,%var7,%var8,%var9,%var10,%var11,%var12,%var13,%var14,%var15);
		if(getField(%custom,0))
		{
			%line = getField(%custom,1);
			for(%e = 2; %e < getFieldCount(%custom); %e ++)
				%var[%e - 1] = getField(%custom,%e);
		}

		%this.commandToAllSlayerClients('Slayer_scoreListAdd',%line,%var1,%var2,%var3,%var4,%var5,%var6,%var7,%var8,%var9,%var10,%var11,%var12,%var13,%var14,%var15);
	}
}

function Slayer_MinigameSO::roundStarted(%this)
{
	return %this.roundStarted;
}

function Slayer_MinigameSO::isResetting(%this)
{
	return %this.isResetting;
}

function Slayer_MinigameSO::setResetting(%this,%flag)
{
	%this.isResetting = %flag;

	if(!%flag)
		%this.roundStarted = $Sim::Time;
}

function Slayer_MinigameSO::resetVehicles(%this)
{
	for(%i = 0; %i < vehicleSpawnGroup.getCount(); %i ++)
	{
		%br = vehicleSpawnGroup.getObject(%i);
		if(minigameCanUse(%this,%br))
			%br.respawnVehicle();
	}
}

function Slayer_MinigameSO::resetBots(%this)
{
	if(!isObject(mainBrickGroup))
		return;

	for(%i=0; %i < mainBrickGroup.getCount(); %i++)
	{
		%bg = mainBrickGroup.getObject(%i);
		if(miniGameCanUse(%this,%bg))
		{
			hResetBots(%bg);
		}
	}
}

function Slayer_MinigameSO::resetCapturePoints(%this,%client)
{
	for(%i=0; %i < Slayer.capturePoints.getCount(); %i++)
	{
		%cp = Slayer.capturePoints.getObject(%i);

		if(minigameCanUse(%this,%cp))
			%cp.setCPControl(%cp.origColor,1,%client);
	}
}

function Slayer_MinigameSO::resetBricks(%this)
{
	if(!isObject(mainBrickGroup))
		return;

	//respawn all dead bricks
	for(%i=0; %i < mainBrickGroup.getCount(); %i++)
	{
		%bg = mainBrickGroup.getObject(%i);
		if(%bg.bl_id == 888888 || %bg.bl_id == 999999)
			continue;
		if(miniGameCanUse(%this,%bg))
		{
			%isPublic = (Slayer_Support::getBLIDFromObject(%bg) $= "888888");
			for(%e=0; %e < %bg.getCount(); %e++)
			{
				%br = %bg.getObject(%e);
				if(%br.isFakeDead())
					%br.respawn();
				if(!%isPublic)
					%br.onMiniGameReset();
			}
		}
	}
}

function Slayer_MinigameSO::getMemberListSortedScore(%this)
{
	%count = %this.numMembers;
	for(%i = 0; %i < %count; %i ++)
		%list = setField(%list,getFieldCount(%list),%this.member[%i]);

	while(!%done)
	{
		%done = 1;
		for(%i = 0; %i < %count; %i ++)
		{
			%e = %i + 1;
			if(%e >= %count)
				continue;
			%f1 = getField(%list,%i);
			%f2 = getField(%list,%e);
			if(%f1.score < %f2.score)
			{
				%list = Slayer_Support::swapItemsInList(%list,%i,%e);
				%done = 0;
			}
		}
		%count --;
	}
	return %list;
}

function Slayer_MinigameSO::getLiving(%this)
{
	%living = 0;
	for(%i=0; %i < %this.numMembers; %i++)
	{
		if(!%this.member[%i].dead())
			%living ++;
	}
	return %living;
}

function Slayer_MinigameSO::setPref(%this,%category,%title,%value)
{
	Slayer.Prefs.setPref(%category,%title,%value,%this);
}

function Slayer_MinigameSO::getPref(%this,%category,%title)
{
	return Slayer.Prefs.getPref(%category,%title,%this);
}

function Slayer_MinigameSO::resetPrefs(%this)
{
	Slayer.Prefs.resetPrefs(%this);
}

function Slayer_MinigameSO::updateGamemode(%this,%mode,%current)
{
	%mode = Slayer.Gamemodes.getModeFromFName(%mode);
	%current = Slayer.Gamemodes.getModeFromFName(%current);

	if(!isObject(%mode))
	{
		%this.mode = %current.fName;
		%this.Gamemode = %current;
		return;
	}

	if(isFunction("Slayer_" @ %current.fName @ "_onModeEnd"))
		call("Slayer_" @ %current.fName @ "_onModeEnd",%this);

	if(%current.teams && !%mode.teams)
		%this.Teams.removeAllMembers();
	else if(%mode.teams)
	{
		for(%i=0; %i < %this.numMembers; %i++)
		{
			%cl = %this.member[%i];
			if(!isObject(%cl.slyrTeam))
				%this.Teams.autoSort(%cl);
		}
	}

	%this.mode = %mode.fName;
	%this.Gamemode = %mode;

	if(isFunction("Slayer_" @ %this.mode @ "_onModeStart"))
		call("Slayer_" @ %this.mode @ "_onModeStart",%this);
}

function Slayer_MinigameSO::updateLives(%this,%new,%old)
{
	for(%i=0; %i < %this.numMembers; %i++)
	{
		%cl = %this.member[%i];

		%t = %cl.getTeam();
		if(isObject(%t) && %t.lives >= 0)
			continue;

		if(%new <= 0)
		{
			%cl.setLives(0);
			if(%cl.dead())
			{
				%cl.setDead(0);
				if(!isObject(%cl.player))
					%cl.instantRespawn();
			}
			continue;
		}

		if(%old $= "")
			%cl.setLives(%new);
		else
			%cl.addLives(%new-%old);

		%lives = %cl.getLives();
		if(%lives <= 0 && !%cl.dead())
		{
			%cl.setDead(1);
			if(isObject(%cl.player))
			{
				%cl.camera.setMode(observer);
				%cl.setControlObject(%cl.camera);
				%cl.player.delete();
			}
			%cl.centerPrint("\c5The number of lives was reduced. You are now out of lives.",5);

			%check = 1;
		}
		else if(%lives > 0 && %cl.dead())
		{
			%cl.setDead(0);
			if(!isObject(%cl.player))
				%cl.instantRespawn();
		}
	}

	if(%check)
	{
		%win = %this.victoryCheck_Lives();
		if(%win >= 0)
			%this.endRound(%win);
	}
}

function Slayer_MinigameSO::updatePoints(%this,%new,%old)
{
	%this.victoryCheck_Points();
}

function Slayer_MinigameSO::updateTime(%this,%new,%old)
{
	if(%new <= 0 && %old > 0)
		cancel(%this.timeSchedule);
	else if(%new > 0 && %old <= 0 && %this.Gamemode.rounds)
	{
		cancel(%this.timeSchedule);
		%this.victoryCheck_Time(0);
	}
}

function Slayer_MinigameSO::updateRespawnTime(%this,%type,%flag,%old)
{
	%oldTime = %old * 1000;

	if(%flag == -1)
		%time = -1;
	else
	{
		%time = %flag * 1000;
		%time = Slayer_Support::mRestrict(%time,-999999,999999);
	}

	switch$(%type)
	{
		case "player":
			%this.respawnTime = %time;

			for(%i=0; %i < %this.numMembers; %i++)
			{
				%cl = %this.member[%i];
				%t = %cl.getTeam();
				if(isObject(%t) && %t.lives >= 0)
					continue;
				if(%cl.dynamicRespawnTime == %oldTime)
					%cl.setDynamicRespawnTime(%time);
			}

		case "bot":
			%this.botRespawnTime = %time;

		case "brick":
			%this.brickRespawnTime = %time;

		case "vehicle":
			%this.vehicleRespawnTime = %time;

		case "penalty_FF":
			%this.friendlyFireRespawnPenalty = %time;

		default:
			if(%type !$= "")
			{
				%this.respawnTime_[%type] = %time;
			}
	}
}

function Slayer_MinigameSO::updateNameDistance(%this,%flag)
{
	for(%i=0; %i < %this.numMembers; %i++)
	{
		%cl = %this.member[%i];
		if(isObject(%cl.player))
			%cl.player.setShapeNameDistance(%flag);
	}
}

function Slayer_MinigameSO::updateDefaultMinigame(%this,%flag)
{
	if(%this.isStarting)
		return;

	if(%flag)
		Slayer.Minigames.setDefaultMinigame(%this);
	else if(%this == Slayer.Minigames.defaultMinigame)
		Slayer.Minigames.setDefaultMinigame(0);
}

//different than the default forceEquip
//because it only forces the item if the player hasn't changed it
function Slayer_MinigameSO::updateEquip(%this,%slot,%new,%old)
{
	for(%i=0; %i < %this.numMembers; %i++)
	{
		%cl = %this.member[%i];
		if(!isObject(%cl.getTeam()))
		{
			%cl.updateEquip(%slot,%new,%old);
		}
	}
}

//I had to rewrite this to make spawns work properly
function Slayer_MinigameSO::pickSpawnPoint(%this,%client)
{
	if(!%this.useSpawnBricks)
		return pickSpawnPoint();
	if(Slayer.Spawns.getCount() < 1)
		return pickSpawnPoint();

	if(isFunction("Slayer_" @ %this.mode @ "_pickSpawnPoint"))
		%specialSpawn = call("Slayer_" @ %this.mode @ "_pickSpawnPoint",%this,%client);

	if(isObject(%specialSpawn))
		%spawn = %specialSpawn;
	else
	{
		%team = %client.getTeam();
		%count = 0;
		%openCount = 0;

		if(isObject(%team))
		{
			for(%i=0; %i < Slayer.Spawns.getCount(); %i++)
			{
				%sp = Slayer.Spawns.getObject(%i);
				%db = %sp.getDatablock();
				%type = %db.slyrType;
				%color = %sp.getControl();
	
				if(%type !$= "TeamSpawn")
					continue;

				if(!minigameCanUse(%client,%sp))
					continue;
	
				if(%color == %team.color)
				{
					if(!%sp.isBlocked())
					{
						%openSpawn[%openCount] = %sp;
						%openCount ++;
					}
					%spawn[%count] = %sp;
					%count ++;
				}
			}
		}

		if(%count < 1)
		{
			for(%i=0; %i < Slayer.Spawns.getCount(); %i++)
			{
				%sp = Slayer.Spawns.getObject(%i);
				%db = %sp.getDatablock();
	
				if(%db.getName() !$= "brickSpawnPointData")
					continue;

				if(!minigameCanUse(%client,%sp))
					continue;

				if(%this.spawnBrickColor >= 0 && %sp.getColorID() != %this.spawnBrickColor)
					continue;

				if(!%sp.isBlocked())
				{
					%openSpawn[%openCount] = %sp;
					%openCount ++;
				}
				%spawn[%count] = %sp;
				%count ++;
			}
		}

		Slayer_Support::Debug(2,"Slayer_MinigameSO::pickSpawnPoint","open:" SPC %openCount SPC "total:" SPC %count);

		if(%count < 1)
			return pickSpawnPoint();
		else if(%openCount > 0)
		{
			%r = getRandom(0,%openCount-1);
			%spawn = %openSpawn[%r];
		}
		else
		{
			%r = getRandom(0,%count-1);
			%spawn = %spawn[%r];
		}
	}

	if(isObject(%spawn))
		return %spawn.getSpawnPoint();
	else
		return pickSpawnPoint();
}

function Slayer_MinigameSO::messageAllDead(%this,%cmd,%msg,%a,%b,%c,%d,%e,%f,%g,%h,%i,%j,%k,%l,%m,%n,%o,%p,%q)
{
	for(%i=0; %i < %this.numMembers; %i++)
	{
		%cl = %this.member[%i];
		if(%cl.dead())
			messageClient(%cl,%cmd,%msg,%a,%b,%c,%d,%e,%f,%g,%h,%i,%j,%k,%l,%m,%n,%o,%p,%q);
	}
}

function Slayer_MinigameSO::commandToAll(%this,%a,%b,%c,%d,%e,%f,%g,%h,%i,%j,%k,%l,%m,%n,%o,%p,%q,%r,%s)
{
	for(%i = 0; %i < %this.numMembers; %i ++)
		commandToClient(%this.member[%i],%a,%b,%c,%d,%e,%f,%g,%h,%i,%j,%k,%l,%m,%n,%o,%p,%q,%r,%s);
}

function Slayer_MinigameSO::commandToAllSlayerClients(%this,%a,%b,%c,%d,%e,%f,%g,%h,%i,%j,%k,%l,%m,%n,%o,%p,%q,%r,%s)
{
	for(%i = 0; %i < %this.numMembers; %i ++)
	{
		%cl = %this.member[%i];
		if(%cl.slayer)
			commandToClient(%cl,%a,%b,%c,%d,%e,%f,%g,%h,%i,%j,%k,%l,%m,%n,%o,%p,%q,%r,%s);
	}
}

function Slayer_MinigameSO::play2dAll(%this,%sound)
{
	for(%i=0; %i < %this.numMembers; %i++)
		%this.member[%i].play2d(%sound);
}

//for events
function Slayer_MinigameSO::centerPrintAll(%this,%msg,%time,%client)
{
	if(isObject(%client))
		%msg = strReplace(%msg,"%1",%client.getPlayerName());

	for(%i=0; %i < %this.numMembers; %i++)
		%this.member[%i].centerPrint(%msg,%time);
}

//for events
function Slayer_MinigameSO::bottomPrintAll(%this,%msg,%time,%hideBar,%client)
{
	if(isObject(%client))
		%msg = strReplace(%msg,"%1",%client.getPlayerName());

	for(%i=0; %i < %this.numMembers; %i++)
		%this.member[%i].bottomPrint(%msg,%time,%hideBar);
}

//for events
function Slayer_MinigameSO::chatMsgAll(%this,%msg,%client)
{
	if(isObject(%client))
		%msg = strReplace(%msg,"%1",%client.getPlayerName());

	for(%i=0; %i < %this.numMembers; %i++)
		%this.member[%i].chatMessage(%msg);
}

//for events
function Slayer_MinigameSO::respawnAll(%this,%client)
{
	for(%i=0; %i < %this.numMembers; %i++)
		%this.member[%i].instantRespawn();
}

//for events
function Slayer_MinigameSO::incTime(%this,%flag,%display)
{
	%inc = %flag * 60000;

	//round to the nearest 30 seconds
	%rmndr = %this.timeRemaining % 30000;
	if(%rmndr >= 15000)
	{
		%this.timeTickLength = 60000;
		%remain = %this.timeRemaining + (30000 - %rmndr);
	}
	else
	{
		%this.timeTickLength = 30000;
		%remain = %this.timeRemaining - %rmndr;
	}

	if(%display)
	{
		%min = (%flag == 1 ? "minute" : "minutes");
		%this.messageAll('',"\c5The round has been extended by\c3" SPC %flag SPC "\c5" @ %min @ ".");
	}

	%this.timeRemaining = %remain + %inc + %this.timeTickLength;
	%this.victoryCheck_time(%this.timeTicks + 1);
}

//for events
function Slayer_MinigameSO::setTime(%this,%flag,%display)
{
	%time = %flag * 60000;

	if(%time > 1)
		%this.timeTickLength = 60000;
	else
		%this.timeTickLength = 30000;

	if(%display)
	{
		%min = (%flag == 1 ? "minute" : "minutes");
		%this.messageAll('',"\c5The round has been set to\c3" SPC %flag SPC "\c5" @ %min @ ".");
	}

	%this.timeRemaining = %time + %this.timeTickLength;
	%this.victoryCheck_time(%this.timeTicks + 1);
}

//for events
function MinigameSO::Slayer_setPref(%this,%category,%title,%value,%client)
{
	if(!isSlayerMinigame(%this))
		return;

	%pref = Slayer.Prefs.getPrefSO(%category,%title);
	if(!isObject(%pref))
		return;

	if((!isObject(%client) && %pref.editRights == -1) || %this.canEdit(%client,%pref.editRights))
		%pref.setValue(%value,%this);
}

function Slayer_MinigameSO::canDamage(%this,%objA,%classA,%objB,%classB)
{
	Slayer_Support::Debug(3,"Slayer_MinigameSO::canDamage","A:" SPC %objA SPC %classA SPC "B:" SPC %objB SPC %classB);

	if(%classA $= "fxDtsBrick")
		return %this.brickDamage;
	else if(%classA $= "Player")
	{
		if(!%this.weaponDamage && Slayer_Support::isVehicle(%objB))
			return %this.vehicleDamage;

		return %this.weaponDamage;
	}
	else if(%classA $= "AiPlayer")
	{
		return %this.botDamage;
	}
	else if(%classA $= "GameConnection" || %classA $= "AiConnection")
	{
		if(%classB $= "fxDtsBrick")
			return %this.brickDamage;

		return %this.weaponDamage;
	}
 	else if(Slayer_Support::isVehicle(%objA))
	{
		return %this.vehicleDamage;
	}

	return 1;
}

//for events
function MinigameSO::Win(%this,%mode,%flag,%client)
{
	if(isObject(%client) && getMinigameFromObject(%client) != %this)
		return;

	if(!isSlayerMinigame(%this))
		return;

	switch(%mode)
	{
		case 0: //TRIGGERPLAYER
			if(isObject(%client))
				%this.endRound(%client);

		case 1: //TRIGGERTEAM
			if(isObject(%client))
			{
				%team = %client.getTeam();
				if(isObject(%team))
					%this.endRound(%team);
			}

		case 2: //CUSTOMPLAYER
			%cl = findClientByName(%flag);
			if(isObject(%cl))
				%this.endRound(%cl);

		case 3: //CUSTOMTEAM
			%team = %this.Teams.getTeamFromName(%flag);
			if(isObject(%team))
				%this.endRound(%team);

		case 4: //CUSTOMSTRING
			%this.endRound("CUSTOM" TAB %flag);

		case 5: //NONE
			%this.endRound();
	}
}

function Slayer_MinigameSO::getFakeClient(%this)
{
	if(!isObject(%this.fakeClient))
	{
		%blid = %this.creatorBLID;
		%brickgroup = nameToID("BrickGroup_" @ %blid);

		%this.fakeClient = new AiClient()
		{
			name = "Slayer";
			netName = "Slayer";
			LANName = "Slayer";
			bl_id = %blid;

			minigame = %this;

			brickgroup = %brickgroup;

			isSlayerFakeClient = 1;
		};
	}

	Slayer_Support::Debug(2,"Getting fake client",%this.fakeClient);

	return %this.fakeClient;
}

//PERMISSIONS
function MinigameSO::canEdit(%this,%client,%overRide)
{
	if(!isObject(%client))
		return 0;

	if(!isSlayerMinigame(%this))
		return 0;

	if(%client.isHost)
		return 1;

	%edit = %this.getPref("Permissions","Edit Rights");

	%brickgroup = "Brickgroup_" @ %this.creatorBLID;

	switch(%edit)
	{
		case 0:
			%canEdit = %client.isHost;

		case 1:
			%canEdit = %client.isSuperAdmin;

		case 2:
			%canEdit = %client.isAdmin;

		case 3:
			%canEdit = (getTrustLevel(%client,%brickgroup) >= 3 || %client.getBLID() == %this.creatorBLID);

		case 4:
			%canEdit = (getTrustLevel(%client,%brickgroup) >= 2);

		case 5:
			%canEdit = (getTrustLevel(%client,%brickgroup) >= 1);

		default:
			Slayer_Support::Error("MinigameSO::canEdit","[Permissions | Edit Rights] is invalid.");
			%canEdit = 0;
	}

	if(%canEdit && %overRide !$= "")
	{
		switch(%overRide)
		{
			case 0:
				%canEdit = %client.isHost;

			case 1:
				%canEdit = %client.isSuperAdmin;

			case 2:
				%canEdit = %client.isAdmin;

			case 3:
				%canEdit = (getTrustLevel(%client,%brickgroup) >= 3 || %client.getBLID() == %this.creatorBLID);

			case 4:
				%canEdit = (getTrustLevel(%client,%brickgroup) >= 2);

			case 5:
				%canEdit = (getTrustLevel(%client,%brickgroup) >= 1);
		}
	}

	return %canEdit;
}

function MinigameSO::canReset(%this,%client,%overRide)
{
	if(!isObject(%client))
		return 0;

	if(!isSlayerMinigame(%this))
		return 0;

	if(%client.isHost)
		return 1;

	if(%client.getBLID() == %this.creatorBLID)
		return 1;

	%reset = %this.getPref("Permissions","Reset Rights");

	%brickgroup = "Brickgroup_" @ %this.creatorBLID;

	switch(%reset)
	{
		case 0:
			%canReset = %client.isHost;

		case 1:
			%canReset = %client.isSuperAdmin;

		case 2:
			%canReset = %client.isAdmin;

		case 3:
			%canReset = (getTrustLevel(%client,%brickgroup) >= 3);

		case 4:
			%canReset = (getTrustLevel(%client,%brickgroup) >= 2);

		case 5:
			%canReset = (getTrustLevel(%client,%brickgroup) >= 1);

		default:
			Slayer_Support::Error("MinigameSO::canReset","[Permissions | Reset Rights] is invalid.");
			%canReset = 0;
	}

	if(%canReset && %overRide !$= "")
	{
		switch(%overRide)
		{
			case 0:
				%canReset = %client.isHost;

			case 1:
				%canReset = %client.isSuperAdmin;

			case 2:
				%canReset = %client.isAdmin;

			case 3:
				%canReset = (getTrustLevel(%client,%brickgroup) >= 3);

			case 4:
				%canReset = (getTrustLevel(%client,%brickgroup) >= 2);

			case 5:
				%canReset = (getTrustLevel(%client,%brickgroup) >= 1);
		}
	}

	return %canReset;
}

function MinigameSO::canLeave(%this,%client,%overRide)
{
	if(!isObject(%client))
		return 0;

	if(!isSlayerMinigame(%this))
		return 0;

	if(!%this.isDefaultMinigame)
		return 1;

	if(%client.isHost)
		return 1;

	if(%client.getBLID() == %this.creatorBLID)
		return 1;

	%leave = %this.getPref("Permissions","Leave Rights");

	%brickgroup = "Brickgroup_" @ %this.creatorBLID;

	switch(%leave)
	{
		case 0:
			%canLeave = %client.isHost;

		case 1:
			%canLeave = %client.isSuperAdmin;

		case 2:
			%canLeave = %client.isAdmin;

		case 3:
			%canLeave = (getTrustLevel(%client,%brickgroup) >= 3);

		case 4:
			%canLeave = (getTrustLevel(%client,%brickgroup) >= 2);

		case 5:
			%canLeave = (getTrustLevel(%client,%brickgroup) >= 1);

		default:
			Slayer_Support::Error("MinigameSO::canLeave","[Permissions | Leave Rights] is invalid.");
			%canLeave = 0;
	}

	if(%canLeave && %overRide !$= "")
	{
		switch(%overRide)
		{
			case 0:
				%canLeave = %client.isHost;

			case 1:
				%canLeave = %client.isSuperAdmin;

			case 2:
				%canLeave = %client.isAdmin;

			case 3:
				%canLeave = (getTrustLevel(%client,%brickgroup) >= 3);

			case 4:
				%canLeave = (getTrustLevel(%client,%brickgroup) >= 2);

			case 5:
				%canLeave = (getTrustLevel(%client,%brickgroup) >= 1);
		}
	}

	return %canLeave;
}

function isSlayerMinigame(%mini)
{
	if(!isObject(%mini))
		return 0;

	if(!%mini.isSlayerMinigame)
		return 0;

	if(!isObject(Slayer.Minigames) || !Slayer.Minigames.isMember(%mini))
		return 0;

	return 1;
}

// +------------+
// | serverCmds |
// +------------+
function serverCmdSlayer(%client,%cmd,%a,%b,%c,%d,%e,%f,%g,%h,%i,%j,%k,%l,%m,%n,%o,%p,%q,%r,%s,%t)
{
	if(%client.isSpamming())
		return;

	%mini = getMinigameFromObject(%client);

	switch$(%cmd)
	{
		case "edit":
			if(isObject(%a) || %a == -1)
				%minigame = %a;
			else
				%minigame = %mini;

			if(isSlayerMinigame(%minigame))
			{
				if(!%minigame.canEdit(%client))
				{
					messageClient(%client,'',"\c5You don't have permission to edit that minigame!");
					return;
				}
			}
			else
			{
				if(!Slayer.Minigames.canCreate(%client))
				{
					messageClient(%client,'',"\c5You don't have permission to create a minigame!");
					return;
				}
			}

			if(!%client.slayer)
			{
				if(%client.slayerVersion !$= "")
					commandToClient(%client,'messageBoxOK',"Slayer | Error","Version Mismatch!\n\nYou appear to be running a different version of Slayer than the server. Make sure you have the latest version.\n\n<a:forum.returntoblockland.com/dlm/viewFile.php?id=3755>Click for the latest version.</a>\n\nYour Version:" SPC %client.slayerVersion);
				else
					commandToClient(%client,'messageBoxOK',"Slayer | Error","Missing GUI\n\nThis server is running the Slayer gamemode. To change the settings for Slayer, you need to download and install the Slayer GUI.\n\n<a:forum.returntoblockland.com/dlm/viewFile.php?id=3755>Click for the latest version.</a>\n\nServer Version:" SPC $Slayer::Server::Version @ "\nYour Version: N/A");
				return;
			}

			Slayer.sendData(%client,%minigame,"Slayer_Main");

		case "reset":
			if(isSlayerMinigame(%mini))
			{
				if(!%mini.isResetting)
				{
					if(%mini.canReset(%client))
					{
						%mini.Reset(%client);
					}
				}
			}

		case "end":
			if(isObject(%client.editingMinigame) || %client.editingMinigame == -1)
			{
				commandToClient(%client,'Slayer_ForceGUI',"ALL",0);
				%minigame = %client.editingMinigame;
			}
			else
				%minigame = %mini;

			if(isSlayerMinigame(%minigame))
			{
				if(%minigame.canEdit(%client))
				{
					%minigame.endGame();
				}
			}

		case "rules":
			if(isSlayerMinigame(%mini))
			{
				%team = %client.getTeam();
				%lifeCount = ((isObject(%team) && %team.lives >= 0) ? %team.lives : %mini.lives);
				%lives = "\c3" @ %lifeCount SPC "\c5" @ (%lifeCount == 1 ? "life" : "lives");
				%points = "\c3" @ %mini.points SPC "\c5" @ (%mini.points == 1 ? "point" : "points");
				%time = "\c3" @ %mini.time SPC "\c5" @ (%mini.time == 1 ? "minute" : "minutes");

				%person = (%mini.Gamemode.teams ? "team" : "man");

				messageClient(%client,'',"\c3" @ %mini.Gamemode.uiName);
				if(%lifeCount > 0)
					messageClient(%client,'','\c5 + %1 - The last %2 standing wins.',%lives,%person);
				if(%mini.points > 0)
					messageClient(%client,'','\c5 + %1 - The first %2 to reach %1 wins.',%points,%person);
				if(%mini.time > 0)
					messageClient(%client,'','\c5 + %1 - The %2 with the highest score after %1 wins.',%time,%person);
				if(%mini.customRule !$= "")
					messageClient(%client,'',"\c5 +" SPC %mini.customRule);

				if(isFunction("Slayer_" @ %mini.mode @ "_onRules"))
					call("Slayer_" @ %mini.mode @ "_onRules",%mini,%client);
			}

		case "dump":
			%msg = '\c5[\c3%1\c5|\c3%2\c5] \c3%3';
			for(%i = 0; %i < Slayer.Prefs.getCount();%i ++)
			{
				%pref = Slayer.Prefs.getObject(%i);
				if(striPos(%pref.variable,"%mini") >= 0 && !isObject(%mini))
					%val = "";
				else
					%val = %pref.getValue(%mini);
				messageClient(%client,'',%msg,%pref.category,%pref.title,%val);
			}

		default:
			messageClient(%client,'',"\c5This server is running \c3Slayer v" @ $Slayer::Server::Version @ "\c5, by Greek2me (Blockland ID 11902).");
	}
}

// +--------------------+
// | Packaged Functions |
// +--------------------+
package Slayer_Dependencies_Minigames
{
	function serverCmdCreateMinigame(%client,%a,%b,%c,%d,%e,%f,%g)
	{
		%mini = getMinigameFromObject(%client);

		if(isSlayerMinigame(%mini))
		{
			if(%mini.canLeave(%client))
			{
				%mini.removeMember(%client);
				parent::serverCmdCreateMinigame(%client,%a,%b,%c,%d,%e,%f,%g);
			}
			else
				messageClient(%client,'',"\c5You don't have permission to do that.");
		}
		else
			parent::serverCmdCreateMinigame(%client,%a,%b,%c,%d,%e,%f,%g);
	}

	function serverCmdLeaveMinigame(%client)
	{
		%mini = getMinigameFromObject(%client);

		if(!isSlayerMinigame(%mini) || %can = %mini.canLeave(%client))
			parent::serverCmdLeaveMinigame(%client);
		else if(!%can)
		{
			messageClient(%client,'',"\c5You don't have permission to do that.");
			return;
		}
	}

	function serverCmdJoinMinigame(%client,%minigame)
	{
		%mini = getMinigameFromObject(%client);

		if(isSlayerMinigame(%mini))
		{
			if(!%mini.canLeave(%client))
				return;
		}

		if(isSlayerMinigame(%minigame))
		{
			if(%mini == %minigame)
			{
				messageClient(%client,'','You\'re already in that mini-game.');
				return;
			}
			else if(%minigame.inviteOnly && %client.minigameInvitePending != %minigame && %mini.creatorBLID == %client.getBLID())
			{
				messageClient(%client,'','That mini-game is invite-only.');
				return;
			}
			else
				%minigame.addMember(%client);
		}
		else
			parent::serverCmdJoinMinigame(%client,%minigame);
	}

	function serverCmdResetMinigame(%client)
	{
		%mini = getMinigameFromObject(%client);
		if(isSlayerMinigame(%mini) && %mini.canReset(%client) && !%mini.isResetting)
			%mini.reset(%client);
		else
			parent::serverCmdResetMinigame(%client);
	}

	function serverCmdEndMinigame(%client)
	{
		%mini = getMinigameFromObject(%client);
		if(isSlayerMinigame(%mini) && %mini.canEdit(%client))
			%mini.endGame();
		else
			parent::serverCmdEndMinigame(%client);
	}

	function serverCmdInviteToMinigame(%client,%victim)
	{
		%mini = getMinigameFromObject(%client);
		if(isSlayerMinigame(%mini))
		{
			if(%mini.canEdit(%client))
			{
				if(isObject(%victim))
				{
					%pend = %victim.minigameInvitePending;

					if(isObject(%pend))
					{
						if(%pend == %mini)
							commandToClient(%client,'messageBoxOK','Mini-Game Invite Error','This person hasn\'t responded to your first invite yet.');
						else
							commandToClient(%client,'messageBoxOK','Mini-Game Invite Error','This person is considering another invite right now.');
					}
					else if(%mini.isMember(%victim))
					{
						commandToClient(%client,'messageBoxOK','Mini-Game Invite Error','This person is already in the mini-game.');
					}
					else
					{
						%victim.minigameInvitePending = %mini;
						commandToClient(%victim,'minigameInvite',%mini.title,%mini.hostName,%mini.creatorBLID,%mini);
					}
				}
			}
			else
			{
				commandToClient(%client,'messageBoxOK','Mini-Game Invite Error','You do not have permission to send invites.');
			}
		}
		else
			parent::serverCmdInviteToMinigame(%client,%victim);
	}

	function serverCmdAcceptMinigameInvite(%client,%mini)
	{
		if(isSlayerMinigame(%mini))
		{
			if(!%mini.isMember(%client))
			{
				%mini.addMember(%client);
			}
		}
		else
			parent::serverCmdAcceptMinigameInvite(%client,%mini);
	}

	function serverCmdRejectMinigameInvite(%client,%mini)
	{
		if(isSlayerMinigame(%mini))
		{
			%creator = findClientByBL_ID(%mini.creatorBLID);
			if(isObject(%creator))
				messageClient(%creator,'','%1 rejected your mini-game invitation',%client.getPlayerName());

			%client.minigameInvitePending = 0;
		}
		else
			parent::serverCmdRejectMinigameInvite(%client,%mini);
	}

	function serverCmdIgnoreMinigameInvite(%client,%mini)
	{
		if(isSlayerMinigame(%mini))
		{
			%creator = findClientByBL_ID(%mini.creatorBLID);
			if(isObject(%creator))
				messageClient(%creator,'','%1 rejected your mini-game invitation (ignoring)',%client.getPlayerName());

			%client.minigameInvitePending = 0;
		}
		else
			parent::serverCmdRejectMinigameInvite(%client,%mini);
	}

	function serverCmdRemoveFromMinigame(%client,%victim)
	{
		%mini = getMinigameFromObject(%client);
		if(isSlayerMinigame(%mini))
		{
			if(%mini.canEdit(%client))
			{
				if(isObject(%victim))
				{
					if(%mini.isMember(%victim))
					{
						%clName = %client.getPlayerName();
						%vName = %victim.getPlayerName();

						%mini.removeMember(%victim);
						messageClient(%victim,'','%1 kicked you from the minigame',%clName);
						%mini.messageAll('','%1 kicked %2 from the minigame',%clName,%vname);
					}
				}
			}
		}
		else
			parent::serverCmdRemoveFromMinigame(%client,%victim);
	}

	function Slayer_MinigameSO::addMember(%this,%client)
	{
		if(%this.isStarting)
			return;
		if(!isObject(%client))
			return;
		if(%client.getClassName() !$= "GameConnection")
			return;

		if(isObject(%client.minigame))
		{
			if(%client.minigame == %this || %this.isMember(%client))
				return;

			%client.minigame.removeMember(%client);
		}

		clearCenterPrint(%client);
		clearBottomPrint(%client);

		commandToClient(%client,'SetBuildingDisabled',!%this.getPref("Minigame","Enable Building"));
		commandToClient(%client,'SetPaintingDisabled',!%this.getPref("Minigame","Enable Painting"));
		commandToClient(%client,'SetPlayingMinigame',1); //enables the Leave button in the join minigame GUI

		%client.minigame = %this;
		%client.minigameJoinTime = getSimTime();
		%client.minigameInvitePending = 0;

		%this.member[%this.numMembers] = %client;
		%this.numMembers++;

		//notify members
		%joinMessage = "\c1" @ %client.getPlayerName() SPC "joined the\c3" SPC %this.title SPC "\c1mini-game.";
		%this.messageAll('MsgClientInYourMiniGame',%joinMessage,%client,1);
		for(%i=0; %i < %this.numMembers; %i++)
		{
			%cl = %this.member[%i];
			if(%cl != %client)
				messageClient(%client,'MsgClientInYourMiniGame','',%cl,1);
		}

		if(%this.Gamemode.teams)
			messageClient(%client,'',"\c5Type \c3/joinTeam [Team Name] \c5to join a team. Type \c3/listTeams \c5to view a list of teams.");

		//reset their stats
		%client.setScore(0);
		%client.setDeaths(0);
		%client.setKills(0);
		%client.setLives(%this.lives);
		%client.wins = 0;

		//tell them the gamemode, lives, points, time
		%client.ignoreSpam = 1;
		serverCmdSlayer(%client,rules);

		//autosort into team
		if(%this.Gamemode.teams && %this.Teams.autoSort)
		{
			%team = %this.Teams.autoSort(%client);
			%as = isObject(%team);
		}

		//late join time
		%ljt = %this.lateJoinTime;
		if(%this.getLiving()-1 > 1 && %this.Gamemode.rounds && %ljt >= 0 && ($Sim::Time-%this.roundStarted())/60 > %ljt)
		{
			%client.setDead(1);
			%tar = %client.nextCamTarget();
			if(!isObject(%tar))
			{
				%client.setControlObject(%client.camera);
				%client.camera.setMode(observer);
			}

			messageClient(%client,'',"\c5You will spawn when the next round starts.");
			%client.centerPrint("\c5You will spawn when the next round starts.",6);

			if(isObject(%client.player))
				%client.player.delete();
		}
		else if(!%as) //no team, no LJT
			%client.instantRespawn();

		Slayer_Support::Debug(1,"Player joined",%client.getSimpleName());

		if(isFunction("Slayer_" @ %this.mode @ "_onJoin"))
			call("Slayer_" @ %this.mode @ "_onJoin",%this,%client);

		//call this for compatibility
		parent::addMember(%this,%client);
	}

	function Slayer_MinigameSO::removeMember(%this,%client)
	{
		if(isObject(%client))
		{
			if(%client.getClassName() !$= "GameConnection")
				return;
			if(getMinigameFromObject(%client) != %this)
				return;

			if(isFunction("Slayer_" @ %this.mode @ "_onLeave"))
				call("Slayer_" @ %this.mode @ "_onLeave",%this,%client);

			%team = %client.getTeam();
			if(isObject(%team))
			{
				%team.removeMember(%client);
				if(%client == %team.swapClient)
				{
					cancel(%team.swapTime);
					%team.swapPending.swapOffer = "";
					%team.swapPending = "";
					%team.swapClient = "";
				}
			}

			clearCenterPrint(%client);
			clearBottomPrint(%client);
			commandToClient(%client,'Slayer_ForceGUI',"Slayer_Scores",0);

			%client.setDead(0);
			%client.setLives(0);
			%client.setKills(0);
			%client.setDeaths(0);

			%client.dynamicRespawnTime = "";

			Slayer_Support::Debug(1,"Player left",%client.getSimpleName());
		}

		//for compatibility
		if(%this.numMembers <= 1)
		{
			%subtract = 1;
			%this.numMembers ++; //hack!
		}
		%oldClearEventObjects = %client.doNotClearEventObjects;
		%oldClearVehicles = %client.doNotResetVehicles;
		%client.doNotClearEventObjects = 1; //hack to make vehicles not respawn
		%client.doNotResetVehicles = 1;

		parent::removeMember(%this,%client);

		%client.doNotClearEventObjects = %oldClearEventObjects; //hack to make vehicles not respawn
		%client.doNotResetVehicles = %oldClearVehicles;
		if(%subtract)
			%this.numMembers --;

		if(%this.lives > 0 || %team.lives > 0)
		{
			%winner = %this.victoryCheck_Lives();
			if(%winner >= 0)
				%this.endRound(%winner);
		}
	}

	function Slayer_MinigameSO::Reset(%this,%client)
	{
		if(getSimTime() - %this.lastResetTime < $Slayer::Server::Dependencies::Minigames::ResetTimeOut)
		{
			Slayer_Support::Error("Slayer_MinigameSO::Reset","Minigame being reset too quickly!");
			return;
		}
		//make sure the schedules stop
		cancel(%this.timeSchedule);
		cancel(%this.resetTimer);
		for(%i = 0; %i < %this.resetCountdownTimerCount; %i ++)
			cancel(%this.resetCountdownTimer[%i]);

		//the minigame is resetting
		%this.setResetting(1);

		if(isFunction("Slayer_" @ %this.mode @ "_preReset"))
			call("Slayer_" @ %this.mode @ "_preReset",%this,%client);

		//tell everyone that the minigame is resetting
		if(isObject(%client))
			%this.messageAll('','\c3%1 \c5reset the \c3%2 \c5minigame.',%client.getPlayerName(),%this.title);
		else
		{
			%client = 0;
			%this.messageAll('','\c5The \c3%1 \c5minigame was reset.',%this.title);
		}

		%this.resetBricks();
		%this.resetCapturePoints(%client);
		%this.Teams.onMinigameReset();
		if($AddOnLoaded__Bot_Hole)
			%this.resetBots();

		for(%i=0; %i < %this.numMembers; %i ++)
		{
			%cl = %this.member[%i];
			%t = %cl.getTeam();

			%cl.setDead(0);
			%cl.setLives((isObject(%t) && %t.lives >= 0) ? %t.lives : %this.lives);
			if(%this.clearStats)
			{
				%cl.setKills(0);
				%cl.setDeaths(0);
			}

			clearCenterPrint(%cl);
			clearBottomPrint(%cl);
			commandToClient(%cl,'Slayer_ForceGUI',"Slayer_Scores",0);

			//let them know the rules
			%cl.ignoreSpam = 1;
			serverCmdSlayer(%cl,rules);
		}

		%parent = parent::reset(%this,%client);

		//vehicles must be reset after the parent
		%this.resetVehicles();

		if(isFunction("Slayer_" @ %this.mode @ "_postReset"))
			call("Slayer_" @ %this.mode @ "_postReset",%this,%client);
		%this.setResetting(0); //the minigame is no longer resetting

		Slayer_Support::Debug(1,"Slayer_MinigameSO::Reset");

		//HANDLE STARTING THE NEW ROUND
		%this.onReset();

		return %parent;
	}

	function Slayer_MinigameSO::endGame(%this)
	{
		Slayer_Support::Debug(1,"Minigame","Ending");

		if(isFunction("Slayer_" @ %this.mode @ "_onModeEnd"))
			call("Slayer_" @ %this.mode @ "_onModeEnd",%this);

		for(%i=0; %i < %this.numMembers; %i++)
		{
			%cl = %this.member[%i];

			%cl.setDead(0);
			%cl.setLives(0);
			%cl.setKills(0);
			%cl.setDeaths(0);
			%cl.wins = 0;

			%cl.instantRespawn();

			clearBottomPrint(%cl);
			clearCenterPrint(%cl);
			commandToClient(%cl,'Slayer_ForceGUI',"ALL",0);
		}

		%this.resetBricks();
		%this.resetVehicles();
		%this.resetCapturePoints();

		%parent = parent::endGame(%this);

		%this.delete();

		return %parent;
	}

	function miniGameCanUse(%objA,%objB)
	{
		Slayer_Support::Debug(3,"minigameCanUse",%objA TAB %objB);

		%miniA = getMinigameFromObject(%objA);
		%miniB = getMinigameFromObject(%objB);

		if(isSlayerMinigame(%miniA))
		{
			return minigameCanSlayerUse(%objA,%objB,%miniA,%miniB);
		}
		else if(isSlayerMinigame(%miniB))
		{
			return minigameCanSlayerUse(%objB,%objA,%miniB,%miniA);
		}

		return parent::miniGameCanUse(%objA,%objB);
	}

	function minigameCanSlayerUse(%objA,%objB,%miniA,%miniB)
	{
		if(!isSlayerMinigame(%miniA))
			return -1;

		if(isFunction("Slayer_" @ %miniA.mode @ "_canUse"))
			%special = call("Slayer_" @ %miniA.mode @ "_canUse",%miniA,%objA,%objB);
		if(%special != -1 && %special !$= "")
			return %special;

		%blidA = Slayer_Support::getBLIDFromObject(%objA);
		%blidB = Slayer_Support::getBLIDFromObject(%objB);

		//bricks that we planted online need to work in LAN
		if(%blidA !$= "999999" || %blidB !$= getNumKeyID())
		{
			//public bricks are always true
			if(%blidA !$= "888888" && %blidB !$= "888888") 
			{
				if(%miniA.playersUseOwnBricks)
				{
					%trust = getTrustLevel(%objA,%objB);

					if((%trust !$= "" && %trust < $TrustLevel::Build) || %blidA != %blidB)
						return 0;
				}

				if(%miniA.useAllPlayersBricks)
				{
					if(isObject(%miniB) && %miniA != %miniB)
						return 0;
				}
				else if(%blidB != %miniA.creatorBLID)
				{
					if(%miniA != %objB.minigame)
					{
						if(%blidA != -1 && %blidB != -1)
							return 0;
					}
				}
			}
		}

		//team vehicles
		if(!%objB.doNotCheckTeamVehicle)
		{
			if(Slayer_Support::isVehicle(%objB))
			{
				%spawn = %objB.spawnBrick;
				if(isObject(%spawn))
				{
					if(%spawn.getDatablock().slyrType $= "TeamVehicle")
					{
						%clientA = Slayer_Support::getClientFromObject(%objA);
						%clientB = Slayer_Support::getClientFromObject(%objB);
						if(isObject(%clientA))
						{
							%team = %clientA.getTeam();
							if(isObject(%team))
							{
								%color = %spawn.getControl();
								echo(%color SPC %team.color);
								if(%color != %team.color)
								{
									%teams = %miniA.Teams.getTeamsFromColor(%color,"COM",1);
									if(%teams !$= "")
									{
										//scheduled to suppress the "Greek2me does not trust you..." messages
										%clientA.schedule(0,centerPrint,"<color:ff00ff>Only members of" SPC %teams SPC "\c5may use this vehicle.",2);
										return 0;
									}
								}
							}
						}
					}
				}
			}
		}

		return 1;
	}

	function minigameCanDamage(%objA,%objB)
	{
		Slayer_Support::Debug(3,"minigameCanDamage",%objA TAB %objB);

		if(!isObject(%objA) || !isObject(%objB))
			return parent::minigameCanDamage(%objA,%objB);

		//get minigames
		%miniA = getMinigameFromObject(%objA);
		%miniB = getMinigameFromObject(%objB);

		//figure out the minigame to use
		if(isObject(%miniA) && !isObject(%miniB))
			%mini = %miniA;
		else if(!isObject(%miniA) && isObject(%miniB))
			%mini = %miniB;
		else if(%miniA == %miniB)
			%mini = %miniA;
		else
			return parent::minigameCanDamage(%objA,%objB);

		//bad minigame
		if(!isSlayerMinigame(%mini))
			return parent::minigameCanDamage(%objA,%objB);

		//get classes
		%classA = %objA.getClassName();
		%classB = %objB.getClassName();

		//prevent player damage outside minigame
		if(%classB $= "Player" || %classB $= "AiPlayer")
		{
			if(%miniA != %miniB)
			{
				return 0;
			}
		}
		else //prevent damaging stuff not in the minigame
		{
			%objB.doNotCheckTeamVehicle = 1;
			%canUse = minigameCanUse(%objA,%objB);
			%objB.doNotCheckTeamVehicle = "";

			if(!%canUse)
				return 0;
		}

		//run a basic gamemode canDamage check
		if(isFunction("Slayer_" @ %mini.mode @ "_canDamage"))
			%special = call("Slayer_" @ %mini.mode @ "_canDamage",%mini,%objA,%objB);
		if(%special != -1 && %special !$= "")
			return %special;

		//check if we can damage them, according to minigame prefs
		if(!%mini.canDamage(%objA,%classA,%objB,%classB) || !%mini.canDamage(%objB,%classB,%objA,%classA))
			return 0;

		//get clients
		%clientA = Slayer_Support::getClientFromObject(%objA);
		%clientB = Slayer_Support::getClientFromObject(%objB);

		//gamemode clientCanDamage check
		if(isFunction("Slayer_" @ %mini.mode @ "_ClientCanDamage"))
			%special = call("Slayer_" @ %mini.mode @ "_ClientCanDamage",%mini,%clientA,%clientB);
		if(%special != -1 && %special !$= "")
			return %special;

		if(!%mini.Teams.friendlyFire)
		{
			if(%clientA != %clientB)
			{
				if(isObject(%clientA) && isObject(%clientB))
				{
					%teamA = %clientA.getTeam();
					%teamB = %clientB.getTeam();

					if(isObject(%teamA) && isObject(%teamB))
					{
						if(%teamA == %teamB)
						{
							return 0;
						}
						else if(%mini.Teams.allySameColors)
						{
							if(%teamA.color == %teamB.color)
							{
								return 0;
							}
						}
					}
				}
			}
		}

		return 1;
	}

	//why do minigames return -1 when checked with this...?
	function getMinigameFromObject(%obj)
	{
		%parent = parent::getMinigameFromObject(%obj);

		if(!isObject(%parent) && isObject(%obj) && !%obj.doNotCheckSlayerMinigame)
		{
			%className = %obj.getClassName();

			if(striPos("fxDtsBrick SimGroup",%className) >= 0)
			{
				%blid = Slayer_Support::getBLIDFromObject(%obj);
				if(%blid >= 0)
				{
					%mini = Slayer.Minigames.getHighestPriorityMinigame(%blid);
					if(isObject(%mini))
						return %mini;
				}
			}

			//MINIGAME
			if(%obj.class $= "MinigameSO" || %obj.superClass $= "MinigameSO")
				return %obj;
		}

		return %parent;
	}

	function getBrickGroupFromObject(%obj)
	{
		if(%obj.isSlayerFakeClient && isObject(%obj.brickgroup))
			return %obj.brickgroup;

		return parent::getBrickGroupFromObject(%obj);
	}

	function onMissionLoaded()
	{
		%parent = parent::onMissionLoaded();

		// +-------------------------+
		// | Minigame Initialization |
		// +-------------------------+
		if($Slayer::Server::CurGameModeArg $= $Slayer::Server::GameModeArg)
		{
			Slayer.Minigames.schedule(0,addMinigame,0,$Slayer::Server::ConfigDir @ "/config_last.cs");
		}
		else
		{
			if($Slayer::Server::CurGameModeArg !$= "")
				%configFile = filePath($Slayer::Server::CurGameModeArg) @ "/config_slayer.cs";
			if(isFile(%configFile))
				Slayer.Minigames.schedule(0,addMinigame,0,%configFile);
			else if($Slayer::Server::Preload::Minigame::AutoStartMinigame)
				Slayer.Minigames.schedule(0,addMinigame,0,$Slayer::Server::ConfigDir @ "/config_last.cs");
		}
		

		return %parent;
	}
};
activatePackage(Slayer_Dependencies_Minigames);

// +----------------------+
// | Initialize Component |
// +----------------------+
if(!isObject(Slayer.Minigames))
{
	if(!isObject(Slayer))
		exec("Add-ons/Gamemode_Slayer/Main.cs");

	Slayer.Minigames = new scriptGroup(Slayer_MinigameHandlerSO);
	Slayer.add(Slayer.Minigames);
}

Slayer_Support::Debug(2,"Dependency Loaded","Minigames");