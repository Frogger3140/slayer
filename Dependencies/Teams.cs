// +-------------------------------------------+
// |  ___   _     _____   __  __  ____   ____  |
// | | __| | |   | /_\ |  \ \/ / | ___| | /\ | |
// | |__ | | |_  |  _  |   \  /  | __|  | \/ / |
// | |___| |___| |_| |_|   |__|  |____| |_| \\ |
// |  Greek2me              Blockland ID 11902 |
// +-----------------------------+-------------+
// | DO NOT EDIT BELOW THIS LINE |
// +-----------------------------+

$Slayer::Server::Dependencies::Teams = 1;

$Slayer::Server::Teams::maxTeams = 100;

if(!isObject(Slayer.TeamPrefs))
{
	if(!isObject(Slayer))
		exec("Add-ons/Gamemode_Slayer/Main.cs");

	Slayer.TeamPrefs = new scriptGroup(Slayer_TeamPrefHandlerSO);
	Slayer.add(Slayer.TeamPrefs);
}

// +----------------------+
// | Slayer_TeamHandlerSO |
// +----------------------+
function Slayer_TeamHandlerSO::onAdd(%this)
{
	%this.Prefs = Slayer.TeamPrefs;
}

function Slayer_TeamHandlerSO::addTeam(%this,%isSpecialTeam)
{
	if(%this.getCount() >= $Slayer::Server::Teams::maxTeams)
		return Slayer_Support::Error("Slayer_TeamHandlerSO::addTeam","Team limit reached!");

	%mini = %this.minigame;

	%team = new scriptObject()
	{
		class = Slayer_TeamSO;
		minigame = %mini;
		isSpecialTeam = %isSpecialTeam;

		numMembers = 0;
		wins = 0;
		score = 0;
	};
	%this.add(%team);
	if(isObject(missionCleanup) && missionCleanup.isMember(%team))
		missionCleanup.remove(%team);

	//apply default settings
	%team.resetPrefs();

	if(isFunction("Slayer_" @ %mini.mode @ "_Teams_onAdd"))
		call("Slayer_" @ %mini.mode @ "_Teams_onAdd",%mini,%this,%team);

	return %team;
}

function Slayer_TeamHandlerSO::removeTeam(%this,%team)
{
	if(!isObject(%team))
		return;
	if(!%this.isMember(%team))
		return;

	%mini = %this.minigame;

	if(isFunction("Slayer_" @ %mini.mode @ "_Teams_onRemove"))
		call("Slayer_" @ %mini.mode @ "_Teams_onRemove",%mini,%this,%team);

	%team.removeAllMembers();

	%team.delete();
}

function Slayer_TeamHandlerSO::onMinigameReset(%this)
{
	for(%i=0; %i < %this.getCount(); %i++)
		%this.getObject(%i).onMinigameReset();

	//team shuffling
	if(%this.shuffleTeams)
	{
		%this.shuffleCount ++;

		if(%this.shuffleCount >= %this.roundsBetweenShuffles)
		{
			%this.shuffleTeams(1);
			%this.shuffleCount = 0;
		}
	}
	else
		%this.shuffleCount = 0;
}

function Slayer_TeamHandlerSO::onNewTeam(%this,%team)
{
	%mini = %this.minigame;

	if(%this.balanceOnNew)
	{
		for(%i=0; %i < %mini.numMembers; %i++)
		{
			%cl = %mini.member[%i];
			%t = %cl.slyrTeam;
			if(!isObject(%t))
				%this.autoSort(%cl);
		}

		while(!%team.isTeamBalanced())
		{
			%team.balanceTeam(1); //forced team balance
		}
	}
}

function Slayer_TeamHandlerSO::removeAllMembers(%this)
{
	for(%i=0; %i < %this.getCount(); %i++)
		%this.getObject(%i).removeAllMembers();
}

function Slayer_TeamHandlerSO::messageAllByColor(%this,%color,%cmd,%msg,%a,%b,%c,%d,%e,%f,%g,%h,%i,%j,%k,%l,%m,%n,%o,%p)
{
	for(%i = 0; %i < %this.getCount(); %i ++)
	{
		%t = %this.getObject(%i);
		if(%t.color == %color)
			%t.messageAll(%cmd,%msg,%a,%b,%c,%d,%e,%f,%g,%h,%i,%j,%k,%l,%m,%n,%o,%p);
	}
}

function Slayer_TeamHandlerSO::messageAllDeadByColor(%this,%color,%cmd,%msg,%a,%b,%c,%d,%e,%f,%g,%h,%i,%j,%k,%l,%m,%n,%o,%p)
{
	for(%i = 0; %i < %this.getCount(); %i ++)
	{
		%t = %this.getObject(%i);
		if(%t.color == %color)
			%t.messageAllDead(%cmd,%msg,%a,%b,%c,%d,%e,%f,%g,%h,%i,%j,%k,%l,%m,%n,%o,%p);
	}
}

function Slayer_TeamHandlerSO::shuffleTeams(%this,%doNotRespawn)
{
	%mini = %this.minigame;

	if(%this.getAutoSortTeamCount() < 1)
		return;

	for(%i=0; %i < %mini.numMembers; %i++)
	{
		%cl = %mini.member[%i];
		if(!isObject(%cl.slyrTeam))
			%noTeam = setField(%noTeam,getFieldCount(%noTeam),%cl);
	}

	for(%i=0; %i < %mini.numMembers; %i++)
	{
		%cl = %mini.member[%i];
		if(strPos(%noTeam,%cl) < 0)
		{
			%team = %this.pickTeam(%cl,1);
			if(isObject(%team))
				%team.addMember(%cl,"Team Shuffle",%doNotRespawn);
		}
	}

	if(%this.balanceTeams)
		%this.balanceTeams(0);

	if(isFunction("Slayer_" @ %mini.mode @ "_Teams_onShuffle"))
		call("Slayer_" @ %mini.mode @ "_Teams_onShuffle",%mini,%this,%doNotRespawn);
}

function Slayer_TeamHandlerSO::balanceTeams(%this,%force)
{
	for(%i = 0; %i < %this.getCount(); %i ++)
	{
		%team = %this.getObject(%i);
		if(%team.sort && %team.sortWeight > 0)
		{
			while(!%team.isTeamBalanced())
			{
				%team.balanceTeam(%force);
				if(!%force)
					break;
			}
		}
	}
}

function Slayer_TeamHandlerSO::swapTeamMembers(%this,%clientA,%clientB)
{
	%mini = %this.minigame;
	%resetting = %mini.isResetting();

	%teamA = %clientA.getTeam();
	%teamB = %clientB.getTeam();
	if(!isObject(%teamA) || !isObject(%teamB))
		return;

	if(%teamA.swapPending == %teamB)
	{
		cancel(%teamA.swapTime);
		%teamB.swapOffer = "";
		%teamA.swapPending = "";
		%teamA.swapClient = "";
	}
	else if(%teamB.swapPending == %teamA)
	{
		cancel(%teamB.swapTime);
		%teamA.swapOffer = "";
		%teamB.swapPending = "";
		%teamB.swapClient = "";
	}

	%teamA.addMember(%clientB,"Team Swap",%resetting);
	%teamB.addMember(%clientA,"Team Swap",%resetting);

	%this.onTeamMemberSwap(%this,%clientA,%clientB);
}

function Slayer_TeamHandlerSO::onTeamMemberSwap(%this,%clientA,%clientB)
{

}

function Slayer_TeamHandlerSO::getAutosortTeamCount(%this)
{
	%count = 0;
	for(%i=0; %i < %this.getCount(); %i++)
	{
		%t = %this.getObject(%i);

		if((%t.maxPlayers <= 0 || %t.numMembers < %t.maxPlayers) && %t.sort && %t.sortWeight > 0)
			%count ++;
	}

	return %count;
}

function Slayer_TeamHandlerSO::pickTeam(%this,%client,%ignoreCurrentTeam)
{
	%mini = %this.minigame;

	if(!isObject(%client))
		return 0;

	for(%i=0; %i < %this.getCount(); %i++)
	{
		%t = %this.getObject(%i);
		if(%ignoreCurrentTeam && %t == %client.slyrTeam)
			continue;
        if(!%t.sort || %t.sortWeight <= 0 || (%t.numMembers >= %t.maxPlayers && %t.maxPlayers >= 0))
			continue;

		%sumWeights += %t.sortWeight;
	}

	%sortRatio = %mini.numMembers / %sumWeights;

	%greatest = -1;
	for(%i=0; %i < %this.getCount(); %i++)
	{
		%t = %this.getObject(%i);
		if(%ignoreCurrentTeam && %t == %client.slyrTeam)
			continue;
		if(!%t.sort || %t.sortWeight <= 0 || (%t.numMembers >= %t.maxPlayers && %t.maxPlayers >= 0))
			continue;

		%weight = (%t.sortWeight * %sortRatio) - %t.numMembers;

		if(%weight > %greatest)
		{
			%greatest = %weight;
			%teams = %t;
		}
		else if(%weight >= %greatest)
		{
			%teams = setField(%teams,getFieldCount(%teams),%t);
		}
	}

	if(getFieldCount(%teams) <= 0)
		return 0;

	%r = getRandom(0,getFieldCount(%teams)-1);
	if(%r < 0)
		%r = 0;
	%team = getField(%teams,%r);

	if(!isObject(%team))
		return 0;

	if(%client.slyrTeam == %team)
		return 0;

	return %team;
}

function Slayer_TeamHandlerSO::autoSort(%this,%client)
{
	%team = %this.pickTeam(%client);
	if(!isObject(%team))
		return;

	%team.addMember(%client);

	return %team;
}

function Slayer_TeamHandlerSO::autoSortAll(%this,%doNotRespawn)
{
	%mini = %this.minigame;

	for(%i = 0; %i < %mini.numMembers; %i ++)
	{
		%cl = %mini.member[%i];
		%team = %this.pickTeam(%cl);
		if(isObject(%team))
			%team.addMember(%cl,"",%doNotRespawn);
	}
}

function Slayer_TeamHandlerSO::getTeamFromName(%this,%name)
{
	for(%i=0; %i < %this.getCount(); %i++)
	{
		%t = %this.getObject(%i);
		if(%t.name $= %name)
			return %t;
	}

	return 0;
}

function Slayer_TeamHandlerSO::getTeamsFromColor(%this,%color,%mode,%returnColor,%inverse,%exclude) //TAB returns a tab delimited list of team IDs. COM returns a comma delimited list of team names, which can be colored
{
	if(%mode $= "")
		%mode = "TAB";

	for(%i=0; %i < %this.getCount(); %i++)
	{
		%t = %this.getObject(%i);

		if((%t.color != %color && !%inverse) || (%t.color == %color && %inverse))
			continue;

		if(strPos(%exclude,%t) >= 0)
			continue;

		switch$(%mode)
		{
			case "TAB":
				if(%teams $= "")
					%teams = %t;
				else
					%teams = %teams TAB %t;

			case "COM":
				if(%returnColor)
				{
					if(%teams $= "")
						%teams = "<spush>" @ %t.getColoredName() @ "<spop>";
					else
						%teams = %teams @ "," SPC "<spush>" @ %t.getColoredName() @ "<spop>";
				}
				else
				{
					if(%teams $= "")
						%teams = %t.name;
					else
						%teams = %teams @ "," SPC %t.name;
				}
		}
	}

	return %teams;
}

function Slayer_TeamHandlerSO::getTeamListSortedScore(%this)
{
	%count = %this.getCount();
	for(%i = 0; %i < %count; %i ++)
		%list = setField(%list,getFieldCount(%list),%this.getObject(%i));
	while(!%done)
	{
		%done = 1;
		for(%i = 0; %i < %count; %i ++)
		{
			%e = %i + 1;
			if(%e >= %count)
				continue;
			%f1 = getField(%list,%i);
			%f2 = getField(%list,%e);
			if(%f1.getScore() < %f2.getScore())
			{
				%list = Slayer_Support::swapItemsInList(%list,%i,%e);
				%done = 0;
			}
		}
		%count --;
	}

	return %list;
}

function Slayer_TeamHandlerSO::isNeutralColor(%this,%color)
{
	for(%i = 0; %i < %this.getCount(); %i ++)
	{
		if(%this.getObject(%i).color == %color)
			return 0;
	}

	return 1;
}

function Slayer_TeamHandlerSO::sendTeams(%this,%client,%mini)
{
	if(!isObject(%mini) && %mini != -1)
		%mini = getMinigameFromObject(%client);
	if(!isSlayerMinigame(%mini))
		return;
	if(%this.minigame != %mini)
		return;
	if(!%mini.canEdit(%client))
		return;

	Slayer_Support::Debug(2,"Sending Teams",%client.getSimpleName());

	//START THE TEAM PREF TRANSFER
	commandToClient(%client,'Slayer_getTeamPrefs_Start',Slayer.TeamPrefs.getCount());

	for(%i=0; %i < Slayer.TeamPrefs.getCount(); %i++)
	{
		%p = Slayer.TeamPrefs.getObject(%i);

		%canEdit = %mini.canEdit(%client,%p.editRights);

		commandToClient(%client,'Slayer_getTeamPrefs_Tick',%p.category,%p.title,%p.type,%p.objectClassName,%p.defaultValue,%canEdit,%p.list);
	}

	//END THE TEAM PREF TRANSFER
	commandToClient(%client,'Slayer_getTeamPrefs_End');

	//START THE TEAM TRANSFER
	commandToClient(%client,'Slayer_getTeams_Start',%this.getCount());

	for(%i=0; %i < %this.getCount(); %i++)
	{
		%team = %this.getObject(%i);

		if(%team.isSpecialTeam)
			continue;

		commandToClient(%client,'Slayer_getTeams_Tick',%team,%team.name);

		Slayer_Support::Debug(2,"Sending Team",%team TAB %team.name);

		for(%e=0; %e < Slayer.TeamPrefs.getCount(); %e++)
		{
			%p = Slayer.TeamPrefs.getObject(%e);

			%val = %p.getValue(%team);

			commandToClient(%client,'Slayer_getTeams_PrefTick',%team,%p.category,%p.title,%val);
		}
	}

	//END THE TEAM TRANSFER
	commandToClient(%client,'Slayer_getTeams_End');
}

// +---------------+
// | Slayer_TeamSO |
// +---------------+
function Slayer_TeamSO::onAdd(%this)
{

}

function Slayer_TeamSO::onRemove(%this)
{

}

function Slayer_TeamSO::onMinigameReset(%this)
{
	%mini = getMinigameFromObject(%this);

	if(%mini.clearScores || %mini.points > 0)
		%this.setArtificialScore(0);
}

function Slayer_TeamSO::addMember(%this,%client,%reason,%doNotRespawn)
{
	%mini = getMinigameFromObject(%client);
	%teamHandler = %this.getGroup();

	if(%mini != %this.minigame)
		return 0;
	if(%client.slyrTeam == %this)
		return 0;

	//remove from old team
	if(isObject(%client.slyrTeam))
		%client.slyrTeam.removeMember(%client);

	//add to the team
	%client.slyrTeam = %this;
	%this.member[%this.numMembers] = %client;
	%this.numMembers ++;

	//announce that they joined
	%message = '\c3%1 \c5joined %2 %3';
	if(%reason !$= "")
		%reason = "\c5(\c3" @ %reason @ "\c5)";
	messageClient(%client,'',%message,"\c5You have",%this.getColoredName(),%reason);
	if(%teamHandler.notifyMemberChanges)
		%mini.messageAllExcept(%client,'',%message,%client.getPlayerName() SPC "\c5has",%this.getColoredName(),%reason);

	//fix the player's life count in case this team has a custom number of lives
	if((%client.getLives() > %this.lives || %client.getLives() == %mini.lives) && %this.lives >= 0)
		%client.setLives(%this.lives);

	//spawn the player
	if((isObject(%client.player) || %mini.isResetting() || !%client.hasSpawnedOnce) && !%doNotRespawn)
		%client.instantRespawn();
	clearBottomPrint(%client);
	clearCenterPrint(%client);

	//balance the teams
	if(%teamhandler.balanceTeams)
		%this.balanceTeam();

	%this.onAddMember(%client);

	return 1;
}

function Slayer_TeamSO::onAddMember(%this,%client)
{
	%mini = %this.minigame;
	if(isFunction("Slayer_" @ %mini.mode @ "_Teams_onJoin"))
		call("Slayer_" @ %mini.mode @ "_Teams_onJoin",%mini,%this,%client);
}

function Slayer_TeamSO::removeMember(%this,%client,%respawn)
{
	%mini = %this.minigame;
	%teamHandler = %this.getGroup();

	if(%client.slyrTeam != %this)
	{
		Slayer_Support::Debug(1,"Member not on" SPC %this.name,%client.getSimpleName());
		return 0;
	}

	%this.onRemoveMember(%client);

	//cancel any team swap requests
	if(%client == %this.swapClient)
	{
		%this.swapPending.swapOffer = "";
		%this.swapPending = "";
		%this.swapClient = "";
		cancel(%this.swapTime);
		%this.swapTime = "";
	}

	%start = -1;
	for(%i=0; %i < %this.numMembers; %i++)
	{
		if(%this.member[%i] == %client) //find the member to be removed
		{
			%this.member[%i] = "";
			%start = %i;
			break;
		}
	}
	if(%start >= 0)	
	{
		for(%i=%start+1; %i < %this.numMembers; %i++)
			%this.member[%i-1] = %this.member[%i];

		%this.member[%this.numMembers-1] = "";
		%this.numMembers --;
	}


	%client.slyrTeam = "";

	if(%respawn)
		%client.instantRespawn();
	else if(isObject(%client.player))
	{
		%client.applyBodyParts();
		%client.applyBodyColors();
		%client.player.setShapeNameColor("0 1 0 1");

		%playerDatablock = %mini.playerDatablock;
		%client.player.changeDatablock(%playerDatablock);
		for(%i=0; %i < %playerDatablock.maxTools; %i++)
			%client.forceEquip(%i,%mini.startEquip[%i]);
	}
	if(%client.getLives() >= %this.lives)
		%client.setLives(%mini.lives);

	if(%teamhandler.balanceTeams)
		%this.schedule(0,balanceTeam);

	return 1;
}

function Slayer_TeamSO::onRemoveMember(%this,%client)
{
	%mini = %this.minigame;
	if(isFunction("Slayer_" @ %mini.mode @ "_Teams_onLeave"))
		call("Slayer_" @ %mini.mode @ "_Teams_onLeave",%mini,%this,%client);
}

function Slayer_TeamSO::removeAllMembers(%this)
{
	for(%i=%this.numMembers-1; %i >= 0; %i--)
		%this.removeMember(%this.member[%i]);
}

function Slayer_TeamSO::balanceTeam(%this,%force)
{
	%mini = %this.minigame;
	%teamHandler = %this.getGroup();

	%this.balanceSource.balanceTeam = "";
	%this.balanceSource = "";

	if(!%this.sort || %this.sortWeight <= 0)
		return 0;
	if(%this.numMembers >= %this.maxPlayers && %this.maxPlayers >= 0)
		return 0;

	%ratio = %this.numMembers / %this.sortWeight;

	//find the most suitable source
	%highest = 0;
	for(%i=0; %i < %teamHandler.getCount(); %i++)
	{
		%t = %teamHandler.getObject(%i);
		if(!%t.sort || %t.sortWeight <= 0 || (%t.numMembers >= %t.maxPlayers && %t.maxPlayers >= 0))
			continue;

		%r = %t.numMembers / %t.sortWeight;
		%ratioSum += %r;
		%ratioCount ++;

		if(%t == %this)
			continue;

		if(%r > %highest)
		{
			%highest = %r;

			%source = %t;
			%sourceRatio = %r;
		}
	}

	if(%ratioCount > 1)
	{
		%ratioAvg = %ratioSum / %ratioCount;

		if(%ratio < %ratioAvg)
		{
			//we can no longer give away members
			%this.balanceTeam.balanceSource = "";
			%this.balanceTeam = "";
		}
	}

	if(!isObject(%source))
		return 0;

	%ratioDif = %sourceRatio - %ratio;

	if(%ratioDif > 1)
	{
		//is this team completely out of members?
		if(%force || (%this.numMembers < 1 && %teamHandler.getCount() == 2))
		{
			//yes - we need to move someone immediately
			%this.addMember(%source.member[0],"Team Balance");
		}
		else
		{
			//no - wait until someone dies
			%source.balanceTeam = %this;
			%this.balanceSource = %source;
		}

		return %source;
	}

	return 0;
}

function Slayer_TeamSO::isTeamBalanced(%this)
{
	%mini = %this.minigame;
	%teamHandler = %this.getGroup();

	if(!%this.sort || %this.sortWeight <= 0)
		return -1;
	if(%this.numMembers >= %this.maxPlayers && %this.maxPlayers >= 0)
		return 1;

	%ratio = %this.numMembers / %this.sortWeight;

	//find the most suitable source
	%highest = 0;
	for(%i=0; %i < %teamHandler.getCount(); %i++)
	{
		%t = %teamHandler.getObject(%i);
		if(!%t.sort || %t.sortWeight <= 0 || (%t.numMembers >= %t.maxPlayers && %t.maxPlayers >= 0))
			continue;

		%r = %t.numMembers / %t.sortWeight;
		%ratioSum += %r;
		%ratioCount ++;

		if(%t == %this)
			continue;

		if(%r > %highest)
		{
			%highest = %r;

			%source = %t;
			%sourceRatio = %r;
		}
	}

	%ratioDif = %sourceRatio - %ratio;

	return (%ratioDif <= 1);
}

function Slayer_TeamSO::messageAll(%this,%cmd,%msg,%a,%b,%c,%d,%e,%f,%g,%h,%i,%j,%k,%l,%m,%n,%o,%p,%q)
{
	for(%i=0; %i < %this.numMembers; %i++)
		messageClient(%this.member[%i],%cmd,%msg,%a,%b,%c,%d,%e,%f,%g,%h,%i,%j,%k,%l,%m,%n,%o,%p,%q);
}

function Slayer_TeamSO::messageAllDead(%this,%cmd,%msg,%a,%b,%c,%d,%e,%f,%g,%h,%i,%j,%k,%l,%m,%n,%o,%p,%q)
{
	for(%i=0; %i < %this.numMembers; %i++)
	{
		%cl = %this.member[%i];
		if(%cl.dead())
			messageClient(%cl,%cmd,%msg,%a,%b,%c,%d,%e,%f,%g,%h,%i,%j,%k,%l,%m,%n,%o,%p,%q);
	}
}

//for events
function Slayer_TeamSO::chatMsgAll(%this,%msg,%client)
{
	if(isObject(%client))
		%msg = strReplace(%msg,"%1",%client.getPlayerName());

	for(%i=0; %i < %this.numMembers; %i++)
		%this.member[%i].chatMessage(%msg);
}

//for events
function Slayer_TeamSO::centerPrintAll(%this,%msg,%time,%client)
{
	if(isObject(%client))
		%msg = strReplace(%msg,"%1",%client.getPlayerName());

	for(%i=0; %i < %this.numMembers; %i++)
		%this.member[%i].centerPrint(%msg,%time);
}

//for events
function Slayer_TeamSO::bottomPrintAll(%this,%msg,%time,%hideBar,%client)
{
	if(isObject(%client))
		%msg = strReplace(%msg,"%1",%client.getPlayerName());

	for(%i=0; %i < %this.numMembers; %i++)
		%this.member[%i].bottomPrint(%msg,%time,%hideBar);
}

//for events
function Slayer_TeamSO::respawnAll(%this,%client)
{
	for(%i=0; %i < %this.numMembers; %i++)
		%this.member[%i].instantRespawn();
}

//for events
function Slayer_TeamSO::incScore(%this,%flag,%client)
{
	%this.incArtificialScore(%flag);
}

function Slayer_TeamSO::incArtificialScore(%this,%flag) //this adds points to the team itself
{
	%this.setArtificialScore(%this.score + %flag);

	return %this.score;
}

function Slayer_TeamSO::setArtificialScore(%this,%flag)
{
	%this.score = %flag;

	%mini = %this.minigame;
	%winner = %mini.victoryCheck_Points();
	if(%winner >= 0)
		%mini.endRound(%winner);

	return %this.score;
}

function Slayer_TeamSO::getArtificialScore(%this)
{
	return %this.score;
}

function Slayer_TeamSO::getScore(%this)
{
	%score = %this.getArtificialScore();
	for(%i=0; %i < %this.numMembers; %i++)
	{
		%score += %this.member[%i].score;
	}
	return %score;
}

function Slayer_TeamSO::getKills(%this)
{
	%kills = 0;
	for(%i=0; %i < %this.numMembers; %i++)
	{
		%kills += %this.member[%i].slyr_kills;
	}
	return %kills;
}

function Slayer_TeamSO::getDeaths(%this)
{
	%deaths = 0;
	for(%i=0; %i < %this.numMembers; %i++)
	{
		%deaths += %this.member[%i].slyr_deaths;
	}
	return %deaths;
}

function Slayer_TeamSO::getSpawned(%this)
{
	%spawned = 0;
	for(%i=0; %i < %this.numMembers; %i++)
	{
		if(isObject(%this.member[%i].player))
			%spawned ++;
	}
	return %spawned;
}

function Slayer_TeamSO::getLiving(%this)
{
	%living = 0;
	for(%i=0; %i < %this.numMembers; %i++)
	{
		if(!%this.member[%i].dead())
			%living ++;
	}
	return %living;
}

function Slayer_TeamSO::isTeamDead(%this)
{
	return (%this.getLiving() <= 0);
}

function Slayer_TeamSO::getColorHex(%this)
{
	return "<color:" @ %this.colorHex @ ">";
}

function Slayer_TeamSO::getColoredName(%this)
{
	return %this.getColorHex() @ %this.name;
}

function Slayer_TeamSO::forceEquip(%this,%slot,%item)
{
	for(%i=0; %i < %this.numMembers; %i++)
		%this.member[%i].forceEquip(%slot,%item);
}

function Slayer_TeamSO::updateEquip(%this,%slot,%new,%old)
{
	for(%i=0; %i < %this.numMembers; %i++)
		%this.member[%i].updateEquip(%slot,%new,%old);
}

function Slayer_TeamSO::updateDatablock(%this,%db)
{
	if(!isObject(%db))
		return;
	if(%db.getClassName() !$= "PlayerData")
		return;

	for(%i=0; %i < %this.numMembers; %i++)
	{
		%cl = %this.member[%i];
		%pl = %cl.player;
		if(isObject(%pl))
			%pl.changeDatablock(%db);
	}
}

function Slayer_TeamSO::updateName(%this,%name)
{
	%this.name = "";

	%name = trim(%name);
	%name = getWords(%name,0,12); //names are limited to 12 words
	if(isObject(%this.getGroup().getTeamFromName(%name)))
		%name = %name SPC getRandom(0,999999);
	%this.name = %name;
}

function Slayer_TeamSO::updateColor(%this,%color)
{
	%this.colorRGB = getColorIDTable(%color);
	%this.colorHex = Slayer_Support::RgbToHex(%this.colorRGB);

	%this.updateUniform();
}

function Slayer_TeamSO::updateUniform(%this)
{
	for(%i=0; %i < %this.numMembers; %i++)
		%this.member[%i].applyUniform();
}

function Slayer_TeamSO::updateRespawnTime(%this,%type,%flag,%old)
{
	%oldTime = %old * 1000;
	%time = %flag * 1000;
	%time = Slayer_Support::mRestrict(%time,-1,999999);

	switch$(%type)
	{
		case player:
			%this.respawnTime = %time;

			for(%i=0; %i < %this.numMembers; %i++)
			{
				%cl = %this.member[%i];
				if(%cl.dynamicRespawnTime == %oldTime || %old == -1)
					%cl.setDynamicRespawnTime(%time);
			}
	}
}

function Slayer_TeamSO::updateLives(%this,%new,%old)
{
	%mini = %this.minigame;

	for(%i=0; %i < %this.numMembers; %i++)
	{
		%cl = %this.member[%i];

		if(%new == 0)
		{
			%cl.setLives(0);
			return;
		}
		else if(%new < 0)
		{
			%cl.setLives(%mini.lives);
			return;
		}

		if(%old $= "" || %old < 0)
			%cl.setLives(%new);
		else
			%cl.addLives(%new-%old);

		%lives = %cl.getLives();
		if(%lives <= 0 && !%cl.dead())
		{
			%cl.setDead(1);
			if(isObject(%cl.player))
			{
				%cl.camera.setMode(observer);
				%cl.setControlObject(%cl.camera);
				%cl.player.delete();
			}
			%cl.centerPrint("\c5The number of lives was reduced. You are now out of lives.",5);

			%check = 1;
		}
		else if(%lives > 0 && %cl.dead())
		{
			%cl.setDead(0);
			if(!isObject(%cl.player))
				%cl.instantRespawn();
		}
	}

	if(%check)
	{
		%win = %mini.victoryCheck_Lives();
		if(%win >= 0)
			%mini.endRound(%win);
	}
}

function Slayer_TeamSO::updatePlayerScale(%this,%flag)
{
	for(%i=0; %i < %this.numMembers; %i++)
	{
		%cl = %this.member[%i];
		if(isObject(%cl.player))
			%cl.player.setScale(%flag SPC %flag SPC %flag);
	}
}

function Slayer_TeamSO::setPref(%this,%category,%title,%value)
{
	return Slayer.TeamPrefs.setPref(%this,%category,%title,%value);
}

function Slayer_TeamSO::getPref(%this,%category,%title)
{
	return Slayer.TeamPrefs.getPref(%this,%category,%title);
}

function Slayer_TeamSO::resetPrefs(%this)
{
	Slayer.TeamPrefs.resetPrefs(%this);
}

// +--------------------------+
// | Slayer_TeamPrefHandlerSO |
// +--------------------------+
function Slayer_TeamPrefHandlerSO::addPref(%this,%category,%title,%variable,%type,%defaultValue,%notify,%editRights,%list,%callback)
{
	%p = %this.getPrefSO(%category,%title);
	if(isObject(%p))
	{
		%p.delete();
		Slayer_Support::Debug(1,"Overwriting Team Pref",%category TAB %title);
	}

	%className = getField(%variable,1);

	%pref = new scriptObject()
	{
		class = Slayer_TeamPrefSO;

		title = %title;
		category = %category;
		type = %type;
		variable = getField(%variable,0);
		defaultValue = %defaultValue;
		notify = %notify;
		editRights = %editRights;
		isObject = (%className !$= "0" && %className !$= "");
		objectClassName = %className;
		objectCannotBeNull = getField(%variable,2);
		list = %list;
		callback = %callback;
	};
	%this.add(%pref);
	if(isObject(missionCleanup) && missionCleanup.isMember(%pref))
		missionCleanup.remove(%pref);
}

function Slayer_TeamPrefHandlerSO::setPref(%this,%team,%category,%title,%value)
{
	if(!isObject(%team))
		return;

	%pref = %this.getPrefSO(%category,%title);
	if(!isObject(%pref))
		return;

	return %pref.setValue(%team,%value);
}

function Slayer_TeamPrefHandlerSO::getPref(%this,%team,%category,%title) //get the value of a pref
{
	if(!isObject(%team))
		return;

	%pref = %this.getPrefSO(%category,%title);
	if(!isObject(%pref))
		return;

	return %pref.getValue(%team);
}

function Slayer_TeamPrefHandlerSO::getPrefSO(%this,%category,%title) //get the SO of a pref
{
	for(%i=0; %i < %this.getCount(); %i++)
	{
		%pref = %this.getObject(%i);
		if(%pref.category $= %category && %pref.title $= %title)
		{
			return %pref;
		}
	}

	return 0;
}

function Slayer_TeamPrefHandlerSO::resetPrefs(%this,%team)
{
	if(!isObject(%team))
		return;

	for(%i=0; %i < %this.getCount(); %i++)
	{
		%pref = %this.getObject(%i);
		%pref.setValue(%team,%pref.defaultValue);
	}
}

//THIS IS USED FOR SENDING BLANK TEAM DATA ON MINIGAME CREATION
function Slayer_TeamPrefHandlerSO::SendBlankTeamData(%this,%client)
{
	Slayer_Support::Debug(2,"Sending Blank Team Data",%client.getSimpleName());

	//START THE TEAM TRANSFER
	commandToClient(%client,'Slayer_getTeams_Start',0);

	//END THE TEAM TRANSFER
	commandToClient(%client,'Slayer_getTeams_End');

	//START THE TEAM PREF TRANSFER
	commandToClient(%client,'Slayer_getTeamPrefs_Start',%this.getCount());

	for(%i=0; %i < %this.getCount(); %i++)
	{
		%p = %this.getObject(%i);

		if(%p.editRights <= 2)
			%canEdit = (%p.editRights <= %client.getAdminLvl());
		else
			%canEdit = 1;

		commandToClient(%client,'Slayer_getTeamPrefs_Tick',%p.category,%p.title,%p.type,%p.objectClassName,%p.defaultValue,%canEdit,%p.list,"");
	}

	//END THE TEAM PREF TRANSFER
	commandToClient(%client,'Slayer_getTeamPrefs_End');
}


// +-------------------+
// | Slayer_TeamPrefSO |
// +-------------------+
function Slayer_TeamPrefSO::setValue(%this,%team,%value)
{
	if(!isObject(%team))
		return false;

	%proof = %this.idiotProof(%value);
	if(getField(%proof,0))
		%value = getField(%proof,1);
	else
		return false;

	if(%this.callback !$= "")
	{
		%callback = strReplace(%this.callback,"%1","\"" @ %value @ "\"");
		%callback = strReplace(%callback,"%2","\"" @ %this.getValue(%team) @ "\"");
		%callback = striReplace(%callback,"%team",%team);
		%callback = striReplace(%callback,"%mini",%team.minigame);
	}

	Slayer_Support::setDynamicVariable(%team @ "." @ %this.variable,%value);

	Slayer_Support::Debug(2,"Team Preference Set",%team TAB %this.category TAB %this.title TAB %value);

	if(%callback !$= "")
		eval(%callback);

	return true;
}

function Slayer_TeamPrefSO::getValue(%this,%team)
{
	return Slayer_Support::getDynamicVariable(%team @ "." @ %this.variable);
}

function Slayer_TeamPrefSO::idiotProof(%this,%value)
{
	if(%this.isObject)
	{
		if(!isObject(%value))
		{
			if(%this.objectCannotBeNull)
				return false;
			%value = 0;
		}
		else
		{
			%className = %value.getClassName();
			if(%this.objectClassName !$= %className && %this.objectClassName !$= "ALL")
				return false;
		}
	}

	%type = %this.type;
	switch$(getWord(%type,0))
	{
		case bool:
			if(%value !$= false && %value !$= true)
				return false;

		case string:
			%value = getSubStr(%value,0,getWord(%type,1));

		case int:
			if(!Slayer_Support::isNumber(%value))
				return false;
			%value = Slayer_Support::StripTrailingZeros(%value);
			%value = Slayer_Support::mRestrict(%value,getWord(%type,1),getWord(%type,2));

		case slide:
			if(!Slayer_Support::isNumber(%value))
				return false;
			%value = Slayer_Support::StripTrailingZeros(%value);
			%value = Slayer_Support::mRestrict(%value,getWord(%type,1),getWord(%type,2));

		case list:
			%count = getFieldCount(%type);
			if(%count > 1)
			{
				for(%i = 1; %i < %count; %i ++)
				{
					%f = getField(%type,%i);
					%w = firstWord(%f);
					if(%w == %value)
					{
						%ok = true;
						break;
					}
				}
				if(!%ok)
					return false;
			}
	}

	return true TAB %value;
}

// +------------+
// | serverCmds |
// +------------+
function serverCmdTeams(%client,%cmd,%a,%b,%c,%d,%e,%f,%g,%h,%i,%j,%k,%l)
{
	if(%client.isSpamming())
		return;

	%mini = getMinigameFromObject(%client);

	if(!isSlayerMinigame(%mini))
		return;

	if(!%mini.Gamemode.teams)
		return;

	%clTeam = %client.getTeam();

	switch$(%cmd)
	{
		case addMember:
			if(isSlayerMinigame(%client.editingMinigame))
				%mini = %client.editingMinigame;

			if(!%mini.canEdit(%client))
				return;

			if(%a $= "")
			{
				messageClient(%client,'',"\c5Incorrect usage. Please enter a player name, then a team name.");
				return;
			}

			if(isObject(%a) && %a.getClassName() $= "GameConnection")
				%victim = %a;
			else
				%victim = findClientByName(%a);

			if(!isObject(%victim))
			{
				messageClient(%client,'',"\c5Couldn't find player:\c3" SPC %a);
				return;
			}

			if(%victim.minigame != %mini)
			{
				messageClient(%client,'',"\c3" @ %victim.getPlayerName() SPC "\c5is not in the\c3" SPC %mini.Title SPC "\c5minigame.");
				return;
			}

			%name = trim(%b SPC %c SPC %d SPC %e SPC %f SPC %g SPC %h SPC %i SPC %j SPC %k SPC %l SPC %m);
			%team = %mini.Teams.getTeamFromName(%name);
			if(!isObject(%team))
			{
				messageClient(%client,'',"\c5Couldn't find team:\c3" SPC %name);
				return;
			}

			%tb = %mini.Teams.balanceTeams;
			%mini.Teams.balanceTeams = 0;

			%team.addMember(%victim);

			%mini.Teams.balanceTeams = %tb;

		case removeMember:
			if(isSlayerMinigame(%client.editingMinigame))
				%mini = %client.editingMinigame;

			if(!%mini.canEdit(%client))
				return;

			if(%a $= "")
			{
				messageClient(%client,'',"\c5Incorrect usage. Please enter a player name, then a team name.");
				return;
			}

			if(isObject(%a) && %a.getClassName() $= "GameConnection")
				%victim = %a;
			else
				%victim = findClientByName(%a);

			if(!isObject(%victim))
			{
				messageClient(%client,'',"\c5Couldn't find player:\c3" SPC %a);
				return;
			}
			if(%victim.minigame != %mini)
			{
				messageClient(%client,'',"\c3" @ %victim.getPlayerName() SPC "\c5is not in the\c3" SPC %mini.Title SPC "\c5minigame.");
				return;
			}

			%name = trim(%b SPC %c SPC %d SPC %e SPC %f SPC %g SPC %h SPC %i SPC %j SPC %k SPC %l SPC %m);
			%team = %mini.Teams.getTeamFromName(%name);
			if(!isObject(%team))
			{
				messageClient(%client,'',"\c5Couldn't find team:\c3" SPC %name);
				return;
			}

			%team.removeMember(%victim,1);

		case "balance": //MANUAL TEAM BALANCE
			if(!%mini.canEdit(%client))
				return;

			%mini.Teams.balanceTeams(1);
			%mini.messageAll('',"\c3" @ %client.getPlayerName() SPC "\c5balanced \c3all \c5teams.");

		case Join: //TEAM SWAPPING & JOINING
			if(%mini.Teams.lock && isObject(%clTeam))
			{
				messageClient(%client,'',"\c5Teams are locked, you cannot change teams.");
				return;
			}

			%name = trim(%a SPC %b SPC %c SPC %d SPC %e SPC %f SPC %g SPC %h SPC %i SPC %j SPC %k SPC %l);
			if(%name $= "")
			{
				messageClient(%client,'',"\c5Please enter a team to join. Type \c3/joinTeam Team Name \c5 to join a team. Type \c3/listTeams \c5to view a list of teams.");
				return;
			}

			%team = %mini.Teams.getTeamFromName(%name);
			if(!isObject(%team))
			{
				messageClient(%client,'',"\c5Couldn't find team:\c3" SPC %name);
				return;
			}

			if(%clTeam.lock || %team.lock)
			{
				messageClient(%client,'',"\c5Team is locked.");
				return;
			}

			if(%clTeam == %team)
			{
				messageClient(%client,'',"\c5You're already on" SPC %team.getColoredName() @ "\c5.");
				return;
			}

			if(isObject(%clTeam.swapPending))
			{
				messageClient(%client,'',"\c5Someone on your team is already trying to swap teams.");
				return;
			}
			if(isObject(%team.swapOffer))
			{
				messageClient(%client,'',"\c5Someone is already trying to swap to that team.");
				return;
			}
			if(%team.numMembers >= %team.maxPlayers && %team.maxPlayers >= 0)
			{
				messageClient(%client,'',"\c5That team is not accepting any new members.");
				return;
			}
			if(isObject(%clTeam))
			{
				if(%team.swapPending == %clTeam)
				{
					%mini.Teams.swapTeamMembers(%client,%team.swapClient);
					return;
				}
				if((%clTeam.numMembers > %team.numMembers && %team.maxPlayers == -1) || (%clTeam.numMembers > %team.numMembers && %team.numMembers < %team.maxPlayers && %team.maxPlayers != -1))
					%team.addMember(%client,"",%mini.isResetting());
				else
				{
					%team.messageAll('',%clTeam.getColorHex() @ %client.name SPC "\c5wants to swap with someone on" SPC %team.getColorHex() @ %team.name @ "\c5. Type \c3/acceptSwap \c5to swap teams.");
					%team.swapOffer = %clTeam;
					messageClient(%client,'',"\c5The members of" SPC %team.getColorHex() @ %team.name SPC "\c5have been asked if they want to swap with you.");
					%clTeam.swapPending = %team;
					%clTeam.swapClient = %client;
					%clTeam.swapTime = schedule(20000,0,serverCmdTeams,%client,cancelSwap);
				}	
			}
			else
				%team.addMember(%client,"",%mini.isResetting());

		case Leave:
			if(!isObject(%clTeam))
			{
				messageClient(%client,'',"\c5You aren't on a team.");
				return;
			}
			if((%mini.Teams.lock || %mini.Teams.autoSort) && !%mini.canEdit(%client))
			{
				messageClient(%client,'',"\c5You cannot leave your team.");
				return;
			}

			messageClient(%client,'',"\c5You have left" SPC %clTeam.getColoredName());
			if(%mini.Teams.notifyMemberChanges)
				%mini.messageAllExcept(%client,'',"\c3" @ %client.getPlayerName() SPC "\c5has left" SPC %clTeam.getColoredName());
			%clTeam.removeMember(%client,1);

		case AcceptSwap:
			if(!isObject(%clTeam))
				return;
			if(!isObject(%clTeam.swapOffer))
			{
				messageClient(%client,'',"\c5No one is trying to swap teams right now.");
				return;
			}

			%mini.Teams.swapTeamMembers(%client,%clTeam.swapOffer.swapClient);

		case cancelSwap:
			if(%client != %clTeam.swapClient)
				return;
			messageClient(%client,'',"\c5No one swapped teams.");
			%clTeam.swapPending.swapOffer = "";
			%clTeam.swapPending = "";
			%clTeam.swapClient = "";

		case List:
			if(%mini.Teams.getCount() <= 0)
			{
				messageClient(%client,'',"\c5There are no teams.");
				return;
			}
			for(%i=0; %i < %mini.Teams.getCount(); %i++)
			{
				%team = %mini.Teams.getObject(%i);
				messageClient(%client,'',%team.getColorHex() @ %team.name);
			}

		case Count:
			if(%mini.Teams.getCount() <= 0)
			{
				messageClient(%client,'',"\c5There are no teams.");
				return;
			}
			for(%i=0; %i < %mini.Teams.getCount(); %i++)
			{
				%team = %mini.Teams.getObject(%i);
				messageClient(%client,'',"\c3(" @ %team.numMembers @ ")" SPC %team.getColorHex() @ %team.name);
			}

		case Score:
			if(%mini.Teams.getCount() <= 0)
			{
				messageClient(%client,'',"\c5There are no teams.");
				return;
			}
			for(%i=0; %i < %mini.Teams.getCount(); %i++)
			{
				%team = %mini.Teams.getObject(%i);
				messageClient(%client,'',"\c3(" @ %team.getScore() @ ")" SPC %team.getColorHex() @ %team.name);
			}

		case Living:
			if(%mini.Teams.getCount() <= 0)
			{
				messageClient(%client,'',"\c5There are no teams.");
				return;
			}
			for(%i=0; %i < %mini.Teams.getCount(); %i++)
			{
				%team = %mini.Teams.getObject(%i);
				messageClient(%client,'',"\c3(" @ %team.getLiving() @ ")" SPC %team.getColorHex() @ %team.name);
			}

		case listMembers:
			%name = trim(%a SPC %b SPC %c SPC %d SPC %e SPC %f SPC %g SPC %h SPC %i SPC %j SPC %k SPC %l);
			%team = %mini.Teams.getTeamFromName(%name);
			if(!isObject(%team))
			{
				messageClient(%client,'',"\c5Couldn't find team:\c3" SPC %name);
				return;
			}

			messageClient(%client,'',%team.getColoredName() SPC "\c3(" @ %team.numMembers @ ")\c5:");
			for(%i=0; %i < %team.numMembers; %i++)
			{
				%cl = %team.member[%i];
				messageClient(%client,''," \c5+ \c3" @ %cl.getPlayerName());
			}

		default:
			messageClient(%client,'',"\c5Please enter a valid command.");
	}
}

function serverCmdSlayer_removeTeams(%client,%removeTeamList)
{
	%mini = %client.editingMinigame;
	if(!isObject(%mini))
		%mini = getMinigameFromObject(%client);

	if(!isSlayerMinigame(%mini))
		return;
	if(!%mini.canEdit(%client))
		return;

	for(%i=0; %i < getFieldCount(%removeTeamList); %i++) //remove teams
	{
		%t = getField(%removeTeamList,%i);

		if(isObject(%t) && %t.class $= "Slayer_TeamSO")
			%mini.Teams.removeTeam(%t);
	}
}

function serverCmdSlayer_getTeamPrefs_Start(%client,%team,%count)
{
	%mini = %client.editingMinigame;
	if(!isObject(%mini))
		%mini = getMinigameFromObject(%client);

	if(!isSlayerMinigame(%mini))
		return;
	if(!%mini.canEdit(%client))
		return;
	if(%team < -1) //check if it's a new team
	{
		%team = %mini.Teams.addTeam(0);
		%mini.Teams.TEMP = %team;
	}
	if(!isObject(%team))
		return;

	if(%team != %mini.Teams.TEMP && %count > 0 && %mini.notifyOnChange)
	{
		%message = '\c3 + \c5Updated %1 \c5(team).';
		%mini.messageAll('',%message,%team.getColoredName());
	}
}

function serverCmdSlayer_getTeamPrefs_Tick(%client,%team,%category,%title,%value)
{
	%mini = %client.editingMinigame;
	if(!isObject(%mini))
		%mini = getMinigameFromObject(%client);

	if(!isSlayerMinigame(%mini))
		return;
	if(!%mini.canEdit(%client))
		return;

	if(%team < -1)
		%team = %mini.Teams.TEMP;
	if(!isObject(%team))
		return;

	if(!%mini.Teams.isMember(%team))
		return;

	%pref = Slayer.TeamPrefs.getPrefSO(%category,%title);

	if(!%mini.canEdit(%client,%pref.editRights))
		return;

	Slayer_Support::Debug(2,"Recieved Team Pref",%client SPC %team SPC %category SPC %title SPC %value);

	if(%pref.getValue(%team) $= %value)
		return;

	%success = %pref.setValue(%team,%value);

	if(%success)
	{
		if(%team != %mini.Teams.TEMP && %pref.notify && %mini.notifyOnChange)
		{
			%type = %pref.type;
			switch$(getWord(%type,0))
			{
				case bool:
					if(%value)
						%val = "True";
					else
						%val = "False";

				case list:
					for(%i=0; %i < getFieldCount(%type); %i++)
					{
						%f = getField(%type,%i);
						if(firstWord(%f) $= %value)
						{
							%val = restWords(%f);
							break;
						}
					}

				default:
					%val = %value;
			}
			%message = '\c3 + + \c5[\c3%1\c5|\c3%2\c5] is now \c3%3';
			%mini.messageAll('',%message,%category,%title,%val);
		}
	}
}

function serverCmdSlayer_getTeamPrefs_End(%client,%team)
{
	%mini = %client.editingMinigame;
	if(!isObject(%mini))
		%mini = getMinigameFromObject(%client);

	if(!isSlayerMinigame(%mini))
		return;
	if(!%mini.canEdit(%client))
		return;

	if(%team < -1)
		%team = %mini.Teams.TEMP;
	if(!isObject(%team))
		return;

	if(!%mini.Teams.isMember(%team))
		return;

	if(%team == %mini.Teams.TEMP)
	{
		%mini.Teams.onNewTeam(%team);
		%mini.Teams.TEMP = "";
		%mini.notifyNewTeam ++;
	}
}

// +--------------------+
// | Packaged Functions |
// +--------------------+
package Slayer_Dependencies_Teams
{
	function GameConnection::onDeath(%client,%obj,%killer,%type,%area)
	{
		parent::onDeath(%client,%obj,%killer,%type,%area);

		%mini = getMinigameFromObject(%client);
		if(!isSlayerMinigame(%mini))
			return;

		%team = %client.getTeam();
		if(!isObject(%team))
			return;

		if(isObject(%team.balanceTeam) && %mini.Teams.balanceTeams)
			%team.balanceTeam.addMember(%client,"Team Balance");
	}

	function serverCmdTeamMessageSent(%client,%msg)
	{
		%mini = getMinigameFromObject(%client);
		if(!isSlayerMinigame(%mini))
			return parent::servercmdTeamMessageSent(%client,%msg);

		serverCmdStopTalking(%client);

		%team = %client.getTeam();

		if((isObject(%team) && !%team.enableTeamChat) || !%mini.Teams.enableTeamChat)
		{
			messageClient(%client,'',"\c5Team chat disabled.");
			return;
		}

		%msg = stripMLControlChars(trim(%msg));
		%time = getSimTime();

		if(%msg $= "")
			return;

		if(!isObject(%team))
			return parent::servercmdTeamMessageSent(%client,%msg);

		%length = strlen(%msg);

		//did they repeat the same message recently?
		if(%msg $= %client.lastMsg && %time-%client.lastMsgTime < $SPAM_PROTECTION_PERIOD)
		{
			if(!%client.isSpamming)
			{
				messageClient(%client,'',"\c5Do not repeat yourself.");
				if(!%client.isAdmin)
				{
					%client.isSpamming = 1;
					%client.spamProtectStart = %time;
					%client.schedule($SPAM_PENALTY_PERIOD,spamReset);
				}
			}
		}

		//are they sending messages too quickly?
		if(!%client.isAdmin && !%client.isSpamming)
		{
			if(%client.spamMessageCount >= $SPAM_MESSAGE_THRESHOLD)
			{
				%client.isSpamming = 1;
				%client.spamProtectStart = %time;
				%client.schedule($SPAM_PENALTY_PERIOD,spamReset);
			}
			else
			{
				%client.spamMessageCount ++;
				%client.schedule($SPAM_PROTECTION_PERIOD,spamMessageTimeout);
			}
		}

		//tell them they're spamming and block the message
		if(%client.isSpamming)
		{
			spamAlert(%client);
			return;
		}

		//eTard Filter, which I hate, but have to include
		if($Pref::Server::eTardFilter)
		{
			%list = strReplace($Pref::Server::eTardList,",","\t");

			for(%i=0; %i < getFieldCount(%list); %i++)
			{
				%wrd = trim(getField(%list,%i));
				if(%wrd $= "")
					continue;
				if(striPos(" " @ %msg @ " "," " @ %wrd @ " ") >= 0)
				{
					messageClient(%client,'',"\c5This is a civilized game. Please use full words.");
					return;
				}
			}
		}

		//URLs
		for(%i=0; %i < getWordCount(%msg); %i++)
		{
			%word = getWord(%msg,%i);
			%url  = getSubStr(%word,7,strLen(%word));

			if(getSubStr(%word,0,7) $= "http://" && strPos(%url,":") == -1)
			{
				%word = "<sPush><a:" @ %url @ ">" @ %url @ "</a><sPop>";
				%msg = setWord(%msg,%i,%word);
			}
		}

		//get the names and clan tags
		%name = %client.getPlayerName();
		%pre  = %client.clanPrefix;
		%suf  = %client.clanSuffix;

		//let people know what team they're on
		%tColor = %team.getPref("Chat","Name Color");
		if(%tColor $= "" || strLen(%tColor) != 6)
			%color = %team.getColorHex();
		else
			%color = "<color:" @ %tColor @ ">";

		switch(%mini.chat_teamDisplayMode)
		{
			case 1:
				%all  = '%5%1\c3%2%5%3%7: %4';

			case 2:
				%all  = '\c7%1%5%2\c7%3%7: %4';

			case 3:
				%all  = '\c7[%5%6\c7] %1\c3%2\c7%3%7: %4';
		}

		if(%all $= "")
			%all  = '\c7%1\c3%2\c7%3%7: %4';

		if(%client.dead() && (%mini.chat_deadChatMode == 0 || %mini.chat_deadChatMode == 2) && !%mini.isResetting())
		{
			if(%mini.Teams.allySameColors)
			{
				%mini.Teams.messageAllDeadByColor(%team.color,'chatMessage',%all,%pre,%name,%suf,%msg,%color,%team.name," \c7[DEAD]<color:00ffff>");
			}
			else
			{
				%team.messageAllDead('chatMessage',%all,%pre,%name,%suf,%msg,%color,%team.name," \c7[DEAD]<color:00ffff>");
			}
		}
		else
		{
			if(%mini.Teams.allySameColors)
			{
				%mini.Teams.messageAllByColor(%team.color,'chatMessage',%all,%pre,%name,%suf,%msg,%color,%team.name,"<color:00ffff>");
			}
			else
			{
				%team.messageAll('chatMessage',%all,%pre,%name,%suf,%msg,%color,%team.name,"<color:00ffff>");
			}
		}

		echo("(T" SPC %team.name @ ")" @ %client.getSimpleName() @ ":" SPC %msg);

		%client.lastMsg = %msg;
		%client.lastMsgTime = %time;

		if(isObject(%client.player))
		{
			%client.player.playThread(3,"talk");
			%client.player.schedule(%length * 50,playThread,3,"root");
		}

		if(isFunction("Slayer_" @ %mini.mode @ "_Teams_onChat"))
			call("Slayer_" @ %mini.mode @ "_Teams_onChat",%mini,%client,%msg);
	}

	//The below functions are packaged for TDM compatibilty
	function serverCmdJoinTeam(%client,%a,%b,%c,%d,%e,%f,%g,%h,%i,%j,%k,%l)
	{
		if(isSlayerMinigame(%client.minigame) || $Addon__Gamemode_TeamDeathMatch != 1)
			serverCmdTeams(%client,Join,%a,%b,%c,%d,%e,%f,%g,%h,%i,%j,%k,%l);
		else
			parent::serverCmdJoinTeam(%client,%a,%b,%c,%d,%e,%f,%g,%h,%i,%j,%k,%l);
	}

	function serverCmdSwapTeam(%client,%a,%b,%c,%d,%e,%f,%g,%h,%i,%j,%k,%l)
	{
		if(isSlayerMinigame(%client.minigame) || $Addon__Gamemode_TeamDeathMatch != 1)
			serverCmdTeams(%client,Join,%a,%b,%c,%d,%e,%f,%g,%h,%i,%j,%k,%l);
		else
			parent::serverCmdSwapTeam(%client,%a,%b,%c,%d,%e,%f,%g,%h,%i,%j,%k,%l);
	}

	function serverCmdSwapTeams(%client,%a,%b,%c,%d,%e,%f,%g,%h,%i,%j,%k,%l)
	{
//		if(isSlayerMinigame(%client.minigame) || $Addon__Gamemode_TeamDeathMatch != 1)
			serverCmdTeams(%client,Join,%a,%b,%c,%d,%e,%f,%g,%h,%i,%j,%k,%l);
//		else
//			parent::serverCmdSwapTeams(%client,%a,%b,%c,%d,%e,%f,%g,%h,%i,%j,%k,%l);
	}

	function serverCmdAcceptSwap(%client)
	{
		if(isSlayerMinigame(%client.minigame) || $Addon__Gamemode_TeamDeathMatch != 1)
			serverCmdTeams(%client,AcceptSwap);
		else
			parent::serverCmdAcceptSwap(%client);
	}

	function serverCmdLeaveTeam(%client)
	{
		if(isSlayerMinigame(%client.minigame) || $Addon__Gamemode_TeamDeathMatch != 1)
			serverCmdTeams(%client,Leave);
		else
			parent::serverCmdLeaveTeam(%client);
	}

	function serverCmdTeamList(%client)
	{
		if(isSlayerMinigame(%client.minigame) || $Addon__Gamemode_TeamDeathMatch != 1)
			serverCmdTeams(%client,List);
		else
			parent::serverCmdTeamList(%client);
	}

	function serverCmdListTeams(%client)
	{
//		if(isSlayerMinigame(%client.minigame) || $Addon__Gamemode_TeamDeathMatch != 1)
			serverCmdTeams(%client,List);
//		else
//			parent::serverCmdListTeams(%client);
	}

	function serverCmdTeamCount(%client)
	{
		if(isSlayerMinigame(%client.minigame) || $Addon__Gamemode_TeamDeathMatch != 1)
			serverCmdTeams(%client,Count);
		else
			parent::serverCmdTeamCount(%client);
	}

	function serverCmdTeamScore(%client)
	{
//		if(isSlayerMinigame(%client.minigame) || $Addon__Gamemode_TeamDeathMatch != 1)
			serverCmdTeams(%client,Score);
//		else
//			parent::serverCmdTeamScore(%client);
	}

	function serverCmdTeamLiving(%client)
	{
//		if(isSlayerMinigame(%client.minigame) || $Addon__Gamemode_TeamDeathMatch != 1)
			serverCmdTeams(%client,Living);
//		else
//			parent::serverCmdTeamScore(%client);
	}
};
activatePackage(Slayer_Dependencies_Teams);

// +-----------------+
// | Add Preferences |
// +-----------------+
// USAGE //(category,title,variable,type,defaultValue,notify,editRights,list,callback)
Slayer.TeamPrefs.addPref("Team","Name","name","string 50","New Team",1,-1,"","%team.updateName(%1);");
Slayer.TeamPrefs.addPref("Team","Color","color","colorID",0,0,-1,"","%team.updateColor(%1);");
Slayer.TeamPrefs.addPref("Team","Auto Sort","sort","bool",1,1,-1);
Slayer.TeamPrefs.addPref("Team","Auto Sort Weight","sortWeight","int 0 99",1,1,-1);
Slayer.TeamPrefs.addPref("Team","Max Players","maxPlayers","int -1 99",-1,1,-1);
Slayer.TeamPrefs.addPref("Team","Lock Team","lock","bool",0,1,-1,"Advanced");

Slayer.TeamPrefs.addPref("Team","Playertype","playerDatablock" TAB "PlayerData" TAB 1,"list",nameToID(playerStandardArmor),0,-1,"","%team.updateDatablock(%1);");
Slayer.TeamPrefs.addPref("Team","Start Equip 0","startEquip0" TAB "ItemData","list",nameToID(hammerItem),0,-1,"","%team.updateEquip(0,%1,%2);");
Slayer.TeamPrefs.addPref("Team","Start Equip 1","startEquip1" TAB "ItemData","list",nameToID(wrenchItem),0,-1,"","%team.updateEquip(1,%1,%2);");
Slayer.TeamPrefs.addPref("Team","Start Equip 2","startEquip2" TAB "ItemData","list",nameToID(printGun),0,-1,"","%team.updateEquip(2,%1,%2);");
Slayer.TeamPrefs.addPref("Team","Start Equip 3","startEquip3" TAB "ItemData","list",0,0,-1,"","%team.updateEquip(3,%1,%2);");
Slayer.TeamPrefs.addPref("Team","Start Equip 4","startEquip4" TAB "ItemData","list",0,0,-1,"","%team.updateEquip(4,%1,%2);");

Slayer.TeamPrefs.addPref("Team","Uniform","uniform","list" TAB "0 NONE" TAB "1 Shirt Only" TAB "2 Full" TAB "3 Custom",2,1,-1,"","%team.updateUniform();");

Slayer.TeamPrefs.addPref("Team","Lives","lives","int -1 999",-1,1,-1,"Advanced","%team.updateLives(%1,%2);");
Slayer.TeamPrefs.addPref("Team","Respawn Time","respawnTime_Player","int -1 999",-1,1,-1,"Advanced","%team.updateRespawnTime(player,%1,%2);");

Slayer.TeamPrefs.addPref("Team","Win on Time Up","winOnTimeUp","bool",0,1,-1,"Advanced");

Slayer.TeamPrefs.addPref("Team","Player Scale","playerScale","slide 0 5 4 1",1,1,-1,"Advanced","%team.updatePlayerScale(%1);");

Slayer.TeamPrefs.addPref("Team","Enable Team Chat","enableTeamChat","bool",1,1,-1,"Advanced");

Slayer.TeamPrefs.addPref("Chat","Name Color","chatColor","string 6","",0,-1,"Advanced");

//UNIFORMS
Slayer.TeamPrefs.addPref("Uniform","Accent","uni_accent","string 100","0",0,-1);
Slayer.TeamPrefs.addPref("Uniform","AccentColor","uni_accentColor","string 100","TEAMCOLOR",0,-1);
Slayer.TeamPrefs.addPref("Uniform","Chest","uni_chest","string 100","0",0,-1);
Slayer.TeamPrefs.addPref("Uniform","ChestColor","uni_chestColor","string 100","",0,-1);
Slayer.TeamPrefs.addPref("Uniform","DecalColor","uni_decalColor","string 100","0",0,-1);
Slayer.TeamPrefs.addPref("Uniform","DecalName","uni_decalName","string 100","AAA-None",0,-1);
Slayer.TeamPrefs.addPref("Uniform","FaceColor","uni_faceColor","string 100","0",0,-1);
Slayer.TeamPrefs.addPref("Uniform","FaceName","uni_faceName","string 100","smiley",0,-1);
Slayer.TeamPrefs.addPref("Uniform","Hat","uni_hat","string 100","1",0,-1);
Slayer.TeamPrefs.addPref("Uniform","HatColor","uni_hatColor","string 100","TEAMCOLOR",0,-1);
Slayer.TeamPrefs.addPref("Uniform","HeadColor","uni_headColor","string 100","1 0.878431 0.611765 1",0,-1);
Slayer.TeamPrefs.addPref("Uniform","Hip","uni_hip","string 100","0",0,-1);
Slayer.TeamPrefs.addPref("Uniform","HipColor","uni_hipColor","string 100","TEAMCOLOR",0,-1);
Slayer.TeamPrefs.addPref("Uniform","LArm","uni_lArm","string 100","0",0,-1);
Slayer.TeamPrefs.addPref("Uniform","LArmColor","uni_lArmColor","string 100","TEAMCOLOR",0,-1);
Slayer.TeamPrefs.addPref("Uniform","LHand","uni_lHand","string 100","0",0,-1);
Slayer.TeamPrefs.addPref("Uniform","LHandColor","uni_lHandColor","string 100","1 0.878431 0.611765 1",0,-1);
Slayer.TeamPrefs.addPref("Uniform","LLeg","uni_lLeg","string 100","0",0,-1);
Slayer.TeamPrefs.addPref("Uniform","LLegColor","uni_lLegColor","string 100","TEAMCOLOR",0,-1);
Slayer.TeamPrefs.addPref("Uniform","Pack","uni_pack","string 100","0",0,-1);
Slayer.TeamPrefs.addPref("Uniform","PackColor","uni_packColor","string 100","TEAMCOLOR",0,-1);
Slayer.TeamPrefs.addPref("Uniform","RArm","uni_rArm","string 100","0",0,-1);
Slayer.TeamPrefs.addPref("Uniform","RArmColor","uni_rArmColor","string 100","TEAMCOLOR",0,-1);
Slayer.TeamPrefs.addPref("Uniform","RHand","uni_rHand","string 100","0",0,-1);
Slayer.TeamPrefs.addPref("Uniform","RHandColor","uni_rHandColor","string 100","1 0.878431 0.611765 1",0,-1);
Slayer.TeamPrefs.addPref("Uniform","RLeg","uni_rLeg","string 100","0",0,-1);
Slayer.TeamPrefs.addPref("Uniform","RLegColor","uni_rLegColor","string 100","TEAMCOLOR",0,-1);
Slayer.TeamPrefs.addPref("Uniform","SecondPack","uni_secondPack","string 100","0",0,-1);
Slayer.TeamPrefs.addPref("Uniform","SecondPackColor","uni_secondPackColor","string 100","TEAMCOLOR",0,-1);
Slayer.TeamPrefs.addPref("Uniform","TorsoColor","uni_torsoColor","string 100","TEAMCOLOR",0,-1);

Slayer.TeamPrefs.addPref("Team","Uni Update Trigger","uni_updateTrigger","int 0 999999",0,0,-1,"","%team.updateUniform();");

Slayer_Support::Debug(2,"Dependency Loaded","Teams");