// +-------------------------------------------+
// |  ___   _     _____   __  __  ____   ____  |
// | | __| | |   | /_\ |  \ \/ / | ___| | /\ | |
// | |__ | | |_  |  _  |   \  /  | __|  | \/ / |
// | |___| |___| |_| |_|   |__|  |____| |_| \\ |
// |  Greek2me              Blockland ID 11902 |
// +-----------------------------+-------------+
// | DO NOT EDIT BELOW THIS LINE |
// +-----------------------------+

$Slayer::Server::Sounds::MaxCountDown = 10;

for($sound = 1; $sound <= $Slayer::Server::Sounds::MaxCountDown; $sound ++)
{
	datablock AudioProfile(slayerSound)
	{
		filename = "./" @ $sound @ ".wav";
		description = AudioClosest3d;
		preload = false;
	};
	slayerSound.setName("Slayer_" @ $sound @ "_Seconds_Sound");
}

datablock AudioProfile(Slayer_Begin_Sound)
{
	filename = "./buzzer.wav";
	description = AudioClosest3d;
	preload = false;
};