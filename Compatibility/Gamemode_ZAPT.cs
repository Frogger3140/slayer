// +-------------------------------------------+
// |  ___   _     _____   __  __  ____   ____  |
// | | __| | |   | /_\ |  \ \/ / | ___| | /\ | |
// | |__ | | |_  |  _  |   \  /  | __|  | \/ / |
// | |___| |___| |_| |_|   |__|  |____| |_| \\ |
// |  Greek2me              Blockland ID 11902 |
// +-----------------------------+-------------+
// | DO NOT EDIT BELOW THIS LINE |
// +-----------------------------+

$Slayer::Server::Compatibility::Gamemode_ZAPT = 1;

if($AddOn__GameMode_ZAPT == 1)
{
	Slayer.Prefs.addPref("ZAPT","Enable ZAPT!","%mini.ZAPT_Enabled","bool",1,1,1,3,"Advanced");
	Slayer.Prefs.addPref("ZAPT","Gamemode","%mini.ZAPT_Gamemode","list" TAB "0 Infection" TAB "1 Versus" TAB "2 Man vs Tank",0,1,1,-1,"Advanced");
	Slayer.Prefs.addPref("ZAPT","Disable Friendly-Fire","%mini.ZAPT_FF","bool",1,0,1,3,"Advanced");
	Slayer.Prefs.addPref("ZAPT","Outbreak","%mini.ZAPT_Outbreak","bool",1,0,1,-1,"Advanced");
	Slayer.Prefs.addPref("ZAPT","Spawn Mobs","%mini.ZAPT_Mobs","bool",1,0,1,-1,"Advanced");
	Slayer.Prefs.addPref("ZAPT","Enable Tank Selection","%mini.ZAPT_Tank","bool",1,0,1,-1,"Advanced");
	Slayer.Prefs.addPref("ZAPT","Enable Player Zombies","%mini.ZAPT_PlayerZombies","bool",1,0,1,-1,"Advanced");
	Slayer.Prefs.addPref("ZAPT","Rescue on Countdown","%mini.ZAPT_Rescue","bool",1,0,1,-1,"Advanced");
	Slayer.Prefs.addPref("ZAPT","Round Timer Enabled","%mini.ZAPT_Timer","bool",1,0,1,-1,"Advanced");
	Slayer.Prefs.addPref("ZAPT","Allow Suicide","%mini.ZAPT_Suicide","bool",0,0,1,-1,"Advanced");

	Slayer.Prefs.addPref("ZAPT - Points","Eating a Player","%mini.ZAPT_Points_EatingPlayer","int -999 999",3,0,0,-1,"Advanced");
	Slayer.Prefs.addPref("ZAPT - Points","Getting Eaten","%mini.ZAPT_Points_GettingEaten","int -999 999",0,0,0,-1,"Advanced");
	Slayer.Prefs.addPref("ZAPT - Points","Killing a Bot Zombie","%mini.ZAPT_Points_KillBot","int -999 999",1,0,0,-1,"Advanced");
	Slayer.Prefs.addPref("ZAPT - Points","Killing a Player Zombie","%mini.ZAPT_Points_KillPlayer","int -999 999",2,0,0,-1,"Advanced");
	Slayer.Prefs.addPref("ZAPT - Points","Killing a Tank","%mini.ZAPT_Points_KillTank","int -999 999",10,0,0,-1,"Advanced");
	Slayer.Prefs.addPref("ZAPT - Points","Killing a Teammate","%mini.ZAPT_Points_KillTeam","int -999 999",-5,0,0,-1,"Advanced");

	Slayer.Prefs.addPref("ZAPT - Time","Outbreak Timer","%mini.ZAPT_Time_Outbreak","int 5 999",30,0,1,-1,"Advanced");
	Slayer.Prefs.addPref("ZAPT - Time","Round Timer","%mini.ZAPT_Time_Rescue","int 5 999",180,0,1,-1,"Advanced");
	Slayer.Prefs.addPref("ZAPT - Time","Reset Timer","%mini.ZAPT_Time_Reset","int 5 30",10,0,0,-1,"Advanced");
}

// +--------------------+
// | Packaged Functions |
// +--------------------+
package Slayer_Compatibility_Gamemode_ZAPT
{
	function Slayer_MinigameSO::onAdd(%this)
	{
		parent::onAdd(%this);

		if($AddOn__GameMode_ZAPT == 1 && isFunction(Slayer_MinigameSO,assembleZombieGame))
		{
			%this.assembleZombieGame();
		}
	}

	function zombieValidTarget(%obj,%target)
	{
		%mini = getMinigameFromObject(%obj);

		if(isSlayerMinigame(%mini) && !isObject(%mini.owner))
		{
			%fake = %mini.getFakeClient();
			%mini.owner = %fake;

			%par = parent::zombieValidTarget(%obj,%target);

			%mini.owner = "";

			return %par;
		}

		return parent::zombieValidTarget(%obj,%target);
	}

	function fxDtsBrick::zombieCanSpawn(%this)
	{
		%mini = getMinigameFromObject(%this);

		if(isSlayerMinigame(%mini) && !isObject(%mini.owner))
		{
			%fake = %mini.getFakeClient();
			%mini.owner = %fake;

			%par = parent::zombieCanSpawn(%this);

			%mini.owner = "";

			return %par;
		}

		return parent::zombieCanSpawn(%this);
	}
};
activatePackage(Slayer_Compatibility_Gamemode_ZAPT);

Slayer_Support::Debug(2,"Compatibility File Loaded","Gamemode_ZAPT");