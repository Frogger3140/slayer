// +-------------------------------------------+
// |  ___   _     _____   __  __  ____   ____  |
// | | __| | |   | /_\ |  \ \/ / | ___| | /\ | |
// | |__ | | |_  |  _  |   \  /  | __|  | \/ / |
// | |___| |___| |_| |_|   |__|  |____| |_| \\ |
// |  Greek2me              Blockland ID 11902 |
// +-----------------------------+-------------+
// | DO NOT EDIT BELOW THIS LINE |
// +-----------------------------+

// +--------+
// | Slayer |
// +--------+
function Slayer::onAdd(%this)
{
	%this.isStarting = 1;

	//PRELOAD CONFIG
	if(isFile($Slayer::Server::ConfigDir @ "/config_preload.cs")) //preload
		exec($Slayer::Server::ConfigDir @ "/config_preload.cs");

	//DISPLAY INFO
	warn("Slayer Version" SPC $Slayer::Server::Version);
	warn(" + Compatible Version:" SPC $Slayer::Server::CompatibleVersion);
	warn(" + Core Directory:" SPC $Slayer::Server::Directory);
	warn(" + Config Directory:" SPC $Slayer::Server::ConfigDir);
	warn(" + Debug Mode:" SPC $Slayer::Server::Debug);

	//LOAD SUPPORT FILES
	exec($Slayer::Server::Directory @ "/Support/Support.cs");
	Slayer_Support::loadFiles($Slayer::Server::Directory @ "/Support/Support_*.cs","Support.cs");

	//DELETE PRE-V3 CONFIG FILES
	%this.DeleteOldConfigFiles();

	//INITIALIZE MAIN COMPONENTS
	exec($Slayer::Server::Directory @ "/Dependencies/Preferences.cs"); //we want to load this first
	Slayer_Support::LoadFiles($Slayer::Server::Directory @ "/Dependencies/*.cs","Preferences.cs"); //load dependencies
	Slayer_Support::LoadFiles($Slayer::Server::Directory @ "/Modules/*.cs"); //load modules

	//INITIALIZE BUILT-IN GAMEMODES
	Slayer_Support::LoadFiles($Slayer::Server::Directory @ "/Gamemodes/*.cs");

	//INITIALIZE COMPATIBILITY FILES
	Slayer_Support::LoadFiles($Slayer::Server::Directory @ "/Compatibility/*.cs"); //load compatibility files

	//CLIENT CHECK
		//Check if any clients that connected before Slayer loaded have the mod.
	for(%i=0; %i < clientGroup.getCount(); %i++)
		commandToClient(clientGroup.getObject(%i),'Slayer_Handshake');

	//BRICK CHECK
		//This is to check if any bricks were placed before Slayer was loaded.
		//If they were, check to see if they're Slayer bricks.
	Slayer_checkBricks();

	%this.isStarting = 0;
}

function Slayer::onRemove(%this)
{
	//minigames need to be deleted first
	%this.Minigames.delete();

	//export preload config
	%this.Prefs.exportPreload($Slayer::Server::ConfigDir @ "/config_preload.cs");

	Slayer_Support::Debug(0,"Deactivating Slayer");

	//close the debug logger
	if(isObject(%this.debugFO))
		%this.debugFO.close();
}

function Slayer::initClientAuth(%this,%client)
{
	%client.isHost = (%client.isLocalConnection() || %client.getBLID() == Slayer_Support::getBlocklandID());

	//tell the client that Slayer is enabled
	Slayer_Support::Debug(0,"Sending Handshake",%client.getSimpleName());
	commandToClient(%client,'Slayer_Handshake',$Slayer::Server::Version,$Slayer::Server::ModVersions);
}

function Slayer::onClientHandshakeAccepted(%this,%client,%version,%modVersions)
{
	//Slayer_Support::Debug(0,"Handshake ACCEPTED",%client.getSimpleName());

	//TELL THE CLIENT
	commandToClient(%client,'Slayer_getInitialData_Start');
}

function Slayer::onClientHandshakeDenied(%this,%client,%version,%modVersions,%reason)
{
	if(%reason $= "")
		%reason = "reason unspecified";
	Slayer_Support::Debug(0,"Handshake DENIED (" @ %reason @ ")",%client.getSimpleName());
}

function Slayer::sendInitialData(%this,%client)
{
	Slayer_Support::Debug(0,"Sending Initial Data",%client.getSimpleName());

	//START THE INITIAL DATA TRANSFER
	commandToClient(%client,'Slayer_getInitialData_Start');

	//SEND GAMEMODES
	%this.Gamemodes.sendGamemodes(%client);

	//END THE INITIAL DATA TRANSFER
	commandToClient(%client,'Slayer_getInitialData_End');

	%client.slayerInitialDataSent = 1;

	%this.onInitialDataSent(%client);
}

function Slayer::onInitialDataSent(%this,%client)
{

}

function Slayer::sendData(%this,%client,%mini,%gui)
{
	if(!isObject(%mini) && %mini != -1)
		%mini = getMinigameFromObject(%client);

	if(isSlayerMinigame(%mini) && !%mini.canEdit(%client))
		return;

	//SEND INITIAL DATA
	if(!%client.slayerInitialDataSent)
		%this.sendInitialData(%client);

	Slayer_Support::Debug(1,"Sending Data",%client.getSimpleName());

	%client.editingMinigame = %mini;

	if(isSlayerMinigame(%mini))
		%miniState = "EDIT";
	else
		%miniState = "CREATE";

	//START THE DATA TRANSFER
	commandToClient(%client,'Slayer_getData_Start',%mini,%client.getAdminLvl(),%miniState);

	//SEND PRERERENCES
	%this.Prefs.sendPrefs(%client,%mini);

	//SEND TEAMS
	if(isObject(%mini.Teams))
		%mini.Teams.sendTeams(%client,%mini);
	else
		%this.TeamPrefs.sendBlankTeamData(%client);

	//END THE DATA TRANSFER
	commandToClient(%client,'Slayer_getData_End',%gui);

	%this.onDataSent(%client,%mini,%gui);
}

function Slayer::onDataSent(%this,%client,%mini,%gui)
{

}

function Slayer::deleteOldConfigFiles(%this)
{
	%path = $Slayer::Server::ConfigDir;

	if(isFile(%path @ "/Preferences.cs"))
	{
		fileDelete(%path @ "/Preferences.cs");
		%warn = 1;
	}

	if(isFile(%path @ "/Preload.cs"))
	{
		fileDelete(%path @ "/Preload.cs");
		%warn = 1;
	}

	if(isFile(%path @ "/server.log"))
	{
		fileDelete(%path @ "/server.log");
		%warn = 1;
	}

	if(%warn)
		Slayer_Support::Error("Deleting old config files");
}

// +------------+
// | serverCmds |
// +------------+
function serverCmdSlayer_Handshake(%client,%version,%modVersions)
{
	if(%client.isSpamming())
	{
		if(!%client.slayer_handshakeSpam)
		{
			Slayer_Support::Error("serverCmdSlayer_Handshake",%client.getSimpleName() SPC "is shaking hands like crazy!");
			%client.slayer_handshakeSpam = 1;
		}
		return;
	}

	%client.slayerVersion = %version;
	if(%version !$= $Slayer::Server::Version)
	{
		if(Slayer_Support::isNewerVersion($Slayer::Server::Version,%version))
		{
			if(restWords($Slayer::Server::Version) $= "" || restWords(%version) !$= "")
				messageClient(%client,'',"\c0WARNING: \c6You have an \c3older \c6version of \c3Slayer \c6than the server. You can download the latest version <a:forum.returntoblockland.com/dlm/viewFile.php?id=3755>here</a>\c6.");
		}
		else if(!Slayer_Support::isNewerVersion(%version,"3.4.1")) //check if their client displays this automatically or not
			messageClient(%client,'',"\c0WARNING: \c6You have a \c3newer \c6version of \c3Slayer \c6than the server. Ask the host to update it.");

		%newer = Slayer_Support::getNewerVersion(%version,$Slayer::Server::CompatibleVersion);
		if($Slayer::Server::CompatibleVersion $= "" || (%newer !$= %version && %newer !$= "-1"))
		{
			Slayer.onClientHandshakeDenied(%client,%version,%modVersions,"version not compatible");
			return;
		}

		messageClient(%client,'',"\c0 + \c6You are still able to use the Slayer GUI, but some features may not work.");
	}

	%client.slayer = 1;

	Slayer_Support::Debug(0,"Slayer client registered",%client.getSimpleName() SPC "has Version" SPC %version);

	if($Slayer::Server::ModVersions !$= "" && %modVersions !$= "")
	{
		for(%i=0; %i < getFieldCount(%modVersions); %i++)
		{
			%field = getField(%modVersions,%i);
			%mod = firstWord(%field);
			%mv = restWords(%field);

			for(%e=0; %e < getFieldCount($Slayer::Server::ModVersions); %e++)
			{
				%field = getField($Slayer::Server::ModVersions,%e);
				if(%mod $= firstWord(%field))
				{
					%client.slayerModVersion_[%mod] = %mv;
					if(%mv $= restWords(%field))
					{
						%client.slayerMod_[%mod] = 1;
					}

					Slayer_Support::Debug(0," + Client mod registered",%mod SPC "version" SPC %mv);
				}
			}
		}
	}

	Slayer.onClientHandshakeAccepted(%client,%version,%modVersions);
}

function serverCmdSlayer_sendMiniState(%client)
{
	if(!%client.slayer)
		return;

	if(%client.isSpamming())
		return;

	%mini = getMinigameFromObject(%client);
	%canEdit = (isObject(%mini) && %mini.canEdit(%client));

	commandToClient(%client,'Slayer_getMiniState',%client.getMiniState(),%canEdit);

	Slayer.Minigames.broadcastAllMinigames(%client,1);
}

// +--------------------+
// | Packaged Functions |
// +--------------------+
package Slayer_Main
{
	//CLEANUP
	function destroyServer()
	{
		if(isObject(Slayer))
			Slayer.delete();

		parent::destroyServer();
	}

	//CLEANUP
	function onExit()
	{
		if(isObject(Slayer))
			Slayer.delete();

		parent::onExit();
	}
};
activatePackage(Slayer_Main);

// +-------------------+
// | Initialize Slayer |
// +-------------------+
if(!isObject(Slayer))
{
	$Slayer::Server = new scriptGroup(Slayer);
}