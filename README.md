Gamemode_Slayer
===============

This is a gamemode for [Blockland][1].

[Download Slayer From RTB][2]

[Modification Discussion][3]

    +-------------------------------------------+
    |  ___   _     _____   __  __  ____   ____  |
    | | __| | |   | /_\ |  \ \/ / | ___| | /\ | |
    | |__ | | |_  |  _  |   \  /  | __|  | \/ / |
    | |___| |___| |_| |_|   |__|  |____| |_| \\ |
    |  Greek2me              Blockland ID 11902 |
    +-------------------------------------------+

[1]: http://blockland.us
[2]: http://forum.returntoblockland.com/dlm/viewFile.php?id=3755
[3]: http://forum.blockland.us/index.php?topic=170322.0