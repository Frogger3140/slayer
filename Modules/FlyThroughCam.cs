// +-------------------------------------------+
// |  ___   _     _____   __  __  ____   ____  |
// | | __| | |   | /_\ |  \ \/ / | ___| | /\ | |
// | |__ | | |_  |  _  |   \  /  | __|  | \/ / |
// | |___| |___| |_| |_|   |__|  |____| |_| \\ |
// |  Greek2me              Blockland ID 11902 |
// +-----------------------------+-------------+
// | DO NOT EDIT BELOW THIS LINE |
// +-----------------------------+

Slayer.Prefs.addPref("Fly-Through Cam","Play At Beginning of Round","%mini.flyCam_playOnReset","bool",1,0,0,3,"Advanced");
Slayer.Prefs.addPref("Fly-Through Cam","Countdown During Fly-Thru","%mini.flyCam_playDuringCountdown","bool",0,0,0,3,"Advanced");
Slayer.Prefs.addPref("Fly-Through Cam","Rounds Between Fly-Thrus","%mini.flyCam_roundsBetweenFlyThroughs","int 0 999",0,0,0,3,"Advanced");

registerOutputEvent("Minigame","StartFlyThrough","",0);
$Slayer::Server::Events::RestrictedEvent__["Minigame","startFlyThrough"] = 3;

datablock PathCameraData(Slayer_PathCamData : Observer)
{
	maxNodes = 20; //path cameras only work with up to 20 nodes

	defaultPath = "Spline";
	defaultType = "Normal";
	defaultSpeed = 7;
};

function serverCmdCreateFlyCam(%client)
{
	%mini = getMinigameFromObject(%client);
	if(!%mini.canEdit(%client))
		return;

	%datablock	= Slayer_PathCamData;
	%transform	= %client.getControlObject().getTransform();
	%speed		= Slayer_PathCamData.defaultSpeed;
	%type		= Slayer_PathCamData.defaultType;
	%path		= Slayer_PathCamData.defaultPath;

	%mini.createPathCamera(%datablock,%transform,%speed,%type,%path);

	%client.bottomPrint("<just:center>\c2Fly-through camera created. Type \c3/setKnot ? \c2for help adding knots.",3);
}

function serverCmdDeleteFlyCam(%client)
{
	%mini = getMinigameFromObject(%client);
	if(!%mini.canEdit(%client))
		return;
	if(isObject(%mini.flyCam))
	{
		%mini.flyCam.delete();

		%path = %mini.configFile;
		if(isFile(%path))
		{
			%newPath = filePath(%path) @ "/" @ fileBase(%path) @ ".pathcam";
			if(isFile(%newPath))
				fileDelete(%newPath);
		}

		%client.bottomPrint("<just:center>\c2Fly-through camera deleted.",3);
	}
}

function serverCmdSetKnot(%client,%speed,%type,%path)
{
	%mini = getMinigameFromObject(%client);
	if(!%mini.canEdit(%client))
		return;
	if(!isObject(%mini.flyCam))
	{
		messageClient(%client,'',"\c2Please create the fly-through camera first by typing \c3/createFlyCam\c2.");
		return;
	}
	if(%speed $= "?")
	{
		messageClient(%client,'',"\c2USAGE: \c3/setKnot [speed | int 1-100] [type | string Normal, Kink, Position] [path | Spline, Linear]");
		return;
	}

	%datablock = Slayer_PathCamData;
	%transform = %client.getControlObject().getTransform();

	if(%mini.flyCam.numNodes >= %datablock.maxNodes)
	{
		messageClient(%client,'',"\c2You have reached the limit of" SPC %datablock.maxNodes SPC "nodes.");
		return;
	}

	if(%speed $= "")
		%speed = %datablock.defaultSpeed;
	if(%type $= "")
		%type = %datablock.defaultType;
	if(%type $= "Position")
		%type = "Position Only";
	if(%path $= "")
		%path = %datablock.defaultPath;

	Slayer_PathCamData.addNode(%mini.flyCam,%transform,%speed,%type,%path,0);

	%client.bottomPrint("<just:center>\c2Knot added to camera path." SPC "(" @ %mini.flyCam.numNodes @ "/" @ %datablock.maxNodes @ ")",3);
}

function serverCmdSetJump(%client,%speed,%type,%path)
{
	%mini = getMinigameFromObject(%client);
	if(!%mini.canEdit(%client))
		return;
	if(!isObject(%mini.flyCam))
	{
		messageClient(%client,'',"\c2Please create the fly-through camera first by typing \c3/createFlyCam\c2.");
		return;
	}
	if(%speed $= "?")
	{
		messageClient(%client,'',"\c2USAGE: \c3/setJump [speed | int 1-100] [type | string Normal, Kink, Position] [path | Spline, Linear]");
		return;
	}

	%datablock = Slayer_PathCamData;
	%transform = %client.getControlObject().getTransform();

	if(%mini.flyCam.numNodes >= %datablock.maxNodes)
	{
		messageClient(%client,'',"\c2You have reached the limit of" SPC %datablock.maxNodes SPC "nodes.");
		return;
	}

	if(%speed $= "")
		%speed = %datablock.defaultSpeed;
	if(%type $= "")
		%type = "Kink";
	if(%type $= "Position")
		%type = "Position Only";
	if(%path $= "")
		%path = %datablock.defaultPath;

	Slayer_PathCamData.addNode(%mini.flyCam,%transform,%speed,%type,%path,1);

	%client.bottomPrint("<just:center>\c2Jump added to camera path." SPC "(" @ %mini.flyCam.numNodes @ "/" @ %datablock.maxNodes @ ")",3);
}

//function serverCmdUndoKnot(%client)
//{
//	%mini = getMinigameFromObject(%client);
//	if(!%mini.canEdit(%client))
//		return;
//	if(!isObject(%mini.flyCam))
//	{
//		messageClient(%client,'',"\c2Please create the fly-through camera first by typing \c3/createFlyCam\c2.");
//		return;
//	}
//
//	%mini.flyCam.popBack(); //I GUESS THIS ISN'T A METHOD :/
//	%mini.flyCam.numKnots --;
//
//	%client.bottomPrint("<just:center>\c2Knot removed from camera path.",3);
//}

function serverCmdTestFlyCam(%client)
{
	%mini = getMinigameFromObject(%client);
	if(!%mini.canEdit(%client))
		return;
	if(!isObject(%mini.flyCam))
	{
		messageClient(%client,'',"\c2Please create the fly-through camera first. Type \c3/flyCamHelp \c2for more info.");
		return;
	}

	%mini.flyCam.testClient = %client;
	%client.flyCam_lastControlObject = %client.getControlObject();
	%mini.flyCam.setPosition(0.0);
	%client.setControlObject(%mini.flyCam);
	%mini.flyCam.setTarget(%mini.flyCam.numNodes);
	%mini.flyCam.setState("forward");
}

function Slayer_MinigameSO::createPathCamera(%this,%datablock,%transform,%speed,%type,%path)
{
	if(isObject(%this.flyCam))
		%this.flyCam.delete();

	%this.flyCam = new PathCamera()
	{
		dataBlock = %datablock;
		minigame = %this;

		numNodes = 0;
	};
	%this.flyCam.setState("stop");

	%datablock.addNode(%this.flyCam,%transform,%speed,%type,%path,0);

	%this.flyCam.setPosition(1.0);
	%this.flyCam.popFront();

	return %this.flyCam;
}

function Slayer_MinigameSO::startFlyThrough(%this,%startRound)
{
	%this.flyCam.startRound = %startRound;

	%timeout = 50;

	%this.flyCam.testClient = "";
	%this.flyCam.setPosition(0.0);
	%this.flyCam.setTarget(%this.flyCam.numNodes);
	%this.flyCam.setState("stop");
	for(%i = 0; %i < %this.numMembers; %i ++)
	{
		%cl = %this.member[%i];
		%cl.flyCam_lastControlObject = %cl.getControlObject();
		%cl.schedule(%i * %timeout,setControlObject,%this.flyCam);
	}

	%time = (%this.numMembers - 1) * %timeout;
	%time = Slayer_Support::mRestrict(%time,0,10000); //wait no more than 10 seconds

	%this.flyCam.schedule(%time,setState,"forward");

	%displayTime = %time / 1000;
	if(%displayTime >= 1)
		%this.bottomPrintAll("<just:center>\c5Preparing fly-through camera.",%displayTime,1);
}

function Slayer_PathCamData::addNode(%this,%cam,%transform,%speed,%type,%path,%jump)
{
	if(%cam.numNodes >= %this.maxNodes)
		return;

	%cam.pushBack(%transform,%speed,%type,%path);
	%cam.nodeTransform[%cam.numNodes]	= %transform;
	%cam.nodeSpeed[%cam.numNodes]		= %speed;
	%cam.nodeType[%cam.numNodes]		= %type;
	%cam.nodePath[%cam.numNodes]		= %path;
	%cam.nodeJump[%cam.numNodes]		= %jump;
	%cam.numNodes ++;
}

function Slayer_PathCamData::onNode(%this,%cam,%node)
{
	Slayer_Support::Debug(2,"Slayer_PathCamData::onNode",%node);
	%mini = %cam.minigame;

	if(%node == %cam.numNodes - 1)
	{
		Slayer_Support::Debug(2,"Slayer_PathCamData::onNode","END OF PATH");
		if(isObject(%cam.testClient))
		{
			if(isObject(%cam.testClient.flyCam_lastControlObject))
				%cam.testClient.setControlObject(%cam.testClient.flyCam_lastControlObject);
			%cam.testClient.flyCam_lastControlObject = "";
		}
		else if(isObject(%mini))
		{
			if(%cam.startRound && !%mini.flyCam_playDuringCountdown)
			{
				if(%mini.preRoundSeconds > 0)
					%mini.preRoundCountdownTick(0);
				else
					%mini.startRound();
			}

			for(%i = 0; %i < %mini.numMembers; %i ++)
			{
				%cl = %mini.member[%i];
				if(%cl.getControlObject() == %cam)
				{
					if(isObject(%cl.flyCam_lastControlObject))
						%cl.setControlObject(%cl.flyCam_lastControlObject);
					%cl.flyCam_lastControlObject = "";
				}
			}
		}
		else
		{
			Slayer_Support::Error("Slayer_PathCamData::onNode","Fly-through cam has no minigame! Deleting.");
			%cam.delete();
		}
	}
	else if(%cam.nodeJump[%node + 1])
	{
		Slayer_Support::Debug(2,"Slayer_PathCamData::onNode","Jumping to next node");
		%cam.setPosition(%node + 1);
	}
}

package Slayer_Module_FlyThruCam
{
	function PathCamera::setPosition(%this,%pos)
	{
		parent::setPosition(%this,%pos);

		if(%this.getDatablock().getID() == Slayer_PathCamData.getID())
		{
			if(%this.nodeTransform[%pos] !$= "")
				Slayer_PathCamData.onNode(%this,%pos);
		}
	}

	function Slayer_MinigameSO::onRemove(%this)
	{
		%cam = %this.flyCam;
		%parent = parent::onRemove(%this);
		if(isObject(%cam))
			%cam.delete();
		return %parent;
	}

	function Slayer_MinigameSO::preRoundCountdownTick(%this,%ticks)
	{
		%parent = parent::preRoundCountdownTick(%this,%ticks);
		
		%remain = %this.preRoundSeconds - %ticks;
		if(%remain <= 0)
		{
			for(%i = 0; %i < %this.numMembers; %i ++)
			{
				%cl = %this.member[%i];
				if(%cl.getControlObject() == %this.flyCam)
				{
					if(isObject(%cl.flyCam_lastControlObject))
						%cl.setControlObject(%cl.flyCam_lastControlObject);
					%cl.flyCam_lastControlObject = "";
				}
			}
		}
		
		return %parent;
	}

	function Slayer_MinigameSO::onReset(%this)
	{
		if(%this.flyCam_playOnReset && isObject(%this.flyCam) && %this.numMembers > 0)
		{
			%this.flyCam_roundsPassed ++;

			if(%this.flyCam_roundsPassed > %this.flyCam_roundsBetweenFlyThroughs)
			{
				%this.flyCam_roundsPassed = 0;
				if(%this.flyCam_playDuringCountdown && %this.preRoundSeconds > 0)
				{
					%parent = parent::onReset(%this);
					%this.startFlyThrough(1);
					return %parent;
				}
				else
					%this.startFlyThrough(1);
			}
		}
		else
		{
			%this.flyCam_roundsPassed = 0;
			return parent::onReset(%this);
		}
	}

	function Slayer_MinigameSO::postConfigLoad(%this,%path)
	{
		if(%path !$= "")
		{
			%newPath = filePath(%path) @ "/" @ fileBase(%path) @ ".pathcam";
			if(isFile(%newPath))
			{
				%file = new fileObject();
				%file.openForRead(%newPath);

				%ln = %file.readLine();
				%cam = %this.createPathCamera(Slayer_PathCamData,getField(%ln,0),getField(%ln,1),getField(%ln,2),getField(%ln,3));

				while(!%file.isEOF())
				{
					%ln = %file.readLine();
					Slayer_PathCamData.addNode(%cam,getField(%ln,0),getField(%ln,1),getField(%ln,2),getField(%ln,3),getField(%ln,4));
				}
			}
		}

		return parent::postConfigLoad(%this,%file);
	}

	function Slayer_PrefHandlerSO::exportPrefs(%this,%path,%mini)
	{
		%parent = parent::exportPrefs(%this,%path,%mini);

		%cam = %mini.flyCam;
		if(isFile(%path) && isObject(%cam))
		{
			%newPath = filePath(%path) @ "/" @ fileBase(%path) @ ".pathcam";
			%file = new fileObject();
			%file.openForWrite(%newPath);
			for(%i = 0; %i < %cam.numNodes; %i ++)
			{
				%transform	= %cam.nodeTransform[%i];
				%speed		= %cam.nodeSpeed[%i];
				%type		= %cam.nodeType[%i];
				%path		= %cam.nodePath[%i];
				%jump		= %cam.nodeJump[%i];
				%file.writeLine(%transform TAB %speed TAB %type TAB %path TAB %jump);
			}
			%file.close();
			%file.delete();
		}
	}
};
activatePackage(Slayer_Module_FlyThruCam);