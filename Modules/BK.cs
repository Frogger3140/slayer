// +-------------------------------------------+
// |  ___   _     _____   __  __  ____   ____  |
// | | __| | |   | /_\ |  \ \/ / | ___| | /\ | |
// | |__ | | |_  |  _  |   \  /  | __|  | \/ / |
// | |___| |___| |_| |_|   |__|  |____| |_| \\ |
// |  Greek2me              Blockland ID 11902 |
// +-----------------------------+-------------+
// | DO NOT EDIT BELOW THIS LINE |
// +-----------------------------+

Slayer.Prefs.addPref("Bonus Kills","Enable","%mini.bKills_enable","bool",1,0,1,-1,"Advanced");
Slayer.Prefs.addPref("Bonus Kills","Kill Spree Start","%mini.bKills_kSpree","int 2 30",5,0,1,-1,"Advanced");
Slayer.Prefs.addPref("Bonus Kills","Bonus Kill Points","%mini.bKills_points","int -999 999",1,0,1,-1,"Advanced");

function isSpecialKill_Slayer_BK(%client,%sourceObject,%killer,%mini)
{
	//bonus kills
	if(%mini.bKills_enable)
	{
		if(%killer != %client && isObject(%killer))
		{
			if(%killer.slyrTeam != %client.slyrTeam || !isObject(%killer.slyrTeam) || !isObject(%client.slyrTeam))
			{
				//KILL SPREES
				%killer.player.killSpree ++;
				%ks = %killer.player.killSpree;
				if(%ks >= %mini.bKills_kSpree)
				{
					//add the kill spree message
					if(%msg $= "")
						%msg = "\c3(Kill Spree:" SPC %ks @ ")";
					else
						%msg = %msg SPC "\c3(Kill Spree:" SPC %ks @ ")";

					//bonus points
					%killer.incScore(%mini.bKills_points);
				}
	
				//QUICK KILLS
				%killer.player.quickKills ++;
				schedule(750,0,eval,%killer @ ".player.quickKills --;");
	
				switch(%killer.player.quickKills)
				{
					case 2:
						%qk = "\c3(Double Kill)";
					case 3:
						%qk = "\c3(Triple Kill)";
					case 4:
						%qk = "\c3(Quadruple Kill)";
					default:
						if(%killer.player.quickKills > 4)
							%qk = "\c3(Multi Kill:" SPC %killer.player.quickKills @ ")";
				}
				if(%qk !$= "")
				{
					if(%msg $= "")
						%msg = %qk;
					else
						%msg = %msg SPC %qk;
					
					//bonus points
					%killer.incScore(%mini.bKills_points);
				}
			}
		}
	}

	if(%msg $= "")
		return 0;
	else
		return 2 TAB %msg;
}
addSpecialDamageMsg("Slayer_BK","%2%3%1 %4","%3%1");

Slayer_Support::Debug(2,"Module Loaded","BK");