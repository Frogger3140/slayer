// +-------------------------------------------+
// |  ___   _     _____   __  __  ____   ____  |
// | | __| | |   | /_\ |  \ \/ / | ___| | /\ | |
// | |__ | | |_  |  _  |   \  /  | __|  | \/ / |
// | |___| |___| |_| |_|   |__|  |____| |_| \\ |
// |  Greek2me              Blockland ID 11902 |
// +-------------------------------------------+

$Slayer::Server::Debug = 0;

// +-----------------------------+
// | DO NOT EDIT BELOW THIS LINE |
// +-----------------------------+

$Slayer::Server::Version = "3.7.3";
$Slayer::Server::CompatibleVersion = "3.6";

$Slayer::Server::Directory = filePath(expandFileName("./Server.cs"));
$Slayer::Server::ConfigDir = "config/server/Slayer";

$Slayer::Server::GameModeArg = "Add-Ons/Gamemode_Slayer/gamemode.txt";
$Slayer::Server::CurGameModeArg = $gameModeArg;

// +------+
// | Core |
// +------+
exec($Slayer::Server::Directory @ "/Main.cs"); //load main scripts

// +--------------+
// | Fully Loaded |
// +--------------+
warn("Slayer Version" SPC $Slayer::Server::Version);