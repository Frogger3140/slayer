// +-------------------------------------------+
// |  ___   _     _____   __  __  ____   ____  |
// | | __| | |   | /_\ |  \ \/ / | ___| | /\ | |
// | |__ | | |_  |  _  |   \  /  | __|  | \/ / |
// | |___| |___| |_| |_|   |__|  |____| |_| \\ |
// |  Greek2me              Blockland ID 11902 |
// +-----------------------------+-------------+
// | DO NOT EDIT BELOW THIS LINE |
// +-----------------------------+

$Slayer::Server::Support::Main = 1;

// +-------------------------+
// | Stuff for finding stuff |
// +-------------------------+
function Slayer_Support::getClientFromObject(%obj)
{
	if(!isObject(%obj))
		return -1;

	if(isObject(%obj.client))
		return %obj.client;

	%class = %obj.getClassName();

	if(%class $= "GameConnection" || %class $= "AiConnection")
		%client = %obj;
	if(%class $= "AiPlayer" || strPos(%class,"Vehicle") >= 0)
		%client = %obj.getControllingObject().client;

	if(isObject(%client))
		return %client;

	return -1;
}

function Slayer_Support::getBLIDFromObject(%obj)
{
	if(!isObject(%obj))
		return -1;

	if(%obj.bl_id !$= "")
		return %obj.bl_id;

	%class = %obj.getClassName();

	if(%class $= "GameConnection")
		return %obj.getBLID();
	if(%class $= "Player" && isObject(%obj.client))
		return %obj.client.getBLID();
	if(%class $= "fxDtsBrick" && isObject(%obj.getGroup()))
		return %obj.getGroup().bl_id;
	if(Slayer_Support::isVehicle(%obj) && isObject(%obj.brickGroup))
		return %obj.brickGroup.bl_id;
	if(%class $= "Item")
	{
		if(isObject(%obj.spawnBrick))
			return %obj.spawnBrick.getGroup().bl_id;
	}
	if(%obj.class $= "Slayer_MinigameSO")
		return %obj.creatorBLID;

	return -1;
}

function Slayer_Support::getBlocklandID()
{
	if($Server::LAN)
		return getLAN_BLID();
	else
		return getNumKeyID();
}

function Slayer_Support::getNewerVersion(%version1,%version2)
{
	%num1 = strReplace(firstWord(%version1),"."," ");
	%num2 = strReplace(firstWord(%version2),"."," ");

	%count = getMax(getWordCount(%num1),getWordCount(%num2));
	for(%i = 0; %i < %count; %i ++)
	{
		%n1 = getWord(%num1,%i);
		%n2 = getWord(%num2,%i);

		if(%n1 > %n2)
			return %version1;
		else if(%n2 > %n1)
			return %version2;
	}

	%tags = "\tAlpha\tBeta\tRC\t\t";
	%tag1 = striPos(%tags,"\t" @ restWords(%version1) @ "\t");
	%tag2 = striPos(%tags,"\t" @ restWords(%version2) @ "\t");

	if(%tag1 > %tag2)
		return %version1;
	else if(%tag2 > %tag1)
		return %version2;

	return -1;
}

function Slayer_Support::isNewerVersion(%version1,%version2) //check if %version1 is newer than %version2
{
	return Slayer_Support::getNewerVersion(%version1,%version2) $= %version1;
}

// +-----------+
// | SimObject |
// +-----------+
function SimObject::getGroupID(%this)
{
	%parent = %this.getGroup();
	if(!isObject(%parent))
		return -1;

	for(%i=0; %i < %parent.getCount(); %i++)
	{
		%obj = %parent.getObject(%i);
		if(%obj == %this)
			return %i;
	}

	return -1;
}

// +-------+
// | Debug |
// +-------+
function Slayer_Support::Debug(%level,%title,%value)
{
	if(%value $= "")
		%val = %title;
	else
		%val = %title @ ":" SPC %value; 

	if(%level <= $Slayer::Server::Debug)
		echo("\c4Slayer (Server):" SPC %val);

	%path = $Slayer::Server::ConfigDir @ "/debug_server.log";
	if(isWriteableFileName(%path))
	{
		if(!isObject(Slayer.DebugFO))
		{
			Slayer.debugFO = new fileObject();
			Slayer.add(Slayer.debugFO);

			Slayer.debugFO.openForWrite(%path);
			Slayer.debugFO.writeLine("//Slayer Version" SPC $Slayer::Server::Version SPC "-----" SPC getDateTime());
			Slayer.debugFO.writeLine("//By Greek2me, Blockland ID 11902");
		}

		Slayer.debugFO.writeLine(%val);
	}
}

function Slayer_Support::Error(%title,%value)
{
	if(%value $= "")
		%val = %title;
	else
		%val = %title @ ":" SPC %value; 

	error("Slayer Error (Server):" SPC %val);

	%path = $Slayer::Server::ConfigDir @ "/debug_server.log";
	if(isWriteableFileName(%path))
	{
		if(!isObject(Slayer.DebugFO))
		{
			Slayer.debugFO = new fileObject();
			Slayer.add(Slayer.debugFO);

			Slayer.debugFO.openForWrite(%path);
			Slayer.debugFO.writeLine("//Slayer Version" SPC $Slayer::Server::Version SPC "-----" SPC getDateTime());
			Slayer.debugFO.writeLine("//By Greek2me, Blockland ID 11902");
		}

		Slayer.debugFO.writeLine("ERROR:" SPC %val);
	}
}

function Slayer_Support::Benchmark(%times,%function,%a,%b,%c,%d,%e,%f,%g,%h,%i,%j,%k,%l,%m,%n,%o)
{
	if(!isFunction(%function))
	{
		warn("BENCHMARKING RESULT FOR" SPC %function @ ":" NL "Function does not exist.");
		return -1;
	}

	%start = getRealTime();

	for(%i=0; %i < %times; %i++)
	{
		call(%function,%a,%b,%c,%d,%e,%f,%g,%h,%i,%j,%k,%l,%m,%n,%o);
	}

	%end = getRealTime();

	%result = (%end-%start) / %times;

	warn("BENCHMARKING RESULT FOR" SPC %function @ ":" NL %result);

	return %result;
}

// +--------------+
// | File Loading |
// +--------------+
function Slayer_Support::loadFiles(%path,%ignoreFile) //load all files in the specified path
{
	if(%ignoreFile !$= "")
		%ignorePath = filePath(%path) @ "/" @ %ignoreFile;
	%count = getFileCount(%path);
	for(%i=0; %i < %count; %i++)
	{
		%f = findNextFile(%path);
		if(%f $= %ignorePath)
			continue;
		if(isFile(%f))
			exec(%f);
	}
}

// +--------+
// | Colors |
// +--------+
function Slayer_Support::RgbToHex(%rgb)
{
	//use % to find remainder
	%r = getWord(%rgb,0);
	if(%r <= 1)
		%r = %r * 255;
	%g = getWord(%rgb,1);
	if(%g <= 1)
		%g = %g * 255;
	%b = getWord(%rgb,2);
	if(%b <= 1)
		%b = %b * 255;
	%a = "0123456789ABCDEF";

	%r = getSubStr(%a,(%r-(%r % 16))/16,1) @ getSubStr(%a,(%r % 16),1);
	%g = getSubStr(%a,(%g-(%g % 16))/16,1) @ getSubStr(%a,(%g % 16),1);
	%b = getSubStr(%a,(%b-(%b % 16))/16,1) @ getSubStr(%a,(%b % 16),1);

	return %r @ %g @ %b;
}

function Slayer_Support::getClosestPaintColor(%rgba)
{
	%prevdist = 100000;
	%colorMatch = 0;
	for(%i = 0; %i < 64; %i++)
	{
		%color = getColorIDTable(%i);
		if(vectorDist(%rgba,getWords(%color,0,2)) < %prevdist && getWord(%rgba,3) - getWord(%color,3) < 0.3 && getWord(%rgba,3) - getWord(%color,3) > -0.3)
		{
			%prevdist = vectorDist(%rgba,%color);
			%colormatch = %i;
		}
	}
	return %colormatch;
}

function Slayer_Support::getAverageColor(%c0,%c1,%c2,%c3,%c4,%c5,%c6,%c7,%c8,%c9,%c10,%c11,%c12,%c13,%c14,%c15,%c16,%c18)
{
	%red = 0;
	%green = 0;
	%blue = 0;
	%alpha = 0;
	%count = 0;

	for(%i = 0; %i < 19; %i ++) //19 is the max number of arguments in a function
	{
		if(%c[%i] $= "")
		{
			break;
		}
		else
		{
			%red += getWord(%c[%i],0);
			%green += getWord(%c[%i],1);
			%blue += getWord(%c[%i],2);
			%alpha += getWord(%c[%i],3);
			%count ++;
		}
	}

	%red = %red / %count;
	%green = %green / %count;
	%blue = %blue / %count;
	%alpha = %alpha / %count;

	return %red SPC %green SPC %blue SPC %alpha;
}

// +------------+
// | Math Stuff |
// +------------+
function Slayer_Support::isNumber(%flag)
{
	if(%flag $= "")
		return 0;

	%check = "-.0123456789";

	%lastPos = -1;
	%length = strLen(%flag);
	for(%i=0; %i < %length; %i++)
	{
		%char = getSubStr(%flag,%i,1);

		%pos = strPos(%check,%char);
		if(%pos < 0)
			return 0;
		if(%pos == 0)
		{
			if(%neg)
				return 0;
			if(%i != 0)
				return 0;
			if(%length == 1)
				return 0;
			%neg = 1;
		}
		if(%pos == 1)
		{
			if(%dec)
				return 0;
			if(%i == %length - 1)
				return 0;
			%dec = 1;
		}

		%lastPos = %pos;
	}

	return 1;
}

function Slayer_Support::stripTrailingZeros(%flag)
{
	if(!Slayer_Support::isNumber(%flag))
		return %flag;

	%pos = -1;
	for(%i=0; %i < strLen(%flag); %i++)
	{
		%char = getSubStr(%flag,%i,1);

		if(%char $= ".")
		{
			%dec = 1;
			%pos = %i;
			%zero = 1;
			continue;
		}
		if(%char $= "0" && %dec)
		{
			if(!%zero)
				%pos = %i;
			%zero = 1;
		}
		else
		{
			%zero = 0;
			%pos = -1;
		}
	}

	if(%pos > 0)
		%flag = getSubStr(%flag,0,%pos);

	return %flag;
}

function Slayer_Support::mRestrict(%val,%min,%max)
{
	%stripChars = "~`!@#$%^&*()_+=qwertyuiop{[}]|\asdfghjkl;:\"\'zxcvbnm<,>?/ ";
	%val = stripChars(%val,%stripChars);

	if(%val < %min)
		%val = %min;
	if(%val > %max)
		%val = %max;

	return %val;
}

function Slayer_Support::isItemInList(%list,%item)
{
	for(%i=0; %i < getFieldCount(%list); %i++)
	{
		if(getField(%list,%i) $= %item)
			return 1;
	}

	return 0;
}

function Slayer_Support::swapItemsInList(%list,%index1,%index2)
{
	if(%index1 >= 0 && %index2 >= 0)
	{
		%item1 = getField(%list,%index1);
		%item2 = getField(%list,%index2);

		%list = setField(%list,%index1,%item2);
		%list = setField(%list,%index2,%item1);
	}

	return %list;
}

// +-------+
// | Other |
// +-------+
function Slayer_Support::setDynamicVariable(%var,%val)
{
	eval(%var SPC "= \"" @ %val @ "\";");
	return %val;
}

function Slayer_Support::getDynamicVariable(%var)
{
	%val = eval("return" SPC %var @ ";");
	return %val;
}

function Slayer_Support::isVehicle(%obj)
{
	if(!isObject(%obj))
		return 0;

	if(!isFunction(%obj.getClassName(),"getDatablock"))
		return 0;

	if((%obj.getType() & $TypeMasks::VehicleObjectType) || %obj.getDatablock().numMountPoints > 0)
		return 1;
	else
		return 0;
}